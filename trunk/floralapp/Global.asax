﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup    
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs
        //WriteLog();  
        try
        {
            if (this.Context != null && this.Context.Error != null && this.Context.Error.Message !=null)
                Library.Utility.Log.ErrorLog("floralapp Website", this.Context.Error.Message, this.Context.Error.InnerException.ToString());
        }
        catch { }

        if (this.Context != null)
            this.Context.Response.Redirect(GetHostUrl() + "Error.aspx", true);         
    } 

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started 

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised. 

        FormsAuthentication.RedirectToLoginPage();
        
    }

    private string GetHostUrl()
    {
        String retstr = String.Empty;
        string sSiteUrl = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
        string[] sUrl = sSiteUrl.Split('/');

        retstr = sUrl[0].ToString() + "//" + sUrl[2].ToString() + this.Context.Request.ApplicationPath + "/";

        return retstr;
    }
    
    private void WriteLog()
    { 
        // Write Log in Server
        string logfilepath = System.IO.Path.Combine(System.IO.Path.Combine(Server.MapPath(Request.ApplicationPath), "Log"), "BenevaFlowers_Error_Log.txt");

        using (System.IO.StreamWriter logfile = new System.IO.StreamWriter(logfilepath, true))
        {
            try
            {
                logfile.WriteLine(" ");
                logfile.WriteLine("-------------------------------------------------------------------------------------");
                logfile.WriteLine("ERROR :	In floralapp Website		" + " Date: " + DateTime.Now.ToString("MM dd yyyy HH:mm:ss"));
                logfile.WriteLine("-------------------------------------------------------------------------------------");
                logfile.WriteLine(" ");
                if ((this.Context.Error.Message != null) && (this.Context.Error.Message.ToString().Trim() != ""))
                {
                    logfile.WriteLine("Inner Message: ");
                    logfile.WriteLine("------------------------ ");
                    logfile.Write(this.Context.Error.Message);
                    logfile.WriteLine(" ");
                }

                if ((this.Context.Error.InnerException != null) && (this.Context.Error.InnerException.ToString().Trim() != ""))
                {
                    logfile.WriteLine("Inner Exception: ");
                    logfile.WriteLine("------------------------ ");
                    logfile.Write(this.Context.Error.InnerException);
                    logfile.WriteLine(" ");
                }
            }
            catch (Exception ex)
            {
                logfile.WriteLine(ex.ToString());
            }
        } 
    } 
       
</script>

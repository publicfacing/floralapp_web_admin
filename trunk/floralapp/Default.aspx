﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BenevaFlowers | Default</title>
    <link href="Css/Default.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <ajax:ModalPopupExtender ID="mpeProduct" runat="server" TargetControlID="lnkUpcharge"
        PopupControlID="pnlUpcharge" BackgroundCssClass="modalBackground" DropShadow="False"
        CancelControlID="btnCancel">
    </ajax:ModalPopupExtender>
    <asp:Panel runat="server" ID="pnlUpcharge" Style="display: none; background: #f0f0f0;">
        <table border="0" cellpadding="0" style="border-collapse: collapse; background: #ffffff;"
            class="arial-12">
            <tr>
                <th class="TableHeadingBg TableHeading" colspan="2">
                    Upcharge Percentage (%)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:ImageButton ID="btnClosePopup" runat="server" CausesValidation="false" ImageUrl="~/Images/close.gif" />
                </th>
            </tr>
            <tr>
                <td class="TableBorder" style="vertical-align: top;">
                    <asp:TextBox ID="txtUpchargePercentage" Width="60px" runat="server" CssClass="textbox"></asp:TextBox>%<br />
                    <asp:RegularExpressionValidator ID="revUpchargePercentage" runat="server" ControlToValidate="txtUpchargePercentage"
                        ValidationExpression="^\d{1,8}($|\.\d{1,2}$)" ErrorMessage="Enter a valid upcharge percentage."
                        Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="padding: 10px" align="center">
                    <asp:Button ID="btnApply" runat="server" Text="Apply Upcharge" class="btn" CausesValidation="false" />
                    &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn" CausesValidation="false" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    </form>
</body>
</html>

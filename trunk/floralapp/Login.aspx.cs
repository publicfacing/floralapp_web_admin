using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Library.AppSettings;
using Library.Utility;
 
public partial class pages_Login : System.Web.UI.Page
{
    AppVars v = new AppVars();

    protected void Page_Load(object sender, EventArgs e)
    {
        Boolean isStagingSite = false;
        if(!String.IsNullOrEmpty(ConfigurationManager.AppSettings["IsStagingSite"])) 
            isStagingSite = Convert.ToBoolean(ConfigurationManager.AppSettings["IsStagingSite"]);

        lblIsStagingSite.Visible = isStagingSite;

        Util.HideError(lblErrMsg);
        Session.Clear();

    }  

    private Boolean AutenticateUser(string loginID, string password, ref string errMsg)
    {
        Boolean retval = false;
        errMsg = "Error in Login. Invalid Login ID or Password.";

        DataTable dtUser = null;
        Data.Users user = new Data.Users();

        try
        {
            if (true == user.IsValidLogin(loginID, password, ref dtUser))
            {
                Int64 userID = Util.GetDataInt64(dtUser, 0, "UserID");
                Int32 roleID = Util.GetDataInt32(dtUser, 0, "RoleID");
                String roleName = Util.GetDataString(dtUser, 0, "RoleName"); 
                Int32 isActive = Util.GetDataInt32(dtUser, 0, "IsActive");
                               
                // Admin & Manager cannot login into the system if customerStatusID = 0;
                if (isActive == 0)
                {
                    errMsg = "User is inactivated. Please contact to administrator for activating this user.";
                    return false;
                }

                if (roleID == (Int32)AppEnum.UserRole.SUPERADMIN || roleID == (Int32)AppEnum.UserRole.RESELLERADMIN )
                {
                    user.LoadID(userID);

                    // Set session variables
                    v.DisplayName = (!String.IsNullOrEmpty(user.FirstName.value)) ? user.FirstName.value : "Guest";
                    v.UserID = userID;
                    v.UserRoleID = roleID;

                    if (roleID == (Int32)AppEnum.UserRole.RESELLERADMIN)
                    {
                        Data.Reseller rs = new Data.Reseller(); 
                        Int32 resellerID = rs.GetResellerByUser(userID);;
                        v.ResellerID = resellerID;
                    }
                }
                else
                {
                    errMsg = "You are not permitted to login into this system.";
                    return false;
                }



                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, userID.ToString(), DateTime.Now, DateTime.Now.AddMinutes(3600), false, "");
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                HttpContext.Current.Response.Cookies.Add(authCookie);

                retval = true;
            }
        }
        catch (Exception ex)
        {
            // Add Log in DB
            Data.ErrorLog el = new Data.ErrorLog();
            el.ApplicationType.value = (Int32) AppEnum.ApplicationType.WEBSITE;
            el.ModuleInfo.value = "Login : AutenticateUser()";
            el.SmallMessage.value = ex.Message;
            if (ex.InnerException != null)
                el.Message.value = ex.InnerException.Message;
            else
                el.Message.value = ex.ToString() ;
            el.DateAdded.value = Util.GetServerDate();
            el.Save();

            // Show in UI
            errMsg = ex.Message; 
            retval = false;
        }

        // Entry in login history
        Data.LoginHistory lh = new Data.LoginHistory(); 
        lh.LoginID.value = loginID;
        lh.UserID.value = v.UserID;
        lh.Password.value = password;
        lh.LastLogin.value = Util.GetServerDate();
        lh.IPAddress.value = Request.UserHostAddress;
        lh.IsSuccess.value = (v.UserID > 0) ? true : false;
        lh.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        lh.Save();
        
        return retval;
    }

    protected void btnLogin_click(object sender, EventArgs e)
    {
        String errorMsg = String.Empty;
        Data.AppUseAcceptance aua = new Data.AppUseAcceptance();

        try
        {
            if (!AutenticateUser(txtLoginID.Text.Trim(), txtPassword.Text.Trim(), ref errorMsg))
            {
                Util.ShowError(lblErrMsg, errorMsg);
            }
            else if (  (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)
                        && aua.IsAcceptTerms_n_Conditions(v.UserID))
                        || (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
                    )
                HttpContext.Current.Response.Redirect(FormsAuthentication.GetRedirectUrl(v.UserID.ToString(), false), true);
            else
            {
                FormsAuthentication.SetAuthCookie(v.UserID.ToString(), false);
                Response.Redirect("Admin/TermsAndConditions.aspx");
            }
        }
        catch( Exception ex) 
        {
            //ShowError(lblErrMsg, ex.Message);
            Util.ShowError(lblErrMsg, "Login failed due to some internal error. Contact your administrator.");
        }
    }

    protected void btnForgetPassword_Click(object sender, EventArgs e)
    {
        FormsAuthentication.SetAuthCookie(v.UserID.ToString(), false);
        Response.Redirect("ForgetPassword.aspx");
    }           
}

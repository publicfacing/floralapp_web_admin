﻿using System;
using Library.AppSettings;
using Library.Utility;
using System.Collections.Specialized;

public partial class Admin_AddViralMarketing : AppPage
{
    private Int64 ViralMarketingRequestID
    {
        get;
        set;
    }

    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                ViralMarketingRequestID = Convert.ToInt32(nvc["ViralMarketingRequestID"]);
            }

            Util.setValue(lblPageTitle, (ViralMarketingRequestID > 0) ? "Edit Viral Marketing" : "Add Viral Marketing");
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            ddlReseller.Enabled = false;
        }

        if (ViralMarketingRequestID <= 0)
            return;

        Data.ViralMarketingRequest viral = new Data.ViralMarketingRequest();
        viral.LoadID(ViralMarketingRequestID);

        if (!(ddlReseller.SelectedIndex > 0))
            Util.setValue(ddlReseller, viral.ResellerID.value);

        //Util.setValue(txtreq, viral.ViralCode.value); REQUESTED  BY......
        Util.setValue(txtReferralCode, viral.ReferralCode.value);
        Util.setValue(txtEmailID, viral.EmailIDList.value);
        Util.setValue(txtRequestLink, viral.RequestLink.value);
        Util.setValue(txtMessage, viral.Message.value);
    }

    private void SaveData()
    {
        Data.ViralMarketingRequest viral = new Data.ViralMarketingRequest();
        viral.LoadID(ViralMarketingRequestID);
        viral.ResellerID.value = Int32.Parse(Util.getValue(ddlReseller));
        viral.RequestedBy.value = v.UserID;//REQUESTED   BY .... LINK BETWEEN USERS TABLE [USER ID]
        viral.ReferralCode.value = Util.getValue(txtReferralCode);
        viral.EmailIDList.value = Util.getValue(txtEmailID);
        viral.RequestLink.value = Util.getValue(txtRequestLink);
        viral.Message.value = Util.getValue(txtMessage);
        viral.DateAdded.value = Util.GetServerDate();
        viral.AddedBy.value = v.UserID;
        viral.DateModified.value = Util.GetServerDate();
        viral.ModifiedBy.value = v.UserID;
        viral.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        viral.Save();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            // Save data to Database
            SaveData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect("ViralMarketing.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViralMarketing.aspx");
    }

    protected void ddlReseller_SelectedIndexChanged(object sender, EventArgs e)
    {
    //ReGenrateRandom:
    //    if (ddlReseller.SelectedIndex > 0)
    //    {
    //        string charRandoms = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    //        int length = 5;
    //        Random rnd = new Random();
    //        StringBuilder sbRandom = new StringBuilder();
    //        Data.ViralMarketingRequest viral = new Data.ViralMarketingRequest();
    //        DataSet dsViral = viral.List();
    //        String ViralNumber = string.Empty;
    //        while ((length--) > 0)
    //            sbRandom.Append(charRandoms[(int)(rnd.NextDouble() * charRandoms.Length)]);
    //        ViralNumber = Convert.ToString(sbRandom);
    //        DataRow[] drViral = dsViral.Tables[0].Select("ReferralCode='" + ViralNumber + "'");
    //        if (drViral.Length > 0)
    //            goto ReGenrateRandom;
    //        else
    //            txtReferralCode.Text = ViralNumber;
    //    }
    //    else
    //        txtReferralCode.Text = String.Empty;
    }
}


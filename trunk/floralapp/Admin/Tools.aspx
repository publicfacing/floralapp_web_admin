﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="Tools.aspx.cs" Inherits="Admin_Tools" Title="BenevaFlowers | Tools" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Tools"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="vsFaq" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                <tr>
                                    <td class="col">
                                        Shop ID<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtResellerID" runat="server" SkinID="textbox" class="FormItem" MaxLength="250"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvtxtResellerID" runat="server" ControlToValidate="txtResellerID"
                                            ErrorMessage="Enter a valid shop ID." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr> 
                            </table>
                            <div class="actionDiv">
                                <asp:Button ID="btnGenerateImagesByReseller" runat="server" Text="Generate Images" CssClass="btn" ToolTip="Click here to generate images for the shop ID specified."
                                    OnClick="btnGenerateImagesByReseller_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                                    CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

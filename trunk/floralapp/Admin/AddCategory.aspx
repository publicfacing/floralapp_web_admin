﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="AddCategory.aspx.cs" Inherits="Admin_AddCategory" Title="floralapp&reg; | Add Category"
    Theme="GraySkin" %>

<%@ Register Src="../UserControls/HelpMsg.ascx" TagName="HelpMsg" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <div style="position: relative;">
        <div style="position: absolute; z-index: 1; width: 735px">
            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                <tr>
                    <td valign="top" align="left">
                        <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                            <tr>
                                <th class="TableHeadingBg" colspan="2">
                                    <asp:Label ID="lblPageTitle" runat="server" Text="Add Category"></asp:Label>
                                </th>
                            </tr>
                            <tr>
                                <td class="TableBorder" colspan="2">
                                    <uc1:HelpMsg ID="HelpMsg1" runat="server" />
                                    <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                                    <asp:ValidationSummary ID="vsReseller" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                        HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                                    <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                        <tr runat="server" id="trShop">
                                            <td class="col">
                                                Select Shop<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvReseller" runat="server" ControlToValidate="ddlReseller"
                                                    ErrorMessage="Select a shop for this category." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col showhh">
                                                Category Name<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <asp:TextBox ID="txtCategoryName" runat="server" SkinID="textbox" class="FormItem"
                                                    MaxLength="50"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ControlToValidate="txtCategoryName"
                                                    ErrorMessage="Enter a category name." Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col showhh">
                                                Category Image<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <asp:FileUpload ID="fuCategoryImage" runat="server" />
                                                <div class="desc">
                                                    Supported image format(s): *.png, *.jpg, *.jpeg, *.gif.
                                                    <%-- <br />
                                            Suggested image size(ht x wt) is 50 x 50 pixel(s).--%>
                                                    <br />
                                                    Image will be resized to a 75 x 75 pixel(s) box.
                                                </div>
                                                <div>
                                                    <asp:Image ID="imgCat" runat="server" /></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Is Enabled
                                            </td>
                                            <td class="val">
                                                <asp:CheckBox ID="chkIsEnabled" runat="server" SkinID="checkbox" class="FormItem" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Is Featured
                                            </td>
                                            <td class="val">
                                                <asp:CheckBox ID="chkIsFeatured" runat="server" SkinID="checkbox" class="FormItem" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="actionDiv">
                                        <table width="100%" style="text-align: center">
                                            <tr>
                                                <td align="center">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this category."
                                                                    OnClick="btnSave_Click" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                                                                    CausesValidation="false" />
                                                            </td>
                                                            <td>
                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:Button ID="btnImport" runat="server" Text="Import Products" CssClass="btn" OnClick="btnImport_Click"
                                                                            CausesValidation="false" Visible="false" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:UpdatePanel runat="server" ID="upnlMsg" UpdateMode="Always">
                                                        <ContentTemplate>
                                                            <div style="height: 110px; overflow-y: auto;">
                                                                <asp:Label runat="server" ID="lblImportMsg" ForeColor="Red"></asp:Label>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div style="position: absolute; z-index: 9999; width: 730px; height: 430px; background-color: Gray;
                    margin: auto; filter: alpha(opacity=50); opacity: 0.5;">
                    <div style="position: relative; color: White; font-weight: bold; margin: auto; top: 50%;">
                        <table width="100%" style="text-align: center; font-size: 18px;">
                            <tr>
                                <td>
                                    ... Importing products, please wait ...
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    ... This process may take a few minutes ...
                                </td>
                            </tr>                            
                        </table>
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
</asp:Content>

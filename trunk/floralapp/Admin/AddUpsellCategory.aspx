﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="AddUpsellCategory.aspx.cs" Inherits="Admin_AddUpsellCategory" Title="floralapp&reg; | Add Upsell Category"
    Theme="GraySkin" %>

<%@ Register Src="../UserControls/HelpMsg.ascx" TagName="HelpMsg" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Add Upsell Category"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="vsUpsellCategory" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                <tr runat="server" id="trShop">
                                    <td class="col">
                                        Select Shop<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvReseller" runat="server" ControlToValidate="ddlReseller"
                                            ErrorMessage="Select a shop for this upsell category." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        Upsell Category Name<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtCategoryName" runat="server" SkinID="textbox" class="FormItem"
                                            MaxLength="50"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ControlToValidate="txtCategoryName"
                                            ErrorMessage="Enter a upsell category name." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        Upsell Category Image<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:FileUpload ID="fuUpsellCategoryImage" runat="server" />
                                        <div class="desc">
                                            <br />
                                            Supported image format(s): *.png, *.jpg, *.jpeg, *.gif.
                                            <%-- <br />
                                            Suggested image size(ht x wt) is 50 x 50 pixel(s).--%>
                                            <br />
                                            Image will be resized to a 75 x 75 pixel(s) box.
                                        </div>
                                        <div>
                                            <asp:Image ID="imgUc" runat="server" /></div>
                                    </td>
                                </tr>
                            </table>
                            <div class="actionDiv">
                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this category."
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                                    CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

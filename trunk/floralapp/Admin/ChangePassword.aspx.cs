﻿using System;
using Library.Utility;
using Library.AppSettings;

public partial class Admin_ChangePassword : AppPage
{ 
     
    public override void Page_Load(object sender, EventArgs e)
    {
        //btnSave.Attributes.Add("onclick", "javascript: return validateFormData();");

        HideError(lblErrMsg); 
    }
    
    private void SaveData()
    {
        AppVars v = new AppVars();
        Data.Users u = new Data.Users(v.UserID);
        u.Password.value = Util.getValue(txtPassword); 
        u.Save();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        { 
            SaveData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        lblSucessMsg.Visible = true;
        lblSucessMsg.Text = "Your Password has been changed sucessfully.";

        //Response.Redirect("../Default.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("../Default.aspx");
    }   
}


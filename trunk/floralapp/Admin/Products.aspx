﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="Products.aspx.cs" Inherits="Admin_Products" Title="floralapp&reg; | Products"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/javascript">
        function DateTimeFormat(sender, args) {
            var d = sender._selectedDate;
            var now = new Date();
            sender.get_element().value = d.format("MM/dd/yyyy")
        }
        function isValidPercent() {
            var txtPercent = $get("<%=txtUpchargePercentage.ClientID %>");
            var popup = $find("<%= this.mpeProduct.BehaviorID %>");

            if (txtPercent.value != '') {
                if (popup) {
                    if (Page_ClientValidate("<%= this.btnApply.ValidationGroup %>")) {
                        // Data is valid, and can be close the popup.
                        popup.hide();
                    }
                }
            }
            else
                return false;

        }
    </script>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12">
                    <tr>
                        <th class="TableHeadingBg TableHeading">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                text-indent: 5px;">
                                <tr>
                                    <td>
                                        Products
                                    </td>
                                    <td style="text-align: right; padding-right: 5px; text-transform: capitalize;">
                                        <asp:HyperLink ID="lnkAddProduct" runat="server" NavigateUrl="~/Admin/AddProduct.aspx"
                                            ToolTip="Add Product">Add Product </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label">
                                            Search :
                                        </td>
                                        <td>
                                            <label class="label" runat="server" id="lblShop">
                                                Shop:</label>
                                            <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                                OnSelectedIndexChanged="ddlReseller_SelectedIndexChanged">
                                                <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                                OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                                <asp:ListItem Value="0" Text="Select Category" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="Label1" CssClass="label" runat="server" Text="Date Added:"></asp:Label>
                                            <asp:TextBox runat="server" Width="70px" OnKeyUP="this.value=''" CssClass="textbox"
                                                ID="txtStartDate"></asp:TextBox>
                                            <ajax:CalendarExtender ID="ceStartDate" OnClientDateSelectionChanged="DateTimeFormat"
                                                Format="MM/dd/yyyy" runat="server" TargetControlID="txtStartDate" PopupButtonID="imgCalIcon1">
                                            </ajax:CalendarExtender>
                                            <asp:Image ImageUrl="~/Images/calender-icon.gif" Style="cursor: hand" ID="imgCalIcon1"
                                                runat="server" />
                                            &nbsp;&nbsp;
                                            <asp:TextBox runat="server" Width="70px" OnKeyUP="this.value=''" CssClass="textbox"
                                                ID="txtEndDate"></asp:TextBox>
                                            <ajax:CalendarExtender Format="MM/dd/yyyy" OnClientDateSelectionChanged="DateTimeFormat"
                                                ID="ceEndDate" runat="server" TargetControlID="txtEndDate" PopupButtonID="imgCalIcon2">
                                            </ajax:CalendarExtender>
                                            <asp:Image ImageUrl="~/Images/calender-icon.gif" ID="imgCalIcon2" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSearchField" runat="server" AutoPostBack="false">
                                                <asp:ListItem Text="SKU#" Value="SKUNo"></asp:ListItem>
                                                <asp:ListItem Text="ID" Value="ProductID"></asp:ListItem>
                                                <asp:ListItem Text="Product" Value="ProductName"></asp:ListItem>
                                                <asp:ListItem Text="Description" Value="Description"></asp:ListItem>
                                                <asp:ListItem Text="Price" Value="ProductPrice"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchOperator" runat="server">
                                                <asp:ListItem Value="Begins With">Begins With</asp:ListItem>
                                                <asp:ListItem Value="Ends With">Ends With</asp:ListItem>
                                                <asp:ListItem Value="Contains">Contains</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnFilter" class="btnFilter" runat="server" Text="Filter" CausesValidation="false"
                                                OnClick="btnFilter_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Button ID="btnUpchargePercentage" class="btnGreen" runat="server" Text="Apply Upcharge"
                                    CausesValidation="false" />
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpTop" runat="server" PageSize="25" PagedControlID="gvProducts">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <mo:ExtGridView ID="gvProducts" runat="server" DataKeyNames="ProductID" AllowSorting="true"
                                SkinID="gvskin" AutoGenerateColumns="false" OnSorting="gvProducts_OnSorting"
                                OnPageIndexChanging="gvProducts_OnPageIndexChanging" EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:TemplateField HeaderStyle-CssClass="gridHeader" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnPreview" runat="server" CommandArgument='<%# Eval("ProductID") +","+ Eval("ProdUpchargeID")%>'
                                                CausesValidation="false" OnClick="btnPreview_Click" ToolTip="Product Preview"
                                                ImageUrl="~/Images/Pop-Up.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="ID" DataField="ProductID" SortExpression="ProductID"
                                        ItemStyle-CssClass="gridContent" />
                                    <asp:BoundField HeaderText="SKU#" DataField="SKUNo" SortExpression="SKUNo" ItemStyle-CssClass="gridContent" />
                                    <asp:TemplateField HeaderText="Product Name" SortExpression="ProductName" HeaderStyle-CssClass="gridHeader"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="150px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("ProductName")%>'>
                                            </asp:Label>
                                            <br />
                                            <i>(<asp:Label ID="lblCategory" runat="server" Style="font-weight: bold;" Text='<%# Eval("CategoryList")%>'></asp:Label>)</i>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" ItemStyle-CssClass="gridContent" HeaderStyle-CssClass="gridHeader">
                                        <ItemTemplate>
                                            <table style="width: 99%">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top">
                                                        <asp:Image ID="imgProduct" Style="border: 2px solid #ffcc00;" runat="server" Visible="true"
                                                            ImageUrl='<%# getThumbImage(Eval("ProductID").ToString() + Eval("ProductThumbExt").ToString()) %>'
                                                            mageAlign="Top" />
                                                    </td>
                                                    <td style="text-align: left; padding-left: 5px; vertical-align: top; text-align: justify;">
                                                        <asp:Label ID="lblDescription" CssClass="label" runat="server" Text='<%# Eval("Description")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Price" DataField="ProductPrice" SortExpression="Productprice"
                                        HeaderStyle-Width="50px" DataFormatString="{0:C}" ItemStyle-CssClass="gridContent halign-right"
                                        ItemStyle-VerticalAlign="Top" />
                                    <asp:BoundField HeaderText="New Price" DataField="ProductNewPrice" SortExpression="ProductNewPrice"
                                        HeaderStyle-Width="60px" DataFormatString="{0:C}" ItemStyle-CssClass="gridContent halign-right"
                                        ItemStyle-VerticalAlign="Top" />
                                    <asp:TemplateField HeaderText="Published" SortExpression="IsPublished" HeaderStyle-CssClass="gridHeader"
                                        HeaderStyle-Width="60px" ItemStyle-CssClass="gridContent halign-center">
                                        <ItemTemplate>
                                            <asp:Image ID="imgIsPublished" runat="server" ImageUrl='<%# Eval("IsPublished").ToString().Equals("True")?"~/Images/tick.png":"~/Images/tick-gray.png" %>'
                                                ToolTip="Published" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="gridHeader" HeaderStyle-Width="50px"
                                        ItemStyle-CssClass="gridContent">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("ProductID") + "," + Eval("ProdUpchargeID") %>'
                                                CausesValidation="false" OnClick="btnEdit_Click" ToolTip="Edit" ImageUrl="~/Images/edit-icon.jpg" />
                                            <asp:ImageButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("ProductID")%>'
                                                CausesValidation="false" OnClick="btnDelete_Click" ImageUrl="~/Images/DeleteButton.jpg"
                                                OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="gridHeader" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-Width="25px" ItemStyle-CssClass="gridContent">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkUpchargeAll" AutoPostBack="true" OnCheckedChanged="chkUpchargeAll_CheckedChanged"
                                                runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkUpcharge" runat="server" TabIndex='<%# Convert.ToInt64(Eval("ProdUpchargeID"))%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotalProducts" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="25" PagedControlID="gvProducts">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- PopUp Ajax Model for apply upcharge percentage -->
    <ajax:ModalPopupExtender ID="mpeProduct" runat="server" TargetControlID="btnUpchargePercentage"
        PopupControlID="pnlUpcharge" BackgroundCssClass="modalBackground" DropShadow="False"
        CancelControlID="btnCancel">
    </ajax:ModalPopupExtender>
    <asp:Panel runat="server" ID="pnlUpcharge" Style="display: none; background: #f0f0f0;">
        <table border="0" width="400" cellpadding="0" class="PopUpBox" style="overflow: scroll;">
            <tr>
                <td class="PopUpBoxHeading">
                    <asp:Label ID="lblCategoryType" runat="server" Text="Upcharge Product(s)"></asp:Label>
                </td>
                <td class="PopUpBoxHeading" align="right">
                    <asp:ImageButton ID="btnClosePopup" runat="server" CausesValidation="false" ImageUrl="~/Images/close.gif" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 10px;">
                    <table border="0" cellpadding="0" class="detailTable">
                        <tr>
                            <td class="col" style="width: 150px;">
                                Upcharge Percentage(%)
                            </td>
                            <td class="val">
                                <asp:TextBox ID="txtUpchargePercentage" Width="60px" Text="" runat="server" CssClass="textbox"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revUpchargePercentage" runat="server" ControlToValidate="txtUpchargePercentage"
                                    ValidationExpression="^(100(?:\.0{1,2})?|0*?\.\d{1,2}|\d{1,2}(?:\.\d{1,2})?)$"
                                    ErrorMessage="Enter a valid upcharge percentage." Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 10px" align="center">
                    <asp:Button ID="btnApply" runat="server" Text="Apply Upcharge" class="btn" OnClientClick="return isValidPercent();"
                        OnClick="btnApply_Click" CausesValidation="false" />
                    &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn" CausesValidation="false" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <!-- End of popup upcharge percentage -->
</asp:Content>

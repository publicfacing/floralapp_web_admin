﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="AddBlockDates.aspx.cs" Inherits="Admin_AddBlockDates" Title="floralapp&reg; | Add Block Dates"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="../UserControls/HelpMsg.ascx" TagName="HelpMsg" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/javascript">
        function DateTimeFormat(sender, args) {
            var d = sender._selectedDate;
            var now = new Date();
            sender.get_element().value = d.format("MM/dd/yyyy")
        }
    </script>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Add Block Date"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:UpdatePanel ID="upCoupon" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <uc1:HelpMsg ID="HelpMsg1" runat="server" />
                                    <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                                    <asp:ValidationSummary ID="vsBlockDates" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                        HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                                    <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                        <tr runat="server" id="trShop">
                                            <td class="col">
                                                Shop<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="Select One " Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="rfvResellerID" runat="server" ControlToValidate="ddlReseller"
                                                    ErrorMessage="Select a shop for this block date." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Type<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="Select Type" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Delivery"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Pick-up"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="Both"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="rfvType" runat="server" ControlToValidate="ddlType"
                                                    ErrorMessage="Select type." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Repeat
                                            </td>
                                            <td class="val">
                                                <asp:DropDownList ID="ddlRepeat" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                                    OnSelectedIndexChanged="ddlRepeat_SelectedIndexChanged">
                                                    <asp:ListItem Value="0" Text="Never" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yearly"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Monthly"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="Weekly"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="trRepeatUntil" runat="server" visible="false">
                                            <td class="col">
                                                Repeat Until
                                            </td>
                                            <td class="val">
                                                <asp:TextBox runat="server" OnKeyUP="this.value=''" ID="txtRepeatUntil" Width="60px"></asp:TextBox>
                                                <ajax:CalendarExtender ID="ceRepeatUntil" OnClientDateSelectionChanged="DateTimeFormat"
                                                    Format="MM/dd/yyyy" runat="server" TargetControlID="txtRepeatUntil" PopupButtonID="imgRepeat">
                                                </ajax:CalendarExtender>
                                                <asp:Image ImageUrl="~/Images/calender-icon.gif" Style="cursor: hand" ID="imgRepeat"
                                                    runat="server" />
                                                <ajax:TextBoxWatermarkExtender ID="twmRepeatUntil" runat="server" TargetControlID="txtRepeatUntil"
                                                    WatermarkText="MM/DD/YYYY" WatermarkCssClass="watermarked">
                                                </ajax:TextBoxWatermarkExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col" style="height: 34px">
                                                Block Dates
                                            </td>
                                            <td class="val" style="height: 34px">
                                                <div style="width: 99%; border: none !important; text-decoration: none;">
                                                    <b>Start Date:</b><span class="reqd">*</span>
                                                    <asp:TextBox runat="server" OnKeyUP="this.value=''" CssClass="textbox" ID="txtStartDate"
                                                        Width="70px"></asp:TextBox>
                                                    <ajax:CalendarExtender ID="ceStartDate" OnClientDateSelectionChanged="DateTimeFormat"
                                                        Format="MM/dd/yyyy" runat="server" TargetControlID="txtStartDate" PopupButtonID="imgCalIcon1">
                                                    </ajax:CalendarExtender>
                                                    <asp:Image ImageUrl="~/Images/calender-icon.gif" Style="cursor: hand" ID="imgCalIcon1"
                                                        runat="server" />
                                                    <ajax:TextBoxWatermarkExtender ID="twmStartDate" runat="server" TargetControlID="txtStartDate"
                                                        WatermarkText="MM/DD/YYYY" WatermarkCssClass="watermarked">
                                                    </ajax:TextBoxWatermarkExtender>
                                                </div>
                                                <div style="width: 99%; border: none !important; text-decoration: none; margin-top: 8px;">
                                                    <b>End Date:</b> &nbsp;&nbsp;&nbsp;
                                                    <asp:TextBox runat="server" OnKeyUP="this.value=''" CssClass="textbox" ID="txtEndDate"
                                                        Width="70px"></asp:TextBox>
                                                    <ajax:CalendarExtender Format="MM/dd/yyyy" OnClientDateSelectionChanged="DateTimeFormat"
                                                        ID="ceEndDate" runat="server" TargetControlID="txtEndDate" PopupButtonID="imgCalIcon2">
                                                    </ajax:CalendarExtender>
                                                    <asp:Image ImageUrl="~/Images/calender-icon.gif" ID="imgCalIcon2" runat="server" />
                                                    <ajax:TextBoxWatermarkExtender ID="twmEndDate" runat="server" TargetControlID="txtEndDate"
                                                        WatermarkText="MM/DD/YYYY" WatermarkCssClass="watermarked">
                                                    </ajax:TextBoxWatermarkExtender>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="col">
                                                Order Allowed
                                            </td>
                                            <td class="val">
                                                <asp:CheckBox ID="chkOrderAllowed" runat="server" SkinID="checkbox" class="FormItem"
                                                    Checked="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col showhh">
                                                Message<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <mo:ExtTextBox ID="txtMessage" runat="server" TextMode="MultiLine" MaxLength="1000"
                                                    SkinID="textarea"></mo:ExtTextBox>
                                                Max. 1000 character.
                                                <br />
                                                <asp:RequiredFieldValidator ID="rfvMessage" runat="server" ControlToValidate="txtMessage"
                                                    ErrorMessage="Enter block date message." Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="actionDiv">
                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this shop."
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                                    CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

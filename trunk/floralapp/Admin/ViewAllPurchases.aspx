﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="ViewAllPurchases.aspx.cs" Inherits="Admin_ViewAllPurchases" Title="floralapp&reg; | View All Purchases"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/javascript">
        function DateTimeFormat(sender, args) {
            var d = sender._selectedDate;
            var now = new Date();
            sender.get_element().value = d.format("MM/dd/yyyy")
        }
    </script>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12">
                    <tr>
                        <th class="TableHeadingBg TableHeading">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                text-indent: 5px;">
                                <tr>
                                    <td>
                                        View All Purchases
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label">
                                            Search :
                                        </td>
                                        <td>
                                            <label class="label" runat="server" id="lblShop">
                                                Shop:</label>
                                            <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="Label1" CssClass="label" runat="server" Text="Purchase Date:"></asp:Label>
                                            <asp:TextBox runat="server" OnKeyUP="this.value=''" CssClass="textbox purcahseDateFilter"
                                                ID="txtStartDate"></asp:TextBox>
                                            <ajax:CalendarExtender ID="ceStartDate" OnClientDateSelectionChanged="DateTimeFormat"
                                                Format="MM/dd/yyyy" runat="server" TargetControlID="txtStartDate" PopupButtonID="imgCalIcon1">
                                            </ajax:CalendarExtender>
                                            <asp:Image ImageUrl="~/Images/calender-icon.gif" Style="cursor: hand" ID="imgCalIcon1"
                                                runat="server" />
                                            &nbsp;&nbsp;
                                            <asp:TextBox runat="server" OnKeyUP="this.value=''" CssClass="textbox purcahseDateFilter"
                                                ID="txtEndDate"></asp:TextBox>
                                            <ajax:CalendarExtender Format="MM/dd/yyyy" ID="ceEndDate" OnClientDateSelectionChanged="DateTimeFormat"
                                                runat="server" TargetControlID="txtEndDate" PopupButtonID="imgCalIcon2">
                                            </ajax:CalendarExtender>
                                            <asp:Image ImageUrl="~/Images/calender-icon.gif" ID="imgCalIcon2" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <label class="label">
                                                Order Status:</label>
                                            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                <asp:ListItem Value="-1" Text="New" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Processed"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="InActive"></asp:ListItem>
                                            </asp:DropDownList>
                                            <label class="label">
                                                Trans Status:</label>
                                            <asp:DropDownList ID="ddlTransactionStatus" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                <asp:ListItem Text="All" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="100" Text="Success"></asp:ListItem>
                                                <asp:ListItem Value="101" Text="Failed"></asp:ListItem>
                                                <asp:ListItem Value="102" Text="Pending"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchField" runat="server" AutoPostBack="false">
                                                <asp:ListItem Text="Purchase ID" Value="PurchaseID"></asp:ListItem>
                                                <asp:ListItem Text="Order No" Value="OrderID"></asp:ListItem>
                                                <asp:ListItem Text="Shop" Value="ResellerName"></asp:ListItem>
                                                <asp:ListItem Text="User" Value="UserName"></asp:ListItem>
                                                <asp:ListItem Text="Registered User" Value="IsRegisteredUser"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchOperator" runat="server">
                                                <asp:ListItem Value="Begins With">Begins With</asp:ListItem>
                                                <asp:ListItem Value="Ends With">Ends With</asp:ListItem>
                                                <asp:ListItem Value="Contains">Contains</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnFilter" class="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click"
                                                CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <asp:DataPager ID="dpTop" runat="server" PageSize="10" PagedControlID="gvPurchase">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                        ShowLastPageButton="false" ShowNextPageButton="false" />
                                    <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                        NextPreviousButtonCssClass="command" />
                                    <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                        ShowLastPageButton="true" ShowNextPageButton="true" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false"></asp:Label>
                            <mo:ExtGridView ID="gvPurchase" runat="server" DataKeyNames="PurchaseID" AllowPaging="true"
                                PageSize="10" AllowSorting="true" OnSorting="gvPurchase_OnSorting" SkinID="mo_gvskin"
                                OnPageIndexChanging="gvPurchase_OnPageIndexChanging" EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:BoundField DataField="PurchaseID" SortExpression="PurchaseID" HeaderText="Purchase ID"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="30px"></asp:BoundField>
                                    <asp:BoundField DataField="OrderID" SortExpression="OrderID" HeaderText="Order No"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="30px"></asp:BoundField>
                                    <asp:BoundField DataField="DateAdded" SortExpression="DateAdded" HeaderText="Order Date"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="60px" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}">
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Tran. Status" HeaderStyle-Width="50px" SortExpression="TransactionStatusID"
                                        ItemStyle-CssClass="gridContent">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTransactionStatus" runat="server" Text='<%# Eval("TransStatusValue")%>'></asp:Label>
                                            <div style="color: Navy;">
                                                <i>
                                                    <%# Eval("AccountNo") %>
                                                </i>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="UserName" SortExpression="UserName" HeaderText="User"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="IsRegisteredUser" SortExpression="IsRegisteredUser" HeaderText="Registered"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="60px"></asp:BoundField>
                                    <asp:BoundField DataField="IsPickUp" SortExpression="IsPickUp" HeaderText="Pick-up"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="40px"></asp:BoundField>
                                    <asp:BoundField DataField="Deliveryfee" SortExpression="Deliveryfee" HeaderText="Delivery Fee"
                                        HeaderStyle-Width="70px" DataFormatString="{0:C}" ItemStyle-CssClass="gridContent halign-right"
                                        HeaderStyle-HorizontalAlign="Center"></asp:BoundField>
                                    <asp:BoundField DataField="ServiceFee" SortExpression="ServiceFee" HeaderText="Service Fee"
                                        DataFormatString="{0:C}" HeaderStyle-Width="70px" ItemStyle-CssClass="gridContent halign-right"
                                        HeaderStyle-HorizontalAlign="Center"></asp:BoundField>
                                    <asp:BoundField DataField="TotalAmount" SortExpression="TotalAmount" HeaderText="Total Amount"
                                        DataFormatString="{0:C}" HeaderStyle-Width="80px" ItemStyle-CssClass="gridContent halign-right"
                                        HeaderStyle-HorizontalAlign="Center"></asp:BoundField>
                                    <asp:BoundField DataField="DeliveryDate" SortExpression="DeliveryDate" HeaderText="Delivery Date"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="75px" DataFormatString="{0:MM/dd/yyyy}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ResellerName" SortExpression="ResellerName" HeaderText="Shop"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:TemplateField HeaderStyle-CssClass="gridHeader" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-Width="50px" ItemStyle-CssClass="gridContent" HeaderText="Preview">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnPurchaseInfo" runat="server" CommandArgument='<%# Eval("PurchaseID") + "," + Eval("OrderID") %>'
                                                CausesValidation="false" OnClick="btnPurchasePreview_Click" ToolTip="Purchase Preview"
                                                ImageUrl="~/Images/Pop-Up.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-CssClass="gridHeader" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-Width="50px" ItemStyle-CssClass="gridContent" HeaderText="Status">
                                        <ItemTemplate>
                                            Status:<%# Eval("OrderStatus") %><br />
                                            ModifiedBy:<%# Eval("ModifiedUser") %>
                                            <br />
                                            <i>ModifiedOn:<%# DateTime.Parse(Eval("DateModified").ToString()).Year <= 1900 ? Eval("DateAdded") : Eval("DateModified")%></i>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails" align="right">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotalMagazinePurchased" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="10" PagedControlID="gvPurchase">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <%--<asp:TextBox runat="server" ID="txtDate2" Text="11/01/2006" />
    <asp:Image runat="server" ID="btnDate2" AlternateText="cal2" ImageUrl="~/images/calendaricon.jpg" />
    <ajax:CalendarExtender runat="server" ID="calExtender2" PopupButtonID="btnDate2"
        CssClass="AjaxCalendar" TargetControlID="txtDate2" Format="MMMM d, yy" />--%>
</asp:Content>

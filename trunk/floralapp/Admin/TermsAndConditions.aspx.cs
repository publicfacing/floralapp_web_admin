﻿using System;
using System.Configuration;
using System.Web;
using Library.Utility;
using Library.AppSettings;
using System.IO;
using System.Net.Mail;

public partial class TermsAndConditions : System.Web.UI.Page
{
    AppVars v = new AppVars();

    protected void Page_Load(object sender, EventArgs e)
    {
        Util.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            Data.AppUseAcceptance aua = new Data.AppUseAcceptance();

            if (
                (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))

            || (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)
                        && aua.IsAcceptTerms_n_Conditions(v.UserID))
              )
                Response.Redirect("~/Admin/Dashboard.aspx");

            String terms_n_Conditions= ConfigurationManager.AppSettings["Terms_n_Conditions"].ToString();
            divTAndC.InnerHtml = ReadTemplate(terms_n_Conditions);
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            Data.AppUseAcceptance aua = new Data.AppUseAcceptance();

            aua.UserID.value = v.UserID;
            aua.AcceptTermsAndConditions.value = true;
            aua.DateAdded.value = Util.GetServerDate();
            aua.AddedBy.value = v.UserID;
            aua.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
            aua.Save();

            #region Sending an email to super admin against the reseller that has accept the T&C.

            Data.Reseller r = new Data.Reseller(v.ResellerID);
            String superAdminMail = String.Empty;

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["SuperAdminEmail"].ToString()))
                superAdminMail = ConfigurationManager.AppSettings["SuperAdminEmail"].ToString();

            String strMailFrom =  r.Email.value;
            String strMailTo = superAdminMail;
            String strMailSubject = "Acceptance T&C.";
            String strMailMessage = GetMessageTemplate("TermsAndConditions.htm", r.ResellerName.value);

            if (ConfigurationManager.AppSettings["SendEmail"].ToString() == "1")
            {
                MailMessage objMailMsg = new MailMessage();
                SmtpClient SmtpMail = new SmtpClient();
                SmtpMail.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                MailAddress fromAddress = new MailAddress(strMailFrom);

                objMailMsg.To.Add(strMailTo);

                objMailMsg.From = fromAddress;
                objMailMsg.Subject = strMailSubject;
                objMailMsg.IsBodyHtml = true;
                objMailMsg.Body = strMailMessage;
                SmtpMail.Send(objMailMsg);
            }

            #endregion



            Response.Redirect("~/Admin/Dashboard.aspx");
        }
        catch (Exception ex)
        {
            lblErrMsg.Text = ex.Message;
            lblErrMsg.Visible = true;
            return;
        }
    }

    private String GetMessageTemplate(String templateName, String resellerName)
    {
        String path = HttpContext.Current.Server.MapPath(@"~/EmailTemplate\");
        Data.Users u = new Data.Users(v.UserID);

        path += templateName;

        StreamReader sr = new StreamReader(path);
        String mailBody = sr.ReadToEnd();
        sr.Close();
        sr.Dispose();

        mailBody = mailBody.Replace("{shop}", resellerName);
        mailBody = mailBody.Replace("{FirstName}", u.FirstName.value);
        mailBody = mailBody.Replace("{LastName}", u.LastName.value);

        return mailBody;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("../Login.aspx");
    }
 
    private String ReadTemplate(String fileName)
    {
        String path = HttpContext.Current.Server.MapPath(@"../DynFiles\MoreSettings\");

        String cmdAllPath = path + fileName;
        StreamReader sr = new StreamReader(cmdAllPath);
        String emailTemplate = sr.ReadToEnd();
        sr.Close();
        sr.Dispose();

        return emailTemplate;
    }
}

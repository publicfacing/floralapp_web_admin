﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="AddCoupon.aspx.cs" Inherits="Admin_AddCoupon" Title="floralapp&reg; | Coupon"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="HTMLEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="../UserControls/HelpMsg.ascx" TagName="HelpMsg" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/javascript">
        function DateTimeFormat(sender, args) {
            var d = sender._selectedDate;
            var now = new Date();
            sender.get_element().value = d.format("MM/dd/yyyy")
        } 
    </script>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <asp:UpdateProgress ID="prgCoupon" runat="server">
                                <ProgressTemplate>
                                    <div style="color: #ff0000; font-size: 12px; padding: 0px; width: 100px; float: right;">
                                        Loading...</div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:Label ID="lblPageTitle" runat="server" Text="Add Coupon"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:UpdatePanel ID="upCoupon" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <uc1:HelpMsg ID="HelpMsg1" runat="server" />
                                    <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                                    <asp:ValidationSummary ID="vsCoupon" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                        HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                                    <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                        <tr runat="server" id="trShop">
                                            <td class="col">
                                                Shop<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                                    OnSelectedIndexChanged="ddlReseller_SelectedIndexChanged">
                                                    <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="rfvReseller" runat="server" ControlToValidate="ddlReseller"
                                                    ErrorMessage="Select a shop." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col showhh">
                                                Coupon Code<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <asp:TextBox ID="txtCouponCode" runat="server" SkinID="textbox" class="FormItem"
                                                    MaxLength="8" Width="85px" ReadOnly="true"></asp:TextBox>
                                                <asp:ImageButton ID="imgCheckCouponCode" runat="server" OnClick="imgCheckCouponCode_Click"
                                                    CausesValidation="true" ImageUrl="~/Images/key_go.png" Text="Check availibility of coupon code."
                                                    ToolTip="Click here to check availibility of coupon code in floralapp." Height="16px"
                                                    Width="16px" Style="vertical-align: middle;" />
                                                <asp:Label ID="lblCouponCode" runat="server" Visible="false" CssClass="error"></asp:Label>
                                                <br />
                                                <asp:RequiredFieldValidator ID="rfvCouponCode" runat="server" ControlToValidate="txtCouponCode"
                                                    ErrorMessage="Enter coupon code." Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revCouponCode" runat="server" ControlToValidate="txtCouponCode"
                                                    ValidationExpression="^([a-zA-Z0-9]{4,8})$" ErrorMessage="Coupon code should be min. 4 digit(s) and max. 8 digit(s)."
                                                    Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Referral
                                            </td>
                                            <td class="val">
                                                <asp:TextBox ID="txtReferral" runat="server" SkinID="textarea" class="FormItem" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col showhh">
                                                Discount Type<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <asp:DropDownList ID="ddlDiscountType" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlDiscountType_SelectedIndexChanged">
                                                    <asp:ListItem Value="1" Text="Percentage"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Fixed Value" Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="DiscountPercentTR" runat="server" visible="false">
                                            <td class="col showhh">
                                                Discount Percentage<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <asp:TextBox ID="txtDiscountPercent" runat="server" SkinID="textbox" Width="100px"
                                                    class="FormItem" MaxLength="8"></asp:TextBox>&#32;&#32;&#37;&#32;
                                                <br />
                                                &#32;
                                                <br />
                                                <asp:RegularExpressionValidator ID="revDiscountPercent" runat="server" ControlToValidate="txtDiscountPercent"
                                                    ValidationExpression="^(100(?:\.0{1,2})?|0*?\.\d{1,2}|\d{1,2}(?:\.\d{1,2})?)$"
                                                    ErrorMessage="Enter a valid percentage." Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr id="DiscountFixedAmountTR" runat="server">
                                            <td class="col showhh">
                                                Discount Fixed Amount<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                &#32;&#36;&#32;
                                                <asp:TextBox ID="txtDiscountFixedAmount" runat="server" SkinID="textbox" Width="100px"
                                                    class="FormItem" MaxLength="8"></asp:TextBox>
                                                <br />
                                                <asp:RegularExpressionValidator ID="revDiscountFixedAmount" runat="server" ControlToValidate="txtDiscountFixedAmount"
                                                    ValidationExpression="^\d{0,8}($|\.\d{0,2}$)" ErrorMessage="Enter a valid fixed amount."
                                                    Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Coupon Type<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <asp:DropDownList ID="ddlCouponType" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                                    OnSelectedIndexChanged="ddlCouponType_SelectedIndexChanged">
                                                    <asp:ListItem Value="0" Text="Select Coupon Type" Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="rfvCouponType" runat="server" ControlToValidate="ddlCouponType"
                                                    ErrorMessage="Select coupon type." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Visible (on phone)
                                            </td>
                                            <td class="val">
                                                <asp:CheckBox ID="chkIsVisibleOnPhone" SkinID="checkbox" class="FormItem" runat="server"
                                                    Enabled="false" AutoPostBack="true" OnCheckedChanged="chkIsVisibleOnPhone_OnCheckedChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col">
                                                Is Enabled
                                            </td>
                                            <td class="val">
                                                <asp:CheckBox ID="chkIsEnabled" SkinID="checkbox" class="FormItem" runat="server"
                                                    AutoPostBack="true" OnCheckedChanged="chkIsEnabled_OnCheckedChanged" />
                                                <br />
                                                <asp:Label ID="lblCouponEnableMsg" runat="server" Text="" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trDateRange" runat="server" visible="false">
                                            <td class="col" style="height: 34px">
                                                Date Range
                                            </td>
                                            <td class="val" style="height: 34px">
                                                <div style="width: 99%; border: none !important; text-decoration: none;">
                                                    <b>Start Date:</b><span class="reqd">*</span>
                                                    <asp:TextBox runat="server" OnKeyUP="this.value=''" CssClass="textbox" ID="txtStartDate"
                                                        Width="70px"></asp:TextBox>
                                                    <ajax:CalendarExtender ID="ceStartDate" OnClientDateSelectionChanged="DateTimeFormat"
                                                        Format="MM/dd/yyyy" runat="server" TargetControlID="txtStartDate" PopupButtonID="imgCalIcon1">
                                                    </ajax:CalendarExtender>
                                                    <asp:Image ImageUrl="~/Images/calender-icon.gif" Style="cursor: hand" ID="imgCalIcon1"
                                                        runat="server" />
                                                    <ajax:TextBoxWatermarkExtender ID="twmStartDate" runat="server" TargetControlID="txtStartDate"
                                                        WatermarkText="MM/DD/YYYY" WatermarkCssClass="watermarked">
                                                    </ajax:TextBoxWatermarkExtender>
                                                </div>
                                                <div style="width: 99%; border: none !important; text-decoration: none; margin-top: 8px;">
                                                    <b>End Date:</b> &nbsp;&nbsp;&nbsp;
                                                    <asp:TextBox runat="server" OnKeyUP="this.value=''" CssClass="textbox" ID="txtEndDate"
                                                        Width="70px"></asp:TextBox>
                                                    <ajax:CalendarExtender Format="MM/dd/yyyy" OnClientDateSelectionChanged="DateTimeFormat"
                                                        ID="ceEndDate" runat="server" TargetControlID="txtEndDate" PopupButtonID="imgCalIcon2">
                                                    </ajax:CalendarExtender>
                                                    <asp:Image ImageUrl="~/Images/calender-icon.gif" ID="imgCalIcon2" runat="server" />
                                                    <ajax:TextBoxWatermarkExtender ID="twmEndDate" runat="server" TargetControlID="txtEndDate"
                                                        WatermarkText="MM/DD/YYYY" WatermarkCssClass="watermarked">
                                                    </ajax:TextBoxWatermarkExtender>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col showhh">
                                                Coupon Image
                                            </td>
                                            <td class="val">
                                                <asp:FileUpload ID="fuCouponImage" runat="server" Width="189px" />
                                                <div class="desc">
                                                    Coupon Image is recommended but not required.
                                                    <br />
                                                    Supported image format(s): *.png, *.jpg, *.jpeg, *.gif.
                                                    <%--  <br />
                                                    Suggested image size (ht x wt) is 200 x 320 pixel(s).--%>
                                                    <br />
                                                    Image will be resized to a 200 x 200 pixel(s) box.
                                                    <br />
                                                </div>
                                                <div>
                                                    <asp:Image ID="imgCoupon" runat="server" /></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col showhh">
                                                Coupon Text
                                            </td>
                                            <td class="val">
                                                <HTMLEditor:Editor ID="rteCouponText" runat="server" NoScript="true" Height="300px"
                                                    Width="320px" />
                                                Max. 1000 character.
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <%--<Triggers>
                                    <asp:PostBackTrigger ControlID="btnSave" />
                                </Triggers>--%>
                            </asp:UpdatePanel>
                            <div class="actionDiv">
                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this coupon."
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                                    CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

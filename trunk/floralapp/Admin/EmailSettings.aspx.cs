﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.IO;
using System.Configuration;
using System.Text;

public partial class Admin_EmailSettings : AppPage
{
    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        if (v.UserRoleID != 1)
        {
            LabelAuthenticateError.Visible = true;
            contentTable.Visible = false;
            LabelAuthenticateError.Text = "Your User does not have enought rights to perform this action";
        }

        if (!IsPostBack)
            LoadEmails();
    }

    private void LoadEmails()
    {
        try
        {
            string emails = ReadEmails();

            string[] emailList = emails.Split(',');
            if (emailList.Length >= 1)
                txtEmail1.Text = emailList[0];
            if (emailList.Length >= 2)
                txtEmail2.Text = emailList[1];
            if (emailList.Length >= 3)
                txtEmail3.Text = emailList[2];
            if (emailList.Length >= 4)
                txtEmail4.Text = emailList[3];
            if (emailList.Length >= 5)
                txtEmail5.Text = emailList[4];
        }
        catch (Exception ex)
        {
            Data.ErrorLog el = new Data.ErrorLog();
            el.SmallMessage.value = "Get Email Settings Error";
            el.Message.value = ex.Message;
            el.ModuleInfo.value = "floralapp Email Settings : EmailSettings.Load()";
            el.ApplicationType.value = 11;
            el.DateAdded.value = Util.GetServerDate();
            el.Save();
            ShowError(lblErrMsg, "Error loading data!");
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            StringBuilder emails = new StringBuilder();
            if (txtEmail1.Text.Length > 0)
                emails.Append(txtEmail1.Text + ",");
            if (txtEmail2.Text.Length > 0)
                emails.Append(txtEmail2.Text + ",");
            if (txtEmail3.Text.Length > 0)
                emails.Append(txtEmail3.Text + ",");
            if (txtEmail4.Text.Length > 0)
                emails.Append(txtEmail4.Text + ",");
            if (txtEmail5.Text.Length > 0)
                emails.Append(txtEmail5.Text + ",");

            if (emails.ToString().Length == 0)
            {
                ShowError(lblErrMsg, "Please enter at least one Email address!");
                return;
            }

            string finalEmails = emails.ToString();
            finalEmails = finalEmails.Substring(0, finalEmails.Length - 1);
            WriteEmails(finalEmails);

            ShowError(lblErrMsg, "Saved successfully!");
        }
        catch (Exception ex)
        {
            Data.ErrorLog el = new Data.ErrorLog();
            el.SmallMessage.value = "Save Email Settings Error";
            el.Message.value = ex.Message;
            el.ModuleInfo.value = "floralapp Email Settings : EmailSettings.Save()";
            el.ApplicationType.value = 11;
            el.DateAdded.value = Util.GetServerDate();
            el.Save();
            ShowError(lblErrMsg, "Error saving data!");
        }
    }

    private String ReadEmails()
    {
        try
        {
            String path = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EmailSettingsFile"]);

            StreamReader sr = new StreamReader(path);
            String emailTemplate = sr.ReadToEnd();
            sr.Close();
            sr.Dispose();

            return emailTemplate;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private void WriteEmails(string emails)
    {
        String path = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EmailSettingsFile"]);

        StreamWriter sw = new StreamWriter(path, false);
        sw.Write(emails);

        sw.Close();
        sw.Dispose();
    }
}
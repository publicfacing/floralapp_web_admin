﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using Library.AppSettings;
using Library.Utility;
using System.Drawing;

public partial class Admin_ViewAllPurchases : AppPage
{
    AppVars v = new AppVars();
    private int rowCount = 0;
    private int totalRowCount = 0;

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            InitializeControls();
            BindData();
        }
    }

    private void InitializeControls()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            //ddlReseller.Enabled = false;
            lblShop.Visible = false;
            ddlReseller.Visible = false;
            ddlSearchField.Items.RemoveAt(0); // Removing option for Purchase ID from search option.
            ddlSearchField.Items.RemoveAt(1); // Removing option for Shop from search option.
        }
    }

    private void BindData()
    {
        Label lblStatus;

        Util.setValue(gvPurchase, GetPurchaseList());

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvPurchase.Columns[0].Visible = false;
            gvPurchase.Columns[11].Visible = false;
        }

        #region Create a background indicator for failure item(s)

        for (int i = 0; i < gvPurchase.Rows.Count; i++)
        {
            lblStatus = (Label)gvPurchase.Rows[i].FindControl("lblTransactionStatus");
            if (lblStatus.Text.Equals("Failed"))
            {
                gvPurchase.Rows[i].BackColor = Color.LightPink;
            }

        }

        #endregion
    }

    private DataSet GetPurchaseList()
    {
        StringBuilder sbFilter = new StringBuilder();
        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();

        switch (searchOperator)
        {
            case "Begins With":
                sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                break;
            case "Ends With":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                break;
            case "Contains":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                break;
            case "Equals":
                sbFilter.Append(" And " + searchField + " = " + searchString);
                break;
            default:
                break;
        }

        Int32 resellerID = Convert.ToInt32(Util.getValue(ddlReseller));
        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);

        //if Transaction status not all then 
        if (ddlTransactionStatus.SelectedIndex > 0)
            sbFilter.Append(" And TransactionStatusID =" + Convert.ToInt32(Util.getValue(ddlTransactionStatus)));

        if (txtStartDate.Text != "" && txtStartDate.Text != "")
            sbFilter.Append(" And  (DateAdded  >='" + txtStartDate.Text + "' and DateAdded  <='" + txtEndDate.Text + "' )");

        #region New concept of purchase now are displaying active and inactive records...

        if (ddlStatus.SelectedItem.Value == "-1")// This concept will displaying the current new record(s)
            sbFilter.Append(" And  Status = -1");

        if (ddlStatus.SelectedItem.Value == "1")// This concept will displaying the successed record(s)
            sbFilter.Append(" And  Status = 1 ");
        //sbFilter.Append(" And  Status = 1  And TransactionStatusID = 100");

        if (ddlStatus.SelectedItem.Value == "0")// This concept will displaying the inactive record(s)
            sbFilter.Append(" And  Status = 0"); // This concept will displaying the inactive record(s)

        #endregion

        String orderBy = gvPurchase.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " PurchaseID Desc ";

        Data.Purchase p = new Data.Purchase();
        DataSet dsPurchase = p.FilteredList(gvPurchase.PageIndex, gvPurchase.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvPurchase.VirtualItemCount = totalRowCount;

        // Set Total Count in grid footer
        Util.setValue(lblTotalMagazinePurchased, String.Format("{0} Product(s) purchased.", totalRowCount));

        return dsPurchase;
    }

    protected void gvPurchase_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPurchase.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvPurchase_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void btnPurchasePreview_Click(object sender, EventArgs e)
    {
        String redirectUrl = "PurchaseInformation.aspx";
        
        try
        {
            ImageButton objButton = (ImageButton)sender;
            String[] purchaseValues = objButton.CommandArgument.Split(',');

            Int64 purchaseID = Util.GetDataInt64(purchaseValues[0].ToString());
            Int64 orderID = Util.GetDataInt64(purchaseValues[1].ToString());

            String qs = Crypto.EncryptQueryString("PurchaseID=" + purchaseID + "&OrderID=" + orderID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        gvPurchase.PageIndex = 0;
        BindData();
    }

    //protected void ImgStatus_Click(object sender, EventArgs e)
    //{
    //    //Int64 purchaseID = Util.GetDataInt64(((ImageButton)sender).ToolTip);
    //    //Int32 isActive = Util.GetDataInt32(((ImageButton)sender).CommandArgument);

    //    //Data.Purchase purchase = new Data.Purchase();

    //    //if (isActive == 1)
    //    //    isActive = 0;
    //    //else
    //    //    isActive = 1;

    //    //purchase.InactivePurchaseByID(purchaseID, isActive);

    //    //BindData();
    //}
}


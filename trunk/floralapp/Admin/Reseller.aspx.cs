﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;
using System.Drawing;

public partial class Admin_Reseller : AppPage
{

    private static Int32 resellerID
    {
        get;
        set;
    }

    private int rowCount = 0;
    private int totalRowCount = 0;
    public int isSuperAdmin = 0;

    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            InitializeControls();
            BindData();
        }
    }

    private void InitializeControls()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            isSuperAdmin = 0;
            //ddlReseller.Enabled = false;
            divShop.Visible = false;
            ddlSearchField.Items.RemoveAt(0);
            ddlSearchField.Items.RemoveAt(0);
        }
        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
            isSuperAdmin = 1;
    }

    private void BindData()
    {
        if (!CanAddReseller())
            lnkAddReseller.Visible = false;

        Util.setValue(gvReseller, GetResellerList());

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvReseller.Columns[0].Visible = false;
        }
    }

    private DataSet GetResellerList()
    {
        StringBuilder sbFilter = new StringBuilder();

        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();
        Int32 resellerID = 0;

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            resellerID = v.ResellerID;
            sbFilter.Append(" And resellerid=" + resellerID);
            lnkAddReseller.Visible = false;
        }

        switch (searchOperator)
        {
            case "Begins With":
                sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                break;
            case "Ends With":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                break;
            case "Contains":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                break;
            case "Equals":
                sbFilter.Append(" And " + searchField + " = " + searchString);
                break;
            default:
                break;
        }


        resellerID = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? Convert.ToInt32(v.ResellerID) : Convert.ToInt32(Util.getValue(ddlReseller));

        resellerID = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? v.ResellerID : Convert.ToInt32(Util.getValue(ddlReseller));


        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);

        String orderBy = gvReseller.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = "ResellerName ASC";

        Data.Reseller resel = new Data.Reseller();
        DataSet dsReseller = resel.FilteredList(gvReseller.PageIndex, gvReseller.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvReseller.VirtualItemCount = totalRowCount;

        #region Binding images ...
        /*
        foreach (DataRow drReseller in dsReseller.Tables[0].Rows)
        {

            Byte[] fileResellerLogoByteData = (byte[])drReseller["ResellerLogo"];
            Byte[] fileSplashImageByteData = (byte[])drReseller["SplashImage"];
            Int32 ResellerID = Util.GetDataInt32(drReseller["ResellerID"].ToString());
            String filePath = Server.MapPath(@"~/DynImages\Reseller\");

            String strPathResellerLogo = String.Format("ResellerLogo_{0}{1}", ResellerID, ".png");
            String strPathSplashImage = String.Format("SplashImage_{0}{1}", ResellerID, ".png");



            if (fileResellerLogoByteData != null)
            {
                if (File.Exists(filePath + strPathResellerLogo))
                    File.Delete(filePath + strPathResellerLogo);

                FileStream fs = new FileStream(filePath + strPathResellerLogo, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileResellerLogoByteData, 0, fileResellerLogoByteData.Length);
                fs.Close();
            }

            filePath += strPathSplashImage;

            if (fileSplashImageByteData != null)
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);

                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileSplashImageByteData, 0, fileSplashImageByteData.Length);
                fs.Close();
            }
        }
        */
        #endregion End of binding image

        // Set Total Count in grid footer
        Util.setValue(lblTotalReseller, String.Format("{0} Shop(s).", totalRowCount));

        return dsReseller;
    }

    private Boolean CanAddReseller()
    {
        Boolean retval = true;
        Data.SuperAdminSetting sas = new Data.SuperAdminSetting(1); // 1 for Beneva
        Int32 limit = Convert.ToInt32(Crypto.DecryptQueryString(sas.ResellersLimit.value));
        Int32 activeResellerCount = 0;

        Data.Reseller res = new Data.Reseller();
        DataSet dsReseller = res.List();
        if (Util.IsValidDataSet(dsReseller))
            activeResellerCount = dsReseller.Tables[0].Rows.Count;

        if (activeResellerCount >= limit)
            retval = false;

        return retval;
    }

    protected void gvReseller_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvReseller.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvReseller_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddReseller.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String[] value = objButton.CommandArgument.ToString().Split(',');
            String ResellerID = value[0].ToString().Trim();
            String ResellerSettID = value[1].ToString().Trim();
            String ResMerchantID = value[2].ToString().Trim();
            String qs = Crypto.EncryptQueryString("ResellerID=" + ResellerID + "&ResellerSettID=" + ResellerSettID + "&ResMerchantID=" + ResMerchantID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int32 ResellerID = Convert.ToInt32(objButton.CommandArgument.ToString());
            Data.Reseller resel = new Data.Reseller(ResellerID);

            resel.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
            resel.DateDeleted.value = Util.GetServerDate();
            resel.DeletedBy.value = v.UserID;
            resel.Save();

            #region Inactivate/Delete the Relationship table(s) for this user

            resel.InActiveRelationshipTables(ResellerID, "Reseller");

            #endregion End of inactivate/delete process.

            //Save in ServerUpdates
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatus(ResellerID, "resellersettings");

            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData();
    }

    public Boolean GetVisibility()
    {
        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
            return true;
        else
            return false;
    }

    protected void imgBtnResellerClone_Click(object sender, EventArgs e)
    {
        try
        {
            lblError.Text = String.Empty;
            resellerID = Util.GetDataInt32(((ImageButton)sender).CommandArgument);
            mpeReseller.Show();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }
    }

    protected void btnCreateClone_Click(object sender, EventArgs e)
    {
        try
        {
            Data.Reseller res = new Data.Reseller();

            String newFloristCode = Util.getValue(txtCloneFloristCode);
            String newReselerName = Util.getValue(txtCloneResellerName);
            Int32 newResellerID = 0;
            Int32 isExistFloristCode = res.isValidFloristCodeByReseller(newFloristCode, 0);

            lblError.ForeColor = Color.Red;
            lblError.Visible = isExistFloristCode > 0 ? true : false;

            if (isExistFloristCode > 0)
            {
                ShowError(lblError, "Florist code is already in used. Please provide another one.");
                mpeReseller.Show();
            }
            else
            {
                mpeReseller.Hide();
                newResellerID = res.CreateResellerClone(resellerID, newFloristCode, newReselerName, v.UserID);

                #region Generate images for new reseller in web directory.

                String resellerDirPath = Server.MapPath(@"~/DynImages\Reseller\");
                String categoryDirPath = Server.MapPath(@"~/DynImages\Category\");
                String productDirPath = Server.MapPath(@"~/DynImages\Product\");
                String upsellCategoryDirPath = Server.MapPath(@"~/DynImages\UpsellCategory\");
                //String couponDirPath = Server.MapPath(@"~/DynImages\Coupon\");

                res.GenerateImagesByReseller(newResellerID, resellerDirPath, categoryDirPath, productDirPath, upsellCategoryDirPath);

                #endregion

                Response.Redirect("Reseller.aspx", true);
            }
            Util.setValue(txtCloneFloristCode, String.Empty);
            Util.setValue(txtCloneResellerName, String.Empty);
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

    }
}

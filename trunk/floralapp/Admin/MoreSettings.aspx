﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="MoreSettings.aspx.cs" Inherits="Admin_MoreSettings" Title="floralapp&reg; | More Settings"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="HTMLEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">

    <script language="javascript" type="text/javascript">
    function IsChecked(objRadio)
   {  
        var grid = document.getElementById("<%=gvMoreSettings.ClientID %>");
        var inputTags =  grid.getElementsByTagName("input");
        for(var i=0; i<grid.rows.length-2; i++)
        { 
            if(inputTags[i].type == "radio")
               inputTags[i].checked = false;
        } 
        objRadio.checked = true;
   }
    </script>

    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg TableHeading">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                padding-left: 5px;">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPageTitle" runat="server" Text="More Settings"></asp:Label>
                                    </td>
                                    <td style="text-align: right; padding-right: 5px; text-transform: capitalize;">
                                        <asp:LinkButton ID="lbMoreSettings" runat="server" ToolTip="Select &amp; edit more setting files."
                                            Text="Edit" OnClick="lbMoreSettings_Click" Visible="false"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <asp:GridView ID="gvMoreSettings" runat="server" PageSize="10" AutoGenerateColumns="false"
                                PagerSettings-Position="TopAndBottom" SkinID="gvskinSmall" EmptyDataText="No record(s) found.">
                                <Columns>
                                    <%--<asp:TemplateField HeaderStyle-CssClass="gridHeader" HeaderStyle-Width="20px" ItemStyle-CssClass="gridContent">
                                        <ItemTemplate>
                                            <asp:RadioButton ID="rdoMoreSettings" runat="server" CausesValidation="false" Visible='<%# Eval("FileName").ToString().IndexOf("Faq") != -1 ?  false   :  true   %>'
                                                onclick="IsChecked(this);" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderStyle-CssClass="gridHeader" HeaderText="" ItemStyle-CssClass="gridContent">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlSettings" runat="server" Text='<%# Eval("FileName") %>'
                                                Target="_blank" NavigateUrl='<%#Eval("Url") %>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- PopUp Ajax Model for create coupon report. -->
    <asp:LinkButton ID="lnkEdit" runat="server" Style="display: none"></asp:LinkButton>
    <ajax:ModalPopupExtender ID="mpeMoreSettings" TargetControlID="lnkEdit" runat="server"
        PopupControlID="pnlMoreSettings" BackgroundCssClass="modalBackground" DropShadow="False"
        CancelControlID="btnCancel">
    </ajax:ModalPopupExtender>
    <asp:Panel runat="server" ID="pnlMoreSettings" Style="display: none; background: #ffffff;">
        <table border="0" width="700" cellpadding="0" class="PopUpBox" style="overflow: scroll;">
            <tr>
                <td class="PopUpBoxHeading">
                    <asp:Label ID="lblMoreSettings" runat="server" Text="Edit Settings"></asp:Label>
                    <asp:Label ID="lblFileNames" runat="server"></asp:Label>
                </td>
                <td class="PopUpBoxHeading" align="right">
                    <asp:ImageButton ID="btnClosePopup" runat="server" CausesValidation="false" ImageUrl="~/Images/close.gif" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 10px;">
                    <HTMLEditor:Editor ID="rteMoreSettings" runat="server" Height="300px" Width="100%" />
                </td>
            </tr>
            <tr>
                <td class="val" colspan="2" style="text-align: center;">
                    <div style="margin: 10px">
                        <asp:Button ID="btnSave" runat="server" Width="60px" Text="Save" CssClass="btn" OnClick="btnSave_Click"
                            CausesValidation="false" />
                        <asp:Button ID="btnCancel" runat="server" Width="60px" Text="Cancel" CssClass="btn"
                            CausesValidation="false" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <!-- End of popup coupon report."  -->
</asp:Content>

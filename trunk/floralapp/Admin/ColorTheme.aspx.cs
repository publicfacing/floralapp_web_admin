﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;

public partial class Admin_ColorTheme : AppPage
{
    private int rowCount = 0;
    private int totalRowCount = 0;
    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        Util.setValue(gvColorTheme, GetColorThemeList());
    }

    private DataSet GetColorThemeList()
    {

        StringBuilder sbFilter = new StringBuilder();
        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();

        switch (searchOperator)
        {
            case "Begins With":
                sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                break;
            case "Ends With":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                break;
            case "Contains":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                break;
            case "Equals":
                sbFilter.Append(" And " + searchField + " = " + searchString);
                break;
            default:
                break;
        }

        String orderBy = gvColorTheme.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " ColorThemeID asc";

        Data.ColorTheme ct = new Data.ColorTheme();
        DataSet dsColorTheme = ct.FilteredList(gvColorTheme.PageIndex, gvColorTheme.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvColorTheme.VirtualItemCount = totalRowCount;

        #region Binding images ...
        /*
        foreach (DataRow drCt in dsColorTheme.Tables[0].Rows)
        {
            Byte[] fileByteNaviBarImage = (byte[])drCt["NavigationBarImage"];
            Byte[] fileByteBgImage = (byte[])drCt["BackGroundImage"];
            Byte[] fileByteLeftOnImage = (byte[])drCt["NavLeftImgOn"];
            Byte[] fileByteLeftOffImage = (byte[])drCt["NavLeftImgOff"];
            Byte[] fileByteRightOnImage = (byte[])drCt["NavRightImgOn"];
            Byte[] fileByteRightOffImage = (byte[])drCt["NavRightImgOff"];

            String imageExt = ".png";
            Int32 ctID = Convert.ToInt32(drCt["ColorThemeID"]);

            String filePath = Server.MapPath(@"~/DynImages\ColorTheme\");
            String strNaviBarImage = String.Format("NavigationBarImage_{0}{1}", ctID, imageExt);
            String strBgImage = String.Format("BackGroundImage_{0}{1}", ctID, imageExt);
            String strLeftOnImage = String.Format("NavLeftImgOn_{0}{1}", ctID, imageExt);
            String strLeftOffImage = String.Format("NavLeftImgOff_{0}{1}", ctID, imageExt);
            String stRightOnImage = String.Format("NavRightImgOn_{0}{1}", ctID, imageExt);
            String strRightOffImage = String.Format("NavRightImgOff_{0}{1}", ctID, imageExt);




            if (fileByteNaviBarImage != null)
            {
                if (File.Exists(filePath + strNaviBarImage))
                    File.Delete(filePath + strNaviBarImage);

                FileStream fs = new FileStream(filePath + strNaviBarImage, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteNaviBarImage, 0, fileByteNaviBarImage.Length);
                fs.Close();
            }

            if (fileByteBgImage != null)
            {
                if (File.Exists(filePath + strBgImage))
                    File.Delete(filePath + strBgImage);

                FileStream fs = new FileStream(filePath + strBgImage, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteBgImage, 0, fileByteBgImage.Length);
                fs.Close();
            }

            if (fileByteLeftOnImage != null)
            {
                if (File.Exists(filePath + strLeftOnImage))
                    File.Delete(filePath + strLeftOnImage);

                FileStream fs = new FileStream(filePath + strLeftOnImage, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteLeftOnImage, 0, fileByteLeftOnImage.Length);
                fs.Close();
            }

            if (fileByteLeftOffImage != null)
            {
                if (File.Exists(filePath + strLeftOffImage))
                    File.Delete(filePath + strLeftOffImage);

                FileStream fs = new FileStream(filePath + strLeftOffImage, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteLeftOffImage, 0, fileByteLeftOffImage.Length);
                fs.Close();
            }

            if (fileByteRightOnImage != null)
            {
                if (File.Exists(filePath + stRightOnImage))
                    File.Delete(filePath + stRightOnImage);

                FileStream fs = new FileStream(filePath + stRightOnImage, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteRightOnImage, 0, fileByteRightOnImage.Length);
                fs.Close();
            }

            if (fileByteRightOffImage != null)
            {
                if (File.Exists(filePath + strRightOffImage))
                    File.Delete(filePath + strRightOffImage);

                FileStream fs = new FileStream(filePath + strRightOffImage, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteRightOffImage, 0, fileByteRightOffImage.Length);
                fs.Close();
            }
        }
         */
        #endregion End of binding image

        // Set Total Count in grid footer
        Util.setValue(lblTotalColorTheme, String.Format("{0} Color Theme(s).", totalRowCount));

        #region visibilities of adding themes [True or False] settings by user's role

        if (v.UserRoleID == 1) // 1 for Super admin can add theme upto 3
            //lnkAddColorTheme.Visible = totalRowCount == 3 ? false : true;
            lnkAddColorTheme.Visible = true;
        else if (v.UserRoleID == 2) // 2 for reseller admin that can't add theme
            lnkAddColorTheme.Visible = false;

        #endregion

        return dsColorTheme;
    }

    protected void gvColorTheme_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvColorTheme.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvColorTheme_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddColorTheme.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String ColorThemeID = objButton.CommandArgument.ToString();
            String qs = Crypto.EncryptQueryString("ColorThemeID=" + ColorThemeID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int32 ColorThemeID = Convert.ToInt32(objButton.CommandArgument.ToString());

            Data.ColorTheme ct = new Data.ColorTheme(ColorThemeID);

            ct.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
            ct.DateDeleted.value = Util.GetServerDate();
            ct.DeletedBy.value = v.UserID;
            ct.Save();

            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatusColorTheme(ColorThemeID);
 
            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData();
    }

    public bool getVisibility(object colorThemeID)
    {
        Data.ResellerSetting rs = new Data.ResellerSetting();

        bool isVisible = false;
        isVisible = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? false : true;

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
        {
            DataSet dsResellerSettings = rs.List();
            DataRow[] drResellerSettings = dsResellerSettings.Tables[0].Select("ColorThemeID=" + colorThemeID);
            if (drResellerSettings.Length > 0)
                isVisible = false;
            else
                isVisible = true;
        }

        return isVisible;
    }

    public bool getEditVisibility()
    {   
        return (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN)) ? true : false;
    }
}

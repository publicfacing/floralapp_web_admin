﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="ReorderCategory.aspx.cs" Inherits="Admin_ReorderCategory" Title="floralapp&reg; | Reorder Category"
    Theme="GraySkin" %>

<%@ Register Namespace="MetaOption.Web.UI.WebControls" TagPrefix="mo" %> 

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <asp:UpdatePanel ID="upNewPayment" runat="server">
        <ContentTemplate>
            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                <tr>
                    <td valign="top" align="left">
                        <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                            class="arial-12">
                            <tr>
                                <th class="TableHeadingBg TableHeading" >
                                    Reorder Category (WIP)
                                </th>
                            </tr>
                            <tr>
                                <td class="TableBorder"  >
                                    <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                                     
                                    <table style="width:100%;border:0px;border-collapse: collapse" cellpadding="0" class="grid">
                                        <tr class="gridHeader">
                                            <th style="width:16px;">&nbsp;</th>
                                            <th style="padding-left:8px;">Category Name</th> 
                                        </tr>
                                    </table> 
                                    <mo:ExtReorderList runat="server" ID="rlCategory" PostBackOnReorder="true" CssClass="reorderList"
                                        CallbackCssStyle="callbackStyle" DragHandleAlignment="Left" ItemInsertLocation="End"
                                        DataKeyField="CategoryID" SortOrderField="CategoryName" OnItemReorder="rlCategory_ItemReorder">
                                        <ItemTemplate>
                                            <div class="itemArea">
                                                <table style="width:100%;border:0px;border-collapse: collapse" cellpadding="0">
                                                    <tr>
                                                        <td><%# HttpUtility.HtmlEncode(Convert.ToString(Eval("CategoryName")))%></td>  
                                                    </tr>
                                                </table> 
                                            </div>
                                        </ItemTemplate> 
                                        <ReorderTemplate>
                                            <asp:Panel ID="Panel2" runat="server" CssClass="reorderCue" > 
                                                <%# HttpUtility.HtmlEncode(Convert.ToString(Eval("CategoryName")))%>
                                            </asp:Panel> 
                                        </ReorderTemplate>
                                        <DragHandleTemplate>
                                            <div class="dragHandle"> 
                                            </div>
                                        </DragHandleTemplate> 
                                    </mo:ExtReorderList>  
                                </td>
                            </tr> 
                        </table>
                        <div class="actionDiv"> 
                            <asp:Button ID="btnBack" runat="server" Text="Back" ToolTip="Click here to go back to category list." CssClass="btn" OnClick="btnBack_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="Coupons.aspx.cs" Inherits="Admin_Coupons" Title="floralapp&reg; | Coupons"
    Theme="GraySkin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12">
                    <tr>
                        <th class="TableHeadingBg TableHeading">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                padding-left: 5px;">
                                <tr>
                                    <td>
                                        Coupons
                                    </td>
                                    <td style="text-align: right; padding-right: 5px; text-transform: capitalize;">
                                        <asp:HyperLink ID="lnkAdd" runat="server" NavigateUrl="~/Admin/AddCoupon.aspx" ToolTip="Add Coupon">Add Coupon</asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label" valign="top">
                                            Search :
                                        </td>
                                        <td valign="top">
                                            <div runat="server" id="divShop">
                                                <label class="label">
                                                    Shop:</label>
                                                <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                            </div>
                                            <asp:DropDownList ID="ddlSearchField" runat="server" AutoPostBack="false">
                                                <asp:ListItem Text="ID" Value="CouponID"></asp:ListItem>
                                                <asp:ListItem Text="Coupon Code" Value="CouponCode"></asp:ListItem>
                                                <asp:ListItem Text="Coupon Type" Value="CouponUsageType"></asp:ListItem>
                                                <asp:ListItem Text="Shop" Value="ResellerName"></asp:ListItem>
                                                <asp:ListItem Text="Discount $" Value="DiscountFixedAmount"></asp:ListItem>
                                                <asp:ListItem Text="Discount %" Value="DiscountPercent"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchOperator" runat="server">
                                                <asp:ListItem Value="Begins With">Begins With</asp:ListItem>
                                                <asp:ListItem Value="Ends With">Ends With</asp:ListItem>
                                                <asp:ListItem Value="Contains">Contains</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnFilter" class="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click"
                                                CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <asp:DataPager ID="dpTop" runat="server" PageSize="25" PagedControlID="gvCoupon">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                        ShowLastPageButton="false" ShowNextPageButton="false" />
                                    <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                        NextPreviousButtonCssClass="command" />
                                    <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                        ShowLastPageButton="true" ShowNextPageButton="true" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <mo:ExtGridView ID="gvCoupon" runat="server" DataKeyNames="CouponID" AllowSorting="True"
                                OnSorting="gvCoupon_OnSorting" OrderBy="CouponID Desc" SkinID="gvskin" OnPageIndexChanging="gvCoupon_OnPageIndexChanging"
                                EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:BoundField DataField="CouponID" SortExpression="CouponID" HeaderText="ID" ItemStyle-CssClass="gridContent">
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Image" ItemStyle-CssClass="gridContent">
                                        <ItemTemplate>
                                            <asp:Image ID="imgCat" runat="server" Visible="true" ImageUrl='<%# getCouponImage(Eval("CouponID").ToString() + Eval("CouponImageExt").ToString()) %>'
                                                mageAlign="Top" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CouponCode" SortExpression="CouponCode" HeaderText="Coupon Code"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>
                                    <asp:TemplateField SortExpression="Referral" HeaderText="Referral">
                                        <ItemStyle CssClass="gridContent" />
                                        <ItemTemplate>
                                            <div style="max-width: 200px; min-width:150px; overflow-x: auto;">
                                                <%# Eval("Referral").ToString()%>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ResellerName" SortExpression="ResellerName" HeaderText="Shop"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Discount" ItemStyle-CssClass="gridContent halign-right"
                                        SortExpression="DiscountType" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiscount" runat="server" Text='<%# Eval("DiscountType").ToString().Equals("Percentage")? Eval("DiscountPercent") + " %" :  String.Format("{0:C}", Eval("DiscountFixedAmount") ) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="IsVisibleOnPhone" SortExpression="IsVisibleOnPhone" HeaderText="Visible On Ph."
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>
                                    <asp:BoundField DataField="CouponUsageType" SortExpression="CouponUsageType" HeaderText="Coupon Type"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Is Enabled" ItemStyle-CssClass="gridContent halign-center"
                                        HeaderStyle-Width="60px" SortExpression="IsEnabled">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEnable" runat="server" CommandArgument='<%# Eval("CouponID") + "," + Eval("IsEnabled") + "," + Eval("CouponUsageTypeID") %>'
                                                OnClick="lnkEnable_Click" ImageUrl='<%# GetImage(Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsEnabled"))) %>'>
                                            </asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="gridContent" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("CouponID")%>'
                                                CausesValidation="false" OnClick="lnkEdit_Click" ToolTip="Edit" ImageUrl="~/Images/edit-icon.jpg" />
                                            <asp:ImageButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("CouponID")%>'
                                                CausesValidation="false" OnClick="lnkDelete_Click" ImageUrl="~/Images/DeleteButton.jpg"
                                                OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotalCoupon" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="25" PagedControlID="gvCoupon">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

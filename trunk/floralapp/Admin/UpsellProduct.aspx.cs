﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;
using System.IO;

public partial class Admin_UpsellProduct : AppPage
{
    AppVars v = new AppVars();
    private int rowCount = 0;
    private int totalRowCount = 0;

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            InitializeControls();
            BindData();
        }
    }

    private void InitializeControls()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            //ddlReseller.Enabled = false;
            lblShop.Visible = false;
            ddlReseller.Visible = false;
        }
    }

    private void BindData()
    {
        if (!CanAddNewUpsellProduct() && v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
            lnkAddProduct.Visible = false;

        Util.setValue(gvProducts, GetProductList());
    }

    private DataSet GetProductList()
    {
        StringBuilder sbFilter = new StringBuilder();

        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();

        if (!String.IsNullOrEmpty(searchString))
        {
            switch (searchOperator)
            {
                case "Begins With":
                    sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                    break;
                case "Ends With":
                    sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                    break;
                case "Contains":
                    sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                    break;
                case "Equals":
                    sbFilter.Append(" And " + searchField + " = " + searchString);
                    break;
                default:
                    break;
            }
        }

        Int32 resellerID = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? Convert.ToInt32(v.ResellerID) : Convert.ToInt32(Util.getValue(ddlReseller));

        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);

        if (txtStartDate.Text != "" && txtStartDate.Text != "")
            sbFilter.Append(" And  (DateAdded  >='" + Convert.ToDateTime(txtStartDate.Text).ToShortDateString() + "' and DateAdded  <='" + Convert.ToDateTime(txtEndDate.Text).ToShortDateString() + "' )");

        String orderBy = gvProducts.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " SKUNo asc ";

        Data.UpsellProduct up = new Data.UpsellProduct();
        DataSet dsProducts = up.GetList(gvProducts.PageIndex, gvProducts.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvProducts.VirtualItemCount = totalRowCount;

        // Set Total Count in grid footer
        Util.setValue(lblTotalProducts, String.Format("{0} product(s).", totalRowCount));

        return dsProducts;
    }

    private Boolean CanAddNewUpsellProduct()
    {
        Int32 resellerID = Int32.Parse(Util.getValue(this.ddlReseller));
        Data.Reseller rs = new Data.Reseller();
        return rs.CanAddNewUpsellProduct(resellerID);
    }

    protected void gvProducts_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void gvProducts_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProducts.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void btnPreview_Click(object sender, EventArgs e)
    {
        String redirectUrl = "ManageUpsellProduct.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String[] value = objButton.CommandArgument.ToString().Split(',');
            String UpsellProductID = value[0].ToString();
            String UpsellCategoryID = value[1].ToString();
            String qs = Crypto.EncryptQueryString("UpsellProductID=" + UpsellProductID + "&UpsellCategoryID=" + UpsellCategoryID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddUpsellProduct.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            string[] value = objButton.CommandArgument.ToString().Split(',');
            String UpsellProductID = value[0].ToString();
            String prodCatID = value[1].ToString();

            String qs = Crypto.EncryptQueryString("UpsellProductID=" + UpsellProductID + " &UpsellProductCategoryID=" + prodCatID);

            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int64 upsellProductID = Convert.ToInt64(objButton.CommandArgument.ToString());

            Data.UpsellProduct up = new Data.UpsellProduct(upsellProductID);
            up.IsActive.value = 0;
            up.DateDeleted.value = Util.GetServerDate();
            up.DeletedBy.value = Convert.ToInt64(v.UserID);
            up.Save();

            #region Inactivate/Delete the Relationship table(s) for this user

            up.InActiveRelationshipTables(upsellProductID, "UpsellProduct");

            #endregion End of inactivate/delete process.

            #region Save in ServerUpdates
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatus(up.ResellerID.value, "upsellproduct");
            #endregion

            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void ddlReseller_SelectedIndexChanged(object sender, EventArgs e)
    {

        BindData();
    }

    public String getThumbImage(String fileExtension)
    {
        String thumbPath = "../DynImages/UpsellProduct_New/ProductThumb_" + fileExtension;

        if (!File.Exists(Server.MapPath(thumbPath)))
            thumbPath = "~/Images/blank_img.png";

        return thumbPath;

    }
}
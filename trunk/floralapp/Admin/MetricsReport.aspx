﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="MetricsReport.aspx.cs" Inherits="Admin_MetricsReport" Title="floralapp&reg; | Metrics Report"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script type="text/javascript">
        function checkDate(sender, args) {
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a day earlier than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
            var pastDate = new Date();
            pastDate.setMonth(pastDate.getMonth() - 3);
            if (sender._selectedDate < pastDate) {
                alert("You cannot select a day over 3 months!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12">
                    <tr>
                        <th class="TableHeadingBg TableHeading">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                padding-left: 5px;">
                                <tr>
                                    <td>
                                        Metrics Report
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label" valign="top">
                                            Search :
                                        </td>
                                        <td valign="top">
                                            <div runat="server" id="divShop">
                                                <label class="label">
                                                    Shop:</label>
                                                <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                            </div>
                                            <asp:Label ID="Label1" CssClass="label" runat="server" Text="Date Range:"></asp:Label>
                                            <asp:TextBox runat="server" Width="70px" OnKeyUP="this.value=''" CssClass="textbox"
                                                ID="txtStartDate"></asp:TextBox>
                                            <ajax:CalendarExtender ID="ceStartDate" runat="server" TargetControlID="txtStartDate"
                                                PopupButtonID="imgCalIcon1" OnClientDateSelectionChanged="checkDate">
                                            </ajax:CalendarExtender>
                                            <asp:Image ImageUrl="~/Images/calender-icon.gif" Style="cursor: hand" ID="imgCalIcon1"
                                                runat="server" />
                                            &nbsp;&nbsp;
                                            <asp:TextBox runat="server" Width="70px" OnKeyUP="this.value=''" CssClass="textbox"
                                                ID="txtEndDate"></asp:TextBox>
                                            <ajax:CalendarExtender ID="ceEndDate" runat="server" TargetControlID="txtEndDate"
                                                PopupButtonID="imgCalIcon2" OnClientDateSelectionChanged="checkDate">
                                            </ajax:CalendarExtender>
                                            <asp:Image ImageUrl="~/Images/calender-icon.gif" ID="imgCalIcon2" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnFilter" class="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click"
                                                CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <asp:DataPager ID="DataPager1" runat="server" PageSize="25" PagedControlID="gvMetrics">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                        ShowLastPageButton="false" ShowNextPageButton="false" />
                                    <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                        NextPreviousButtonCssClass="command" />
                                    <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                        ShowLastPageButton="true" ShowNextPageButton="true" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <mo:ExtGridView ID="gvMetrics" runat="server" DataKeyNames="ResellerID" AllowSorting="True"
                                OnSorting="gvMetrics_OnSorting" orderby="ResellerID" SkinID="gvskin" OnPageIndexChanging="gvMetrics_OnPageIndexChanging"
                                EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:BoundField DataField="ResellerName" SortExpression="ResellerName" HeaderText="Shop Name"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="180px"></asp:BoundField>
                                    <%--<asp:BoundField DataField="NrDownloads" SortExpression="NrDownloads" HeaderText="Downloads"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>--%>
                                    <asp:TemplateField SortExpression="NrRegUsers" HeaderText="Registered Users" ItemStyle-CssClass="gridContent"
                                        HeaderStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lnkConsumers" Text='<%# Eval("NrRegUsers") %>'
                                                CommandArgument='<%# Eval("ResellerID") %>' OnClick="lnkConsumers_Click"></asp:LinkButton>
                                            <%--<a href="Users.aspx?RoldeID=3&Shop=<%# Eval("ResellerID") %>"><%# Eval("NrRegUsers") %></a>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="NrOrders" SortExpression="NrOrders" HeaderText="Orders"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>
                                    <asp:BoundField DataField="DollarsSpent" SortExpression="DollarsSpent" HeaderText="Dollars Spent"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px" DataFormatString="{0:c}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AvgPerOrder" SortExpression="AvgPerOrder" HeaderText="Avg per Order"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px" DataFormatString="{0:c}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CouponsRedeemed" SortExpression="CouponsRedeemed" HeaderText="Coupons Redeemed"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="120px"></asp:BoundField>
                                    <asp:BoundField DataField="TotalCoupons" SortExpression="TotalCoupons" HeaderText="Coupon Savings"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px" DataFormatString="{0:c}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NrProducts" SortExpression="NrProducts" HeaderText="Products Sold"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="gridContent" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkViewReport" runat="server" CommandArgument='<%# Eval("ResellerID") +","+ Eval("ResellerName") %>'
                                                CausesValidation="false" OnClick="lnkViewReport_Click" ToolTip="View Previous Year Comparison Report"
                                                ImageUrl="~/Images/Pop-Up.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="25" PagedControlID="gvMetrics">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- PopUp Ajax Model for create report. -->
    <asp:Button ID="btnReport" Style="display: none" runat="server" />
    <ajax:ModalPopupExtender ID="mpeReport" TargetControlID="btnReport" runat="server"
        PopupControlID="pnlReport" BackgroundCssClass="modalBackground" DropShadow="False"
        CancelControlID="btnCancel">
    </ajax:ModalPopupExtender>
    <asp:Panel runat="server" ID="pnlReport" Style="display: none; background: #ffffff;">
        <table border="0" width="700" cellpadding="0" class="PopUpBox" style="overflow: scroll;">
            <tr>
                <td class="PopUpBoxHeading">
                    <asp:Label ID="lblReport" runat="server" Text="Previous Year Report"></asp:Label>
                </td>
                <td class="PopUpBoxHeading" align="right">
                    <asp:ImageButton ID="btnClosePopup" runat="server" CausesValidation="false" ImageUrl="~/Images/close.gif" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 10px;">
                    <div class="filterSection">
                        <asp:Label runat="server" ID="lblShopName"></asp:Label>
                        <br />
                        Period &nbsp;<asp:DropDownList runat="server" ID="ddlMonths">
                            <asp:ListItem Value="1" Text="01(January)"></asp:ListItem>
                            <asp:ListItem Value="2" Text="02(February)"></asp:ListItem>
                            <asp:ListItem Value="3" Text="03(March)"></asp:ListItem>
                            <asp:ListItem Value="4" Text="04(April)"></asp:ListItem>
                            <asp:ListItem Value="5" Text="05(May)"></asp:ListItem>
                            <asp:ListItem Value="6" Text="06(June)"></asp:ListItem>
                            <asp:ListItem Value="7" Text="07(July)"></asp:ListItem>
                            <asp:ListItem Value="8" Text="08(August)"></asp:ListItem>
                            <asp:ListItem Value="9" Text="09(September)"></asp:ListItem>
                            <asp:ListItem Value="10" Text="10(October)"></asp:ListItem>
                            <asp:ListItem Value="11" Text="11(November)"></asp:ListItem>
                            <asp:ListItem Value="12" Text="12(December)"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList runat="server" ID="ddlYear">
                        </asp:DropDownList>
                        <asp:Button runat="server" ID="btnShowReport" Text="View" OnClick="btnShowReport_Click"
                            class="btnFilter" />
                    </div>
                    <br />
                    <br />
                    <mo:ExtGridView ID="gvReport" runat="server" AllowSorting="false" SkinID="gvskin"
                        EmptyDataText="No record(s) found.">
                        <Columns>
                            <asp:BoundField DataField="ResellerName" SortExpression="ResellerName" HeaderText="Period"
                                ItemStyle-CssClass="gridContent" HeaderStyle-Width="200px"></asp:BoundField>
                            <%--<asp:BoundField DataField="NrDownloads" SortExpression="NrDownloads" HeaderText="Downloads"
                                ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>--%>
                            <asp:TemplateField SortExpression="NrRegUsers" HeaderText="Registered Users" ItemStyle-CssClass="gridContent"
                                HeaderStyle-Width="120px">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="lnkConsumers" Text='<%# Eval("NrRegUsers") %>'
                                        CommandArgument='<%# Eval("ResellerID") %>' OnClick="lnkConsumers_Click"></asp:LinkButton>
                                    <%--<a href="Users.aspx?RoldeID=3&Shop=<%# Eval("ResellerID") %>"><%# Eval("NrRegUsers") %></a>--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="NrOrders" SortExpression="NrOrders" HeaderText="Orders"
                                ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>
                            <asp:BoundField DataField="DollarsSpent" SortExpression="DollarsSpent" HeaderText="Dollars Spent"
                                ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px" DataFormatString="{0:c}">
                            </asp:BoundField>
                            <asp:BoundField DataField="AvgPerOrder" SortExpression="AvgPerOrder" HeaderText="Avg per Order"
                                ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px" DataFormatString="{0:c}">
                            </asp:BoundField>
                            <asp:BoundField DataField="CouponsRedeemed" SortExpression="CouponsRedeemed" HeaderText="Coupons Redeemed"
                                ItemStyle-CssClass="gridContent" HeaderStyle-Width="120px"></asp:BoundField>
                            <asp:BoundField DataField="TotalCoupons" SortExpression="TotalCoupons" HeaderText="Coupon Savings"
                                ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px" DataFormatString="{0:c}">
                            </asp:BoundField>
                            <asp:BoundField DataField="NrProducts" SortExpression="NrProducts" HeaderText="Products Sold"
                                ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>
                        </Columns>
                    </mo:ExtGridView>
                </td>
            </tr>
            <tr>
                <td class="val" colspan="2" style="text-align: center;">
                    <div style="margin: 10px">
                        <asp:Button ID="btnCancel" runat="server" Width="60px" Text="Close" CssClass="btn"
                            CausesValidation="false" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <!-- End of popup coupon report."  -->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    <script>
        var isAdmin = '<%=IsAdmin %>';
        if (isAdmin === "1")
            init();
        function init() {
            //$('.gridHeader th:nth-child(2) a').attr('title', '<%=NrDownloadsTotal %>');
            $('.gridHeader th:nth-child(2) a').attr('title', '<%=NrRegUsersTotal %>');
            $('.gridHeader th:nth-child(3) a').attr('title', '<%=NrOrdersTotal %>');
            $('.gridHeader th:nth-child(4) a').attr('title', '<%=DollarsSpentTotal %>');
            $('.gridHeader th:nth-child(5) a').attr('title', '<%=AvgPerOrderTotal %>');
            $('.gridHeader th:nth-child(6) a').attr('title', '<%=CouponsRedeemedTotal %>');
            $('.gridHeader th:nth-child(7) a').attr('title', '<%=CouponsTotal %>');
            $('.gridHeader th:nth-child(8) a').attr('title', '<%=ProductsTotal %>');

            $('.gridHeader th a').tooltip({
                position: {
                    my: "center bottom-20",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
            .addClass("arrow")
            .addClass(feedback.vertical)
            .addClass(feedback.horizontal)
            .appendTo(this);
                    }
                }
            });
        }
    </script>
    <style>
        .ui-tooltip, .arrow:after
        {
            background: gray;
            border: 2px solid white;
        }
        .ui-tooltip
        {
            padding: 10px 10px;
            color: white;
            border-radius: 10px;
            box-shadow: 0 0 7px black;
            width: 120px;
        }
        .arrow
        {
            width: 70px;
            height: 16px;
            overflow: hidden;
            position: absolute;
            left: 50%;
            margin-left: -35px;
            bottom: -16px;
        }
        .arrow.top
        {
            top: -16px;
            bottom: auto;
        }
        .arrow.left
        {
            left: 20%;
        }
        .arrow:after
        {
            content: "";
            position: absolute;
            left: 20px;
            top: -20px;
            width: 25px;
            height: 25px;
            box-shadow: 6px 5px 9px -9px black;
            -webkit-transform: rotate(45deg);
            -moz-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            -o-transform: rotate(45deg);
            tranform: rotate(45deg);
        }
        .arrow.top:after
        {
            bottom: -20px;
            top: auto;
        }
    </style>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="CouponUsageReport.aspx.cs" Inherits="Admin_CouponUsageReport" Title="floralapp&reg; | Coupon Usage Report"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12">
                    <tr>
                        <th class="TableHeadingBg TableHeading">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                padding-left: 5px;">
                                <tr>
                                    <td>
                                        Coupon Usage Report
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label" valign="top">
                                            Search :
                                        </td>
                                        <td valign="top">
                                            <div runat="server" id="divShop">
                                                <label class="label">
                                                    Shop:</label>
                                                <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                            </div>
                                            <asp:DropDownList ID="ddlSearchField" runat="server" AutoPostBack="false">
                                                <asp:ListItem Text="Coupon Code" Value="CouponCode"></asp:ListItem>
                                                <asp:ListItem Text="Coupon Type" Value="CouponUsageType"></asp:ListItem>
                                                <asp:ListItem Text="Shop" Value="ResellerName"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchOperator" runat="server">
                                                <asp:ListItem Value="Begins With">Begins With</asp:ListItem>
                                                <asp:ListItem Value="Ends With">Ends With</asp:ListItem>
                                                <asp:ListItem Value="Contains">Contains</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnFilter" class="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click"
                                                CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <asp:DataPager ID="DataPager1" runat="server" PageSize="25" PagedControlID="gvCouponUsage">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                        ShowLastPageButton="false" ShowNextPageButton="false" />
                                    <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                        NextPreviousButtonCssClass="command" />
                                    <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                        ShowLastPageButton="true" ShowNextPageButton="true" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <mo:ExtGridView ID="gvCouponUsage" runat="server" DataKeyNames="ResellerID" AllowSorting="True"
                                OnSorting="gvCouponUsage_OnSorting" OrderBy="ResellerID Desc" SkinID="gvskin"
                                OnPageIndexChanging="gvCouponUsage_OnPageIndexChanging" EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:BoundField DataField="CouponCode" SortExpression="CouponCode" HeaderText="Coupon Code"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>
                                    <asp:BoundField DataField="CouponUsageCount" SortExpression="CouponUsageCount" HeaderText="Usage Count"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="100px"></asp:BoundField>
                                    <asp:BoundField DataField="ResellerName" SortExpression="ResellerName" HeaderText="Shop"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="CouponUsageType" SortExpression="CouponUsageType" HeaderText="Coupon Type"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>
                                    <asp:BoundField DataField="LastUsed" SortExpression="LastUsed" HeaderText="Last Used"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="gridContent" HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkViewPurchase" runat="server" CommandArgument='<%# Eval("ResellerID") +","+ Eval("CouponCode") %>'
                                                CausesValidation="false" OnClick="lnkViewPurchase_Click" ToolTip="View Purchase Report"
                                                ImageUrl="~/Images/Pop-Up.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="25" PagedControlID="gvCouponUsage">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- PopUp Ajax Model for create coupon report. -->
    <asp:Button ID="btnCouponReport" Style="display: none" runat="server" />
    <ajax:ModalPopupExtender ID="mpeCouponReport" TargetControlID="btnCouponReport" runat="server"
        PopupControlID="pnlCouponReport" BackgroundCssClass="modalBackground" DropShadow="False"
        CancelControlID="btnCancel">
    </ajax:ModalPopupExtender>
    <asp:Panel runat="server" ID="pnlCouponReport" Style="display: none; background: #ffffff;">
        <table border="0" width="700" cellpadding="0" class="PopUpBox" style="overflow: scroll;">
            <tr>
                <td class="PopUpBoxHeading">
                    <asp:Label ID="lblCouponReport" runat="server" Text="Purchase Coupons"></asp:Label>
                </td>
                <td class="PopUpBoxHeading" align="right">
                    <asp:ImageButton ID="btnClosePopup" runat="server" CausesValidation="false" ImageUrl="~/Images/close.gif" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 10px;">
                    <asp:GridView ID="gvReport" runat="server" AllowPaging="true" PageSize="10" AutoGenerateColumns="false"
                        PagerSettings-Position="TopAndBottom" DataKeyNames="PurchaseID" AllowSorting="True"
                        OnSorting="gvReport_OnSorting" PagerStyle-BackColor="#E1E6EE" PagerStyle-HorizontalAlign="Right"
                        OrderBy="PurchaseID Desc" SkinID="gvskinSmall" OnPageIndexChanging="gvReport_OnPageIndexChanging"
                        EmptyDataText="No record(s) found.">
                        <Columns>
                            <asp:TemplateField SortExpression="PurchaseID" HeaderStyle-CssClass="gridHeader"
                                HeaderStyle-Width="65px" ItemStyle-HorizontalAlign="Center" HeaderText="Purchase ID"
                                Visible="false" ItemStyle-CssClass="gridContent">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkPurchase" runat="server" Text='<%# Eval("PurchaseID") %>'
                                        CommandArgument='<%# Eval("PurchaseID") %>' CausesValidation="false" OnClick="lnkPurchase_Click"
                                        ToolTip="View Purchase Preview">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="OrderID" HeaderStyle-CssClass="gridHeader" HeaderStyle-Width="65px"
                                ItemStyle-HorizontalAlign="Center" HeaderText="Order No" ItemStyle-CssClass="gridContent">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkOrderNo" runat="server" Text='<%# Eval("OrderID") %>' CommandArgument='<%# Eval("PurchaseID") %>'
                                        CausesValidation="false" OnClick="lnkPurchase_Click" ToolTip="View Purchase Preview">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Email" SortExpression="Email" HeaderText="Email" ItemStyle-CssClass="gridContent"
                                HeaderStyle-Width="100px"></asp:BoundField>
                            <asp:BoundField DataField="ResellerName" SortExpression="ResellerName" HeaderText="Shop"
                                ItemStyle-CssClass="gridContent"></asp:BoundField>
                            <asp:BoundField DataField="CouponCode" SortExpression="CouponCode" HeaderText="Coupon Code"
                                ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>
                            <asp:BoundField DataField="DateUsed" SortExpression="DateUsed" HeaderText="Date Used"
                                ItemStyle-CssClass="gridContent" HeaderStyle-Width="80px"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="val" colspan="2" style="text-align: center;">
                    <div style="margin: 10px">
                        <asp:Button ID="btnCancel" runat="server" Width="60px" Text="Cancel" CssClass="btn"
                            CausesValidation="false" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <!-- End of popup coupon report."  -->
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true" CodeFile="ReorderProducts.aspx.cs" Inherits="Admin_ReorderProducts" Title="floralapp&reg; | Reorder Products" Theme="GraySkin" %>

<%@ Register Namespace="MetaOption.Web.UI.WebControls" TagPrefix="mo" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" Runat="Server">
    <asp:UpdatePanel ID="upReorderProd" runat="server">
         <ContentTemplate>
            <table border="0" width="100%"  style="border-collapse: collapse">
                <tr>
                    <td valign="top" align="left">
                        <table border="0" width="100%"  style="border-collapse: collapse"
                            class="arial-12">
                            <tr>
                                <th class="TableHeadingBg TableHeading">
                                    Reorder Products
                                </th>
                            </tr>
                            <tr>
                                <td class="TableBorder">
                                    <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                                    <table style="width:100%;border:0px;border-collapse: collapse"  class="grid">
                                        <tr class="gridHeader">
                                            <th style="width:16px;">&nbsp;</th>
                                            <th style="width:50px;">ID</th>
                                            <th style="width:150px;">Product</th>
                                            <th style="width:235px">Description</th>
                                        </tr>
                                    </table> 
                                    <mo:ExtReorderList runat="server" ID="rlProduct" PostBackOnReorder="true" CssClass="reorderList"
                                        CallbackCssStyle="callbackStyle" DragHandleAlignment="Left" ItemInsertLocation="End"
                                        DataKeyField="ProductID" SortOrderField="ProductName" OnItemReorder="rlProduct_ItemReorder">
                                        <ItemTemplate>
                                            <div class="itemArea">
                                                <table style="width:100%;border:0px;border-collapse: collapse" >
                                                    <tr>
                                                        <td style="width:50px;"><%# HttpUtility.HtmlEncode(Convert.ToString(Eval("ProductID")))%></td>
                                                        <td style="width:150px;"><%# HttpUtility.HtmlEncode(Convert.ToString(Eval("ProductName")))%></td>
                                                        <td style="width:235px"><%# HttpUtility.HtmlEncode(Convert.ToString(Eval("Description")))%></td>
                                                    </tr>
                                                </table> 
                                            </div>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div class="itemArea">
                                                 EditProductTemplate
                                            </div>
                                        </EditItemTemplate>
                                        <ReorderTemplate>
                                            <asp:Panel ID="Panel2" runat="server" CssClass="reorderCue" > 
                                                <%# HttpUtility.HtmlEncode(Convert.ToString(Eval("ProductName")))%>
                                            </asp:Panel> 
                                        </ReorderTemplate>
                                        <DragHandleTemplate>
                                            <div class="dragHandle"> 
                                            </div>
                                        </DragHandleTemplate>
                                        
                                    </mo:ExtReorderList> 
                                </td>
                            </tr>
                        </table>
                        <div class="actionDiv"> 
                            <asp:Button ID="btnBack" runat="server" Text="Back" ToolTip="Click here to go back to product list." CssClass="btn" OnClick="btnBack_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


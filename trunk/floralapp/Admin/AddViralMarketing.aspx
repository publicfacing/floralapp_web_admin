﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="AddViralMarketing.aspx.cs" Inherits="Admin_AddViralMarketing" Title="floralapp&reg; | Add Viral Marketing"
    Theme="GraySkin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Add Viral Marketing"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="vsReseller" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                <tr>
                                    <td class="col">
                                        Shop<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                            OnSelectedIndexChanged="ddlReseller_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvReseller" runat="server" ControlToValidate="ddlReseller"
                                            ErrorMessage="Select a shop." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Requested by
                                    </td>
                                    <td class="val">
                                        Not necessery to add this row. its (UserID)
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Referral Code<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtReferralCode" runat="server" SkinID="textbox" class="FormItem"
                                            MaxLength="60"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Email ID (List)
                                    </td>
                                    <td class="val">
                                        <mo:ExtTextBox ID="txtEmailID" runat="server" TextMode="MultiLine" MaxLength="1000"
                                            SkinID="textarea" Height="114px" Width="655px"></mo:ExtTextBox>
                                        <br />
                                        Max. 1000 character.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Request Link
                                    </td>
                                    <td class="val">
                                        <mo:ExtTextBox ID="txtRequestLink" runat="server" TextMode="MultiLine" MaxLength="500"
                                            SkinID="textarea" Height="120px" Width="655px"></mo:ExtTextBox>
                                        <br />
                                        Max. 500 character.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Message
                                    </td>
                                    <td class="val">
                                        <mo:ExtTextBox ID="txtMessage" runat="server" TextMode="MultiLine" MaxLength="4000"
                                            SkinID="textarea" Height="197px" Width="655px"></mo:ExtTextBox>
                                        <br />
                                        Max. 4000 character.
                                    </td>
                                </tr>
                            </table>
                            <div class="actionDiv">
                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this coupon."
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                                    CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

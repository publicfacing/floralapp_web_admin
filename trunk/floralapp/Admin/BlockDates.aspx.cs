﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;

public partial class Admin_BlockDates : AppPage
{
    private int rowCount = 0;
    private int totalRowCount = 0;

    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            InitializeControls();
            BindData();
        }
    }

    private void InitializeControls()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            //ddlReseller.Enabled = false;
            divShop.Visible = false;

            ddlSearchField.Items.RemoveAt(1);
            ddlSearchField.Items.RemoveAt(1);
        }
    }

    private void BindData()
    {
        Util.setValue(gvBlockDates, GetBlockDatesList());

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvBlockDates.Columns[1].Visible = false;
            gvBlockDates.Columns[2].Visible = false;
        }
    }

    private DataSet GetBlockDatesList()
    {
        StringBuilder sbFilter = new StringBuilder();

        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();

        switch (searchOperator)
        {
            case "Begins With":
                sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                break;
            case "Ends With":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                break;
            case "Contains":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                break;
            case "Equals":
                sbFilter.Append(" And " + searchField + " = " + searchString);
                break;
            default:
                break;
        }

        Int32 resellerID = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? Convert.ToInt32(v.ResellerID) : Convert.ToInt32(Util.getValue(ddlReseller));

        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);

        String orderBy = gvBlockDates.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " ResellerID asc";

        Data.BlockDates BlockDates = new Data.BlockDates();
        DataSet dsBlockDates = BlockDates.FilteredList(gvBlockDates.PageIndex, gvBlockDates.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvBlockDates.VirtualItemCount = totalRowCount;

        // Set Total Count in grid footer
        Util.setValue(lblTotalCategory, String.Format("{0} Block Date(s).", totalRowCount));

        return dsBlockDates;
    }

    protected void gvBlockDates_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvBlockDates.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvBlockDates_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddBlockDates.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String BlockDateID = objButton.CommandArgument.ToString();
            String qs = Crypto.EncryptQueryString("BlockDateID=" + BlockDateID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int32 blockDateID = Convert.ToInt32(objButton.CommandArgument.ToString());
            Data.BlockDates bd = new Data.BlockDates(blockDateID);

            bd.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
            bd.DateDeleted.value = Util.GetServerDate();
            bd.DeletedBy.value = v.UserID;
            bd.Save();

            //Save in ServerUpdates
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatus(bd.ResellerID.value, "blockdates");

            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData();
    }
}

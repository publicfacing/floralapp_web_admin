﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.IO;
using System.Collections.Specialized;
using System.Drawing;

public partial class Admin_AddReseller : AppPage
{
    private Int32 ResellerID
    {
        get;
        set;
    }

    private Int32 ResellerSettID
    {
        get;
        set;
    }

    private Int32 ResellerMerchantID
    {
        get;
        set;
    }

    AppVars appVar = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);
        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                ResellerID = Util.GetDataInt32(nvc["ResellerID"]);
                ResellerSettID = Util.GetDataInt32(nvc["ResellerSettID"]);
                ResellerMerchantID = Util.GetDataInt32(nvc["ResMerchantID"]);
            }

            Util.setValue(lblPageTitle, (ResellerID > 0) ? "Edit Shop Information" : "Add Shop Information");
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        Int32 showAppDistribution = Convert.ToInt32(ConfigurationManager.AppSettings["ShowAppDistribution"]);
        if (showAppDistribution == 1)
            ShowAppDistributionLinks();

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        if (!CanAddReseller() && ResellerID <= 0)
            btnSave.Visible = false;

        // Country DropDownList
        Data.Country country = new Data.Country();
        Util.setValue(ddlCountry, country.List(), "CountryName", "CountryId");

        //Set USA By Defult
        //Util.setValue(ddlCountry, 233);
        //FillState(233);

        //Set Canada By Defult
        Util.setValue(ddlCountry, 39);
        FillState(39);

        // Color Theme DropDownList
        Data.ColorTheme theme = new Data.ColorTheme();
        Util.setValue(ddlColorTheme, theme.List(), "ThemeName", "ColorThemeID");

        if (appVar.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            chkIsViralMktOn.Enabled = false;
            tdSpan.ColSpan = 3;
            tdVmCol.Visible = false;
            tdVmVal.Visible = false;
        }

        if (appVar.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
        {
            tpSettingsBySuperAdmin.Visible = true;
            chkiPhone.Enabled = true;
            chkAndroid.Enabled = true;
            txtMinCategoryLimit.Enabled = true;
            txtMaxCategoryLimit.Enabled = true;
            txtActiveProductLimit.Enabled = true;
            txtResellerCode.Enabled = true;
            txtCategMaxLimit.Enabled = true;
            txtZipCodeLimit.Enabled = true;
            txtRssFeed.Enabled = true;
        }
        lblZipCodes.Text = "Delivery Zip Codes<span class='reqd'>*</span>";

        //Setting initially as blank image.
        imgSplashImage.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        imgResellerLogo.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");

        if (ResellerID <= 0)
            return;

        if (ResellerID == 127)//floralapp* Not mendatory zip code.
            lblZipCodes.Text = "Delivery Zip Codes";

        Data.Reseller resel = new Data.Reseller();
        resel.LoadID(ResellerID);

        Util.setValue(txtResellerName, resel.ResellerName.value);
        Util.setValue(txtResellerCode, resel.ResellerCode.value);
        Util.setValue(txtAddress1, resel.Address1.value);
        Util.setValue(txtAddress2, resel.Address2.value);
        Util.setValue(ddlCountry, resel.CountryID.value);

        // Fill State ddl
        if (resel.CountryID.value > 0)
            FillState(resel.CountryID.value);
        // Set 
        Util.setValue(ddlState, resel.State.value);

        Util.setValue(txtCity, resel.City.value);
        Util.setValue(txtZip, resel.Zip.value);
        Util.setValue(txtPhone1, resel.Phone1.value);
        Util.setValue(txtPhone2, resel.Phone2.value);
        Util.setValue(txtCell, resel.Cell.value);
        Util.setValue(txtFax, resel.Fax.value);
        Util.setValue(txtEmail1, resel.Email.value);
        Util.setValue(txtEmail2, resel.Email2.value);
        Util.setValue(txtLat, resel.Lat.value);
        Util.setValue(txtLng, resel.Lng.value);
        Util.setValue(txtWebsiteUrl, resel.WebsiteURL.value);


        #region Populating images ...

        String fileResellerName = String.Format("ResellerLogo_{0}{1}", resel.ResellerID.value.ToString(), ".png");
        String fileSplashName = String.Format("SplashImage_{0}{1}", resel.ResellerID.value.ToString(), ".png");

        imgSplashImage.ImageUrl = String.Format("../DynImages/Reseller/{0}", fileSplashName);
        imgResellerLogo.ImageUrl = String.Format("../DynImages/Reseller/{0}", fileResellerName);

        if (!File.Exists(Server.MapPath("../DynImages/Reseller/" + fileResellerName)))
        {
            byte[] shopByteData = resel.ResellerLogo.value;

            if (shopByteData != null)
            {
                FileStream fs = new FileStream(Server.MapPath("../DynImages/Reseller/" + fileResellerName), FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(shopByteData, 0, shopByteData.Length);
                fs.Close();
            }
            else
                imgResellerLogo.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        }

        if (!File.Exists(Server.MapPath("../DynImages/Reseller/" + fileSplashName)))
        {
            byte[] shopSplashByteData = resel.SplashImage.value;

            if (shopSplashByteData != null)
            {
                FileStream fs = new FileStream(Server.MapPath("../DynImages/Reseller/" + fileSplashName), FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(shopSplashByteData, 0, shopSplashByteData.Length);
                fs.Close();
            }
            else
                imgSplashImage.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        }

        #endregion Eng of Binding images..

        #region Binding controls for reseller settings ...
        Data.ResellerSetting resSett = new Data.ResellerSetting();

        resSett.LoadID(ResellerSettID);

        Util.setValue(ddlColorTheme, resSett.ColorThemeID.value);
        chkIsAllFeeInclusive.Checked = Convert.ToBoolean(resSett.IsAllFeeInclusive.value);
        cbxReceiveUserSignInEmail.Checked = Convert.ToBoolean(resSett.ReceiveUserSignInEmail.value);
        Util.setValue(txtDeliveryFee, resSett.DeliveryFee.value);
        Util.setValue(txtServiceFee, resSett.ServiceFee.value);
        Util.setValue(txtSaleTax, resSett.SalesTax.value);
        chkIsSaleTaxOn.Checked = Convert.ToBoolean(resSett.IsSalesTaxOn.value);
        chkIsViralMktOn.Checked = Convert.ToBoolean(resSett.IsViralMarketingOn.value);

        viralIncentiveMesageControls.Visible = Convert.ToBoolean(resSett.IsViralMarketingOn.value);
        viralIncentiveMesageLabel.Visible = Convert.ToBoolean(resSett.IsViralMarketingOn.value);
        divCustomEmailTextControls.Visible = Convert.ToBoolean(resSett.IsViralMarketingOn.value);
        divCustomEmailTextLabel.Visible = Convert.ToBoolean(resSett.IsViralMarketingOn.value);

        txtFloristToFloristMsg.Text = resSett.FloristToFloristMsg.value;
        txtDirectShipMsg.Text = resSett.DirectShipMsg.value;
        Util.setValue(txtSupportPhone, resSett.SupportPhone.value);
        Util.setValue(txtPickUpMsg, resSett.PickupMessage.value);
        Util.setValue(txtDeliveryZipCode, resSett.DeliveryZipCode.value);
        Util.setValue(txtZipCodeLimit, resSett.DeliveryZipCodeLimit.value);

        txtServiceFee.Enabled = chkIsAllFeeInclusive.Checked ? false : true;
        txtDeliveryFee.Enabled = chkIsAllFeeInclusive.Checked ? false : true;

        Util.setValue(txtMinCategoryLimit, resSett.MinCategoryEnabled.value);
        Util.setValue(txtMaxCategoryLimit, resSett.MaxCategoryEnabled.value);
        Util.setValue(txtActiveProductLimit, resSett.ActiveProductLimit.value);
        Util.setValue(txtCategMaxLimit, resSett.CategoryProductLimit.value);
        Util.setValue(etbProfileMessage, resSett.ProfileMsg.value);
        chkShowProfMsg.Checked = resSett.ShowProfileMsg.value;
        chkDeliveryFeeTax.Checked = resSett.IsDeliveryTaxOn.value;
        String supportedDeviceTypeList = resSett.SupportedDeviceTypeID.value;
        if (supportedDeviceTypeList.Contains(Convert.ToInt32(AppEnum.DeviceType.IPHONE).ToString()))
            Util.setValue(chkiPhone, true);

        if (supportedDeviceTypeList.Contains(Convert.ToInt32(AppEnum.DeviceType.ANDROID).ToString()))
            Util.setValue(chkAndroid, true);
        cbxShowViralIncentiveMessage.Checked = Convert.ToBoolean(resSett.IsViralIncentiveMessageOn.value);
        txtViralIncentiveMessage.Text = resSett.ViralIncentiveMessage.value;
        Util.setValue(ddlShopTimeZone, resSett.TimeZone.value);
        Util.setValue(txtRssFeed, resSett.RssFeed.value);
        Util.setValue(txtMediaUrl, resSett.MediaUrl.value);
        cbEnableCustomEmail.Checked = Convert.ToBoolean(resSett.IsCustomEmailTextOn.value);
        txtCustomEmail.Text = resSett.CustomEmailText.value;

        #endregion End of Binding

        #region Binding controls for reseller merchant settings ...

        Data.ResellerMerchantSetting resMerSett = new Data.ResellerMerchantSetting();

        resMerSett.LoadID(ResellerMerchantID);

        //Util.setValue(ddlPaymentType, resMerSett.PaymentType.value);
        ddlPaymentType_SelectedIndexChanged(null, null);
        Util.setValue(txtMercuryEmail, resMerSett.MercuryEmailAddress.value);
        Util.setValue(txtFTDEncryptionKey, resMerSett.FTDEncryptionKey.value);
        Util.setValue(txtMerchantLogID, resMerSett.MerchantLoginID.value);
        Util.setValue(txtMerchantTransKey, resMerSett.MerchantTransKey.value);
        Util.setValue(txtSignature, resMerSett.APISignature.value);
        Util.setValue(txtExpCheckoutLogID, resMerSett.ExpCkeckoutLoginID.value);
        //chkMerchantVarified.Checked = Convert.ToBoolean(resMerSett.IsMerchantVerified.value);
        //Util.setValue(lblMerchantVarified, resMerSett.DateMechantVerified.value);
        Util.setValue(txtTfTelefloraID, resMerSett.TFTelefloraID.value);
        Util.setValue(txtTfMerchantAccountID, resMerSett.TFMerchantAccountID.value);
        Util.setValue(txtTfTerminalID, resMerSett.TFTerminalID.value);
        Util.setValue(cbUseTeleflora, resMerSett.UseTFPayment.value);
        EnableDisablePaymentValidators();
        #endregion End of Binding

        #region Updating zip code limit settings ...

        long maxZipCodeLimit = resSett.DeliveryZipCodeLimit.value;

        txtDeliveryZipCode.MaxLength = Convert.ToInt32(maxZipCodeLimit);
        lblZipCodeDisplayMsg.Text = maxZipCodeLimit.ToString();

        #endregion
        if (appVar.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            if (!Convert.ToBoolean(resSett.IsViralMarketingOn.value))
            {
                viralIncentiveMesageLabelC.Visible = false;
                viralIncentiveMesageControlsC.Visible = false;
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "test", String.Format("var txt = document.getElementById('{0}'); var input = document.getElementById('txtViralIncentiveMessageCounter'); textCounter(txt,input,250);", txtViralIncentiveMessage.ClientID), true);
            }
        }
    }

    private void SaveData(byte[] ResellerFileByteData, byte[] SplashFileByteData)
    {
        String mode = (ResellerID > 0) ? "EDIT" : "NEW";

        Data.Reseller reseller = new Data.Reseller(ResellerID);

        reseller.ResellerName.value = Util.getValue(txtResellerName);
        reseller.ResellerCode.value = Util.getValue(txtResellerCode);

        if (ResellerFileByteData != null)
            reseller.ResellerLogo.value = ResellerFileByteData;

        if (SplashFileByteData != null)
            reseller.SplashImage.value = SplashFileByteData;

        reseller.Address1.value = Util.getValue(txtAddress1);
        reseller.Address2.value = Util.getValue(txtAddress2);
        reseller.City.value = Util.getValue(txtCity);
        reseller.State.value = Util.getValue(ddlState);
        reseller.CountryID.value = Int32.Parse(Util.getValue(ddlCountry));
        reseller.Zip.value = Util.getValue(txtZip);
        reseller.Phone1.value = Util.getValue(txtPhone1);
        reseller.Phone2.value = Util.getValue(txtPhone2);
        reseller.Cell.value = Util.getValue(txtCell);
        reseller.Fax.value = Util.getValue(txtFax);
        reseller.Email.value = Util.getValue(txtEmail1);
        reseller.Email2.value = Util.getValue(txtEmail2);
        reseller.Lat.value = txtLat.Text.Trim() == String.Empty ? 0 : Convert.ToDouble(Util.getValue(txtLat));
        reseller.Lng.value = txtLng.Text.Trim() == String.Empty ? 0 : Convert.ToDouble(Util.getValue(txtLng));
        reseller.WebsiteURL.value = Util.getValue(txtWebsiteUrl);

        if (ResellerID > 0)
        {
            reseller.DateModified.value = Util.GetServerDate();
            reseller.ModifiedBy.value = appVar.UserID;
        }
        else
        {
            reseller.DateAdded.value = Util.GetServerDate();
            reseller.AddedBy.value = appVar.UserID;
        }

        reseller.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        reseller.Save();

        Data.ServerUpdates su = new Data.ServerUpdates();
        if (mode == "NEW" && reseller.ResellerID.value > 0)
        {
            //Save in ServerUpdates            
            su.UpdateStatus(reseller.ResellerID.value, "");
        }

        #region Saving data into [ResellerSetting] table ...
        Data.ResellerSetting resellerSett = new Data.ResellerSetting(ResellerSettID);
        resellerSett.ResellerID.value = reseller.ResellerID.value;
        resellerSett.ColorThemeID.value = Int32.Parse(Util.getValue(ddlColorTheme));

        // HardCoded Active ProductLimit;
        resellerSett.ActiveProductLimit.value = String.IsNullOrEmpty(Util.getValue(txtActiveProductLimit)) ? 0 : Convert.ToInt32(Util.getValue(txtActiveProductLimit));
        resellerSett.CategoryProductLimit.value = String.IsNullOrEmpty(Util.getValue(txtCategMaxLimit)) ? 0 : Convert.ToInt32(Util.getValue(txtCategMaxLimit));
        resellerSett.TimeZone.value = Convert.ToInt32(Util.getValue(ddlShopTimeZone));
        resellerSett.RssFeed.value = txtRssFeed.Text;
        resellerSett.MediaUrl.value = txtMediaUrl.Text;
        resellerSett.IsAllFeeInclusive.value = Convert.ToBoolean(chkIsAllFeeInclusive.Checked);
        resellerSett.DeliveryFee.value = txtDeliveryFee.Text == String.Empty ? 0 : Convert.ToDecimal(Util.getValue(txtDeliveryFee));
        resellerSett.ServiceFee.value = txtServiceFee.Text == String.Empty ? 0 : Convert.ToDecimal(Util.getValue(txtServiceFee));
        resellerSett.SalesTax.value = txtSaleTax.Text == String.Empty ? 0 : Convert.ToDouble(Util.getValue(txtSaleTax));
        resellerSett.IsSalesTaxOn.value = Convert.ToBoolean(chkIsSaleTaxOn.Checked);
        resellerSett.IsViralMarketingOn.value = Convert.ToBoolean(chkIsViralMktOn.Checked);
        resellerSett.IsDeliveryTaxOn.value = Convert.ToBoolean(chkDeliveryFeeTax.Checked);
        resellerSett.FloristToFloristMsg.value = txtFloristToFloristMsg.Text;
        resellerSett.DirectShipMsg.value = txtDirectShipMsg.Text;
        resellerSett.DeliveryZipCode.value = txtDeliveryZipCode.Text;
        resellerSett.DeliveryZipCodeLimit.value = txtZipCodeLimit.Text.Trim() == "0000" ? 1000 : Util.GetDataInt64(txtZipCodeLimit.Text);
        resellerSett.SupportPhone.value = Util.getValue(txtSupportPhone);
        resellerSett.ShowProfileMsg.value = chkShowProfMsg.Checked;
        resellerSett.ProfileMsg.value = Util.getValue(etbProfileMessage);

        String supportedDeviceType = String.Empty;
        if (chkiPhone.Checked)
            supportedDeviceType = String.IsNullOrEmpty(supportedDeviceType) ? Convert.ToInt32(AppEnum.DeviceType.IPHONE).ToString() : supportedDeviceType + "," + Convert.ToInt32(AppEnum.DeviceType.IPHONE).ToString();

        if (chkAndroid.Checked)
            supportedDeviceType = String.IsNullOrEmpty(supportedDeviceType) ? Convert.ToInt32(AppEnum.DeviceType.ANDROID).ToString() : supportedDeviceType + "," + Convert.ToInt32(AppEnum.DeviceType.ANDROID).ToString();

        resellerSett.SupportedDeviceTypeID.value = supportedDeviceType;
        resellerSett.MaxCategoryEnabled.value = String.IsNullOrEmpty(Util.getValue(txtMaxCategoryLimit)) ? 0 : Util.GetDataInt32(Util.getValue(txtMaxCategoryLimit));
        resellerSett.MinCategoryEnabled.value = String.IsNullOrEmpty(Util.getValue(txtMinCategoryLimit)) ? 0 : Util.GetDataInt32(Util.getValue(txtMinCategoryLimit));
        resellerSett.PickupMessage.value = Util.getValue(txtPickUpMsg);
        resellerSett.BlockOutDatesMessage.value = "";// Util.getValue(txtBlockDtMsg);

        if (ResellerSettID > 0)
        {
            resellerSett.DateModified.value = Util.GetServerDate();
            resellerSett.ModifiedBy.value = appVar.UserID;
        }
        else
        {
            resellerSett.DateAdded.value = Util.GetServerDate();
            resellerSett.AddedBy.value = appVar.UserID;
        }

        resellerSett.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        resellerSett.ReceiveUserSignInEmail.value = Convert.ToInt32(cbxReceiveUserSignInEmail.Checked);
        resellerSett.IsViralIncentiveMessageOn.value = Convert.ToInt32(cbxShowViralIncentiveMessage.Checked);
        resellerSett.ViralIncentiveMessage.value = Util.getValue(txtViralIncentiveMessage);
        resellerSett.IsCustomEmailTextOn.value = Convert.ToInt32(cbEnableCustomEmail.Checked);
        resellerSett.CustomEmailText.value = Util.getValue(txtCustomEmail);

        resellerSett.Save();

        //Save in ServerUpdates 
        su.UpdateStatus(reseller.ResellerID.value, "resellersettings");
        #endregion

        #region Saving data into [ResellerMerchantSetting] table ...

        Data.ResellerMerchantSetting resMerSett = new Data.ResellerMerchantSetting(ResellerMerchantID);

        resMerSett.ResellerID.value = reseller.ResellerID.value;
        resMerSett.PaymentType.value = 1;//int.Parse(Util.getValue(ddlPaymentType));
        resMerSett.MercuryEmailAddress.value = Util.getValue(txtMercuryEmail);
        resMerSett.FTDEncryptionKey.value = Util.getValue(txtFTDEncryptionKey);
        resMerSett.MerchantLoginID.value = Util.getValue(txtMerchantLogID);
        resMerSett.MerchantTransKey.value = Util.getValue(txtMerchantTransKey);
        resMerSett.APISignature.value = Util.getValue(txtSignature);
        resMerSett.ExpCkeckoutLoginID.value = Util.getValue(txtExpCheckoutLogID);
        resMerSett.IsMerchantVerified.value = true;// Convert.ToBoolean(chkMerchantVarified.Checked);
        resMerSett.DateMechantVerified.value = Util.GetServerDate();// Convert.ToDateTime(Util.getValue(lblMerchantVarified));
        resMerSett.TFTelefloraID.value = Util.getValue(txtTfTelefloraID);
        resMerSett.TFMerchantAccountID.value = Util.getValue(txtTfMerchantAccountID);
        resMerSett.TFTerminalID.value = Util.getValue(txtTfTerminalID);
        resMerSett.UseTFPayment.value = Util.getValue(cbUseTeleflora);

        if (ResellerMerchantID > 0)
        {
            resMerSett.DateModified.value = Util.GetServerDate();
            resMerSett.ModifiedBy.value = appVar.UserID;
        }
        else
        {
            resMerSett.DateAdded.value = Util.GetServerDate();
            resMerSett.AddedBy.value = appVar.UserID;
        }

        resMerSett.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        resMerSett.Save();
        #endregion

        #region Populate Default Category for this Reseller
        if (mode == "NEW")
        {
            Data.Category cat = new Data.Category();
            if (!cat.PopulateDefaultCategory(reseller.ResellerID.value))
            {
                Data.ErrorLog el = new Data.ErrorLog();
                el.ModuleInfo.value = "AddReseller : SaveData()";
                el.Message.value = "Error in populating default category for this shop. ResellerID = " + reseller.ResellerID.value;
                el.SmallMessage.value = "Error in populating default category for this shop.";
                el.ApplicationType.value = Convert.ToInt32(AppEnum.ApplicationType.WEBSITE);
                el.Save();

                ShowError(lblErrMsg, "Error in populating default category for this shop.");
                return;
            }
        }
        #endregion Save Data Into Category

        #region Binding Images...

        String filePath = Server.MapPath(@"~/DynImages\Reseller\");
        String fileResellerName = String.Format("ResellerLogo_{0}{1}", reseller.ResellerID.value.ToString(), ".png");
        String fileSplashName = String.Format("SplashImage_{0}{1}", reseller.ResellerID.value.ToString(), ".png");

        if (ResellerFileByteData != null)
        {
            if (File.Exists(filePath + fileResellerName))
                File.Delete(filePath + fileResellerName);

            FileStream fs = new FileStream(filePath + fileResellerName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(ResellerFileByteData, 0, ResellerFileByteData.Length);
            fs.Close();
        }

        filePath += fileSplashName;

        if (SplashFileByteData != null)
        {
            if (File.Exists(filePath + fileSplashName))
                File.Delete(filePath + fileSplashName);

            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(SplashFileByteData, 0, SplashFileByteData.Length);
            fs.Close();
        }

        #endregion Eng of Binding images..

        #region Binding category images ...

        Data.Category cat1 = new Data.Category();
        DataSet dsCategory = cat1.GetCategoryListByReseller(reseller.ResellerID.value);

        foreach (DataRow drCat in dsCategory.Tables[0].Rows)
        {
            Byte[] fileByteData = (byte[])drCat["ImageBinary"];
            String imageExt = Convert.ToString(drCat["ImageExt"]);
            Int32 catID = Convert.ToInt32(drCat["CategoryID"]);

            String catfilePath = Server.MapPath(@"~/DynImages\Category\");
            String fileName = String.Format("CategoryImage_{0}{1}", catID, imageExt);
            catfilePath += fileName;

            if (File.Exists(catfilePath))
                File.Delete(catfilePath);

            if (fileByteData != null)
            {
                FileStream fs = new FileStream(catfilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteData, 0, fileByteData.Length);
                fs.Close();
            }
        }

        #endregion End of binding image

        #region Save Import Products Category if needed

        if (txtRssFeed.Enabled && txtRssFeed.Text.Length > 0)
        {
            Data.Category cat = new Data.Category();
            DataSet categList = cat.GetCategoryListByReseller(reseller.ResellerID.value);
            DataRow[] dr = categList.Tables[0].Select("CategoryName = 'Import Products'");
            if (dr.Length > 0)
                return; // the Import Products already exists

            cat.ResellerID.value = reseller.ResellerID.value;
            cat.DefaultCategoryID.value = 0; // 0 - User Defined Category
            cat.CategoryName.value = "Import Products";
            //cat.ImageBinary.value = CategoryFileByteData;
            cat.ImageExt.value = ".png";
            cat.Sequence.value = 0;
            cat.IsEnabled.value = false;
            cat.IsSystemDefined.value = false;
            cat.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
            cat.IsFeatured.value = false;
            cat.DateAdded.value = Util.GetServerDate();
            cat.AddedBy.value = appVar.UserID;
            cat.DateModified.value = Util.GetServerDate();
            cat.ModifiedBy.value = appVar.UserID;

            cat.Save();

            String imageExt = cat.ImageExt.value;
            String noImagePath = Server.MapPath(@"~/Images\blank_img.png");
            filePath = Server.MapPath(@"~/DynImages\Category\");
            String fileName = String.Format("CategoryImage_{0}{1}", cat.CategoryID.value.ToString(), imageExt);
            filePath += fileName;

            if (File.Exists(filePath))
                File.Delete(filePath);

            File.Copy(noImagePath, filePath);
        }

        #endregion
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Data.Reseller res = new Data.Reseller();
        DataSet dsReseller = res.List();
        DataRow[] drResellerCode = dsReseller.Tables[0].Select("ResellerCode='" + txtResellerCode.Text + "' and ResellerID<>" + ResellerID + "");
        if (ddlState.SelectedIndex > 0)
        {
            if (txtPhone1.Text.Equals("___-___-____ "))
            {
                ShowError(lblErrMsg, "Enter phone.");
                tcContainer.ActiveTab = tcContainer.Tabs[0];
                txtPhone1.BackColor = Color.LightPink;
                return;
            }

            if (txtSupportPhone.Text.Equals("___-___-____ "))
            {
                ShowError(lblErrMsg, "Enter support phone.");
                tcContainer.ActiveTab = tcContainer.Tabs[1];
                txtSupportPhone.BackColor = Color.LightPink;
                return;
            }

            if (ResellerID != 127 && String.IsNullOrEmpty(txtDeliveryZipCode.Text.Trim()))
            {
                ShowError(lblErrMsg, "Enter delivery zip codes.");
                tcContainer.ActiveTab = tcContainer.Tabs[1];
                txtDeliveryZipCode.BackColor = Color.LightPink;
                return;
            }

            if (txtPhone2.Text.Equals("___-___-____ "))
            {
                ShowError(lblErrMsg, "Enter toll free number.");
                tcContainer.ActiveTab = tcContainer.Tabs[0];
                txtPhone2.BackColor = Color.LightPink;
                return;
            }
            if (!(drResellerCode.Length > 0))
            {
                txtSupportPhone.BackColor = Color.Transparent;
                FileUpload FuReseller = this.fuResellerLogo;
                FileUpload FuSplash = this.fuSplashImage;
                byte[] ResellerFileByteData = null;
                byte[] SplashFileByteData = null;
                String strImageResellerLogoType = FuReseller.PostedFile.ContentType + Path.GetExtension(FuReseller.PostedFile.FileName).ToLower();
                String strImageSplashType = FuSplash.PostedFile.ContentType + Path.GetExtension(FuSplash.PostedFile.FileName).ToLower();
                int activeTab = tcContainer.ActiveTab.TabIndex;

                Boolean isValidResellerLogoType = isValidFileFormat(strImageResellerLogoType.ToLower());
                Boolean isValidSpalashType = isValidFileFormat(strImageSplashType.ToLower());

                try
                {
                    #region Validation Resellers Limit
                    if (!CanAddReseller() && ResellerID <= 0)
                    {
                        tcContainer.ActiveTab = tcContainer.Tabs[0];
                        ShowError(lblErrMsg, "Your shop creation limit is already reached to it's maximum limit. Please contact Tracer Pro for increasing the shop creation limit.");
                        return;
                    }
                    #endregion End Validation Resellers Limit

                    #region Image Validation
                    // Check if file upload ctrl has any files selected or not.
                    if ((FuReseller.HasFile && fuSplashImage.HasFile) || ResellerID > 0)
                    {

                        switch ((isValidResellerLogoType && isValidSpalashType) || ResellerID > 0)
                        {
                            case false:
                                AddErrorMessage("Upload image only *.png format.");
                                tcContainer.ActiveTab = tcContainer.Tabs[0];
                                break;
                            case true:
                                if (FuReseller.HasFile && !isValidResellerLogoType)
                                {
                                    AddErrorMessage("Upload image only *.png format.");
                                    tcContainer.ActiveTab = tcContainer.Tabs[0];
                                }
                                else if (FuSplash.HasFile && !isValidSpalashType)
                                {
                                    AddErrorMessage("Upload image only *.png format.");
                                    tcContainer.ActiveTab = tcContainer.Tabs[0];
                                }
                                break;
                        }

                        // Check error : if any server side validation error exist.
                        if (_errorMsg != String.Empty)
                        {
                            ShowError(lblErrMsg, _errorMsg);
                            return;
                        }
                        else
                        {
                            if (FuReseller.PostedFile.ContentLength > 1)
                            {
                                // If the selected file extension is valid then 
                                // convert the selected file into bytes for saving in database 
                                Stream imgResellerStream = FuReseller.PostedFile.InputStream;
                                int contentResellerLength = FuReseller.PostedFile.ContentLength;

                                using (BinaryReader readerReseller = new BinaryReader(imgResellerStream))
                                {
                                    ResellerFileByteData = readerReseller.ReadBytes(contentResellerLength);
                                    readerReseller.Close();
                                }

                                MemoryStream memResellerStream = new MemoryStream(ResellerFileByteData);

                                System.Drawing.Image imgResellerObject = System.Drawing.Image.FromStream(memResellerStream);

                                Int32 imgResellerHeight = imgResellerObject.Height;
                                Int32 imgResellerWidth = imgResellerObject.Width;

                                if (imgResellerHeight > 100 || imgResellerWidth > 320)
                                {
                                    tcContainer.ActiveTab = tcContainer.Tabs[0];
                                    ShowError(lblErrMsg, "Shop logo size should not be exceed than the defined size.");
                                    return;
                                }
                            }

                            if (FuSplash.PostedFile.ContentLength > 1)
                            {
                                // If the selected file extension is valid then 
                                // convert the selected file into bytes for saving in database 
                                Stream imgSplashStream = FuSplash.PostedFile.InputStream;
                                int contentSplashLength = FuSplash.PostedFile.ContentLength;

                                using (BinaryReader readerSplash = new BinaryReader(imgSplashStream))
                                {
                                    SplashFileByteData = readerSplash.ReadBytes(contentSplashLength);
                                    readerSplash.Close();
                                }

                                // Read the file bytes in MemoryStream for checking it's dimention as desired. 
                                MemoryStream memSplashStream = new MemoryStream(SplashFileByteData);

                                System.Drawing.Image imgSplashObject = System.Drawing.Image.FromStream(memSplashStream);

                                Int32 imgSplashHeight = imgSplashObject.Height;
                                Int32 imgSplashWidth = imgSplashObject.Width;

                                if (imgSplashHeight > 480 || imgSplashWidth > 320)
                                {
                                    tcContainer.ActiveTab = tcContainer.Tabs[0];
                                    ShowError(lblErrMsg, "Shop splash image size should not be exceed than the defined size.");
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (imgResellerLogo.ImageUrl == String.Empty || imgSplashImage.ImageUrl == String.Empty
                            || imgResellerLogo.ImageUrl.IndexOf("blank_img.png") > 0 || imgSplashImage.ImageUrl.IndexOf("blank_img.png") > 0)
                        {
                            ShowError(lblErrMsg, "Please upload an image for 'Shop Logo' and 'Splash Image'.");
                            tcContainer.ActiveTab = tcContainer.Tabs[0];
                            return;
                        }
                    }

                    #endregion End of Image Validation

                    // Save data to Database
                    SaveData(ResellerFileByteData, SplashFileByteData);
                }
                catch (Exception ex)
                {
                    ShowError(lblErrMsg, ex.Message);
                    return;
                }
            }
            else
            {
                ShowError(lblErrMsg, "Florist code is already in use.");
                tcContainer.ActiveTab = tcContainer.Tabs[0];
                return;
            }
        }
        else
        {
            if (ddlCountry.SelectedItem.Value == "39")
                ShowError(lblErrMsg, "Select providence.");
            else
                ShowError(lblErrMsg, "Select state.");

            tcContainer.ActiveTab = tcContainer.Tabs[0];
            return;
        }

        Response.Redirect("Reseller.aspx");
    }

    private Boolean isValidFileFormat(String fileFormat)
    {
        Boolean isValidFormat = true;

        switch (fileFormat)
        {
            case "image/png.png":
            case "image/x-png.png":
                break;
            default:
                isValidFormat = false;
                break;
        }
        return isValidFormat;
    }

    private Boolean CanAddReseller()
    {
        Boolean retval = true;
        Data.SuperAdminSetting sas = new Data.SuperAdminSetting(1); // 1 for Beneva
        Int32 limit = Convert.ToInt32(Crypto.DecryptQueryString(sas.ResellersLimit.value));
        Int32 activeResellerCount = 0;

        Data.Reseller res = new Data.Reseller();
        DataSet dsReseller = res.List();
        if (Util.IsValidDataSet(dsReseller))
            activeResellerCount = dsReseller.Tables[0].Rows.Count;

        if (activeResellerCount >= limit)
            retval = false;

        return retval;
    }

    private void ShowAppDistributionLinks()
    {
        if (ResellerID > 0)
        {
            tpAppDistribution.Visible = true;

            String appUrl = ConfigurationManager.AppSettings["AppUrl"];
            String androidLink = appUrl + "AppDistribution/Download.aspx?eqs=" + Crypto.EncryptQueryString("AppType=ANDROID&ResellerID=" + ResellerID + "&UserID=0").ToLower();
            String iPhoneLink = appUrl + "AppDistribution/Download.aspx?eqs=" + Crypto.EncryptQueryString("AppType=IPHONE&ResellerID=" + ResellerID + "&UserID=0").ToLower();

            lnkDownloadAndroidApp.Text = androidLink;
            lnkDownloadiPhoneApp.Text = iPhoneLink;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Reseller.aspx");
    }

    private void FillState(int countryID)
    {
        //Clears all list from State DropDownList
        ddlState.Items.Clear();
        // State DropDownList
        Data.State state = new Data.State();
        Util.setValue(ddlState, state.List(countryID), "StateName", "StateID");
        ddlState.Items.Insert(0, "Select One");

        if (countryID == 39)//CANADA
        {
            Util.setValue(lblState, "Providence");
            Util.setValue(lblZip, "Post Code");
            rfvZip.ErrorMessage = "Enter post code.";
        }
        else
        {

            Util.setValue(lblState, "State");
            Util.setValue(lblZip, "Zip");
            rfvZip.ErrorMessage = "Enter zip code.";
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillState(int.Parse(ddlCountry.SelectedValue));
    }

    protected void ddlPaymentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //pnlMercury.Visible = ddlPaymentType.SelectedValue == "3";
        //pnlPayPall.Visible = ddlPaymentType.SelectedValue == "1";
        pnlPayPall.Visible = true;
    }

    protected void cusCustom_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        if (e.Value.Length == 8 || e.Value.Length == 0)
            e.IsValid = true;
        else
            e.IsValid = false;
    }

    /*
    protected void chkIsAllFeeInclusive_CheckedChanged(object sender, EventArgs e)
    {
        if (chkIsAllFeeInclusive.Checked)
        {
            Util.setValue(txtServiceFee, 0);
            Util.setValue(txtDeliveryFee, 0);
        }

        txtServiceFee.Enabled = chkIsAllFeeInclusive.Checked ? false : true;
        txtDeliveryFee.Enabled = chkIsAllFeeInclusive.Checked ? false : true;
    }
    */

    protected void chkIsViralMktOn_CheckedChanged(object sender, EventArgs e)
    {
        viralIncentiveMesageControls.Visible = chkIsViralMktOn.Checked;
        viralIncentiveMesageLabel.Visible = chkIsViralMktOn.Checked;
        divCustomEmailTextControls.Visible = chkIsViralMktOn.Checked;
        divCustomEmailTextLabel.Visible = chkIsViralMktOn.Checked;
    }

    protected void cbUseTeleflora_CheckChanged(object sender, EventArgs e)
    {
        EnableDisablePaymentValidators();
    }

    private void EnableDisablePaymentValidators()
    {   
        // Start -- PAYPAL --
        spnMerchantLogIDReq.Visible = !cbUseTeleflora.Checked;
        rfvMerchantLoginID.Visible = !cbUseTeleflora.Checked;
        
        spnExpCheckoutLogIDReq.Visible = !cbUseTeleflora.Checked;
        rfvExpCheckoutLogID.Visible = !cbUseTeleflora.Checked;
        
        spnMerchantTransKeyReq.Visible = !cbUseTeleflora.Checked;
        rfvMerchantTransKey.Visible = !cbUseTeleflora.Checked;
        
        spnSignatureReq.Visible = !cbUseTeleflora.Checked;
        rfvSignature.Visible = !cbUseTeleflora.Checked;
        // End -- PAYPAL

        // Start -- Teleflora
        spnTfTelefloraIDReq.Visible = cbUseTeleflora.Checked;
        rfvTfTelefloraID.Visible = cbUseTeleflora.Checked;
        // End -- Teleflora
    }
}
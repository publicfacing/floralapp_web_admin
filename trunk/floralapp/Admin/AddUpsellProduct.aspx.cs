﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Collections.Specialized;
using System.IO;

public partial class Admin_AddUpsellProduct : AppPage
{
    private Int64 UpsellProductID
    {
        get;
        set;
    }

    private Int64 UpsellProductCategoryID
    {
        get;
        set;
    }

    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                UpsellProductID = Convert.ToInt64(nvc["UpsellProductID"]);
                UpsellProductCategoryID = Convert.ToInt64(nvc["UpsellProductCategoryID"]);
            }

            Util.setValue(lblPageTitle, (UpsellProductID > 0) ? "Edit Upsell Product" : "Add Upsell Product");
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        if (!CanAddNewUpsellProduct() && UpsellProductID <= 0)
            btnSave.Visible = false;

        // product DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (ddlReseller.SelectedIndex > 0)
            FillCategory(Convert.ToInt32(Util.getValue(ddlReseller)));

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            trShop.Visible = false;
            //ddlReseller.Enabled = false;
        }

        //Setting initially as blank image.
        imgProductImage.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");

        if (UpsellProductID <= 0)
            return;

        Data.UpsellProduct up = new Data.UpsellProduct();
        up.LoadID(UpsellProductID);

        if (!(ddlReseller.SelectedIndex > 0))
        {
            Util.setValue(ddlReseller, up.ResellerID.value);
            FillCategory(Int32.Parse(Util.getValue(ddlReseller)));
        }

        Util.setValue(txtProductName, up.ProductName.value);
        Util.setValue(txtDescription, up.Description.value);
        
        #region Populating images ...

        String imgExtn = up.ProductImageExt.value;
        String imgProductPath = String.Format("ProductImage_{0}{1}", up.UpsellProductID.value.ToString(), imgExtn);

        if (!File.Exists(Server.MapPath("../DynImages/UpsellProduct_New/" + imgProductPath)))
            imgProductImage.ImageUrl = String.Format("~/UpsellProduct_New/{0}", "blank_img.png");
        else
            imgProductImage.ImageUrl = String.Format("../DynImages/UpsellProduct_New/{0}", imgProductPath);

        #endregion End of binding image

        Util.setValue(txtProductPrice, up.ProductPrice.value);
        Util.setValue(chkIsPublished, Convert.ToBoolean(up.IsPublished.value));
        Util.setValue(txtSkuNumber, up.SKUNo.value);

        Data.UpsellProductCategory uPCat = new Data.UpsellProductCategory();
        uPCat.LoadID(UpsellProductCategoryID);
        Util.setValue(ddlCat, Convert.ToInt32(uPCat.UpsellCategoryID.value));
    }

    private void SaveData(byte[] ProductFileByteData, byte[] ThumbFileByteData, string extnProduct, string extnThumb)
    {
        AppVars appVar = new AppVars();
        Data.UpsellProduct up = new Data.UpsellProduct(UpsellProductID);
        up.ResellerID.value = Int32.Parse(Util.getValue(ddlReseller));
        up.ProductName.value = Util.getValue(txtProductName);
        up.Description.value = Util.getValue(txtDescription);
        up.ProductPrice.value = txtProductPrice.Text == String.Empty ? 0 : Convert.ToDecimal(Util.getValue(txtProductPrice));
        up.IsPublished.value = chkIsPublished.Checked;
        up.DateAdded.value = Util.GetServerDate();
        up.AddedBy.value = appVar.UserID;
        up.DateModified.value = Util.GetServerDate();
        up.ModifiedBy.value = appVar.UserID;
        up.DateDeleted.value = Util.GetServerDate();
        up.DeletedBy.value = appVar.UserID;
        up.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        up.SKUNo.value = Util.getValue(txtSkuNumber);
        if (ProductFileByteData != null && ThumbFileByteData != null)
        {
            up.ProductImage.value = ProductFileByteData;
            up.ProductImageExt.value = extnProduct;
            up.ProductThumb.value = ThumbFileByteData;
            up.ProductThumbExt.value = extnThumb;
        }

        up.Save();

        #region Updating Images...


        String filePath = Server.MapPath(@"~/DynImages\UpsellProduct_New\");
        String fileProductImagePath = String.Format("ProductImage_{0}{1}", up.UpsellProductID.value.ToString(), up.ProductImageExt.value);
        String fileProductThumbPath = String.Format("ProductThumb_{0}{1}", up.UpsellProductID.value.ToString(), up.ProductThumbExt.value);

        if (ProductFileByteData != null)
        {
            if (File.Exists(filePath + fileProductImagePath))
                File.Delete(filePath + fileProductImagePath);

            FileStream fs = new FileStream(filePath + fileProductImagePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(ProductFileByteData, 0, ProductFileByteData.Length);
            fs.Close();
        }

        filePath += fileProductThumbPath;

        if (ThumbFileByteData != null)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(ThumbFileByteData, 0, ThumbFileByteData.Length);
            fs.Close();
        }

        #endregion Eng of Binding images..

        // Save or modify data into [ProductCategory] table.
        Data.UpsellProductCategory uPCat = new Data.UpsellProductCategory();
        uPCat.LoadID(UpsellProductCategoryID);

        uPCat.UpsellProductID.value = up.UpsellProductID.value;
        uPCat.UpsellCategoryID.value = Int32.Parse(Util.getValue(ddlCat));
        uPCat.DateAdded.value = Util.GetServerDate();
        uPCat.AddedBy.value = appVar.UserID;
        uPCat.DateModified.value = Util.GetServerDate();
        uPCat.ModifiedBy.value = appVar.UserID;
        uPCat.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        uPCat.Save();
        
        #region Save in ServerUpdates
        Data.ServerUpdates su = new Data.ServerUpdates();
        su.UpdateStatus(up.ResellerID.value, "upsellproduct");
        #endregion
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        byte[] ProductFileByteData = null;
        byte[] fileThumbByteData = null;
        byte[] fileImageResizedByteData = null;
        String extn = Path.GetExtension(fuProductImage.PostedFile.FileName).ToLower();

        try
        {
            #region Validation for Product Limit
            if (!CanAddNewUpsellProduct() && UpsellProductID <= 0)
            {
                ShowError(lblErrMsg, "Your cann't add new product as maximum limit is reached. Please contact floralapp team for increasing the max limit for adding new up.");
                return;
            }
            #endregion End of validation for Product Limit

            #region Image Validation
            // Check if file upload ctrl has any files selected or not.
            if (fuProductImage.HasFile)
            {                
                String strProductImgType = fuProductImage.PostedFile.ContentType + extn;

                switch (strProductImgType)
                {
                    case "image/jpg.jpg":
                    case "image/jpeg.jpg":
                    case "image/pjpeg.jpg":

                    case "image/jpg.jpeg":
                    case "image/jpeg.jpeg":
                    case "image/pjpeg.jpeg":

                    case "image/png.png":
                    case "image/x-png.png":

                    case "image/gif.gif":
                    case "image/bmp.bmp":
                    case "image/ico.ico":
                        break;
                    default:
                        AddErrorMessage("Upload image only *.png, *.jpg, *.jpeg, *.gif, *.bmp, *.ico format.");
                        break;
                }

                // Check error : if any server side validation error exist.
                if (_errorMsg != String.Empty)
                {
                    ShowError(lblErrMsg, _errorMsg);
                    return;
                }
                else
                {

                    // If the selected file extension is valid then 
                    // convert the selected file into bytes for saving in database 
                    Stream imgProductStream = fuProductImage.PostedFile.InputStream;
                    int contentProductLength = fuProductImage.PostedFile.ContentLength;
                    using (BinaryReader readerProduct = new BinaryReader(imgProductStream))
                    {
                        ProductFileByteData = readerProduct.ReadBytes(contentProductLength);
                        readerProduct.Close();
                    }

                    // Read the file bytes in MemoryStream for checking it's dimention as desired. 
                    MemoryStream memProductStream = new MemoryStream(ProductFileByteData);
                    System.Drawing.Image imgProductObject = System.Drawing.Image.FromStream(memProductStream);

                    // Prevent using images internal thumbnail
                    imgProductObject.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                    imgProductObject.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

                    Int32 imgProductHeight = imgProductObject.Height;
                    Int32 imgProductWidth = imgProductObject.Width;

                    //if (imgProductHeight > 200 || imgProductWidth > 200)
                    //{
                    //    ShowError(lblErrMsg, "Uploaded image size should not be greater than the defined size.");
                    //    return;
                    //}

                    #region Resize Image
                    const int MAX_WIDTH = 200;
                    const int MAX_HEIGHT = 200;

                    int NewHeight = imgProductHeight * MAX_WIDTH / imgProductWidth;
                    int NewWidth = MAX_WIDTH;

                    if (NewHeight > MAX_HEIGHT)
                    {
                        // Resize with height instead
                        NewWidth = imgProductWidth * MAX_HEIGHT / imgProductHeight;
                        NewHeight = MAX_HEIGHT;
                    }

                    System.Drawing.Image imgResized = imgProductObject.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

                    MemoryStream mstreamResizedImage = new MemoryStream();

                    imgResized.Save(mstreamResizedImage, System.Drawing.Imaging.ImageFormat.Png);
                    fileImageResizedByteData = new Byte[mstreamResizedImage.Length];
                    mstreamResizedImage.Position = 0;
                    mstreamResizedImage.Read(fileImageResizedByteData, 0, (int)mstreamResizedImage.Length);
                    #endregion

                    #region Create Thumb image...

                    const int MAX_THUMB_WIDTH = 50;
                    const int MAX_THUMB_HEIGHT = 50;

                    int newThumbHeight = imgProductHeight * MAX_THUMB_WIDTH / imgProductWidth;
                    int newThumbWidth = MAX_THUMB_HEIGHT;

                    if (newThumbHeight > MAX_THUMB_HEIGHT)
                    {
                        // Resize with height instead
                        newThumbWidth = imgProductWidth * MAX_THUMB_HEIGHT / imgProductHeight;
                        newThumbHeight = MAX_THUMB_HEIGHT;
                    }

                    System.Drawing.Image imgThumbs = imgProductObject.GetThumbnailImage(newThumbWidth, newThumbHeight, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);
                    MemoryStream mstreamThumb = new MemoryStream();

                    imgThumbs.Save(mstreamThumb, System.Drawing.Imaging.ImageFormat.Png);
                    fileThumbByteData = new Byte[mstreamThumb.Length];
                    mstreamThumb.Position = 0;
                    mstreamThumb.Read(fileThumbByteData, 0, (int)mstreamThumb.Length);
                    #endregion End of byte
                }
            }
            else
            {
                //if (
                //    (imgProductImage.ImageUrl.IndexOf("blank_img.png") > 0)
                //    || (imgProductImage.ImageUrl == String.Empty)
                //   )
                //{
                //    ShowError(lblErrMsg, "Upload an image for product.");
                //    return;
                //}
            }
            #endregion End of Image Validation

            SaveData(fileImageResizedByteData, fileThumbByteData, extn, ".png");
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect("UpsellProduct.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("UpsellProduct.aspx");
    }

    ///  <summary>
    /// Required, but not used
    /// </summary>
    /// <returns>true</returns>
    public bool ThumbnailCallback()
    {
        return true;
    }

    protected void ddlReseller_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCategory(Int32.Parse(Util.getValue(ddlReseller)));
    }

    private void FillCategory(Int32 resellerID)
    {
        Data.UpsellCategory UpsellCat = new Data.UpsellCategory();
        DataSet dsCat = UpsellCat.GetUpsellCategoryListByReseller(resellerID);

        ddlCat.Items.Clear();
        if (dsCat.Tables[0].Rows.Count > 0)
        {
            Util.setValue(ddlCat, dsCat, "CategoryName", "UpsellCategoryID");
        }
        if (ddlCat.Items.Count == 0)
            ddlCat.Items.Add(new ListItem("Categorie(s) not available for this shop.", "0"));
    }

    private Boolean CanAddNewUpsellProduct()
    {
        Int32 resellerID = Int32.Parse(Util.getValue(ddlReseller));
        Data.Reseller rs = new Data.Reseller();
        return rs.CanAddNewUpsellProduct(resellerID);
    }
}
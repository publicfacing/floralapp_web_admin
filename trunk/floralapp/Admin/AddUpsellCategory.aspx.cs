﻿using System;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Collections.Specialized;
using System.IO;

public partial class Admin_AddUpsellCategory : AppPage
{
    private Int32 UpsellCategoryID
    {
        get;
        set;
    }

    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                UpsellCategoryID = Convert.ToInt32(nvc["UpsellCategoryID"]);
            }

            Util.setValue(lblPageTitle, (UpsellCategoryID > 0) ? "Edit Upsell Category" : "Add Upsell Category");
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            //ddlReseller.Enabled = false;
            trShop.Visible = false;
        }

        //Setting initially as blank image;
        imgUc.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");

        if (UpsellCategoryID <= 0)
            return;

        Data.UpsellCategory uc = new Data.UpsellCategory();
        uc.LoadID(UpsellCategoryID);

        if (!(ddlReseller.SelectedIndex > 0))
            Util.setValue(ddlReseller, uc.ResellerID.value);

        Util.setValue(txtCategoryName, uc.CategoryName.value);

        #region Populating images ...

        String imageExt = uc.ImageExt.value;
        String fileName = String.Format("CategoryImage_{0}{1}", uc.UpsellCategoryID.value.ToString(), imageExt);

        if (!File.Exists(Server.MapPath("../DynImages/UpsellCategory/" + fileName)))
            imgUc.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        else
            imgUc.ImageUrl = String.Format("../DynImages/UpsellCategory/{0}", fileName);

        #endregion End of binding image
    }

    private void SaveData(byte[] CategoryFileByteData, String fileExtn)
    {
        Data.UpsellCategory uc = new Data.UpsellCategory(UpsellCategoryID);
        uc.ResellerID.value = Int32.Parse(Util.getValue(ddlReseller));
        uc.CategoryName.value = Util.getValue(txtCategoryName);

        if (CategoryFileByteData != null)
        {
            uc.ImageBinary.value = CategoryFileByteData;
            uc.ImageExt.value = fileExtn;
        }

        uc.Sequence.value = 0;
        uc.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        uc.Save();

        #region Updating images ...

        Byte[] fileByteData = uc.ImageBinary.value;
        String imageExt = uc.ImageExt.value;
        String filePath = Server.MapPath(@"~/DynImages\UpsellCategory\");
        String fileName = String.Format("CategoryImage_{0}{1}", uc.UpsellCategoryID.value.ToString(), imageExt);
        filePath += fileName;

        if (File.Exists(filePath))
            File.Delete(filePath);

        if (CategoryFileByteData != null)
        {
            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(CategoryFileByteData, 0, CategoryFileByteData.Length);
            fs.Close();
        }

        #endregion End of updating image

        #region Save in ServerUpdates
        Data.ServerUpdates su = new Data.ServerUpdates();
        su.UpdateStatus(uc.ResellerID.value, "upsellcategory");
        #endregion
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        FileUpload imgCtrl = fuUpsellCategoryImage;
        byte[] fileByteData = null;
        byte[] fileImageResizedByteData = null;
        String extension = Path.GetExtension(imgCtrl.PostedFile.FileName).ToString().ToLower();
        String imgType = imgCtrl.PostedFile.ContentType + extension;

        try
        {
            // Check if file upload ctrl has any files selected or not.
            if (imgCtrl.HasFile)
            {
                switch (imgType)
                {
                    case "image/jpg.jpg":
                    case "image/jpeg.jpg":
                    case "image/pjpeg.jpg":

                    case "image/jpg.jpeg":
                    case "image/jpeg.jpeg":
                    case "image/pjpeg.jpeg":

                    case "image/png.png":
                    case "image/x-png.png":

                    case "image/gif.gif":
                    case "image/bmp.bmp":
                    case "image/ico.ico":
                        break;
                    default:
                        AddErrorMessage("Upload image only *.png, *.jpg, *.jpeg, *.gif, *.bmp, *.ico format.");
                        break;
                }

                // Check error : if any server side validation error exist.
                if (_errorMsg != String.Empty)
                {
                    ShowError(lblErrMsg, _errorMsg);
                    return;
                }
                else
                {
                    // If the selected file extension is valid then 
                    // convert the selected file into bytes for saving in database 
                    Stream imageStream = imgCtrl.PostedFile.InputStream;
                    int contentLength = imgCtrl.PostedFile.ContentLength;

                    using (BinaryReader reader = new BinaryReader(imageStream))
                    {
                        fileByteData = reader.ReadBytes(contentLength);
                        reader.Close();
                    }

                    // Read the file bytes in MemoryStream for checking it's dimention as desired. 
                    MemoryStream memStream = new MemoryStream(fileByteData);
                    System.Drawing.Image imgObject = System.Drawing.Image.FromStream(memStream);
                    Int32 imgHeight = imgObject.Height;
                    Int32 imgWidth = imgObject.Width;

                    //if (imgHeight != 50 && imgWidth != 50)
                    //{
                    //    ShowError(lblErrMsg, "Uploaded image size is greater or less than the defined size.");
                    //    return;
                    //}
                    #region Resize Image
                    const int MAX_WIDTH = 75;
                    const int MAX_HEIGHT = 75;

                    int NewHeight = imgHeight * MAX_WIDTH / imgWidth;
                    int NewWidth = MAX_WIDTH;

                    if (NewHeight > MAX_HEIGHT)
                    {
                        // Resize with height instead
                        NewWidth = imgWidth * MAX_HEIGHT / imgHeight;
                        NewHeight = MAX_HEIGHT;
                    }

                    System.Drawing.Image imgResized = imgObject.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

                    MemoryStream mstreamResizedImage = new MemoryStream();

                    imgResized.Save(mstreamResizedImage, System.Drawing.Imaging.ImageFormat.Png);
                    fileImageResizedByteData = new Byte[mstreamResizedImage.Length];
                    mstreamResizedImage.Position = 0;
                    mstreamResizedImage.Read(fileImageResizedByteData, 0, (int)mstreamResizedImage.Length);
                    #endregion
                }
            }
            else
            {
                if (
                    (imgUc.ImageUrl.IndexOf("blank_img.png") > 0)
                    || (imgUc.ImageUrl == String.Empty)
                    )
                {
                    ShowError(lblErrMsg, "Upload an image for category.");
                    return;
                }
            }
            // Save data to Database
            extension = ".png";
            SaveData(fileImageResizedByteData, extension);
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect("UpsellCategory.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("UpsellCategory.aspx");
    }
}
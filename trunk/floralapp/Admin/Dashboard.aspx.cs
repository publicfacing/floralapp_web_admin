﻿using System;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;

public partial class Admin_Dashboard : AppPage
{
    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        Data.Purchase p = new Data.Purchase();
        Util.setValue(gvRecentPurchase, p.GetDashboardList(v.ResellerID));
        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvRecentPurchase.Columns[1].Visible = false;
        }

        Data.SpecialOffer  so = new Data.SpecialOffer();
        Util.setValue(gvSpecialOffer, so.GetDashboardList(v.ResellerID));
        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvSpecialOffer.Columns[0].Visible = false;
        }
        
        Data.Coupon cou = new Data.Coupon();
        Util.setValue(gvCoupons, cou.GetDashboardList(v.ResellerID));
        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvCoupons.Columns[0].Visible = false;
        }
    }

    protected void btnPreviewCoupon_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddCoupon.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String couponID = objButton.CommandArgument.ToString();
            String qs = Crypto.EncryptQueryString("CouponID=" + couponID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void btnPreviewPurchase_Click(object sender, EventArgs e)
    {
        String redirectUrl = "PurchaseInformation.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String purchaseID = objButton.CommandArgument.ToString();
            String qs = Crypto.EncryptQueryString("PurchaseID=" + purchaseID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void btnPreviewRecentOffers_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddSpecialOffer.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String specialOfferID = objButton.CommandArgument.ToString();
            String qs = Crypto.EncryptQueryString("SpecialOfferID=" + specialOfferID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    public string getCardName(String CardTypeID)
    {
        String CardName = String.Empty;
        switch (CardTypeID)
        {
            case "1":
                CardName = "VISA";
                break;
            case "2":
                CardName = "DISCOVER";
                break;
            case "3":
                CardName = "MASTER CARD";
                break;
            case "4":
                CardName = "AMEX";
                break;
            default:
                CardName = "None";
                break;
        }
        return CardName;
    }
}

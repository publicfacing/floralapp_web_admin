﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;
using System.Web.UI;

public partial class Admin_MetricsReport : AppPage
{
    public string IsAdmin
    {
        get { return v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN) ? "1" : "0"; }
    }

    public string NrDownloadsTotal
    {
        set { ViewState["NrDownloadsTotal"] = value; }
        get { return ViewState["NrDownloadsTotal"].ToString(); }
    }

    public string NrOrdersTotal
    {
        set { ViewState["NrOrdersTotal"] = value; }
        get { return ViewState["NrOrdersTotal"].ToString(); }
    }

    public string DollarsSpentTotal
    {
        set { ViewState["DollarsSpentTotal"] = value; }
        get { return ViewState["DollarsSpentTotal"].ToString(); }
    }

    public string AvgPerOrderTotal
    {
        set { ViewState["AvgPerOrderTotal"] = value; }
        get { return ViewState["AvgPerOrderTotal"].ToString(); }
    }

    public string NrRegUsersTotal
    {
        set { ViewState["NrRegUsersTotal"] = value; }
        get { return ViewState["NrRegUsersTotal"].ToString(); }
    }

    public string CouponsRedeemedTotal
    {
        set { ViewState["CouponsRedeemedTotal"] = value; }
        get { return ViewState["CouponsRedeemedTotal"].ToString(); }
    }

    public string CouponsTotal
    {
        set { ViewState["CouponsTotal"] = value; }
        get { return ViewState["CouponsTotal"].ToString(); }
    }

    public string ProductsTotal
    {
        set { ViewState["ProductsTotal"] = value; }
        get { return ViewState["ProductsTotal"].ToString(); }
    }

    private static Int32 resellerID
    {
        get;
        set;
    }

    public int ReportResellerID
    {
        set { ViewState["ReportResellerID"] = value; }
        get { return (int)ViewState["ReportResellerID"]; }
    }

    private int rowCount = 0;
    private int totalRowCount = 0;
    public AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            InitializeControls();
            BindData();
        }
    }

    private void InitializeControls()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);
        Util.setValue(txtStartDate, String.Format("{0:MM/dd/yyyy}", DateTime.Now.AddMonths(-1)));
        Util.setValue(txtEndDate, String.Format("{0:MM/dd/yyyy}", DateTime.Now));
        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            //ddlReseller.Enabled = false;
            divShop.Visible = false;
        }
    }

    private void BindData()
    {
        DataSet ds = GetCouponList();
        NrDownloadsTotal = string.Format("Total: {0}", ds.Tables[1].Rows[0]["NrDownloadsTotal"]);
        NrOrdersTotal = string.Format("Total: {0}", ds.Tables[1].Rows[0]["NrOrdersTotal"]);
        DollarsSpentTotal = string.Format("Total: {0:c}", ds.Tables[1].Rows[0]["DollarsSpentTotal"]);
        AvgPerOrderTotal = string.Format("Total: {0:c}", ds.Tables[1].Rows[0]["AvgPerOrderTotal"]);
        NrRegUsersTotal = string.Format("Total: {0}", ds.Tables[1].Rows[0]["NrRegUsersTotal"]);
        CouponsRedeemedTotal = string.Format("Total: {0}", ds.Tables[1].Rows[0]["CouponsRedeemedTotal"]);
        CouponsTotal = string.Format("Total: {0:c}", ds.Tables[1].Rows[0]["TotalCouponsTotal"]);
        ProductsTotal = string.Format("Total: {0}", ds.Tables[1].Rows[0]["NrProductsTotal"]);
        Util.setValue(gvMetrics, ds.Tables[0]);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvMetrics.Columns[0].Visible = false;
        }
    }

    private DataSet GetCouponList()
    {

        StringBuilder sbFilter = new StringBuilder();

        Int32 resellerID = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? Convert.ToInt32(v.ResellerID) : Convert.ToInt32(Util.getValue(ddlReseller));

        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);


        String orderBy = gvMetrics.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " ResellerName asc";
        else
        {
            if (orderBy.Contains("DESC"))
                orderBy = orderBy.Replace("DESC", "");
            else
                orderBy += " DESC";
        }

        Data.PurchaseCoupon pc = new Data.PurchaseCoupon();
        DataSet dsCoupon = pc.GetMetricsReport(gvMetrics.PageIndex, gvMetrics.PageSize, orderBy,
                                    sbFilter.ToString(), ref rowCount, ref totalRowCount,
                                    Util.GetDataDateTime(Util.getValue(txtStartDate)),
                                    Util.GetDataDateTime(Util.getValue(txtEndDate)));
        gvMetrics.VirtualItemCount = totalRowCount;

        // Set Total Count in grid footer
        Util.setValue(lblTotal, String.Format("{0} record(s).", totalRowCount));

        return dsCoupon;
    }

    protected void gvMetrics_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvMetrics.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvMetrics_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void lnkConsumers_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkConsumers";
        Response.Redirect("~/Admin/Users.aspx?RoleID=3&Shop=" + ((LinkButton)sender).CommandArgument);
    }

    protected void lnkViewReport_Click(object sender, EventArgs e)
    {
        String[] values = ((ImageButton)sender).CommandArgument.Split(',');
        ReportResellerID = Util.GetDataInt32(values[0].ToString());
        lblShopName.Text = "Shop name: " + Util.GetDataString(values[1].ToString());

        ddlMonths.SelectedValue = DateTime.Now.Month.ToString();
        ddlYear.Items.Clear();
        for (int i = DateTime.Now.Year; i > DateTime.Now.Year - 10; i--)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        ddlYear.SelectedValue = DateTime.Now.Year.ToString();

        BindReport(ReportResellerID);
        mpeReport.Show();
    }

    private void BindReport(int resellerID)
    {
        StringBuilder sbFilter = new StringBuilder();

        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);


        String orderBy = gvMetrics.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " ResellerName asc";
        else
        {
            if (orderBy.Contains("DESC"))
                orderBy = orderBy.Replace("DESC", "");
            else
                orderBy += " DESC";
        }

        Data.PurchaseCoupon pc = new Data.PurchaseCoupon();
        DateTime toStart = new DateTime(int.Parse(ddlYear.SelectedValue), int.Parse(ddlMonths.SelectedValue), 1, 0, 0, 0);
        DateTime toEnd = new DateTime(int.Parse(ddlYear.SelectedValue), int.Parse(ddlMonths.SelectedValue), 1, 23, 59, 59).AddMonths(1).AddDays(-1);
        DataSet dsCoupon = pc.GetMetricsReport(0, 10, orderBy,
                                    sbFilter.ToString(), ref rowCount, ref totalRowCount,
                                    toStart,
                                    toEnd);

        //Util.GetDataDateTime(Util.getValue(txtStartDate)),
        //Util.GetDataDateTime(Util.getValue(txtEndDate)));
        DateTime fromStart = new DateTime(int.Parse(ddlYear.SelectedValue), int.Parse(ddlMonths.SelectedValue), 1, 0, 0, 0).AddYears(-1);
        DateTime fromEnd = new DateTime(int.Parse(ddlYear.SelectedValue), int.Parse(ddlMonths.SelectedValue), 1, 23, 59, 59).AddYears(-1).AddMonths(1).AddDays(-1);
        DataSet dsCoupon2 = pc.GetMetricsReport(0, 10, orderBy,
                                    sbFilter.ToString(), ref rowCount, ref totalRowCount,
                                    fromStart,
                                    fromEnd);
        //Util.GetDataDateTime(Util.getValue(txtStartDate)).AddYears(-1),
        //Util.GetDataDateTime(Util.getValue(txtEndDate)).AddYears(-1));

        dsCoupon.Tables[0].Rows.Add(dsCoupon2.Tables[0].Rows[0].ItemArray);
        //dsCoupon.Tables[0].Rows[0]["ResellerName"] = Util.getValue(txtStartDate) + "-" + Util.getValue(txtEndDate);
        //dsCoupon.Tables[0].Rows[1]["ResellerName"] = Util.GetDataDateTime(Util.getValue(txtStartDate)).AddYears(-1).ToShortDateString() + "-" + Util.GetDataDateTime(Util.getValue(txtEndDate)).AddYears(-1).ToShortDateString();        
        dsCoupon.Tables[0].Rows[0]["ResellerName"] = toStart.ToShortDateString() + "-" + toEnd.ToShortDateString();
        dsCoupon.Tables[0].Rows[1]["ResellerName"] = fromStart.ToShortDateString() + "-" + fromEnd.ToShortDateString();
        Util.setValue(gvReport, dsCoupon.Tables[0]);
    }

    protected void btnShowReport_Click(object sender, EventArgs e)
    {
        BindReport(ReportResellerID);
        mpeReport.Show();
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="ChangePassword.aspx.cs" Inherits="Admin_ChangePassword" ValidateRequest="true"
    Title="BenevaFlowers | Change Password" Theme="GraySkin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">

    <script type="text/javascript" language="javascript">
        
        function validateFormData()
        { 
            var pwd = document.getElementById('ctl00$cphPageContainer$txtPassword').value;
            var repwd = document.getElementById('ctl00$cphPageContainer$txtConfirmPassword').value; 
            
            if (pwd == '' || pwd.length<= 3 )
            {  
                alert('Please enter a valid password having length more than three charchater.');  
                return false;
            }  
            
            if (repwd == '' )
            {
                alert('Please confirm your password.');
                return false;
            }
             
            if (pwd != repwd)
            {
                alert('Make sure password and confirm password are same.'); 
                return false;
            }
            else 
                 return true;
               
        }
        
    </script>

    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Change Password"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="vsReseller" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                            <asp:Label ID="lblSucessMsg" runat="server" Text="" Visible="false" CssClass="message"></asp:Label>
                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                <tr>
                                    <td class="col">
                                        New Password<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" SkinID="textbox"
                                            class="FormItem" MaxLength="50"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                            ErrorMessage="Enter the new password." Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rvPassword" runat="server" ControlToValidate="txtPassword"
                                            ErrorMessage="Password must be more than 3 charecters." ValidationExpression="^[\s\S]{4,50}$"
                                            Display="Dynamic"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Confirm Password<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtConfirmPassword" TextMode="Password" runat="server" SkinID="textbox"
                                            class="FormItem" MaxLength="50"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rdfConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                                            ErrorMessage="Enter the confirm password." Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="compConfPassword" runat="server" ControlToCompare="txtPassword"
                                            ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Make sure password and confirm password are same."></asp:CompareValidator>
                                    </td>
                                </tr>
                            </table>
                            <div class="actionDiv">
                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save the new password."
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" CausesValidation="false"
                                    OnClick="btnCancel_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

﻿using System;
using Library.Utility;
using Library.AppSettings;
using System.Collections.Specialized;

public partial class Admin_AddFaq : AppPage
{
    private Int32 FaqID
    {
        get;
        set;
    }
     
    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);
         
        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                FaqID = Util.GetDataInt32(nvc["FaqID"]);
            }

            Util.setValue(lblPageTitle, (FaqID > 0) ? "Edit Faq" : "Add Faq");
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }


        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        if (FaqID <= 0)
            return;

        Data.Faq faq = new Data.Faq();
        faq.LoadID(FaqID);
        Util.setValue(txtQuestion, faq.Question.value);
        Util.setValue(txtAnswer, faq.Answer.value);
    }

    private void SaveData()
    {
        AppVars v = new AppVars();
        Data.Faq faq = new Data.Faq(FaqID);
        faq.Question.value = Util.getValue(txtQuestion);
        faq.Answer.value = Util.getValue(txtAnswer);
        faq.Sequence.value = 0;
        faq.AddedBy.value = v.UserID;
        faq.DateAdded.value = Util.GetServerDate();
        faq.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        faq.Save();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            SaveData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect("Faq.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Faq.aspx");
    }
}


﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;

public partial class Admin_Users : AppPage
{
    private int rowCount = 0;
    private int totalRowCount = 0;

    private String PageTitle
    {
        get;
        set;
    }

    private Int32 RoleID
    {
        get;
        set;
    }

    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (v.UserRoleID == 1)
            PageTitle = "Super Admin";
        else if (v.UserRoleID == 2)
            PageTitle = "Shop Admin";

        if (Request.QueryString["RoleID"] != null && !String.IsNullOrEmpty(Request.QueryString["RoleID"].ToString()))
        {
            RoleID = Convert.ToInt32(Request.QueryString["RoleID"].ToString());
            if (v.UserRoleID > RoleID)
            {
                LabelAuthenticateError.Visible = true;
                contentTable.Visible = false;
                LabelAuthenticateError.Text = "Your User does not have enought rights to perform this action";
                //RoleID+=1;
            }

            switch (RoleID.ToString())
            {
                case "1":
                    PageTitle = "Super Admin";
                    break;
                case "2":
                    PageTitle = "Shop Admin";
                    break;
                case "3":
                    PageTitle = "Consumer";
                    break;
            }
        }

        lblTitle.Text = PageTitle;
        lnkAdd.Text = lnkAdd.ToolTip = "Add " + PageTitle;

        if (!IsPostBack)
        {
            InitializeControls();
            BindData();
        }

        #region remove Add Consumer link from [Consumer] page.

        #endregion
    }

    private void InitializeControls()
    {
        if (RoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
        {
            gvUsers.Columns[4].Visible = false; // * Shop (Reseller) column visibility.
            ddlReseller.Visible = false;
            lblReseller.Visible = false;
        }

        if (RoleID == Convert.ToInt32(AppEnum.UserRole.CONSUMER))
        {
            lnkAdd.Visible = false;
            gvUsers.Columns[6].Visible = false; //*Action Edit/Delete column visibility.
            gvUsers.Columns[7].Visible = true; //* Action Status column visibility.
            //dvStatus.Visible = true;
            ddlReseller.Visible = true;
            lblReseller.Visible = true;
        }

        Util.setValue(ddlUserRole, RoleID);
        ddlUserRole.Enabled = false;

        //Binding Reseller into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            //ddlReseller.Enabled = false;
            lblReseller.Visible = false;
            ddlReseller.Visible = false;
        }
        else
        {
            if (!String.IsNullOrEmpty(Request["Shop"]))
                Util.setValue(ddlReseller, int.Parse(Request["Shop"]));
        }
    }

    private void BindData()
    {
        Util.setValue(gvUsers, GetUsersList());

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvUsers.Columns[4].Visible = false;
        }
    }

    private DataSet GetUsersList()
    {
        StringBuilder sbFilter = new StringBuilder();

        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();
        Int32 roleID = Convert.ToInt32(Util.getValue(ddlUserRole));

        switch (searchOperator)
        {
            case "Begins With":
                sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                break;
            case "Ends With":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                break;
            case "Contains":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                break;
            case "Equals":
                sbFilter.Append(" And " + searchField + " = " + searchString);
                break;
        }

        #region New concept of purchase now are displaying active and inactive records...

        if (ddlStatus.SelectedItem.Value == "-1")// This concept will displaying the current new record(s)
            sbFilter.Append(" And  IsActive = 1");

        if (ddlStatus.SelectedItem.Value == "1")// This concept will displaying the successed record(s)
            sbFilter.Append(" And  IsActive = 1 And IsEnabled = 1");

        if (ddlStatus.SelectedItem.Value == "0")// This concept will displaying the inactive record(s)
            sbFilter.Append(" And  IsActive = 0"); // This concept will displaying the inactive record(s)

        #endregion

        if (roleID > 0)
            sbFilter.Append(" And RoleID =" + roleID);

        Int32 resellerID = Convert.ToInt32(Util.getValue(ddlReseller));
        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);

        String orderBy = gvUsers.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " UserID desc";

        Data.Users u = new Data.Users();
        DataSet dsUser = u.GetList(gvUsers.PageIndex, gvUsers.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvUsers.VirtualItemCount = totalRowCount;

        // Set Total Count in grid footer
        Util.setValue(lblTotalCategory, String.Format("{0} Record(s).", totalRowCount));

        return dsUser;
    }

    protected void gvUsers_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUsers.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvUsers_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "CreateUser.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;

            String[] CommandArgument = objButton.CommandArgument.Split(',');
            Int64 userID = Convert.ToInt64(CommandArgument[0].ToString());
            Int32 resellerID = Convert.ToInt32(CommandArgument[1].ToString());
            GridViewRow row = ((ImageButton)sender).Parent.Parent as GridViewRow;
            String role = row.Cells[3].Text;
            Int32 roleID = 0;

            switch (role.ToUpper())
            {
                case "SUPERADMIN":
                    roleID = Convert.ToInt32(AppEnum.UserRole.SUPERADMIN);
                    break;

                case "RESELLERADMIN":
                    roleID = Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN);
                    break;

                case "CONSUMER":
                    roleID = Convert.ToInt32(AppEnum.UserRole.CONSUMER);
                    break;
            }

            String qs = Crypto.EncryptQueryString("UserID=" + userID + "&ResellerID= " + resellerID + "&RoleID=" + roleID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int64 userID = Convert.ToInt64(objButton.CommandArgument.ToString());

            // Update User Active status
            if (v.UserID == userID)
            {
                ShowError(lblErrMsg, "Can not delete current user.");
                return;
            }

            Data.Users u = new Data.Users(userID);
            u.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
            u.DateDeleted.value = Util.GetServerDate();
            u.DeletedBy.value = v.UserID;
            u.Save();

            #region Inactivate/Delete the Relationship table(s) for this user

            u.InActiveRelationshipTables(userID, "Users");

            #endregion End of inactivate/delete process.


            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        gvUsers.PageIndex = 0;
        BindData();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (v.UserRoleID <= RoleID)
        {
            String redirectUrl = "CreateUser.aspx?eqs=" + Crypto.EncryptQueryString("RoleID=" + RoleID);
            Response.Redirect(redirectUrl);
        }
    }

    protected void ImgStatus_Click(object sender, EventArgs e)
    {
        Int64 userID = Util.GetDataInt64(((ImageButton)sender).ToolTip);
        Int32 isActive = ((ImageButton)sender).CommandArgument.Equals("1") ? 1 : 0;

        Data.Users user = new Data.Users();

        if (isActive == 1)
            isActive = 0;
        else
            isActive = 1;

        user.InactiveUserByID(userID, isActive);

        BindData();
    }
}


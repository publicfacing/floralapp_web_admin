﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="ViralMarketing.aspx.cs" Inherits="Admin_ViralMarketing" Title="floralapp&reg; | Viral Marketing Request"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12">
                    <tr>
                        <th class="TableHeadingBg TableHeading">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                padding-left: 5px;">
                                <tr>
                                    <td>
                                        Viral Marketing
                                    </td>
                                    <td style="text-align: right; padding-right: 5px; text-transform: capitalize;">
                                        <asp:HyperLink Visible="false" ID="lnkAdd" runat="server" NavigateUrl="~/Admin/AddViralMarketing.aspx"
                                            ToolTip="Add Viral Marketing">Add Viral Marketing</asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label">
                                            Search :
                                        </td>
                                        <td>
                                            <label class="label" runat="server" id="lblShop">
                                                Shop:</label>
                                            <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="Label1" CssClass="label" runat="server" Text="Date Range:"></asp:Label>
                                            <asp:TextBox runat="server" Width="70px" OnKeyUP="this.value=''" CssClass="textbox"
                                                ID="txtStartDate"></asp:TextBox>
                                            <ajax:CalendarExtender ID="ceStartDate" runat="server" TargetControlID="txtStartDate"
                                                PopupButtonID="imgCalIcon1">
                                            </ajax:CalendarExtender>
                                            <asp:Image ImageUrl="~/Images/calender-icon.gif" Style="cursor: hand" ID="imgCalIcon1"
                                                runat="server" />
                                            &nbsp;&nbsp;
                                            <asp:TextBox runat="server" Width="70px" OnKeyUP="this.value=''" CssClass="textbox"
                                                ID="txtEndDate"></asp:TextBox>
                                            <ajax:CalendarExtender ID="ceEndDate" runat="server" TargetControlID="txtEndDate"
                                                PopupButtonID="imgCalIcon2">
                                            </ajax:CalendarExtender>
                                            <asp:Image ImageUrl="~/Images/calender-icon.gif" ID="imgCalIcon2" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSearchField" runat="server" AutoPostBack="false">
                                                <%--<asp:ListItem Text="Referral Code" Value="ReferralCode"></asp:ListItem>--%>
                                                <asp:ListItem Text="From" Value="FromEmail"></asp:ListItem>
                                                <asp:ListItem Text="Sent mails to" Value="EmailIDList"></asp:ListItem>
                                                <asp:ListItem Text="Request Link" Value="RequestLink"></asp:ListItem>
                                                <asp:ListItem Text="Message" Value="Message"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchOperator" runat="server">
                                                <asp:ListItem Value="Begins With">Begins With</asp:ListItem>
                                                <asp:ListItem Value="Ends With">Ends With</asp:ListItem>
                                                <asp:ListItem Value="Contains">Contains</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnFilter" class="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click"
                                                CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpTop" runat="server" PageSize="50" PagedControlID="gvViralMarketing">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <mo:ExtGridView AutoGenerateColumns="false" ID="gvViralMarketing" runat="server"
                                DataKeyNames="ResellerID" AllowSorting="True" OnSorting="gvViralMarketing_OnSorting"
                                OrderBy="ResellerID Desc" SkinID="gvskin" OnPageIndexChanging="gvViralMarketing_OnPageIndexChanging"
                                EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:BoundField DataField="ResellerName" SortExpression="ResellerName" HeaderText="Shop"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="UserName" SortExpression="UserName" HeaderText="User"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="FromEmail" SortExpression="FromEmail" HeaderText="From Email"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:TemplateField SortExpression="RequestCount" HeaderStyle-CssClass="gridHeader"
                                        HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" HeaderText="Sent Request"
                                        ItemStyle-CssClass="gridContent">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkSentRequest" runat="server" Text='<%# Eval("RequestCount") %>'
                                                CommandArgument='<%# Eval("ResellerID") + "," + Eval("ResellerName") + "," + Eval("UserID") %>'
                                                CausesValidation="false" OnClick="lnkSentRequest_Click" ToolTip="View Sent Request">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="DownloadAppCount" HeaderStyle-CssClass="gridHeader"
                                        HeaderStyle-Width="65px" ItemStyle-HorizontalAlign="Center" HeaderText="Download"
                                        ItemStyle-CssClass="gridContent">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDownload" runat="server" Text='<%# Eval("DownloadAppCount") %>'
                                                CommandArgument='<%#  Eval("ResellerID") + "," + Eval("ResellerName") + "," + Eval("UserID")  %>'
                                                CausesValidation="false" OnClick="lnkDownload_Click" ToolTip="View Purchase Preview">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="ActivateAppCount" HeaderStyle-CssClass="gridHeader"
                                        HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" HeaderText="Activated"
                                        ItemStyle-CssClass="gridContent">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkActivate" runat="server" Text='<%# Eval("ActivateAppCount") %>'
                                                CommandArgument='<%# Eval("ResellerID") + "," + Eval("ResellerName") + "," + Eval("UserID") %>'
                                                CausesValidation="false" OnClick="lnkActivate_Click" ToolTip="View activated Request">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--  <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="gridContent" Visible="false">
                                        <HeaderStyle CssClass="gridHeader" Width="80px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("ViralMarketingRequestID")%>'
                                                CausesValidation="false" OnClick="lnkEdit_Click" ToolTip="Edit" ImageUrl="~/Images/edit-icon.jpg" />
                                            <asp:ImageButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("ViralMarketingRequestID")%>'
                                                CausesValidation="false" OnClick="lnkDelete_Click" ImageUrl="~/Images/DeleteButton.jpg"
                                                OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotalViral" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="50" PagedControlID="gvViralMarketing">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- PopUp Ajax Model for create coupon report. -->
    <asp:Button ID="btnAppDownloads" Style="display: none" runat="server" />
    <ajax:ModalPopupExtender ID="mpeAppDownloads" TargetControlID="btnAppDownloads" runat="server"
        PopupControlID="pnlAppDownloadRequest" BackgroundCssClass="modalBackground" DropShadow="False"
        CancelControlID="btnCancel">
    </ajax:ModalPopupExtender>
    <asp:Panel runat="server" ID="pnlAppDownloadRequest" Style="display: none; background: #ffffff;">
        <table border="0" cellpadding="0" class="PopUpBox" style="overflow: scroll; width: 700px;">
            <tr>
                <td class="PopUpBoxHeading">
                    <asp:Label ID="lblAppDownloads" runat="server"></asp:Label>
                </td>
                <td class="PopUpBoxHeading" align="right">
                    <asp:ImageButton ID="btnClosePopup" runat="server" CausesValidation="false" ImageUrl="~/Images/close.gif" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 10px; text-align: center">
                    <asp:GridView ID="gvReport" runat="server" CssClass="grid" Width="100%" OnPageIndexChanging="gvReport_OnPageIndexChanging"
                        AllowPaging="true" EmptyDataRowStyle-CssClass="gridEmptyRow" PagerSettings-Mode="NumericFirstLast"
                        PageSize="10" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Right"
                        PagerStyle-CssClass="PagerContentDetails" HeaderStyle-CssClass="gridHeader" RowStyle-CssClass="gridContent"
                        AlternatingRowStyle-CssClass="gridContent" EmptyDataText="No record(s) found."
                        CellPadding="4">
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="val" colspan="2" style="text-align: center;">
                    <div style="margin: 10px">
                        <asp:Button ID="btnCancel" runat="server" Width="60px" Text="Close" CssClass="btn"
                            CausesValidation="false" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <!-- End of popup coupon report."  -->
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="AddColorTheme.aspx.cs" Inherits="Admin_AddColorTheme" Title="floralapp&reg; | Add Color Theme"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="../UserControls/HelpMsg.ascx" TagName="HelpMsg" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/javascript">
        function Color_Changed(sender) {
            sender.get_element().value = "#" + sender.get_selectedColor();
        }
    </script>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Add Color Theme"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <uc1:HelpMsg ID="HelpMsg1" runat="server" />
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="vsColorTheme" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                <tr>
                                    <td class="col" style="width: 232px">
                                        Name<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtThemeName" runat="server" SkinID="textbox" class="FormItem" MaxLength="50"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvThemeName" runat="server" ControlToValidate="txtThemeName"
                                            ErrorMessage="Enter a color theme name." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh" style="width: 232px">
                                        Navigation Color
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtNavigationColor" runat="server" SkinID="textbox" class="FormItem"
                                            MaxLength="50"></asp:TextBox>
                                        <img alt="" style="height: 20px; width: 25px; text-decoration: none;" id="imgCpicker"
                                            src="../Images/Color-Picker.PNG" />
                                        <ajax:ColorPickerExtender ID="cpNaviColor" runat="server" TargetControlID="txtNavigationColor"
                                            PopupButtonID="imgCpicker" PopupPosition="Right" OnClientColorSelectionChanged="Color_Changed" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh" style="width: 232px">
                                        Background Color
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtBgColor" runat="server" SkinID="textbox" class="FormItem" MaxLength="50"></asp:TextBox>
                                        <img alt="" style="height: 20px; width: 25px; text-decoration: none;" id="imgColorPicker"
                                            src="../Images/Color-Picker.PNG" />
                                        <ajax:ColorPickerExtender ID="cpBgColor" runat="server" TargetControlID="txtBgColor"
                                            PopupButtonID="imgColorPicker" PopupPosition="Right" OnClientColorSelectionChanged="Color_Changed" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh" style="width: 232px">
                                        Navigation Image
                                    </td>
                                    <td class="val">
                                        <asp:FileUpload ID="fuNavigationImage" runat="server" />
                                        <div class="desc">
                                            <br />
                                            Supported image format(s): *.png
                                            <%-- <br />
                                            Suggested image size (ht x wt) is 50 x 320 pixel(s).--%>
                                            <br />
                                            Max. image size(ht x wt) should not exceed 50 x 320 pixel(s).
                                            <br />
                                        </div>
                                        <div>
                                            <asp:Image ID="imgNavigation" runat="server" /></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh" style="width: 232px">
                                        Background Image<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:FileUpload ID="fuBgImage" runat="server" />
                                        <div class="desc">
                                            <br />
                                            Supported image format(s): *.png
                                            <%--<br />
                                            Suggested image size (ht x wt) is 480 x 320 pixel(s).--%>
                                            <br />
                                            Max. image size(ht x wt) should not exceed 480 x 320 pixel(s).
                                            <br />
                                        </div>
                                        <div>
                                            <asp:Image ID="imgBackground" runat="server" /></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh" style="width: 232px">
                                        Navigation Right Image [On]
                                    </td>
                                    <td class="val">
                                        <asp:FileUpload ID="fuNaviRightOn" runat="server" />
                                        <div class="desc">
                                            *Android use only.
                                            <br />
                                            Supported image format(s): *.png
                                            <%-- <br />
                                            Suggested image size (ht x wt) is 75 x 30 pixel(s). --%>
                                            <br />
                                            Max. image size(ht x wt) should not exceed 75 x 30 pixel(s).
                                            <br />
                                        </div>
                                        <div>
                                            <asp:Image ID="imgNaviRightOn" runat="server" /></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh" style="width: 232px">
                                        Navigation Right Image [Off]
                                    </td>
                                    <td class="val">
                                        <asp:FileUpload ID="fuNaviRightOff" runat="server" />
                                        <div class="desc">
                                            *Android use only.
                                            <br />
                                            Supported image format(s): *.png
                                            <%-- <br />
                                            Suggested image size (ht x wt) is 75 x 30 pixel(s).--%>
                                            <br />
                                            Max. image size(ht x wt) should not exceed 75 x 30 pixel(s).
                                            <br />
                                        </div>
                                        <div>
                                            <asp:Image ID="imgNaviRightOff" runat="server" /></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh" style="width: 232px">
                                        Navigation Left Image [On]
                                    </td>
                                    <td class="val">
                                        <asp:FileUpload ID="fuNaviLeftOn" runat="server" />
                                        <div class="desc">
                                            *Android use only.
                                            <br />
                                            Supported image format(s): *.png
                                            <%--<br />
                                            Suggested image size (ht x wt) is 75 x 30 pixel(s). --%>
                                            <br />
                                            Max. image size(ht x wt) should not exceed 75 x 30 pixel(s).
                                            <br />
                                        </div>
                                        <div>
                                            <asp:Image ID="imgNaviLeftOn" runat="server" /></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh" style="width: 232px">
                                        Navigation Left Image [Off]
                                    </td>
                                    <td class="val">
                                        <asp:FileUpload ID="fuNaviLeftOff" runat="server" />
                                        <div class="desc">
                                            *Android use only.
                                            <br />
                                            Supported image format(s): *.png
                                            <%--  <br />
                                            Suggested image size (ht x wt) is 75 x 30 pixel(s).--%>
                                            <br />
                                            Max. image size(ht x wt) should not exceed 75 x 30 pixel(s).
                                            <br />
                                        </div>
                                        <div>
                                            <asp:Image ID="imgNaviLeftOff" runat="server" /></div>
                                    </td>
                                </tr>
                            </table>
                            <div class="actionDiv">
                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this category."
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                                    CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

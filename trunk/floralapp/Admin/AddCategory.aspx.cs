﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Collections.Specialized;
using System.IO;
using System.Xml;
using System.Net;
using System.Text;

public partial class Admin_AddCategory : AppPage
{
    private Int32 CategoryID
    {
        get;
        set;
    }

    private string RssFeed
    {
        get { return ViewState["RssFeed"] != null ? (string)ViewState["RssFeed"] : ""; }
        set { ViewState["RssFeed"] = value; }
    }

    private bool WasEnabled
    {
        get { return ViewState["WasEnabled"] != null ? (bool)ViewState["WasEnabled"] : false; }
        set { ViewState["WasEnabled"] = value; }
    }

    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                CategoryID = Util.GetDataInt32(nvc["CategoryID"]);
            }

            Util.setValue(lblPageTitle, (CategoryID > 0) ? "Edit Category" : "Add Category");
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            trShop.Visible = false;
            //ddlReseller.Enabled = false;
        }

        //Setting initially as blank image.
        imgCat.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");

        if (CategoryID <= 0)
            return;        

        Data.Category cat = new Data.Category();
        cat.LoadID(CategoryID);

        Data.ResellerSetting resSet = new Data.ResellerSetting();
        DataSet ds = resSet.GetSetting(v.ResellerID);
        if (cat.CategoryName.value.Trim().ToLower().Equals("import products")
            && ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0
            && !string.IsNullOrEmpty(ds.Tables[0].Rows[0]["RssFeed"].ToString()))
        {
            RssFeed = ds.Tables[0].Rows[0]["RssFeed"].ToString();
            btnImport.Visible = true;
        }

        if (!(ddlReseller.SelectedIndex > 0))
            Util.setValue(ddlReseller, cat.ResellerID.value);

        Util.setValue(txtCategoryName, cat.CategoryName.value);
        Util.setValue(chkIsEnabled, cat.IsEnabled.value);
        WasEnabled = cat.IsEnabled.value;
        Util.setValue(chkIsFeatured, cat.IsFeatured.value);

        #region Populating images ...

        String imageExt = cat.ImageExt.value;
        String fileName = String.Format("CategoryImage_{0}{1}", cat.CategoryID.value.ToString(), imageExt);

        if (!File.Exists(Server.MapPath("../DynImages/Category/" + fileName)))
            imgCat.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        else
            imgCat.ImageUrl = String.Format("../DynImages/Category/{0}", fileName);

        #endregion End of binding image
    }

    private void SaveData(byte[] CategoryFileByteData, String fileExtn, Boolean isEnableCategory)
    {
        Data.Category cat = new Data.Category(CategoryID);
        cat.ResellerID.value = Int32.Parse(Util.getValue(ddlReseller));
        cat.DefaultCategoryID.value = 0; // 0 - User Defined Category
        cat.CategoryName.value = Util.getValue(txtCategoryName);

        if (CategoryFileByteData != null)
        {
            cat.ImageBinary.value = CategoryFileByteData;
            cat.ImageExt.value = fileExtn;
        }

        cat.Sequence.value = 0;

        if (chkIsEnabled.Checked == false)
            cat.IsEnabled.value = false;
        else
            cat.IsEnabled.value = (isEnableCategory == false) ? cat.IsEnabled.value : isEnableCategory;

        cat.IsSystemDefined.value = false;
        cat.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        cat.IsFeatured.value = chkIsFeatured.Checked;

        if (CategoryID <= 0)
        {
            cat.DateAdded.value = Util.GetServerDate();
            cat.AddedBy.value = v.UserID;
        }
        cat.DateModified.value = Util.GetServerDate();
        cat.ModifiedBy.value = v.UserID;

        cat.Save();

        #region Updating images ...

        Byte[] fileByteData = cat.ImageBinary.value;
        String imageExt = cat.ImageExt.value;
        String filePath = Server.MapPath(@"~/DynImages\Category\");
        String fileName = String.Format("CategoryImage_{0}{1}", cat.CategoryID.value.ToString(), imageExt);
        filePath += fileName;

        if (File.Exists(filePath))
            File.Delete(filePath);

        if (fileByteData != null)
        {
            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(fileByteData, 0, fileByteData.Length);
            fs.Close();
        }

        #endregion End of updating image

        #region Insert/Update on Server
        //Save in ServerUpdates
        Data.ServerUpdates su = new Data.ServerUpdates();
        su.UpdateStatus(cat.ResellerID.value, "category");
        #endregion
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        FileUpload imgCtrl = fuCategoryImage;
        byte[] fileByteData = null;
        byte[] fileImageResizedByteData = null;
        String extension = Path.GetExtension(imgCtrl.PostedFile.FileName).ToString().ToLower();
        String imgType = imgCtrl.PostedFile.ContentType + extension;
        Boolean isEnabledCategory = false, isDisabledCategory = false;
        try
        {
            #region Getting is enabled / disabled a particular category
            isEnabledCategory = getIsEnabledCategory();
            isDisabledCategory = getIsDisabledCategory();
            #endregion

            if (!isDisabledCategory && !chkIsEnabled.Checked)
            {
                ShowError(lblErrMsg, "Sorry! minimum limit reached for this shop, it can not be disabled.");
                return;
            }

            if (!isEnabledCategory && chkIsEnabled.Checked && !WasEnabled)
            {
                ShowError(lblErrMsg, "Sorry! limit exceed(s) for this shop, it can not be enabled.");
                return;
            }
            // Check if file upload ctrl has any files selected or not.
            if (imgCtrl.HasFile)
            {
                switch (imgType)
                {
                    case "image/jpg.jpg":
                    case "image/jpeg.jpg":
                    case "image/pjpeg.jpg":

                    case "image/jpg.jpeg":
                    case "image/jpeg.jpeg":
                    case "image/pjpeg.jpeg":

                    case "image/png.png":
                    case "image/x-png.png":

                    case "image/gif.gif":
                    case "image/bmp.bmp":
                    case "image/ico.ico":
                        break;
                    default:
                        AddErrorMessage("Upload image only *.png, *.jpg, *.jpeg, *.gif, *.bmp, *.ico format.");
                        break;
                }

                // Check error : if any server side validation error exist.
                if (_errorMsg != String.Empty)
                {
                    ShowError(lblErrMsg, _errorMsg);
                    return;
                }
                else
                {
                    // If the selected file extension is valid then 
                    // convert the selected file into bytes for saving in database 
                    Stream imageStream = imgCtrl.PostedFile.InputStream;
                    int contentLength = imgCtrl.PostedFile.ContentLength;

                    using (BinaryReader reader = new BinaryReader(imageStream))
                    {
                        fileByteData = reader.ReadBytes(contentLength);
                        reader.Close();
                    }

                    // Read the file bytes in MemoryStream for checking it's dimention as desired. 
                    MemoryStream memStream = new MemoryStream(fileByteData);
                    System.Drawing.Image imgObject = System.Drawing.Image.FromStream(memStream);
                    Int32 imgHeight = imgObject.Height;
                    Int32 imgWidth = imgObject.Width;

                    //if (imgHeight != 50 && imgWidth != 50)
                    //{
                    //    ShowError(lblErrMsg, "Uploaded image size is greater or less than the defined size.");
                    //    return;
                    //}
                    #region Resize Image
                    const int MAX_WIDTH = 75;
                    const int MAX_HEIGHT = 75;

                    int NewHeight = imgHeight * MAX_WIDTH / imgWidth;
                    int NewWidth = MAX_WIDTH;

                    if (NewHeight > MAX_HEIGHT)
                    {
                        // Resize with height instead
                        NewWidth = imgWidth * MAX_HEIGHT / imgHeight;
                        NewHeight = MAX_HEIGHT;
                    }

                    System.Drawing.Image imgResized = imgObject.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

                    MemoryStream mstreamResizedImage = new MemoryStream();

                    imgResized.Save(mstreamResizedImage, System.Drawing.Imaging.ImageFormat.Png);
                    fileImageResizedByteData = new Byte[mstreamResizedImage.Length];
                    mstreamResizedImage.Position = 0;
                    mstreamResizedImage.Read(fileImageResizedByteData, 0, (int)mstreamResizedImage.Length);
                    #endregion
                }
            }
            else
            {
                if (
                    (imgCat.ImageUrl.IndexOf("blank_img.png") > 0)
                    || (imgCat.ImageUrl == String.Empty)
                   )
                {
                    ShowError(lblErrMsg, "Upload an image for category.");
                    return;
                }
            }

            // Save data to Database
            extension = ".png";
            SaveData(fileImageResizedByteData, extension, isEnabledCategory);
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect("Category.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Category.aspx");
    }

    protected Boolean getIsEnabledCategory()
    {
        Int32 defaultValue = 0;
        Int32 resellerID = Util.GetDataInt32(Util.getValue(ddlReseller));
        Int32 resellerLimit = getResellerLimitByReseller(resellerID);
        Int32 availableCategory = getAvailableCategoryByReseller(resellerID, out defaultValue, -1);
        Boolean isEnabled = false;

        if (resellerLimit > availableCategory)
            isEnabled = true;

        return isEnabled;
    }

    private Boolean getIsDisabledCategory()
    {
        Int32 categoryID = 0;
        Int32 resellerID = Util.GetDataInt32(Util.getValue(ddlReseller));
        Int32 resellerMinimumLimit = getMinResellerLimitByReseller(resellerID);
        Int32 availableMinimumCategory = getAvailableCategoryByReseller(resellerID, out categoryID, resellerMinimumLimit);
        Boolean isDisabled = false;

        if (
                (resellerMinimumLimit == availableMinimumCategory)
            && (CategoryID == categoryID)
            && (!chkIsEnabled.Checked)
           )
            return isDisabled;
        else if (resellerMinimumLimit <= availableMinimumCategory)
            isDisabled = true;

        return isDisabled;
    }

    private Int32 getAvailableCategoryByReseller(Int32 resellerID, out Int32 categoryID, Int32 minimumLimit)
    {
        categoryID = 0;
        Data.Category cat = new Data.Category();
        DataTable dtCategory = cat.GetCategoryListByReseller(resellerID).Tables[0];
        DataRow[] drCategory = dtCategory.Select("IsEnabled=1");

        if (drCategory.Length == minimumLimit)
        {
            DataRow[] drMatchCategory = dtCategory.Select("IsEnabled = 1 and CategoryID = " + CategoryID);
            if (drMatchCategory.Length > 0)
                categoryID = Util.GetDataInt32(drMatchCategory[0]["categoryid"].ToString());
            else
                categoryID = Util.GetDataInt32(drCategory[0]["categoryid"].ToString());
        }

        return drCategory.Length;
    }

    private Int32 getResellerLimitByReseller(Int32 resellerID)
    {
        Data.ResellerSetting rs = new Data.ResellerSetting();
        DataTable dtReseller = rs.GetSetting(resellerID).Tables[0];

        return Util.GetDataInt32(dtReseller, 0, "maxcatlimit");
    }

    private Int32 getMinResellerLimitByReseller(Int32 resellerID)
    {
        Data.ResellerSetting rs = new Data.ResellerSetting();
        DataTable dtReseller = rs.GetSetting(resellerID).Tables[0];

        return Util.GetDataInt32(dtReseller, 0, "mincatlimit");
    }

    protected void btnImport_Click(object sender, EventArgs e)
    {
        try
        {
            XmlTextReader reader = new XmlTextReader(RssFeed);

            DataSet ds = new DataSet();
            ds.ReadXml(reader);

            StringBuilder failed = new StringBuilder();
            int k = 0;
            int nrImported = 0;
            if (ds != null && ds.Tables.Count > 0 && ds.Tables.Contains("item") && ds.Tables["item"].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables["item"].Rows)
                {
                    k++;
                    string tmp = SaveProduct(dr);
                    if (tmp.Length > 0)
                    {
                        failed.Append(tmp);
                        failed.Append("</br>");
                    }
                    else
                    {
                        nrImported++;
                    }
                    if (k == 20000)
                        break;
                }
            }
            else
            {
                lblImportMsg.Text = "No products were found in the provided RSS Feed!";
                return;
            }
            string message = string.Format("{0} products out of {1} were imported successfully.", nrImported, ds.Tables["item"].Rows.Count);
            if (failed.Length > 0)
            {
                lblImportMsg.Text = message + "</br>Some of the products were not imported:</br>" + failed.ToString();
            }
            else
            {
                lblImportMsg.Text = message;
            }

        }
        catch (Exception ex)
        {
            lblImportMsg.Text = "Error connecting to the provided RSS Feed. Make sure the url is valid and existing!";
            //lblImportMsg.Text = ex.Message + ex.StackTrace;            
            return;
        }
    }

    private string SaveProduct(DataRow dr)
    {
        StringBuilder retVal = new StringBuilder();
        try
        {
            string sourceID = "";
            try
            {
                sourceID = dr["id"].ToString();
                retVal.Append("id=" + sourceID);
            }
            catch { }
            try
            {
                retVal.Append("  sku=" + dr["sku"].ToString());
            }
            catch { }
            try
            {
                retVal.Append("  title=" + Server.HtmlDecode(dr["title"].ToString()));
            }
            catch { }

            if (retVal.Length == 0)
                return "Unable to identify Product.";

            byte[] ProductFileByteData = null;
            byte[] ThumbFileByteData = null;
            byte[] var_ProductFileByteData = null;
            byte[] var_ThumbFileByteData = null;
            string productSKU = dr["sku"].ToString().Split('|')[0];
            string variationSKU = dr["sku"].ToString().Split('|')[1];
            if (variationSKU.Trim().Length == 0)
                variationSKU = productSKU;
            Data.Product product = new Data.Product();
            DataSet dsProd = product.Product_GetByResellerAndSourceId(Int32.Parse(Util.getValue(ddlReseller)), "", productSKU);
            Int64 productID = 0;
            try
            {
                productID = Int64.Parse(dsProd.Tables[0].Rows[0]["ProductID"].ToString());
            }
            catch { }
            if (productID > 0)
            {
                // this is a duplicate product
                return "Duplicate product SKUNo=" + dr["sku"].ToString() + "  - Not Imported!";
            }
            string productSourceId = sourceID.Split('-')[0];
            dsProd = product.Product_GetByResellerAndSourceId(Int32.Parse(Util.getValue(ddlReseller)), productSourceId, productSKU);
            try
            {
                productID = Int64.Parse(dsProd.Tables[0].Rows[0]["ProductID"].ToString());
            }
            catch { }

            BuildPhoto(dr["image_link"].ToString(), ref ProductFileByteData, ref ThumbFileByteData);

            if (productID > 0)
                product.LoadID(productID);
            product.ParentProductID.value = 0;
            product.ResellerID.value = v.ResellerID;
            product.SourceId.value = productSourceId;
            product.ProductSource.value = 1;
            product.ProductName.value = Server.HtmlDecode(dr["title"].ToString().Split('-')[0]);
            product.Color.value = "";
            product.ShowColorInDevice.value = false;
            product.Description.value = Server.HtmlDecode(dr["description"].ToString());

            if (ProductFileByteData != null && ThumbFileByteData != null)
            {
                product.ProductImage.value = ProductFileByteData;
                product.ProductImageExt.value = ".png";
                product.ProductThumb.value = ThumbFileByteData;
                product.ProductThumbExt.value = ".png";
            }

            product.ProductPrice.value = Convert.ToDecimal(dr["price"].ToString().ToLower().Replace("usd", ""));
            product.IsUpchargeActive.value = false;

            product.IsFloristToFlorist.value = false;
            product.IsDirectShip.value = false;
            if (product.ProductID.value <= 0)
                product.IsPublished.value = false;

            if (productID <= 0)
            {
                product.DateAdded.value = Util.GetServerDate();
                product.AddedBy.value = v.UserID;
            }
            product.DateModified.value = Util.GetServerDate();
            product.ModifiedBy.value = v.UserID;

            if (product.ProductID.value <= 0)
                product.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
            product.SKUNo.value = productSKU;
            product.Save();

            #region Updating Images...

            String filePath = Server.MapPath(@"~/DynImages\Product\");
            String fileProductImagePath = String.Format("ProductImage_{0}{1}", product.ProductID.value.ToString(), product.ProductImageExt.value);
            String fileProductThumbPath = String.Format("ProductThumb_{0}{1}", product.ProductID.value.ToString(), product.ProductThumbExt.value);

            if (ProductFileByteData != null)
            {
                if (File.Exists(filePath + fileProductImagePath))
                    File.Delete(filePath + fileProductImagePath);

                FileStream fs = new FileStream(filePath + fileProductImagePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(ProductFileByteData, 0, ProductFileByteData.Length);
                fs.Close();
            }

            filePath += fileProductThumbPath;

            if (ThumbFileByteData != null)
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);

                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(ThumbFileByteData, 0, ThumbFileByteData.Length);
                fs.Close();
            }

            #endregion Eng of Binding images..

            // Delete from ProductCategory by ProductID
            Data.ProductCategory pcat = new Data.ProductCategory();
            pcat.DeleteByProductAndCategory(productID, CategoryID);

            // Save selected Categories [ProductCategory] table.
            pcat = new Data.ProductCategory();
            pcat.ProductID.value = product.ProductID.value;
            pcat.CategoryID.value = CategoryID;
            pcat.DateAdded.value = Util.GetServerDate();
            pcat.AddedBy.value = v.UserID;
            pcat.DateModified.value = Util.GetServerDate();
            pcat.ModifiedBy.value = v.UserID;
            pcat.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
            pcat.Save();

            #region Log in ServerUpdates
            //Save in ServerUpdates
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatus(product.ResellerID.value, "product");
            #endregion

            #region Product Variation

            Data.Product variation = new Data.Product();
            Int64 variationID = 0;
            string variationSourceId = sourceID.Split('-')[1];
            dsProd = variation.Variation_GetByResellerAndSourceId(Int32.Parse(Util.getValue(ddlReseller)), variationSourceId, variationSKU);
            try
            {
                variationID = Int64.Parse(dsProd.Tables[0].Rows[0]["ProductID"].ToString());
            }
            catch { }

            if (!string.IsNullOrEmpty(dr["var_image_link"].ToString()))
                BuildPhoto(dr["var_image_link"].ToString(), ref var_ProductFileByteData, ref var_ThumbFileByteData);

            if (variationID > 0)
                variation.LoadID(variationID);
            variation.ParentProductID.value = (int)product.ProductID.value;
            variation.ResellerID.value = v.ResellerID;
            variation.SourceId.value = variationSourceId;
            variation.ProductSource.value = 1;
            variation.ProductName.value = Server.HtmlDecode(dr["title"].ToString().Split('-')[1]);
            variation.Color.value = "";
            variation.ShowColorInDevice.value = false;
            variation.Description.value = Server.HtmlDecode(dr["description"].ToString());

            if (var_ProductFileByteData != null && var_ThumbFileByteData != null)
            {
                variation.ProductImage.value = var_ProductFileByteData;
                variation.ProductImageExt.value = ".png";
                variation.ProductThumb.value = var_ThumbFileByteData;
                variation.ProductThumbExt.value = ".png";
            }
            else if (ProductFileByteData != null && ThumbFileByteData != null)
            {
                variation.ProductImage.value = ProductFileByteData;
                variation.ProductImageExt.value = ".png";
                variation.ProductThumb.value = ThumbFileByteData;
                variation.ProductThumbExt.value = ".png";
            }

            variation.ProductPrice.value = Convert.ToDecimal(dr["price"].ToString().ToLower().Replace("usd", ""));
            variation.IsUpchargeActive.value = false;

            variation.IsFloristToFlorist.value = false;
            variation.IsDirectShip.value = false;
            if (variation.ProductID.value <= 0)
                variation.IsPublished.value = false;

            if (variationID <= 0)
            {
                variation.DateAdded.value = Util.GetServerDate();
                variation.AddedBy.value = v.UserID;
            }
            variation.DateModified.value = Util.GetServerDate();
            variation.ModifiedBy.value = v.UserID;

            if (variation.ProductID.value <= 0)
                variation.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
            variation.SKUNo.value = variationSKU;
            variation.Save();

            #region Updating Var Images...

            filePath = Server.MapPath(@"~/DynImages\Product\");
            fileProductImagePath = String.Format("ProductImage_{0}{1}", variation.ProductID.value.ToString(), variation.ProductImageExt.value);
            fileProductThumbPath = String.Format("ProductThumb_{0}{1}", variation.ProductID.value.ToString(), variation.ProductThumbExt.value);

            if (var_ProductFileByteData != null)
            {
                if (File.Exists(filePath + fileProductImagePath))
                    File.Delete(filePath + fileProductImagePath);

                FileStream fs = new FileStream(filePath + fileProductImagePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(var_ProductFileByteData, 0, var_ProductFileByteData.Length);
                fs.Close();
            }
            else if (ProductFileByteData != null)
            {
                if (File.Exists(filePath + fileProductImagePath))
                    File.Delete(filePath + fileProductImagePath);

                FileStream fs = new FileStream(filePath + fileProductImagePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(ProductFileByteData, 0, ProductFileByteData.Length);
                fs.Close();
            }

            filePath += fileProductThumbPath;

            if (var_ThumbFileByteData != null)
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);

                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(var_ThumbFileByteData, 0, var_ThumbFileByteData.Length);
                fs.Close();
            }
            else if (ThumbFileByteData != null)
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);

                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(ThumbFileByteData, 0, ThumbFileByteData.Length);
                fs.Close();
            }

            #endregion Eng of Binding images..

            #endregion

            return "";
        }
        catch (Exception ex)
        {
            retVal.Append(" not Imported! - Image Link problem.");
            Data.ErrorLog el = new Data.ErrorLog();
            el.ModuleInfo.value = "Gravity Import Product: SaveProduct()";
            el.Message.value = "Message=" + ex.Message + " Stack=" + ex.StackTrace;
            el.SmallMessage.value = "Error importing product.";
            el.ApplicationType.value = Convert.ToInt32(AppEnum.ApplicationType.WEBSITE);
            el.DateAdded.value = DateTime.Now;
            el.Save();
        }
        return retVal.ToString();
    }

    private void BuildPhoto(string link, ref byte[] fileImageResizedByteData, ref byte[] fileThumbByteData)
    {
        byte[] ProductFileByteData = null;
        WebClient webClient = new WebClient();
        ProductFileByteData = webClient.DownloadData(link);

        MemoryStream memProductStream = new MemoryStream(ProductFileByteData);
        System.Drawing.Image imgProductObject = System.Drawing.Image.FromStream(memProductStream);

        // Prevent using images internal thumbnail
        imgProductObject.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
        imgProductObject.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

        Int32 imgProductHeight = imgProductObject.Height;
        Int32 imgProductWidth = imgProductObject.Width;

        #region Resize Image
        const int MAX_WIDTH = 600;
        const int MAX_HEIGHT = 600;

        int NewHeight = imgProductHeight * MAX_WIDTH / imgProductWidth;
        int NewWidth = MAX_WIDTH;

        if (NewHeight > MAX_HEIGHT)
        {
            // Resize with height instead
            NewWidth = imgProductWidth * MAX_HEIGHT / imgProductHeight;
            NewHeight = MAX_HEIGHT;
        }

        System.Drawing.Image imgResized = imgProductObject.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

        MemoryStream mstreamResizedImage = new MemoryStream();

        imgResized.Save(mstreamResizedImage, System.Drawing.Imaging.ImageFormat.Png);
        fileImageResizedByteData = new Byte[mstreamResizedImage.Length];
        mstreamResizedImage.Position = 0;
        mstreamResizedImage.Read(fileImageResizedByteData, 0, (int)mstreamResizedImage.Length);
        #endregion

        #region Create Thumb image...

        const int MAX_THUMB_WIDTH = 75;
        const int MAX_THUMB_HEIGHT = 75;

        int newThumbHeight = imgProductHeight * MAX_THUMB_WIDTH / imgProductWidth;
        int newThumbWidth = MAX_THUMB_HEIGHT;

        if (newThumbHeight > MAX_THUMB_HEIGHT)
        {
            // Resize with height instead
            newThumbWidth = imgProductWidth * MAX_THUMB_HEIGHT / imgProductHeight;
            newThumbHeight = MAX_THUMB_HEIGHT;
        }

        System.Drawing.Image imgThumbs = imgProductObject.GetThumbnailImage(newThumbWidth, newThumbHeight, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);
        MemoryStream mstreamThumb = new MemoryStream();

        imgThumbs.Save(mstreamThumb, System.Drawing.Imaging.ImageFormat.Png);
        fileThumbByteData = new Byte[mstreamThumb.Length];
        mstreamThumb.Position = 0;
        mstreamThumb.Read(fileThumbByteData, 0, (int)mstreamThumb.Length);
        #endregion End of byte
    }

    private bool ThumbnailCallback()
    {
        return true;
    }
}


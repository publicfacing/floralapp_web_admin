﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="Reminders.aspx.cs" Inherits="Admin_Reminders" Title="floralapp&reg; | Reminders"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/javascript">
        function DateTimeFormat(sender, args) {
            var d = sender._selectedDate;
            var now = new Date();
            sender.get_element().value = d.format("MM/dd/yyyy")
        } 
    </script>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <asp:Label ID="LabelAuthenticateError" runat="server" Text="Label" Visible="false"></asp:Label>
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12" id="contentTable" runat="server">
                    <tr>
                        <th class="TableHeadingBg TableHeading">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                text-indent: 5px;">
                                <tr>
                                    <td>
                                        Reminders
                                    </td>
                                    <td style="text-align: right; padding-right: 5px; text-transform: capitalize;">
                                        <asp:HyperLink ID="lnkAddReminder" runat="server" NavigateUrl="~/Admin/AddReminder.aspx"
                                            ToolTip="Add Reminder">Add Reminder </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label">
                                            Search :
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="Select Country" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="Label1" CssClass="label" runat="server" Text="Date Added:"></asp:Label>
                                            <asp:TextBox runat="server" Width="70px" OnKeyUP="this.value=''" CssClass="textbox"
                                                ID="txtStartDate"></asp:TextBox>
                                            <ajax:CalendarExtender ID="ceStartDate" OnClientDateSelectionChanged="DateTimeFormat"
                                                Format="MM/dd/yyyy" runat="server" TargetControlID="txtStartDate" PopupButtonID="imgCalIcon1">
                                            </ajax:CalendarExtender>
                                            <asp:Image ImageUrl="~/Images/calender-icon.gif" Style="cursor: hand" ID="imgCalIcon1"
                                                runat="server" />
                                            &nbsp;&nbsp;
                                            <asp:TextBox runat="server" Width="70px" OnKeyUP="this.value=''" CssClass="textbox"
                                                ID="txtEndDate"></asp:TextBox>
                                            <ajax:CalendarExtender Format="MM/dd/yyyy" OnClientDateSelectionChanged="DateTimeFormat"
                                                ID="ceEndDate" runat="server" TargetControlID="txtEndDate" PopupButtonID="imgCalIcon2">
                                            </ajax:CalendarExtender>
                                            <asp:Image ImageUrl="~/Images/calender-icon.gif" ID="imgCalIcon2" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSearchField" runat="server" AutoPostBack="false">
                                                <asp:ListItem Text="ID" Value="ReminderDefID"></asp:ListItem>
                                                <asp:ListItem Text="Title" Value="Title"></asp:ListItem>
                                                <asp:ListItem Text="Description" Value="Description"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchOperator" runat="server">
                                                <asp:ListItem Value="Begins With">Begins With</asp:ListItem>
                                                <asp:ListItem Value="Ends With">Ends With</asp:ListItem>
                                                <asp:ListItem Value="Contains">Contains</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnFilter" class="btnFilter" runat="server" Text="Filter" CausesValidation="false"
                                                OnClick="btnFilter_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpTop" runat="server" PageSize="25" PagedControlID="gvReminders">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <mo:ExtGridView ID="gvReminders" runat="server" DataKeyNames="ReminderDefID" AllowSorting="true"
                                SkinID="gvskin" AutoGenerateColumns="false" OnSorting="gvReminders_OnSorting"
                                OnPageIndexChanging="gvReminders_OnPageIndexChanging" EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="ReminderDefID" SortExpression="ReminderDefID"
                                        ItemStyle-CssClass="gridContent" />
                                    <asp:TemplateField HeaderText="Country" HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridContent">
                                        <ItemTemplate>
                                            <%# GetCountryName(Eval("CountryID").ToString())%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Title" DataField="Title" SortExpression="Title" ItemStyle-CssClass="gridContent" />
                                    <asp:BoundField HeaderText="Description" DataField="Description" SortExpression="Description"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-CssClass="gridHeader" />
                                    <asp:BoundField HeaderText="Date" DataField="Date" SortExpression="Date" HeaderStyle-Width="70px"
                                        DataFormatString="{0:MM/dd/yyyy}" ItemStyle-CssClass="gridContent" ItemStyle-VerticalAlign="Top" />
                                    <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="gridHeader" HeaderStyle-Width="50px"
                                        ItemStyle-CssClass="gridContent">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("ReminderDefID") %>'
                                                CausesValidation="false" OnClick="btnEdit_Click" ToolTip="Edit" ImageUrl="~/Images/edit-icon.jpg" />
                                            <asp:ImageButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("ReminderDefID")%>'
                                                CausesValidation="false" OnClick="btnDelete_Click" ImageUrl="~/Images/DeleteButton.jpg"
                                                OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotalReminders" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="25" PagedControlID="gvReminders">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="UpsellProduct.aspx.cs" Inherits="Admin_UpsellProduct" Title="floralapp&reg; | Upsell Products"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">

    <script language="javascript" type="text/javascript">
        function DateTimeFormat(sender, args)
        {
            var d = sender._selectedDate;
            var now = new Date();
            sender.get_element().value = d.format("MM/dd/yyyy")
        }
    </script>

    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12">
                    <tr>
                        <th class="TableHeadingBg TableHeading">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                text-indent: 5px;">
                                <tr>
                                    <td>
                                        Upsell Products
                                    </td>
                                    <td style="text-align: right; padding-right: 5px; text-transform: capitalize;">
                                        <asp:HyperLink ID="lnkAddProduct" runat="server" NavigateUrl="~/Admin/AddUpsellProduct.aspx"
                                            ToolTip="Add UpsellProduct">Add Upsell Product </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label">
                                            Search :
                                        </td>
                                        <td>
                                            <label class="label" runat="server" id="lblShop">
                                                Shop:</label>
                                            <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                                OnSelectedIndexChanged="ddlReseller_SelectedIndexChanged">
                                                <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="Label1" CssClass="label" runat="server" Text="Date Added:"></asp:Label>
                                            <asp:TextBox runat="server" Width="70px" OnKeyUP="this.value=''" CssClass="textbox"
                                                ID="txtStartDate"></asp:TextBox>
                                            <ajax:CalendarExtender ID="ceStartDate" OnClientDateSelectionChanged="DateTimeFormat"
                                                Format="MM/dd/yyyy" runat="server" TargetControlID="txtStartDate" PopupButtonID="imgCalIcon1">
                                            </ajax:CalendarExtender>
                                            <asp:Image ImageUrl="~/Images/calender-icon.gif" Style="cursor: hand" ID="imgCalIcon1"
                                                runat="server" />
                                            &nbsp;&nbsp;
                                            <asp:TextBox runat="server" Width="70px" OnKeyUP="this.value=''" CssClass="textbox"
                                                ID="txtEndDate"></asp:TextBox>
                                            <ajax:CalendarExtender Format="MM/dd/yyyy" OnClientDateSelectionChanged="DateTimeFormat"
                                                ID="ceEndDate" runat="server" TargetControlID="txtEndDate" PopupButtonID="imgCalIcon2">
                                            </ajax:CalendarExtender>
                                            <asp:Image ImageUrl="~/Images/calender-icon.gif" ID="imgCalIcon2" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSearchField" runat="server" AutoPostBack="false">
                                                <asp:ListItem Text="SKU#" Value="SKUNo"></asp:ListItem>
                                                <asp:ListItem Text="ID" Value="UpsellProductID"></asp:ListItem>
                                                <asp:ListItem Text="Product" Value="ProductName"></asp:ListItem>
                                                <asp:ListItem Text="Description" Value="Description"></asp:ListItem>
                                                <asp:ListItem Text="Price" Value="ProductPrice"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchOperator" runat="server">
                                                <asp:ListItem Value="Begins With">Begins With</asp:ListItem>
                                                <asp:ListItem Value="Ends With">Ends With</asp:ListItem>
                                                <asp:ListItem Value="Contains">Contains</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnFilter" CssClass="btnFilter" runat="server" Text="Filter" CausesValidation="false"
                                                OnClick="btnFilter_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <asp:DataPager ID="dpTop" runat="server" PageSize="25" PagedControlID="gvProducts">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                        ShowLastPageButton="false" ShowNextPageButton="false" />
                                    <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                        NextPreviousButtonCssClass="command" />
                                    <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                        ShowLastPageButton="true" ShowNextPageButton="true" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <mo:ExtGridView ID="gvProducts" runat="server" DataKeyNames="UpsellProductID" AllowSorting="true"
                                SkinID="gvskin" OnSorting="gvProducts_OnSorting" OnPageIndexChanging="gvProducts_OnPageIndexChanging"
                                EmptyDataText="No record(s) found.">
                                <Columns>
                                    <%--  <asp:TemplateField HeaderStyle-CssClass="gridHeader" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="20px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnPreview" runat="server" CommandArgument='<%# Eval("UpsellProductID") +","+ Eval("UpsellCategoryID")%>'
                                                CausesValidation="false" OnClick="btnPreview_Click" ToolTip="Product Preview"
                                                ImageUrl="~/Images/Pop-Up.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:BoundField HeaderText="ID" DataField="UpsellProductID" SortExpression="UpsellProductID"
                                        ItemStyle-CssClass="gridContent" />
                                    <asp:BoundField HeaderText="SKU#" DataField="SKUNo" SortExpression="SKUNo" ItemStyle-CssClass="gridContent" />
                                    <asp:TemplateField HeaderText="Product Name" SortExpression="ProductName" HeaderStyle-CssClass="gridHeader"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="150px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("ProductName")%>'>
                                            </asp:Label>
                                            <br />
                                            <i>(<asp:Label ID="lblCategory" runat="server" Style="font-weight: bold;" Text='<%# Eval("CategoryName")%>'></asp:Label>)</i>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" ItemStyle-CssClass="gridContent" HeaderStyle-CssClass="gridHeader">
                                        <ItemTemplate>
                                            <table style="width: 99%">
                                                <tr>
                                                  <td style="text-align: left; vertical-align: top">
                                                        <asp:Image ID="imgProduct" Style="border: 2px solid #ffcc00;" runat="server" Visible="true"
                                                            ImageUrl='<%# getThumbImage(Eval("UpsellProductID").ToString() + Eval("ProductThumbExt").ToString()) %>'
                                                            mageAlign="Top" />
                                                    </td>
                                                    <td style="text-align: left; padding-left: 5px; vertical-align: top">
                                                        <asp:Label ID="lblDescription" CssClass="label" runat="server" Text='<%# Eval("Description")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                     
                                    <asp:BoundField HeaderText="Price" DataField="ProductPrice" SortExpression="Productprice"
                                        ItemStyle-CssClass="gridContent halign-right" HeaderStyle-Width="80px" DataFormatString="{0:C}" />
                                    <asp:TemplateField HeaderText="Published" SortExpression="IsPublished" HeaderStyle-CssClass="gridHeader"
                                        HeaderStyle-Width="60px" ItemStyle-CssClass="gridContent halign-center">
                                        <ItemTemplate>
                                            <asp:Image ID="imgIsPublished" runat="server" ImageUrl='<%# Eval("IsPublished").ToString().Equals("True")?"~/Images/tick.png":"~/Images/tick-gray.png" %>'
                                                ToolTip="Published" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="gridHeader" HeaderStyle-Width="60px"
                                        ItemStyle-CssClass="gridContent">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("UpsellProductID") +","+ Eval("UpsellProdCatID")  %>'
                                                CausesValidation="false" OnClick="btnEdit_Click" ToolTip="Edit" ImageUrl="~/Images/edit-icon.jpg" />
                                            <asp:ImageButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("UpsellProductID")%>'
                                                CausesValidation="false" OnClick="btnDelete_Click" ImageUrl="~/Images/DeleteButton.jpg"
                                                OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotalProducts" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="25" PagedControlID="gvProducts">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;
using System.IO;
using System.Drawing;

public partial class Admin_Products : AppPage
{
    AppVars v = new AppVars();
    private int rowCount = 0;
    private int totalRowCount = 0;

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            InitializeControls();
            BindData();
        }
    }

    private void InitializeControls()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");

        Util.setValue(ddlReseller, v.ResellerID);
        if (ddlReseller.SelectedIndex > 0)
        {
            ddlCategory.Items.Clear();
            // Category DropDownList
            Data.Category cat = new Data.Category();
            DataSet dsReseller = cat.GetCategoryListByReseller(Util.GetDataInt32(Util.getValue(ddlReseller)));
            Util.setValue(ddlCategory, dsReseller, "CategoryName", "CategoryID");
        }
        else
            ddlCategory.Items.Clear();

        ddlCategory.Items.Insert(0, "Select Category");

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            lblShop.Visible = false;
            ddlReseller.Visible = false;
            //ddlReseller.Enabled = false;
        }
    }

    private void BindData()
    {
        //if (!CanAddNewProduct() && v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        //    lnkAddProduct.Visible = false;

        Util.setValue(gvProducts, GetProductList());

        int prevProdID = 0;
        int nextProdID = 0;
        int currentProdID = 0;
        int lastColoredID = 0;
        bool newColor = false;
        for (int i = 0; i < gvProducts.Rows.Count; i++)
        {
            try
            {
                currentProdID = int.Parse(gvProducts.DataKeys[i].Value.ToString());
            }
            catch
            {
                currentProdID = -1;
            }
            try
            {
                prevProdID = int.Parse(gvProducts.DataKeys[i - 1].Value.ToString());
            }
            catch
            {
                prevProdID = 0;
            }
            try
            {
                nextProdID = int.Parse(gvProducts.DataKeys[i + 1].Value.ToString());
            }
            catch
            {
                nextProdID = 0;
            }

            if (prevProdID == currentProdID || currentProdID == nextProdID)
            {
                if (lastColoredID != currentProdID)
                {
                    lastColoredID = currentProdID;
                    newColor = !newColor;
                }

                gvProducts.Rows[i].BackColor = newColor ? Color.FromArgb(207, 234, 251) : Color.LightSeaGreen;
            }
        }
    }

    private DataSet GetProductList()
    {
        StringBuilder sbFilter = new StringBuilder();

        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();

        if (!String.IsNullOrEmpty(searchString))
        {
            switch (searchOperator)
            {
                case "Begins With":
                    sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                    break;
                case "Ends With":
                    sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                    break;
                case "Contains":
                    sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                    break;
                case "Equals":
                    sbFilter.Append(" And " + searchField + " = " + searchString);
                    break;
                default:
                    break;
            }
        }

        Int32 resellerID = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? Convert.ToInt32(v.ResellerID) : Convert.ToInt32(Util.getValue(ddlReseller));

        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);

        if (txtStartDate.Text != "")
            sbFilter.Append(" And  DateAdded  >='" + Convert.ToDateTime(txtStartDate.Text).ToShortDateString() + "' ");
        if (txtEndDate.Text != "")
            sbFilter.Append(" And  DateAdded  <='" + Convert.ToDateTime(txtEndDate.Text).ToShortDateString() + "' ");

        //if (ddlCategory.SelectedIndex > 0)
        //    sbFilter.Append(" And CategoryID =" + Util.getValue(ddlCategory));
        if (ddlCategory.SelectedIndex > 0)
            sbFilter.Append(" and dbo.IsInCategory(productid," + Util.getValue(ddlCategory) + ") = 1 ");

        String orderBy = gvProducts.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " SKUNo asc ";

        Data.Product p = new Data.Product();
        DataSet dsProducts = p.GetList(gvProducts.PageIndex, gvProducts.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvProducts.VirtualItemCount = totalRowCount;

        #region Unused
        /*
        #region Binding thumb image...
        foreach (DataRow drProd in dsProducts.Tables[0].Rows)
        {
            Byte[] fileByteData = (byte[])drProd["ProductThumb"];
            String imgThumbExt = Convert.ToString(drProd["ProductThumbExt"]);
            Int32 prodID = Convert.ToInt32(drProd["ProductID"]);

            String filePath = Server.MapPath(@"~/DynImages\Product\");
            String fileName = String.Format("ProductThumb_{0}{1}", prodID, imgThumbExt);
            filePath += fileName;

            if (File.Exists(filePath))
                File.Delete(filePath);

            if (fileByteData != null)
            {
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteData, 0, fileByteData.Length);
                fs.Close();
            }
        }
        #endregion End of binding image

        #region Binding original image...
        foreach (DataRow drProd in dsProducts.Tables[0].Rows)
        {
            Byte[] fileByteData = (byte[])drProd["ProductImage"];
            String imgThumbExt = Convert.ToString(drProd["ProductImageExt"]);
            Int32 prodID = Convert.ToInt32(drProd["ProductID"]);

            String filePath = Server.MapPath(@"~/DynImages\Product\");
            String fileName = String.Format("ProductImage_{0}{1}", prodID, imgThumbExt);
            filePath += fileName;

            if (File.Exists(filePath))
                File.Delete(filePath);

            if (fileByteData != null)
            {
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteData, 0, fileByteData.Length);
                fs.Close();
            }
        }
        #endregion End of binding image
        */
        #endregion Unused

        // Set Total Count in grid footer
        Util.setValue(lblTotalProducts, String.Format("{0} product(s).", totalRowCount));

        return dsProducts;
    }

    private Boolean CanAddNewProduct()
    {
        Int32 resellerID = Int32.Parse(Util.getValue(this.ddlReseller));
        Data.Reseller rs = new Data.Reseller();
        return rs.CanAddNewProduct(resellerID);
    }

    protected void gvProducts_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void gvProducts_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvProducts.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void btnPreview_Click(object sender, EventArgs e)
    {
        String redirectUrl = "ManageProducts.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String[] value = objButton.CommandArgument.ToString().Split(',');
            String productID = value[0].ToString();
            String qs = Crypto.EncryptQueryString("ProductID=" + productID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddProduct.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            string[] value = objButton.CommandArgument.ToString().Split(',');
            String productID = value[0].ToString();
            String ProdUpchargeID = value[1].ToString();

            String qs = Crypto.EncryptQueryString("ProductID=" + productID + " &ProdUpchargeID=" + ProdUpchargeID);

            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int64 productID = Convert.ToInt64(objButton.CommandArgument.ToString());

            Data.Product p = new Data.Product(productID);
            p.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
            p.DateDeleted.value = Util.GetServerDate();
            p.DeletedBy.value = Convert.ToInt64(v.UserID);
            p.Save();

            #region Inactivate/Delete the Relationship table(s).

            p.InActiveRelationshipTables(productID, "Product");

            #endregion End of inactivate/delete process.

            #region Insert/Update on Server
            //Save in ServerUpdates
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatus(p.ResellerID.value, "product");
            #endregion

            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void ddlReseller_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlReseller.SelectedIndex > 0)
        {
            ddlCategory.Items.Clear();
            // Category DropDownList
            Data.Category cat = new Data.Category();
            DataSet dsReseller = cat.GetCategoryListByReseller(Util.GetDataInt32(Util.getValue(ddlReseller)));
            Util.setValue(ddlCategory, dsReseller, "CategoryName", "CategoryID");
        }
        else
            ddlCategory.Items.Clear();
        ddlCategory.Items.Insert(0, "Select Category");
        BindData();

    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindData();
    }

    protected void btnApply_Click(object sender, EventArgs e)
    {
        try
        {

            Decimal currPrice = 0, newPrice = 0;
            Decimal percentUpcharge = Util.GetDataDecimal(txtUpchargePercentage.Text);

            CheckBox chkUpcharge = new CheckBox();
            CheckBox chkUpchargeAll = (CheckBox)gvProducts.HeaderRow.Cells[9].FindControl("chkUpchargeAll");

            for (int i = 0; i < gvProducts.Rows.Count; i++)
            {
                chkUpcharge = (CheckBox)gvProducts.Rows[i].Cells[9].FindControl("chkUpcharge");

                if (chkUpcharge.Checked)
                {
                    currPrice = Util.GetDataDecimal(gvProducts.Rows[i].Cells[5].Text.Replace("$", ""));
                    newPrice = Product_GetNewPrice(currPrice, percentUpcharge);

                    #region Updating record(s) in [ProductUpcharge] Table for upcharge percentage...
                    Data.ProductUpcharge pu = new Data.ProductUpcharge();

                    pu.LoadID(Util.GetDataInt64(chkUpcharge.TabIndex.ToString()));
                    pu.ProductID.value = Util.GetDataInt64(gvProducts.Rows[i].Cells[1].Text);
                    pu.UpchargePercentage.value = Convert.ToDouble(percentUpcharge);
                    pu.ProductPrice.value = currPrice;
                    pu.NewProductPrice.value = newPrice;
                    pu.Description.value = string.Empty;
                    pu.DateAdded.value = Util.GetServerDate();
                    pu.AddedBy.value = v.UserID;
                    pu.DateModified.value = Util.GetServerDate();
                    pu.ModifiedBy.value = v.UserID;
                    pu.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
                    pu.Save();

                    #endregion End of updating with [ProductUpcharge] Table.
                }
            }
            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }
    }

    private Decimal Product_GetNewPrice(Decimal currentPrice, Decimal percent)
    {
        Decimal newPrice = 0;
        newPrice = currentPrice * (percent / 100);
        newPrice += currentPrice;
        return Util.GetDataDecimal(newPrice.ToString("N2"));
    }

    protected void chkUpchargeAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkUpchargeAll = (CheckBox)gvProducts.HeaderRow.Cells[9].FindControl("chkUpchargeAll");

        for (int i = 0; i < gvProducts.Rows.Count; i++)
        {
            if (chkUpchargeAll.Checked)
                ((CheckBox)gvProducts.Rows[i].Cells[9].FindControl("chkUpcharge")).Checked = true;
            else
                ((CheckBox)gvProducts.Rows[i].Cells[9].FindControl("chkUpcharge")).Checked = false;
        }
    }

    public String getThumbImage(String fileExtension)
    {
        String thumbPath = "../DynImages/Product/ProductThumb_" + fileExtension;

        if (!File.Exists(Server.MapPath(thumbPath)))
            thumbPath = "~/Images/blank_img.png";

        return thumbPath;

    }
}


﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="SpecialOffers.aspx.cs" Inherits="Admin_SpecialOffers" Title="floralapp&reg; | Special Offers"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table class="tblCls1" border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12">
                    <tr>
                        <th class="TableHeadingBg TableHeading" colspan="2">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                text-indent: 5px;">
                                <tr>
                                    <td>
                                        Special Offers
                                    </td>
                                    <td style="text-align: right; padding-right: 5px; text-transform: capitalize;">
                                        <asp:HyperLink ID="lnkAddSpecialOffer" runat="server" NavigateUrl="~/Admin/AddSpecialOffer.aspx"
                                            ToolTip="Add Special Offer">Add Special Offer</asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label" valign="top">
                                            Search :
                                        </td>
                                        <td valign="top">
                                            <div runat="server" id="divShop">
                                                <label class="label">
                                                    Shop:</label>
                                                <asp:DropDownList ID="ddlResellers" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                            </div>
                                            <asp:DropDownList ID="ddlSearchField" runat="server" AutoPostBack="false">
                                                <asp:ListItem Text="Offer" Value="OfferTitle"></asp:ListItem>
                                                <asp:ListItem Text="ID" Value="SpecialOfferID"></asp:ListItem>
                                                <asp:ListItem Text="Description" Value="OfferDescription"></asp:ListItem>
                                                <asp:ListItem Text="Start Date" Value="OfferStartDate"></asp:ListItem>
                                                <asp:ListItem Text="End Date" Value="OfferEndDate"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchOperator" runat="server">
                                                <asp:ListItem Value="Begins With">Begins With</asp:ListItem>
                                                <asp:ListItem Value="Ends With">Ends With</asp:ListItem>
                                                <asp:ListItem Value="Contains">Contains</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnFilter" class="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click"
                                                CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <div style="float: left; width: 600px; text-align: left">
                                <asp:Button ID="btnSendNotification" OnClick="btnSendNotification_Click" class="btnGreen"
                                    runat="server" Text="Send Notification" CausesValidation="false" OnClientClick="return initPopup();" />
                                <span style="color: #535152">Sending notifications may take up to 5 minutes or more.
                                </span>
                            </div>
                            <asp:DataPager ID="dpTop" runat="server" PageSize="25" PagedControlID="gvSpecialOffers">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                        ShowLastPageButton="false" ShowNextPageButton="false" />
                                    <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                        NextPreviousButtonCssClass="command" />
                                    <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                        ShowLastPageButton="true" ShowNextPageButton="true" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <mo:ExtGridView ID="gvSpecialOffers" runat="server" DataKeyNames="SpecialOfferID"
                                AutoGenerateColumns="false" AllowSorting="True" OnSorting="gvSpecialOffers_OnSorting"
                                OrderBy="SpecialOfferID Desc" SkinID="gvskin" OnPageIndexChanging="gvSpecialOffers_OnPageIndexChanging"
                                EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:BoundField DataField="SpecialOfferID" SortExpression="SpecialOfferID" HeaderText="ID"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="OfferTitle" SortExpression="OfferTitle" HeaderText="Offer"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="120px"></asp:BoundField>
                                    <asp:BoundField DataField="OfferDescription" SortExpression="OfferDescription" HeaderText="Description"
                                        ItemStyle-CssClass="gridContent" HtmlEncode="false"></asp:BoundField>
                                    <asp:BoundField DataField="ResellerName" SortExpression="ResellerName" HeaderText="Shop"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="OfferStartDate" SortExpression="OfferStartDate" HeaderText="Start Date"
                                        ItemStyle-CssClass="gridContent" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OfferEndDate" SortExpression="OfferEndDate" HeaderText="End Date"
                                        ItemStyle-CssClass="gridContent" DataFormatString="{0:MM/dd/yyyy hh:mm:ss tt}">
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Notified" SortExpression="IsNotified" ItemStyle-CssClass="gridContent">
                                        <HeaderStyle CssClass="gridHeader" Width="40px" />
                                        <ItemTemplate>
                                            <img id="Img1" alt="" runat="server" src='<%# Eval("IsNotified").ToString().Equals("True")?"~/Images/tick.png":"~/Images/tick-gray.png" %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is Enabled" ItemStyle-CssClass="gridContent halign-center"
                                        HeaderStyle-Width="60px" SortExpression="OfferStatusID">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEnable" runat="server" CommandArgument='<%# Eval("SpecialOfferID") + "," + Eval("ResellerID")  %>'
                                                OnClick="lnkEnable_Click" ImageUrl='<%#  Eval("OfferStatusID").ToString().Equals("1")?"~/Images/tick.png":"~/Images/tick-gray.png" %>'>
                                            </asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="gridContent" HeaderStyle-CssClass="gridHeader"
                                        HeaderStyle-Width="80px">
                                        <%-- <HeaderTemplate>
                                            <asp:CheckBox ID="chkNotifyAll" Text="Action" TextAlign="Left" AutoPostBack="true"
                                                OnCheckedChanged="chkNotifyAll_CheckedChanged" runat="server" />
                                        </HeaderTemplate>--%>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("SpecialOfferID")%>'
                                                CausesValidation="false" OnClick="lnkEdit_Click" ToolTip="Edit" ImageUrl="~/Images/edit-icon.jpg" />
                                            <asp:ImageButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("SpecialOfferID")%>'
                                                CausesValidation="false" OnClick="lnkDelete_Click" ImageUrl="~/Images/DeleteButton.jpg"
                                                OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                ToolTip="Delete" />
                                            <asp:CheckBox ID="chkNotify" runat="server" Visible='<%# Eval("OfferStatusID").ToString().Equals("1")? true : false %>'
                                                ValidationGroup='<%#  Eval("OfferTitle")  %>' ResellerID='<%# Convert.ToInt32(Eval("ResellerID")) %>'
                                                SpecialOfferID='<%# Eval("SpecialOfferID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="Center" />
                                <RowStyle HorizontalAlign="Right" />
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotalSpecialOffer" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="25" PagedControlID="gvSpecialOffers">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="divPopup" style="display: none; position: absolute; z-index: 9999; width: 990px;
        height: 430px; background-color: Gray; filter: alpha(opacity=50); opacity: 0.8;">
        <div style="position: relative; color: White; font-weight: bold; margin: auto; top: 30%;">
            <table width="100%" style="text-align: center; font-size: 18px;">
                <tr>
                    <td>
                        Please wait while the notifications are sent.
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    <script>
        function initPopup() {
            var tmpHeight = $('.tblCls1').height() + 200;
            $('.divPopup').height(tmpHeight);
            $('.divPopup').css("margin-top", -1 * tmpHeight);
            $('.divPopup').show();
            return true;
        }</script>
</asp:Content>

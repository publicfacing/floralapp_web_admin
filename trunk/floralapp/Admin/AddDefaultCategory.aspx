﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="AddDefaultCategory.aspx.cs" Inherits="Admin_AddDefaultCategory" Title="floralapp&reg; | Add Default Category"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" cellpadding="0" style="border-collapse: collapse; width: 100%;">
                    <tr>
                        <th class="TableHeadingBg">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Add Default Category"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                <tr>
                                    <td class="col">
                                        Category ID
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtCatID" runat="server" SkinID="textbox" class="FormItem" MaxLength="50"
                                            Text="0"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Category Name
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtCategoryName" runat="server" SkinID="textbox" class="FormItem"
                                            MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Category Image
                                    </td>
                                    <td class="val">
                                        <asp:FileUpload ID="imgCtrl" runat="server" />
                                        <div class="desc">
                                            <br />
                                            Supported image format(s): *.png, *.jpg, *.jpeg, *.gif.
                                            <br />
                                            Suggested image size(ht x wt) is 50 x 50 pixel(s).
                                            <br />
                                        </div>
                                        <div>
                                            <asp:Image ID="imgCategory" runat="server" /></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        &nbsp;
                                    </td>
                                    <td class="val">
                                        <asp:Repeater ID="rptImage" runat="server">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbl" Text='<%#Eval("DefaultCategoryID")+" [ "+ Eval("CategoryName") +" ]" %>'
                                                                runat="server"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Image ID="imgCategory" runat="server" ImageUrl='<%# "../DynImages/defCatImg_"+ Eval("DefaultCategoryID")+ Eval("ImageExt") %>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="actionDiv">
                    <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this category."
                        OnClick="btnSave_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                        CausesValidation="false" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

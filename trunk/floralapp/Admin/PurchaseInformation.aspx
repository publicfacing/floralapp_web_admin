﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="PurchaseInformation.aspx.cs" Inherits="Admin_PurchaseInformation" Title="floralapp&reg; | Purchase Information"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/javascript">
        function PrintContent() {
            var rowsContent1 = document.getElementById("tblContent1").innerHTML.replace("<tbody>", "");
            var rowsContent2 = document.getElementById("tblContent2").innerHTML.replace("<tbody>", "");
            var rowsContent3 = document.getElementById("tblContent3").innerHTML.replace("<tbody>", "");
            var tableFrame = "<table  border='0' cellpadding='0' style='font-size:13px; border-collapse:collapse; width: 100%'>";
            var tableContent = "<table  border='0' cellpadding='0' cellspacing='0' style='font-size:13px; border-collapse:collapse; width:100%; border:1px solid #E2E2E2; padding:5px;'>";
            var mycontent = "<html>";
            mycontent += "<head><style type='text/css'> .TableHeadingBg, h2, .gridHeader, .gridContent { font-size:13px; font-weight:normal; }</style></head>";
            rowsContent1 = rowsContent1.replace("</tbody>", "");
            rowsContent2 = rowsContent2.replace("</tbody>", "");
            rowsContent3 = rowsContent3.replace("</tbody>", "");
            mycontent += "<body  border='0' cellpadding='0' cellspacing='0' style='font-size:9px; margin:0px'>";
            mycontent += tableFrame;
            mycontent += "<tr><th colspan='4' style='padding-bottom:30px'>Purchase Information</th></tr>";
            mycontent += rowsContent1;

            var newWin = window.open('');
            newWin.document.write(mycontent + "</table><br /><br />" + tableContent + rowsContent2 + "</table><br /><br />" + tableFrame + rowsContent3 + "</table></body></html>");
            newWin.location.reload(true);
            newWin.focus();
            newWin.print();
            newWin.close();
        }
        
    </script>
    <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
    <table border="0" cellpadding="0" style="border-collapse: collapse; width: 100%">
        <tr>
            <th class="TableHeadingBg TableHeading" colspan="2">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                    text-indent: 5px;">
                    <tr>
                        <td>
                            Purchase Information
                        </td>
                        <td style="text-align: right; padding-right: 5px;">
                            <img alt="Print" src="../Images/Printer.png" onclick="PrintContent();" />
                            <a href="#" onclick="PrintContent();" title="Print">Print</a>
                        </td>
                    </tr>
                </table>
            </th>
        </tr>
        <tr>
            <td class="TableBorder">
                <table id="tblContent1" border="0" style="text-align: center" cellspacing="0" cellpadding="0"
                    class="detailTable">
                    <tr id="trPurchaseID" runat="server" visible="false">
                        <td class="col">
                            Purchase ID
                        </td>
                        <td class="val" colspan="3">
                            <asp:Label ID="lblPurchaseID" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Order No
                        </td>
                        <td class="val">
                            <asp:Label ID="lblOrderNo" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="col">
                            Order Date
                        </td>
                        <td class="val">
                            <asp:Label ID="lblPurchasedOn" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Customer
                        </td>
                        <td class="val">
                            <asp:Label ID="lblUser" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="col">
                            Is Registered User
                        </td>
                        <td class="val">
                            <asp:Label ID="lblIsRegistered" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Email ID
                        </td>
                        <td class="val">
                            <asp:Label ID="lblEmail" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="col">
                            Phone
                        </td>
                        <td class="val">
                            <asp:Label ID="lblMobilePhone" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="trShop">
                        <td class="col">
                            Shop
                        </td>
                        <td class="val">
                            <asp:Label ID="lblReseller" runat="server"></asp:Label>
                        </td>
                        <td class="col">
                            Shop ID
                        </td>
                        <td class="val">
                            <asp:Label ID="lblResellerID" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Is Pick-up
                        </td>
                        <td class="val">
                            <asp:Label ID="lblIsPickup" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="col">
                            Delivery Date
                        </td>
                        <td class="val">
                            <asp:Label ID="lblDeliveryDate" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr style="display: none;">
                        <td class="col">
                            Is Delivered
                        </td>
                        <td class="val">
                            <asp:Label ID="lblDelveryStatus" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="col">
                            Actual Delivery Date
                        </td>
                        <td class="val">
                            <asp:Label ID="lblActualDeliveryDate" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Gift Message
                        </td>
                        <td class="val" colspan="3">
                            <asp:Label ID="lblGiftMessage" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Notes
                        </td>
                        <td class="val" colspan="3">
                            <asp:Label ID="lblNotes" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Device ID
                        </td>
                        <td class="val">
                            <asp:Label ID="lblDeviceID" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="col">
                            Device Type
                        </td>
                        <td class="val">
                            <asp:Label ID="lblDeviceTypeID" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Working Mode
                        </td>
                        <td class="val">
                            <asp:Label ID="lblWorkingMode" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="col">
                            House Account No
                        </td>
                        <td class="val">
                            <asp:Label ID="lblHouseAcNo" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Payment Gateway
                        </td>
                        <td class="val">
                            <asp:Label ID="lblGateway" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="col">
                            Transaction ID
                        </td>
                        <td class="val">
                            <asp:Label ID="lblTransactionID" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Card Type
                        </td>
                        <td class="val">
                            <asp:Label ID="lblCardTypeID" runat="server" CssClass="label"></asp:Label>
                        </td>
                        <td class="col">
                            Card No
                        </td>
                        <td class="val">
                            <asp:Label ID="lblCardNo" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr style="font-weight: bold;">
                        <td class="col">
                            Purchase Status
                        </td>
                        <td class="val" colspan="3">
                            <asp:Image ID="imgTranStatus" runat="server" />
                            <asp:Label ID="lblTransactionStatus" runat="server" Text="" Style="font-size: 12px;
                                font-weight: bold;"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Transaction Message
                        </td>
                        <td class="val" colspan="3">
                            <asp:Label ID="lblTransactionMsg" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Order Status
                        </td>
                        <td class="val">
                            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                <asp:ListItem Value="-1" Text="New" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Processed"></asp:ListItem>
                                <asp:ListItem Value="0" Text="InActive"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="col">
                            Modified by<br />
                            Modified on
                        </td>
                        <td class="val">
                            <asp:Label ID="lblModifiedBy" runat="server" CssClass="label"></asp:Label><br />
                            <asp:Label ID="lblModifiedOn" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <table id="tblContent2" border="0" cellspacing="0" cellpadding="0" class="detailTable">
                    <tr>
                        <td class="col">
                            Address
                        </td>
                        <td class="val" colspan="3">
                            <mo:ExtGridView ID="gvBillingAddress" runat="server" DataKeyNames="addressid" AllowPaging="false"
                                AllowSorting="false" OrderBy="Name" SkinID="mo_gvskin" EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:TemplateField HeaderText="Billing Address" HeaderStyle-CssClass="gridHeader"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <div>
                                                <%# Eval("firstName")%>&nbsp;<%# Eval("lastName")%>
                                            </div>
                                            <div>
                                                <%# Eval("Street")%>
                                                <br />
                                                <%#  String.IsNullOrEmpty(Eval("Suite").ToString()) ? "" : Eval("Suite").ToString() + "<br />" %>
                                                <%# Eval("City")%>
                                            </div>
                                            <div>
                                                <%# String.IsNullOrEmpty(Eval("StateName").ToString()) ? "" : Eval("StateName").ToString() + "," %>
                                                <%# Eval("CountryCode")%>&nbsp;<%# Eval("Zip")%>.
                                            </div>
                                            <div>
                                                <%# String.IsNullOrEmpty(Eval("Phone1").ToString()) ? "" : "Ph: " + Eval("Phone1").ToString()%>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                            <br />
                            <br />
                            <mo:ExtGridView ID="gvShipingAddress" runat="server" DataKeyNames="addressid" AllowPaging="false"
                                AllowSorting="false" OrderBy="Name" SkinID="mo_gvskin" EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:TemplateField HeaderText="Delivery Address" HeaderStyle-CssClass="gridHeader"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <div>
                                                <%# Eval("firstName")%>&nbsp;<%# Eval("lastName")%>
                                            </div>
                                            <div>
                                                <%# Eval("Street")%>
                                                <br />
                                                <%#  String.IsNullOrEmpty(Eval("Suite").ToString()) ? "" : Eval("Suite").ToString() + "<br />" %>
                                                <%# Eval("City")%>
                                            </div>
                                            <div>
                                                <%# String.IsNullOrEmpty(Eval("StateName").ToString()) ? "" : Eval("StateName").ToString() + "," %>
                                                <%# Eval("CountryCode")%>&nbsp;<%# Eval("Zip")%>.
                                            </div>
                                            <div>
                                                <%# String.IsNullOrEmpty(Eval("Phone1").ToString()) ? "" : "Ph: " + Eval("Phone1").ToString()%>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table id="tblContent3" border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <th class="TableHeadingBg">
                Product(s) & Fee(s)
            </th>
        </tr>
        <tr>
            <td class="TableBorder">
                <h2>
                    Product(s)</h2>
                <mo:ExtGridView ID="gvProducts" runat="server" DataKeyNames="ProductID" SkinID="mo_gvskin"
                    ShowFooter="True" AutoGenerateColumns="False" PageSize="50">
                    <Columns>
                        <asp:BoundField DataField="ProductID" HeaderText="Product ID" ItemStyle-CssClass="gridContent"
                            HeaderStyle-Width="60px" FooterStyle-CssClass="gridContent"></asp:BoundField>
                        <asp:BoundField DataField="SkuNo" HeaderText="SKU#" ItemStyle-CssClass="gridContent"
                            HeaderStyle-Width="60px" FooterStyle-CssClass="gridContent"></asp:BoundField>
                        <asp:BoundField DataField="ProductName" HeaderText="Product" ItemStyle-CssClass="gridContent"
                            HeaderStyle-Width="60px" FooterStyle-CssClass="gridContent"></asp:BoundField>
                        <asp:BoundField DataField="ProductColor" HeaderText="Options" ItemStyle-CssClass="gridContent"
                            HeaderStyle-Width="60px" FooterStyle-CssClass="gridContent"></asp:BoundField>
                        <asp:BoundField DataField="ProductPrice" HeaderText="Price" ItemStyle-CssClass="gridContent halign-right"
                            DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="70px"
                            FooterStyle-CssClass="gridContent"></asp:BoundField>
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" FooterText="Total Sum :"
                            FooterStyle-CssClass="gridFooterContent" ItemStyle-CssClass="gridContent halign-center"
                            ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="70px"></asp:BoundField>
                        <asp:TemplateField HeaderText="Total Price" ItemStyle-CssClass="gridContent halign-right"
                            FooterStyle-CssClass="gridFooterContent" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lblTotalPrice" runat="server" Text='<%# "$" + Eval("TotalPrice") %>'
                                    CssClass="label"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblProductTotalAmount" runat="server" CssClass="label"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                </mo:ExtGridView>
                <br />
                <h2>
                    Upsell Product(s)</h2>
                <mo:ExtGridView ID="gvUpsellProduct" runat="server" DataKeyNames="UpsellProductID"
                    SkinID="mo_gvskin" ShowFooter="True" AutoGenerateColumns="False" EmptyDataText="No upsell product."
                    PageSize="50">
                    <Columns>
                        <asp:BoundField DataField="UpsellProductID" HeaderText="Upsell Product ID" ItemStyle-CssClass="gridContent"
                            HeaderStyle-Width="100px" FooterStyle-CssClass="gridContent"></asp:BoundField>
                        <asp:BoundField DataField="SkuNo" HeaderText="SKU#" ItemStyle-CssClass="gridContent"
                            HeaderStyle-Width="60px" FooterStyle-CssClass="gridContent"></asp:BoundField>
                        <asp:BoundField DataField="ProductName" HeaderText="Product" ItemStyle-CssClass="gridContent"
                            FooterStyle-CssClass="gridContent"></asp:BoundField>
                        <asp:BoundField DataField="ProductPrice" HeaderText="Price" ItemStyle-CssClass="gridContent halign-right"
                            DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="70px"
                            FooterStyle-CssClass="gridContent"></asp:BoundField>
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" FooterText="Total Sum :"
                            FooterStyle-CssClass="gridFooterContent" ItemStyle-CssClass="gridContent halign-center"
                            ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="70px"></asp:BoundField>
                        <asp:TemplateField HeaderText="Total Price" ItemStyle-CssClass="gridContent halign-right"
                            FooterStyle-CssClass="gridFooterContent" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lblUpsellTotalPrice" runat="server" Text='<%# "$" + Eval("TotalPrice") %>'
                                    CssClass="label"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblUpsellProductTotalAmount" runat="server" CssClass="label"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                </mo:ExtGridView>
                <br />
                <h2>
                    Fee(s) & Discount</h2>
                <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                    <tr>
                        <td class="col">
                            Service Fee
                        </td>
                        <td class="val">
                            <asp:Label ID="lblServiceFee" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Delivered Fee
                        </td>
                        <td class="val">
                            <asp:Label ID="lblDeliveredFee" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Sales Tax
                        </td>
                        <td class="val">
                            <asp:Label ID="lblSaleTax" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Discount
                        </td>
                        <td class="val">
                            <asp:Label ID="lblCouponAmount" runat="server" CssClass="label"></asp:Label>
                            &nbsp;&nbsp; Coupon No :
                            <asp:HyperLink ID="lnkCouponNo" runat="server" ToolTip="Coupon">
                                <asp:Label ID="lblCouponNo" runat="server" CssClass="label"></asp:Label>
                            </asp:HyperLink>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <h2 class="halign-right">
                    Total Price :
                    <asp:Label ID="lblTotalAmount" runat="server"></asp:Label></h2>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <div class="actionDiv">
        <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this purchase info."
            OnClick="btnSave_Click" />
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" CausesValidation="false"
            OnClick="btnBack_Click" />
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="head">
</asp:Content>

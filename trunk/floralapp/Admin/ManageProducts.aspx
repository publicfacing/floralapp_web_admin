﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="ManageProducts.aspx.cs" Inherits="Admin_ManageProducts" Title="floralapp&reg; | Manage Products"
    Theme="GraySkin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Manage Product"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                <tr>
                                    <td class="col">
                                        Product Name
                                    </td>
                                    <td class="val">
                                        <asp:Label ID="lblProductName" runat="server" Text="" ></asp:Label>
                                    </td>
                                    <td class="col">
                                        Price
                                    </td>
                                    <td class="val">
                                        <asp:Label ID="lblPrice" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Category
                                    </td>
                                    <td class="val">
                                        <asp:Label ID="lblCategory" runat="server" Text="" ></asp:Label>
                                    </td>
                                    <td class="col">
                                        Shop
                                    </td>
                                    <td class="val">
                                        <asp:Label ID="lblResellerName" runat="server" Text="" ></asp:Label>
                                    </td>
                                </tr>
                                <tr> 
                                    <td class="col">
                                        Description
                                    </td>
                                    <td class="val" colspan="3">
                                        <asp:Label ID="lblDescription" runat="server"
                                            Text=""></asp:Label>
                                    </td>
                                </tr> 
                                <tr> 
                                    <td class="col">
                                        Special Offer
                                    </td>
                                    <td class="val" colspan="3"> 
                                        <asp:Label ID="lblSpecialOffer" runat="server"
                                            Text=""></asp:Label>
                                    </td> 
                                </tr> 
                                <tr> 
                                    <td class="col">
                                        Product Image
                                    </td>
                                    <td class="val">
                                        <asp:Image ID="imgProduct" runat="server" /> 
                                    </td> 
                                    <td class="col">
                                        Thumb
                                    </td>
                                    <td class="val">
                                        <asp:Image ID="imgThumb" runat="server" />
                                    </td>
                                </tr>  
                                <tr>
                                    <td class="col">
                                        Published
                                    </td>
                                    <td class="val">
                                        <asp:Image ID="imgPublished" runat="server"/>
                                    </td>
                                    <td class="col">
                                        Is UpCharge
                                    </td>
                                    <td class="val">
                                        <asp:Image ID="imgIsUpCharge" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="actionDiv">
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" CausesValidation="false"
                        OnClick="btnBack_Click" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

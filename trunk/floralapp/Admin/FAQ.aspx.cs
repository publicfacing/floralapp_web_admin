﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.Utility;
using System.IO;
using Library.AppSettings;

public partial class Admin_FAQ : AppPage
{
    private String mFaqHtml = String.Empty;
    AppVars v = new AppVars();
    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            BindData();
            if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
            {
                lnkAdd.Visible = false;
                gvFaq.Columns[2].Visible = false;
            }
        }
    }

    private void BindData()
    {
        Data.Faq faq = new Data.Faq();
        DataSet dsFaq = faq.List();

        gvFaq.DataSource = dsFaq;
        gvFaq.DataBind();

        GenerateHtml(dsFaq);

    }

    public void GenerateHtml(DataSet dsFaqList)
    { 
        String faqHtml = String.Empty;

        if (Util.IsValidDataSet(dsFaqList))
        {
            DataTable dtFaq = Util.GetDataTable(dsFaqList);

            foreach (DataRow dr in dtFaq.Rows)
            {
                String question = dr["Question"].ToString();
                String ans = dr["Answer"].ToString();
                AppendQuestionAnswer(question, ans);
            }

            faqHtml = String.Format(@"<div style=""font-family: Arial, Verdana; font-size: 12px;"">{0}</div>", mFaqHtml);
        }

        #region Save File in file system...       
        String filePath = Server.MapPath(@"~/DynFiles\MoreSettings\");
        String fileName = String.Format("Faq_{0}{1}", 0, ".htm");
        filePath += fileName;

        if (File.Exists(filePath))
            File.Delete(filePath);

        faqHtml = String.Format(@"<html xmlns=""http://www.w3.org/1999/xhtml"">
                    <head>
                        <meta http-equiv=""Pragma"" content=""no-cache"" />
                        <title>floralapp : FAQ</title>
                    </head>
                    <body>
                        {0}
                    </body>
                    </html>", faqHtml);

        File.WriteAllText(filePath, faqHtml);

        #endregion Save File in file System...
    }

    private void AppendQuestionAnswer(String ques, String ans)
    {
        String quesAns = String.Format(@"<div style=""margin-bottom: 10px;"">
                                <p style=""font-weight: bold; margin: 4px; line-height: 18px;"">
                                    Q. {0}
                                </p>
                                <p style=""font-weight: normal; margin: 4px; line-height: 18px;"">
                                    <span style=""font-weight: bold;"">Ans : </span>{1}
                                </p>
                            </div>",
                    ques, ans);

        mFaqHtml += quesAns;
    }


    protected void btnAdd_Group(object sender, EventArgs e)
    {
        Response.Redirect("AddFaq.aspx");
    }

    protected void lnkBtn_Edit(object sender, EventArgs e)
    {
        String redirectUrl = "AddFaq.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String faqID = objButton.CommandArgument.ToString();
            String qs = Crypto.EncryptQueryString("FaqID=" + faqID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void lnkBtn_Delete(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int32 faqID = Convert.ToInt32(objButton.CommandArgument.ToString());
            Data.Faq faq = new Data.Faq(faqID);

            faq.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
            faq.DateDeleted.value = Util.GetServerDate();
            faq.DeletedBy.value = v.UserID;
            faq.Save();

            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void gvFaq_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvFaq.PageIndex = e.NewPageIndex;
        BindData();
    }
}


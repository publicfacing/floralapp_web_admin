﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="Reseller.aspx.cs" Inherits="Admin_Reseller" Title="floralapp&reg; | Shop"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/javascript">
        function isValidRequiredField() {
            var reseller = $get("<%=txtCloneResellerName.ClientID %>");
            var floristCode = $get("<%=txtCloneFloristCode.ClientID %>");
            var popup = $find("<%= this.mpeReseller.BehaviorID %>");
            var lblErr = $get("<%= lblError.ClientID %>");

            if (reseller.value.trim() != '' && floristCode.value.trim() != '') {
                if (popup) {
                    if (Page_ClientValidate("<%= this.btnCreateClone.ValidationGroup %>")) {
                        //Data is valid, and can be close the popup.
                        popup.hide();
                    }
                }
            }
            else if (reseller.value.trim() == '' && floristCode.value.trim() == '') {
                lblErr.innerHTML = "<span style ='color:red'><b>Enter shop name. <br/> Enter florist code.</b></span>"
                return false;
            }
            else if (reseller.value.trim() != '' && floristCode.value.trim() == '') {
                lblErr.innerHTML = "<span style ='color:red'><b>Enter florist code.</b></span>"
                return false;
            }
            else if (reseller.value.trim() == '' && floristCode.value.trim() != '') {
                lblErr.innerHTML = "<span style ='color:red'><b>Enter shop name.</b></span>"
                return false;
            }
            else {
                lblErr.innerHTML = "";
            }
        }
    </script>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12">
                    <tr>
                        <th class="TableHeadingBg TableHeading" colspan="2">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                text-indent: 5px;">
                                <tr>
                                    <td>
                                        View Shop(s)
                                    </td>
                                    <td style="text-align: right; padding-right: 5px; text-transform: capitalize;">
                                        <asp:HyperLink ID="lnkAddReseller" runat="server" NavigateUrl="~/Admin/AddReseller.aspx"
                                            ToolTip="Add Shop">Add Shop</asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label" valign="top">
                                            Search :
                                        </td>
                                        <td valign="top">
                                            <div runat="server" id="divShop">
                                                <label class="label">
                                                    Shop:</label>
                                                <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                            </div>
                                            <asp:DropDownList ID="ddlSearchField" runat="server" AutoPostBack="false">
                                                <asp:ListItem Text="ID" Value="ResellerID"></asp:ListItem>
                                                <asp:ListItem Text="Shop" Value="ResellerName"></asp:ListItem>
                                                <asp:ListItem Text="Code" Value="ResellerCode"></asp:ListItem>
                                                <asp:ListItem Text="Address" Value="Address1"></asp:ListItem>
                                                <asp:ListItem Text="City" Value="City"></asp:ListItem>
                                                <asp:ListItem Text="Phone" Value="Phone1"></asp:ListItem>
                                                <asp:ListItem Text="Alt. Phone" Value="Phone2"></asp:ListItem>
                                                <asp:ListItem Text="Cell" Value="Cell"></asp:ListItem>
                                                <asp:ListItem Text="Fax" Value="Fax"></asp:ListItem>
                                                <asp:ListItem Text="Mail ID" Value="Email"></asp:ListItem>
                                                <asp:ListItem Text="Web Url" Value="WebsiteURL"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchOperator" runat="server">
                                                <asp:ListItem Value="Begins With">Begins With</asp:ListItem>
                                                <asp:ListItem Value="Ends With">Ends With</asp:ListItem>
                                                <asp:ListItem Value="Contains">Contains</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnFilter" CssClass="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click"
                                                CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <asp:DataPager ID="dpTop" runat="server" PageSize="25" PagedControlID="gvReseller">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                        ShowLastPageButton="false" ShowNextPageButton="false" />
                                    <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                        NextPreviousButtonCssClass="command" />
                                    <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                        ShowLastPageButton="true" ShowNextPageButton="true" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <mo:ExtGridView ID="gvReseller" runat="server" DataKeyNames="ResellerID" AllowSorting="True"
                                OnSorting="gvReseller_OnSorting" SkinID="gvskin" OnPageIndexChanging="gvReseller_OnPageIndexChanging"
                                AutoGenerateColumns="False" EmptyDataText="No record(s) found.">
                                <PagerSettings Mode="NumericFirstLast"></PagerSettings>
                                <Columns>
                                    <asp:BoundField DataField="ResellerID" SortExpression="ResellerID" HeaderText="ID"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="ResellerName" SortExpression="ResellerName" HeaderText="Shop"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="ResellerCode" SortExpression="ResellerCode" HeaderText="Code"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="Address1" SortExpression="Address1" HeaderText="Address"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="City" SortExpression="City" HeaderText="City" ItemStyle-CssClass="gridContent">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Phone1" SortExpression="Phone1" HeaderText="Phone" ItemStyle-CssClass="gridContent">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Phone2" SortExpression="Phone2" HeaderText="Toll Free No."
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="Cell" SortExpression="Cell" HeaderText="Cell" ItemStyle-CssClass="gridContent">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Fax" SortExpression="Fax" HeaderText="Fax" ItemStyle-CssClass="gridContent">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Email" SortExpression="Email" HeaderText="Email" ItemStyle-CssClass="gridContent">
                                    </asp:BoundField>
                                    <%--<asp:BoundField DataField="WebsiteURL" SortExpression="WebsiteURL" HeaderText="Web Url"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="gridContent" HeaderStyle-CssClass="gridHeader"
                                        ItemStyle-Width="60px" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("ResellerID") +","+ Eval("ResellerSettID") +","+ Eval("ResMerchantID")%>'
                                                CausesValidation="false" OnClick="lnkEdit_Click" ToolTip="Edit" ImageUrl="~/Images/edit-icon.jpg" />
                                            <asp:ImageButton ID="imgBtnResellerClone" runat="server" CommandArgument='<%# Eval("ResellerID") %>'
                                                OnClick="imgBtnResellerClone_Click" ToolTip="Clone Shop" CausesValidation="false"
                                                ImageUrl="~/Images/clone.png" Visible='<%# GetVisibility() %>' />
                                            <asp:ImageButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("ResellerID")%>'
                                                CausesValidation="false" OnClick="lnkDelete_Click" ImageUrl="~/Images/DeleteButton.jpg"
                                                OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                ToolTip="Delete" Visible='<%# GetVisibility() %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotalReseller" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="25" PagedControlID="gvReseller">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- PopUp Ajax Model for create reseller clone. -->
    <asp:Button ID="btnCreateResellerClone" Style="display: none" runat="server" />
    <ajax:ModalPopupExtender ID="mpeReseller" TargetControlID="btnCreateResellerClone"
        runat="server" PopupControlID="pnlResellerClone" BackgroundCssClass="modalBackground"
        DropShadow="False" CancelControlID="btnCancel">
    </ajax:ModalPopupExtender>
    <asp:Panel runat="server" ID="pnlResellerClone" Style="display: none; background: #f0f0f0;">
        <table border="0" width="400" cellpadding="0" class="PopUpBox" style="overflow: scroll;">
            <tr>
                <td class="PopUpBoxHeading">
                    <asp:Label ID="lblResellerClone" runat="server" Text="Clone Shop"></asp:Label>
                </td>
                <td class="PopUpBoxHeading" align="right">
                    <asp:ImageButton ID="btnClosePopup" runat="server" CausesValidation="false" ImageUrl="~/Images/close.gif" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 10px;">
                    <table border="0" cellpadding="0" class="detailTable">
                        <tr>
                            <td colspan="2" class="val">
                                <asp:Label ID="lblError" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 150px;">
                                Shop Name<span class="reqd">*</span>:
                            </td>
                            <td class="val">
                                <asp:TextBox ID="txtCloneResellerName" MaxLength="150" Width="150px" Text="" runat="server"
                                    CssClass="textbox"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="rfvCloneResellerName" runat="server" ControlToValidate="txtCloneResellerName"
                                    ErrorMessage="Enter shop name." Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="col" style="width: 150px;">
                                Florist Code<span class="reqd">*</span>:
                            </td>
                            <td class="val">
                                <asp:TextBox ID="txtCloneFloristCode" MaxLength="10" Width="60px" Text="" runat="server"
                                    CssClass="textbox"></asp:TextBox><br />
                                <asp:RequiredFieldValidator ID="rfvCloneFloristCode" runat="server" ControlToValidate="txtCloneFloristCode"
                                    ErrorMessage="Enter florist code." Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="val" colspan="2" style="text-align: center; vertical-align: top; margin-top: 10px;">
                                <asp:Button ID="btnCreateClone" runat="server" Text="Create" CssClass="btn" OnClick="btnCreateClone_Click"
                                    CausesValidation="false" Width="60px" OnClientClick="return isValidRequiredField();" />
                                <asp:Button ID="btnCancel" runat="server" Width="60px" Text="Cancel" CssClass="btn"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <!-- End of popup reseller clone. OnClientClick="return isValidRequiredField();"  -->
</asp:Content>

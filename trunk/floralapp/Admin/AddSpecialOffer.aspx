﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="AddSpecialOffer.aspx.cs" Inherits="Admin_AddSpecialOffer" Title="floralapp&reg; | Add Special Offer"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="HTMLEditor" %>
<%@ Register Src="../UserControls/HelpMsg.ascx" TagName="HelpMsg" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/javascript">
        function DateTimeFormat(sender, args) {
            var d = sender._selectedDate;
            var now = new Date();
            sender.get_element().value = d.format("MM/dd/yyyy")
        }

        function getLength(sender) {
            var editor = $find("<%= rteOfferDescription.ClientID %>");
            var text = editor.GetText();
            alert(text);
            args.IsValid = text.length <= 10000;
        }

    </script>
    <table class="tblCls1" border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Add Special Offer"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <uc1:HelpMsg ID="HelpMsg1" runat="server" />
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="vsSpecialOffer" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                <tr runat="server" id="trShop">
                                    <td class="col">
                                        Shop<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="False" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="Select One " Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvResellerID" runat="server" ControlToValidate="ddlReseller"
                                            ErrorMessage="Select a shop for this special offer." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        Special Offer<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtOfferTitle" runat="server" SkinID="textbox" class="FormItem"
                                            MaxLength="500"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvSpecialOffer" runat="server" ControlToValidate="txtOfferTitle"
                                            ErrorMessage="Enter a special offer name." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        Description
                                    </td>
                                    <td class="val">
                                        <HTMLEditor:Editor ID="rteOfferDescription" runat="server" Height="300px" Width="100%" />
                                        <%--<asp:TextBox runat="server" ID="txtOfferDescription" Width="100%" Rows="10" TextMode="MultiLine"
                                            Height="135px"></asp:TextBox>--%>
                                        Max. 1000 character.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Is Enabled
                                    </td>
                                    <td class="val">
                                        <asp:CheckBox ID="chkIsEnabled" runat="server" SkinID="checkbox" class="FormItem"
                                            Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Start Date
                                    </td>
                                    <td class="val">
                                        <asp:TextBox runat="server" OnKeyUP="this.value=''" CssClass="textbox" ID="txtStartDate"
                                            Width="60px"></asp:TextBox>
                                        <ajax:CalendarExtender ID="ceStartDate" OnClientDateSelectionChanged="DateTimeFormat"
                                            Format="MM/dd/yyyy" runat="server" TargetControlID="txtStartDate" PopupButtonID="imgCalIcon1">
                                        </ajax:CalendarExtender>
                                        <asp:Image ImageUrl="~/Images/calender-icon.gif" Style="cursor: hand" ID="imgCalIcon1"
                                            runat="server" />
                                        <asp:TextBox ID="txtStartTime" runat="server" CssClass="textbox" Width="50px"></asp:TextBox>
                                        <ajax:MaskedEditExtender runat="server" ID="mEStartTime" TargetControlID="txtStartTime"
                                            AcceptAMPM="true" Mask="99:99" MessageValidatorTip="true" UserDateFormat="None"
                                            UserTimeFormat="None" MaskType="Time" InputDirection="RightToLeft" AcceptNegative="Right"
                                            DisplayMoney="Right" ErrorTooltipEnabled="True">
                                        </ajax:MaskedEditExtender>
                                        <ajax:TextBoxWatermarkExtender ID="tWEStartTime" runat="server" TargetControlID="txtStartTime"
                                            WatermarkText="hh:mm PM" WatermarkCssClass="watermarked">
                                        </ajax:TextBoxWatermarkExtender>
                                        <ajax:TextBoxWatermarkExtender ID="twmStartDate" runat="server" TargetControlID="txtStartDate"
                                            WatermarkText="MM/DD/YYYY" WatermarkCssClass="watermarked">
                                        </ajax:TextBoxWatermarkExtender>
                                        <ajax:MaskedEditValidator ID="mvStartTime" runat="server" ControlExtender="mEStartTime"
                                            ControlToValidate="txtStartTime" IsValidEmpty="True" EmptyValueMessage="Time is required"
                                            InvalidValueMessage="Time is invalid" Display="Dynamic" TooltipMessage="Input a time"
                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="Time is invalid"></ajax:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        End Date
                                    </td>
                                    <td class="val">
                                        <asp:TextBox runat="server" OnKeyUP="this.value=''" CssClass="textbox" ID="txtEndDate"
                                            Width="60px"></asp:TextBox>
                                        <ajax:CalendarExtender Format="MM/dd/yyyy" OnClientDateSelectionChanged="DateTimeFormat"
                                            ID="ceEndDate" runat="server" TargetControlID="txtEndDate" PopupButtonID="imgCalIcon2">
                                        </ajax:CalendarExtender>
                                        <asp:Image ImageUrl="~/Images/calender-icon.gif" ID="imgCalIcon2" runat="server" />
                                        <asp:TextBox ID="txtEndTime" runat="server" CssClass="textbox" Width="50px"></asp:TextBox>
                                        <ajax:MaskedEditExtender runat="server" ID="mEEndTime" TargetControlID="txtEndTime"
                                            AcceptAMPM="true" Mask="99:99" MessageValidatorTip="true" UserDateFormat="None"
                                            UserTimeFormat="None" MaskType="Time" InputDirection="RightToLeft" AcceptNegative="Right"
                                            DisplayMoney="Right" ErrorTooltipEnabled="True">
                                        </ajax:MaskedEditExtender>
                                        <ajax:TextBoxWatermarkExtender ID="tWEEndTime" runat="server" TargetControlID="txtEndTime"
                                            WatermarkText="hh:mm PM" WatermarkCssClass="watermarked">
                                        </ajax:TextBoxWatermarkExtender>
                                        <ajax:TextBoxWatermarkExtender ID="twmEndDate" runat="server" TargetControlID="txtEndDate"
                                            WatermarkText="MM/DD/YYYY" WatermarkCssClass="watermarked">
                                        </ajax:TextBoxWatermarkExtender>
                                        <ajax:MaskedEditValidator ID="mvEndTime" runat="server" ControlExtender="mEEndTime"
                                            ControlToValidate="txtEndTime" IsValidEmpty="True" EmptyValueMessage="Time is required"
                                            InvalidValueMessage="Time is invalid" Display="Dynamic" TooltipMessage="Input a time"
                                            EmptyValueBlurredText="*" InvalidValueBlurredMessage="Time is invalid"></ajax:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Is Notified
                                    </td>
                                    <td class="val">
                                        <asp:CheckBox ID="chkIsNotified" runat="server" SkinID="checkbox" class="FormItem"
                                            Checked="false" />
                                        <asp:Label ID="lblLastNotified" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td class="col">
                                        Date Notified
                                    </td>
                                    <td class="val">
                                        <asp:TextBox runat="server" OnKeyUP="this.value=''" Width="60px" ID="txtDateNotified"></asp:TextBox>
                                        <ajax:CalendarExtender ID="ceDateNotified" OnClientDateSelectionChanged="DateTimeFormat"
                                            Format="MM/dd/yyyy" runat="server" TargetControlID="txtDateNotified" PopupButtonID="imgCalIcon3">
                                        </ajax:CalendarExtender>
                                        <asp:Image ImageUrl="~/Images/calender-icon.gif" Style="cursor: hand" ID="imgCalIcon3"
                                            runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <div class="actionDiv">
                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this shop."
                                    OnClick="btnSave_Click" OnClientClick="return initPopup();" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                                    CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="divPopup" style="display: none; position: absolute; margin-left: -255px;
        z-index: 9999; width: 990px; height: 430px; background-color: Gray; filter: alpha(opacity=50);
        opacity: 0.8;">
        <div style="position: relative; color: White; font-weight: bold; margin: auto; top: 30%;">
            <table width="100%" style="text-align: center; font-size: 18px;">
                <tr>
                    <td>
                        Please wait while the notifications are sent.
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    <script>
        function initPopup() {
            var myCb = $('input[type=checkbox]');
            if ($(myCb[1]).prop("checked") && !$(myCb[1]).prop("disabled")) {
                var tmpHeight = $('.tblCls1').height() + 200;
                $('.divPopup').height(tmpHeight);
                $('.divPopup').css("margin-top", -1 * tmpHeight);
                $('.divPopup').show();
            }
            return true;
        }</script>
</asp:Content>

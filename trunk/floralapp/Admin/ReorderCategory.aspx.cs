﻿using System;
using System.Data;
using Library.Utility;
using System.Collections.Generic;


public partial class Admin_ReorderCategory : AppPage
{
    private DataTable CategoryTable
    {
        get
        {
            if (ViewState["dtCategory"] == null)
                ViewState["dtCategory"] = GetCategoryData();

            return (DataTable)ViewState["dtCategory"];
        }
        set { ViewState["dtCategory"] = value; }
    }

    private string[] ItemOrders
    {
        get
        {
            if (ViewState["itemsOrders"] == null)
                ViewState["itemsOrders"] = GetItemOrders();

            return (string[])ViewState["itemsOrders"];
        }
        set { ViewState["itemsOrders"] = value; }
    }

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            BindReorderList();
        }
    }

    private DataTable GetCategoryData()
    {
        Data.Category cat = new Data.Category();
        DataSet dsCat = cat.List();
        return (Util.IsValidDataSet(dsCat)) ? Util.GetDataTable(dsCat) : null;
    }

    private void BindReorderList()
    {
        if (Util.IsValidDataTable(CategoryTable))
        {
            rlCategory.DataSource = CategoryTable;
            rlCategory.DataBind();
        }
    }

    private void SaveNewOrderList()
    {
        Data.Category cat = new Data.Category();
        DataTable dt = CategoryTable;

        for (int i = 0; i < ItemOrders.Length; i++)
        {
            Int32 categoryID = Int32.Parse(ItemOrders[i].ToString());
            cat.LoadID(categoryID);
            cat.Sequence.value = i + 1;
            cat.Save();
        }

        CategoryTable = null;
    }

    private string[] GetItemOrders()
    {
        if (!Util.IsValidDataTable(CategoryTable))
            return null;

        DataTable dt = CategoryTable;

        string idList = "";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            String id = Util.GetDataString(dt, i, "CategoryID");
            idList += (String.IsNullOrEmpty(idList)) ? id : "," + id;
        }

        string[] ret = idList.Split(',');
        //Array.Reverse(ret);

        return ret;
    }

    protected void rlCategory_ItemReorder(object sender, AjaxControlToolkit.ReorderListItemReorderEventArgs e)
    {
        string[] tempArray = ItemOrders;

        List<string> list = new List<string>(tempArray);//using a list for the reordering (convienience)
        string itemToMove = list[e.OldIndex];
        list.Remove(itemToMove);
        list.Insert(e.NewIndex, itemToMove);
        ItemOrders = list.ToArray();

        SaveNewOrderList();
        BindReorderList();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Category.aspx");
    } 
}


﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="AddUpsellProduct.aspx.cs" Inherits="Admin_AddUpsellProduct" Title="floralapp&reg; | Add Upsell Product"
    Theme="GraySkin" %>

<%@ Register Src="../UserControls/HelpMsg.ascx" TagName="HelpMsg" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Add Upsell Product"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <uc1:HelpMsg ID="HelpMsg1" runat="server" />
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="vsReseller" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                <tr runat="server" id="trShop">
                                    <td class="col">
                                        Shop<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                            OnSelectedIndexChanged="ddlReseller_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="Select One " Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvReseller" runat="server" ControlToValidate="ddlReseller"
                                            ErrorMessage="Select the upsell product's shop." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Upsell Category<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:DropDownList ID="ddlCat" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="Select One " Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="ddlCat" ErrorMessage="Select the upsell product's category."
                                            Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        Upsell Product<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtProductName" runat="server" SkinID="textbox" class="FormItem"
                                            MaxLength="500"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvProductName" runat="server" ControlToValidate="txtProductName"
                                            ErrorMessage="Enter upsell product's name." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        Description
                                    </td>
                                    <td class="val">
                                        <mo:ExtTextBox ID="txtDescription" runat="server" TextMode="MultiLine" MaxLength="100"
                                            SkinID="textarea" Height="170px" Width="471px"></mo:ExtTextBox>
                                        <br />
                                        Max. 100 character.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        Product Image
                                    </td>
                                    <td class="val">
                                        <asp:FileUpload ID="fuProductImage" runat="server" />
                                        <div class="desc">
                                            <%--Suggested image size: 200 x 200 pixel(s).
                                            <br />
                                            Suggested image format: *.png
                                            <br />--%>
                                            Supported image format(s): *.png, *.jpg, *.jpeg, *.gif.
                                            <%-- <br />
                                            Suggested image size (ht x wt) is 480 x 320 pixel(s).--%>
                                            <%-- <br />
                                            Max. image size(ht x wt) should not exceed 480 x 320 pixel(s).--%>
                                            <%-- <br />
                                            Max. image size(ht x wt) should not exceed 200 x 200 pixel(s).--%>
                                        </div>
                                        <div>
                                            <asp:Image ID="imgProductImage" runat="server" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        Product Price<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        &#32;&#36;&#32;
                                        <asp:TextBox ID="txtProductPrice" runat="server" SkinID="textbox" class="FormItem"
                                            MaxLength="8" Width="122px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvProductPrice" runat="server" ControlToValidate="txtProductPrice"
                                            ErrorMessage="Enter upsell product's price." Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revProductPrice" runat="server" ControlToValidate="txtProductPrice"
                                            ValidationExpression="^\d{0,8}($|\.\d{0,2}$)" ErrorMessage="Enter a valid product price."
                                            Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        SKU No<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtSkuNumber" runat="server" SkinID="textbox" class="FormItem" Width="50px"
                                            MaxLength="8"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvSkuNumber" runat="server" ControlToValidate="txtSkuNumber"
                                            ErrorMessage="Enter SKU number." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Is Published
                                    </td>
                                    <td class="val">
                                        <asp:CheckBox ID="chkIsPublished" runat="server" SkinID="checkbox" class="FormItem"
                                            Checked="true" />
                                    </td>
                                </tr>
                            </table>
                            <div class="actionDiv">
                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this shop."
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                                    CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="AddProduct.aspx.cs" Inherits="Admin_AddProduct" Title="floralapp&reg; | Add Product"
    Theme="GraySkin" %>

<%@ Register Src="../UserControls/HelpMsg.ascx" TagName="HelpMsg" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/javascript">
        function visibility(chk) {
            var row = document.getElementById("<%= trUpercent.ClientID %>");
            row.style.display = chk.checked ? "" : "none";
        }
    </script>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Add Product"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <uc1:HelpMsg ID="HelpMsg1" runat="server" />
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="vsProduct" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                <tr runat="server" id="trShop">
                                    <td class="col">
                                        Shop<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                                            OnSelectedIndexChanged="ddlReseller_SelectedIndexChanged">
                                            <asp:ListItem Value="0" Text="Select One " Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvReseller" runat="server" ControlToValidate="ddlReseller"
                                            ErrorMessage="Select shop." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Select one or more Categories<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <div style="height: 150px; overflow-y: auto">
                                            <asp:GridView ID="gvCategories" runat="server" DataKeyNames="CategoryID" AllowSorting="false"
                                                ShowHeader="false" AllowPaging="false" EmptyDataText="Categorie(s) not available for this shop."
                                                CssClass="grid" Width="100%" AutoGenerateColumns="false" EmptyDataRowStyle-CssClass="gridEmptyRow"
                                                OnRowDataBound="gvCategories_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="CategoryName" HeaderText="" ItemStyle-CssClass="gridContent"
                                                        ItemStyle-Width="100%" HeaderStyle-Width="100%"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridContent halign-center" ItemStyle-Width="60px"
                                                        HeaderStyle-Width="60px">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="cbChecked" runat="server" />
                                                            <asp:HiddenField ID="hdnCategName" runat="server" />
                                                            <asp:HiddenField ID="wasPublished" runat="server" />
                                                            <asp:HiddenField ID="canPublish" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                                <!--  Removed concept for special offer : Jnan
                                <tr style="display: none">
                                    <td class="col">
                                        Special Offer
                                    </td>
                                    <td class="val">
                                        <asp:DropDownList ID="ddlSpecialOffer" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0" Text="Select One " Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                -->
                                <tr>
                                    <td class="col showhh">
                                        Product<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtProductName" runat="server" SkinID="textbox" class="FormItem"
                                            MaxLength="250"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvProductName" runat="server" ControlToValidate="txtProductName"
                                            ErrorMessage="Enter a product name." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        Color Options
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtColor" runat="server" SkinID="textbox" class="FormItem" MaxLength="250"></asp:TextBox>
                                        <asp:CheckBox ID="chkShowColorInDevice" runat="server" SkinID="checkbox" class="FormItem"
                                            Text="Show In Device" />
                                        <p>
                                            <i>Please enter the options as a comma separated list as follows: </i>Red(SKU 45621),
                                            Yellow (SKU 6934), Green (94784)
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        Description
                                    </td>
                                    <td class="val">
                                        <mo:ExtTextBox ID="txtDescription" runat="server" textmode="MultiLine" MaxLength="4000"
                                            SkinID="textarea" Height="170px" Width="471px"></mo:ExtTextBox>
                                        <br />
                                        Max. 4000 character.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        Product Image<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:FileUpload ID="fuProductImage" runat="server" />
                                        <div class="desc">
                                            <%--Suggested image size: 200 x 200 pixel(s).
                                            <br />
                                            Suggested image format: *.png
                                            <br />--%>
                                            Supported image format(s): *.png, *.jpg, *.jpeg, *.gif.
                                            <%-- <br />
                                            Suggested image size (ht x wt) is 480 x 320 pixel(s).--%>
                                            <%-- <br />
                                            Max. image size(ht x wt) should not exceed 480 x 320 pixel(s).--%>
                                            <%-- <br />
                                            Max. image size(ht x wt) should not exceed 200 x 200 pixel(s).--%>
                                        </div>
                                        <div>
                                            <asp:Image ID="imgProductImage" runat="server" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        Product Price<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        &#32;&#36;&#32;
                                        <asp:TextBox ID="txtProductPrice" runat="server" SkinID="textbox" class="FormItem"
                                            MaxLength="8" Width="70px"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="revProductPrice" runat="server" ControlToValidate="txtProductPrice"
                                            ValidationExpression="^\d{0,8}($|\.\d{0,2}$)" ErrorMessage="Enter a valid product price."
                                            Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvProductPrice" runat="server" ControlToValidate="txtProductPrice"
                                            ErrorMessage="Enter product price." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col showhh">
                                        SKU No<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtSkuNumber" runat="server" SkinID="textbox" class="FormItem" Width="50px"
                                            MaxLength="8"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvSkuNumber" runat="server" ControlToValidate="txtSkuNumber"
                                            ErrorMessage="Enter SKU number." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Is Upcharge Active
                                    </td>
                                    <td class="val">
                                        <asp:CheckBox ID="chkIsUpChargeActive" runat="server" SkinID="checkbox" class="FormItem"
                                            OnClick="return visibility(this);" />
                                    </td>
                                </tr>
                                <tr id="trUpercent" style="display: none" runat="server">
                                    <td class="col">
                                        Upcharge Percentage(%)
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtUpchargePercent" Width="60px" runat="server" SkinID="textbox"
                                            class="FormItem" MaxLength="8"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="revUpchargePercent" runat="server" ControlToValidate="txtUpchargePercent"
                                            ValidationExpression="^(100(?:\.0{1,2})?|0*?\.\d{1,2}|\d{1,2}(?:\.\d{1,2})?)$"
                                            ErrorMessage="Enter a valid upcharge percentage." Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Is Direct Ship
                                    </td>
                                    <td class="val">
                                        <asp:CheckBox ID="chkIsDirectShip" runat="server" SkinID="checkbox" class="FormItem" />
                                        <div>
                                            <i>(Note: By default Florist To Florist is selected if Direct Ship is unchecked.)</i></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Is Published
                                    </td>
                                    <td class="val">
                                        <asp:CheckBox ID="chkIsPublished" runat="server" SkinID="checkbox" class="FormItem"
                                            Checked="true" />
                                    </td>
                                </tr>
                            </table>
                            <div class="actionDiv">
                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this shop."
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btnAddVariation" runat="server" Text="  Add Variation  " CssClass="btn"
                                    ToolTip="Click here to save this shop." OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                                    CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <%--<asp:UpdatePanel runat="server" ID="trProductVariation" UpdateMode="Always">
        <ContentTemplate>--%>
    <div runat="server" id="trProductVariation">
        <br />
        <br />
        <a name="varr"></a>
        <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;">
            <tr>
                <th class="TableHeadingBg" colspan="2">
                    <asp:Label ID="Label1" runat="server" Text="Product Variations"></asp:Label>
                </th>
            </tr>
            <tr>
                <td class="TableBorder" valign="top">
                    <table>
                        <tr>
                            <td class="val">
                                <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                    <tr>
                                        <td colspan="4">
                                            <asp:Label ID="lblErrorVar" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col showhh">
                                            Product<span class="reqd">*</span>
                                        </td>
                                        <td class="val">
                                            <asp:TextBox ID="txtProductNameVar" runat="server" SkinID="textbox" class="FormItem"
                                                MaxLength="250"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProductNameVar"
                                                ErrorMessage="Enter a product name." ValidationGroup="Variation" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col showhh">
                                            Product Price<span class="reqd">*</span>
                                        </td>
                                        <td class="val">
                                            &#32;&#36;&#32;
                                            <asp:TextBox ID="txtProductPriceVar" runat="server" SkinID="textbox" class="FormItem"
                                                MaxLength="8" Width="70px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtProductPriceVar"
                                                ValidationExpression="^\d{0,8}($|\.\d{0,2}$)" ErrorMessage="Enter a valid product price."
                                                Display="Dynamic" InitialValue="0" ValidationGroup="Variation"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtProductPriceVar"
                                                ErrorMessage="Enter product price." Display="Dynamic" ValidationGroup="Variation"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col showhh">
                                            SKU No<span class="reqd">*</span>
                                        </td>
                                        <td class="val">
                                            <asp:TextBox ID="txtSkuNumberVar" runat="server" SkinID="textbox" class="FormItem"
                                                Width="50px" MaxLength="8"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtSkuNumberVar"
                                                ErrorMessage="Enter SKU number." Display="Dynamic" ValidationGroup="Variation"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: left">
                                        </td>
                                    </tr>
                                </table>
                                <div class="actionDiv">
                                    <asp:Button ID="btnCreateProdVar" runat="server" Text="  Create Product Variation  "
                                        CssClass="btn" ToolTip="Click here to Create/Update Product Variation." OnClick="btnCreateProdVar_Click"
                                        ValidationGroup="Variation" />
                                    <asp:Button ID="btnCancelVar" runat="server" Text="  Cancel  " CssClass="btn" ToolTip="Cancel Action."
                                        OnClick="btnCancelVar_Click" CausesValidation="false" Visible="false" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="TableBorder" valign="top">
                    <asp:FileUpload ID="fuVariationImage" runat="server" />
                    <div class="desc">
                        Supported image format(s): *.png, *.jpg, *.jpeg, *.gif. If no image provided product
                        image will be used.
                    </div>
                    <div>
                        <asp:Image ID="imgVariationImage" runat="server" Style="max-width: 350px;" />
                    </div>
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="gvVariations" runat="server" DataKeyNames="ProductID" AllowSorting="false"
            SkinID="gvskin" AutoGenerateColumns="false" EmptyDataText="No variation(s) found."
            AllowPaging="false">
            <Columns>
                <asp:BoundField HeaderText="ID" DataField="ProductID" SortExpression="ProductID"
                    ItemStyle-CssClass="gridContent" HeaderStyle-Width="70px" />
                <asp:BoundField HeaderText="SKU#" DataField="SKUNo" SortExpression="SKUNo" ItemStyle-CssClass="gridContent"
                    HeaderStyle-Width="80px" />
                <asp:TemplateField HeaderText="Product Name" SortExpression="ProductName" HeaderStyle-CssClass="gridHeader"
                    ItemStyle-CssClass="gridContent">
                    <ItemTemplate>
                        <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("ProductName")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Image" ItemStyle-CssClass="gridContent" HeaderStyle-CssClass="gridHeader">
                    <ItemTemplate>
                        <asp:Image ID="imgProduct" Style="border: 2px solid #ffcc00;" runat="server" Visible="true"
                            ImageUrl='<%# getThumbImage(Eval("ProductID").ToString() + Eval("ProductThumbExt").ToString()) %>'
                            mageAlign="Top" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Price" DataField="ProductPrice" SortExpression="Productprice"
                    HeaderStyle-Width="60px" DataFormatString="{0:C}" ItemStyle-CssClass="gridContent halign-right"
                    ItemStyle-VerticalAlign="Top" />
                <asp:BoundField HeaderText="New Price" DataField="ProductNewPrice" SortExpression="ProductNewPrice"
                    HeaderStyle-Width="60px" DataFormatString="{0:C}" ItemStyle-CssClass="gridContent halign-right"
                    ItemStyle-VerticalAlign="Top" />
                <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="gridHeader" HeaderStyle-Width="50px"
                    ItemStyle-CssClass="gridContent">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("ProductID") + "," + Eval("ProdUpchargeID") %>'
                            CausesValidation="false" OnClick="btnEditVariation_Click" ToolTip="Edit" ImageUrl="~/Images/edit-icon.jpg" />
                        <asp:ImageButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("ProductID")%>'
                            CausesValidation="false" OnClick="btnDeleteVariation_Click" ImageUrl="~/Images/DeleteButton.jpg"
                            OnClientClick="return confirm('Are you sure you want to delete this variation?');"
                            ToolTip="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

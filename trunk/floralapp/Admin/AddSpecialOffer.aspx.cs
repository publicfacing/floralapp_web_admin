﻿using System;
using System.Data;
using Library.AppSettings;
using Library.Utility;
using System.IO;
using System.Collections.Specialized;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Security.Authentication;
using System.Net;
using System.Text;
using System.Web;
using System.Configuration;

public partial class Admin_AddSpecialOffer : AppPage
{
    private Int32 SpecialOfferID
    {
        get;
        set;
    }

    AppVars appVar = new AppVars();

    DateTime dtStartDateTime = new DateTime();
    DateTime dtEndDateTime = new DateTime();

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                SpecialOfferID = Convert.ToInt32(nvc["SpecialOfferID"]);
            }

            Util.setValue(lblPageTitle, (SpecialOfferID > 0) ? "Edit Special Offer" : "Add Special Offer");
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        // Reseller DropDownList
        Data.Reseller reseller = new Data.Reseller();

        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, appVar.ResellerID);

        Util.setValue(txtStartDate, String.Format("{0:MM/dd/yyyy}", Util.GetServerDate()));
        Util.setValue(txtEndDate, String.Format("{0:MM/dd/yyyy}", Util.GetServerDate()));
        Util.setValue(txtDateNotified, String.Format("{0:MM/dd/yyyy}", Util.GetServerDate()));

        if (appVar.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, appVar.ResellerID);
            //ddlReseller.Enabled = false;
            trShop.Visible = false;
        }

        if (SpecialOfferID <= 0)
            return;

        Data.SpecialOffer splOffer = new Data.SpecialOffer();
        splOffer.LoadID(SpecialOfferID);

        dtStartDateTime = Convert.ToDateTime(splOffer.OfferStartDate.value);
        dtEndDateTime = Convert.ToDateTime(splOffer.OfferEndDate.value);

        Util.setValue(ddlReseller, splOffer.ResellerID.value);
        Util.setValue(txtOfferTitle, splOffer.OfferTitle.value);
        rteOfferDescription.Content = splOffer.OfferDescription.value;
        //txtOfferDescription.Text = splOffer.OfferDescription.value;

        Util.setValue(txtStartDate, String.Format("{0:MM/dd/yyyy}", dtStartDateTime));
        Util.setValue(txtStartTime, String.Format("{0:t}", dtStartDateTime));
        Util.setValue(txtEndDate, String.Format("{0:MM/dd/yyyy}", dtEndDateTime));
        Util.setValue(txtEndTime, String.Format("{0:t}", dtEndDateTime));
        Util.setValue(txtDateNotified, String.Format("{0:MM/dd/yyyy}", splOffer.DateModified.value));

        chkIsEnabled.Checked = Convert.ToInt32(splOffer.OfferStatusID.value) == 1 ? true : false;
        Util.setValue(chkIsNotified, Convert.ToBoolean(splOffer.IsNotified.value));

        if (chkIsNotified.Checked)
        {
            chkIsNotified.Enabled = false;
            Util.setValue(lblLastNotified, "(Notified on: " + String.Format("{0:MM/dd/yyyy}", splOffer.DateModified.value) + ")");
        }
    }

    private void SaveData()
    {
        Data.SpecialOffer splOffer = new Data.SpecialOffer(SpecialOfferID);
        DateTime dtNotified = new DateTime();
        dtNotified = Convert.ToDateTime("01/01/1900");

        dtStartDateTime = Convert.ToDateTime(Util.getValue(txtStartDate) + " " + Util.getValue(txtStartTime));
        dtEndDateTime = Convert.ToDateTime(Util.getValue(txtEndDate) + " " + Util.getValue(txtEndTime));

        splOffer.ResellerID.value = Int32.Parse(Util.getValue(ddlReseller));
        splOffer.OfferTitle.value = Util.getValue(txtOfferTitle);
        splOffer.OfferDescription.value = rteOfferDescription.Content;
        //splOffer.OfferDescription.value = txtOfferDescription.Text;
        splOffer.OfferStatusID.value = chkIsEnabled.Checked ? 1 : 0;
        splOffer.OfferStartDate.value = dtStartDateTime;
        splOffer.OfferEndDate.value = dtEndDateTime;
        splOffer.IsNotified.value = chkIsNotified.Checked;

        if (chkIsNotified.Checked)
            splOffer.DateNotified.value = Util.GetServerDate();
        else
            splOffer.DateNotified.value = dtNotified;

        splOffer.NotifiedBy.value = appVar.UserID;
        splOffer.DateAdded.value = Util.GetServerDate();
        splOffer.AddedBy.value = appVar.UserID;
        splOffer.DateModified.value = Util.GetServerDate();
        splOffer.ModifiedBy.value = appVar.UserID;
        splOffer.DateDeleted.value = Util.GetServerDate();
        splOffer.DeletedBy.value = appVar.UserID;
        splOffer.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        splOffer.Save();

        #region Save File in file system...

        String filePath = Server.MapPath(@"~/DynFiles\SpecialOffer\");
        String fileName = String.Format("SpecialOffer_{0}{1}", splOffer.SpecialOfferID.value.ToString(), ".htm");
        filePath += fileName;

        if (File.Exists(filePath))
            File.Delete(filePath);

        File.WriteAllText(filePath, splOffer.OfferDescription.value);

        #endregion Save File in file System...

        #region Insert/Update on Server
        //Save in ServerUpdates
        Data.ServerUpdates su = new Data.ServerUpdates();
        su.UpdateStatus(splOffer.ResellerID.value, "specialoffer");
        #endregion
    }

    private void Send_AndroidNotification(String DeviceTokenID, String msg)
    {
        Data.SpecialOffer so = new Data.SpecialOffer();

        String sourceCertificateFile = ConfigurationManager.AppSettings["iPhoneCertificateFile"].ToString();
        String certFilePath = Server.MapPath(sourceCertificateFile);

        so.SendPushNotification_Android(DeviceTokenID, msg, 1);
    }

    private void Send_iPhoneNotification(String DeviceToken, String message)
    {
        Data.SpecialOffer so = new Data.SpecialOffer();

        String sourceCertificateFile = ConfigurationManager.AppSettings["iPhoneCertificateFile"].ToString();
        String certFilePath = Server.MapPath(sourceCertificateFile);

        so.SendPushNotification_iPhone(DeviceToken, message, certFilePath, 1);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Int32 resellerID = Int32.Parse(Util.getValue(ddlReseller));
            Int32 existSpecialOfferID = 0;

            Data.SpecialOffer offer = new Data.SpecialOffer();
            DataSet dsOffer = offer.GetSpecialOfferListByReseller(resellerID);
            String deviceTokenID = String.Empty;
            Int32 deviceTypeID = 0;
            DataRow[] drOfferStatus = dsOffer.Tables[0].Select("OfferStatusID=1 and ResellerID=" + resellerID);

            foreach (DataRow rowOfferStatus in drOfferStatus)
            {
                if (drOfferStatus.Length > 0 && chkIsEnabled.Checked)
                {
                    existSpecialOfferID = Convert.ToInt32(rowOfferStatus["SpecialOfferID"].ToString());
                    offer.LoadID(existSpecialOfferID);
                    offer.OfferStatusID.value = 0; // Only 1 offer will be enable at a time for a particular reseller.
                    offer.Save();
                }
            }

            dtEndDateTime = Convert.ToDateTime(Util.getValue(txtEndDate) + " " + Util.getValue(txtEndTime));
            if (dtEndDateTime >= DateTime.Now)
            {
                //Sending Push Notification
                Data.DeviceSetting deviceSetting = new Data.DeviceSetting();

                DataSet dsGetToken = new DataSet();

                if (chkIsNotified.Checked && chkIsNotified.Enabled)
                {   
                    dsGetToken = deviceSetting.GetListByReseller(resellerID);

                    if (Util.IsValidDataSet(dsGetToken))
                    {
                        foreach (DataRow rowToken in dsGetToken.Tables[0].Rows)
                        {
                            deviceTypeID = Util.GetDataInt32(rowToken["DeviceTypeID"].ToString());
                            deviceTokenID = Util.GetDataString(rowToken["DeviceToken"].ToString());

                            if (deviceTypeID == 1) //Device Type ID 1: iPhone 2: Android 
                            {
                                try
                                {
                                    Send_iPhoneNotification(deviceTokenID, Util.getValue(txtOfferTitle));
                                }
                                catch (Exception ex)
                                {
                                    Data.ErrorLog el = new Data.ErrorLog();
                                    el.ModuleInfo.value = "SaveNotification : Send_iPhoneNotification()";
                                    if (ex.InnerException != null)
                                        el.Message.value = ex.InnerException.Message;
                                    else
                                        el.Message.value = ex.ToString();
                                    el.SmallMessage.value = ex.Message;
                                    el.Save();
                                }
                            }
                            else if (deviceTypeID == 2)
                            {
                                try
                                {
                                    Send_AndroidNotification(deviceTokenID, Util.getValue(txtOfferTitle));
                                }
                                catch (Exception ex)
                                {
                                    Data.ErrorLog el = new Data.ErrorLog();
                                    el.ModuleInfo.value = "SaveNotification : Send_AndroidNotification()";
                                    if (ex.InnerException != null)
                                        el.Message.value = ex.InnerException.Message;
                                    else
                                        el.Message.value = ex.ToString();
                                    el.SmallMessage.value = ex.Message;
                                    el.Save();
                                }
                            }
                        }
                    }

                }
            }

            SaveData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect("SpecialOffers.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("SpecialOffers.aspx");
    }
}
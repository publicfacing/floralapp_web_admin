﻿using System;
using Library.AppSettings;
using Library.Utility;
using System.Collections.Specialized;
using System.Data;

public partial class Admin_AddBlockDates : AppPage
{
    private Int32 BlockDateID
    {
        get;
        set;
    }

    private static DateTime dtToday
    {
        get;
        set;
    }

    private static DateTime dtPrevious
    {
        get;
        set;
    }

    AppVars appVar = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                BlockDateID = Util.GetDataInt32(nvc["BlockDateID"]);
            }

            Util.setValue(lblPageTitle, (BlockDateID > 0) ? "Edit Block Dates" : "Add Block Dates");
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        // Reseller DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, appVar.ResellerID);

        // Binding default date into date time picker.
        Util.setValue(txtStartDate, String.Format("{0:MM/dd/yyyy}", Util.GetServerDate()));
        Util.setValue(txtEndDate, String.Format("{0:MM/dd/yyyy}", Util.GetServerDate()));

        if (appVar.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, appVar.ResellerID);
            //ddlReseller.Enabled = false;
            trShop.Visible = false;
        }

        if (BlockDateID <= 0)
            return;

        Data.BlockDates BlockDates = new Data.BlockDates(BlockDateID);

        Util.setValue(ddlReseller, BlockDates.ResellerID.value);
        Util.setValue(ddlType, BlockDates.BlockDateTypeID.value);
        Util.setValue(ddlRepeat, BlockDates.RepeatTypeID.value);
        Util.setValue(txtMessage, BlockDates.Message.value);
        Util.setValue(chkOrderAllowed, Convert.ToBoolean(BlockDates.OrderAllowed.value));
        Util.setValue(txtStartDate, String.Format("{0:MM/dd/yyyy}", BlockDates.StartDateTime.value));

        if (!BlockDates.RepeatUntilDate.value.Year.Equals(1900))
            Util.setValue(txtRepeatUntil, String.Format("{0:MM/dd/yyyy}", BlockDates.RepeatUntilDate.value));

        if (!BlockDates.EndDateTime.value.Year.Equals(1900))
            Util.setValue(txtEndDate, String.Format("{0:MM/dd/yyyy}", BlockDates.EndDateTime.value));

        if (ddlRepeat.SelectedIndex > 0)
            trRepeatUntil.Visible = true;


        // Disable start date in Edit Mode
        txtStartDate.Enabled = false;
        ceStartDate.Enabled = false;
        ddlType.Items.RemoveAt(3);

        dtToday = Convert.ToDateTime(String.Format("{0:MM/dd/yyyy}", BlockDates.StartDateTime.value));
        dtPrevious = Convert.ToDateTime(String.Format("{0:MM/dd/yyyy}", BlockDates.StartDateTime.value));
    }

    private void SaveData(int blockDateTypeID)
    {
        Data.BlockDates BlockDates = new Data.BlockDates(BlockDateID);

        BlockDates.ResellerID.value = Int32.Parse(Util.getValue(ddlReseller));

        if (blockDateTypeID > 0)
            BlockDates.BlockDateTypeID.value = blockDateTypeID;
        else
            BlockDates.BlockDateTypeID.value = Util.GetDataInt32(Util.getValue(ddlType));

        BlockDates.RepeatTypeID.value = Util.GetDataInt32(Util.getValue(ddlRepeat));
        BlockDates.RepeatUntilDate.value = Util.GetDataDateTime(Util.getValue(txtRepeatUntil));
        BlockDates.Message.value = Util.getValue(txtMessage);

        if (String.IsNullOrEmpty(Util.getValue(txtEndDate)))
        {
            Util.setValue(txtEndDate, Util.GetDataDateTime(Util.getValue(txtStartDate)));
        }

        BlockDates.EndDateTime.value = Util.GetDataDateTime(Util.getValue(txtEndDate));
        BlockDates.OrderAllowed.value = chkOrderAllowed.Checked ? true : false;
        BlockDates.StartDateTime.value = Util.GetDataDateTime(Util.getValue(txtStartDate));
        BlockDates.DateAdded.value = Util.GetServerDate();
        BlockDates.AddedBy.value = appVar.UserID;
        BlockDates.DateModified.value = Util.GetServerDate();
        BlockDates.ModifiedBy.value = appVar.UserID;
        BlockDates.DateDeleted.value = Util.GetServerDate();
        BlockDates.DeletedBy.value = appVar.UserID;
        BlockDates.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        BlockDates.Save();

        //Save in ServerUpdates
        Data.ServerUpdates su = new Data.ServerUpdates();
        su.UpdateStatus(BlockDates.ResellerID.value, "blockdates");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime dtStartDate = new DateTime();
            DateTime dtEndDate = new DateTime();
            DateTime dtRepeatUntil = new DateTime();
            TimeSpan tsResult;

            if (String.IsNullOrEmpty(Util.getValue(txtMessage).Trim()))
            {
                ShowError(lblErrMsg, "Enter block date message.");
                return;
            }

            if (BlockDateID <= 0)
                dtToday = Convert.ToDateTime(String.Format("{0:MM/dd/yyyy}", Util.GetServerDate()));
            else
                dtToday = dtPrevious;

            dtEndDate = Convert.ToDateTime(String.Format("{0:MM/dd/yyyy}", Util.GetServerDate()));
            dtRepeatUntil = Convert.ToDateTime(String.Format("{0:MM/dd/yyyy}", "01/01/1900"));


            if (String.IsNullOrEmpty(Util.getValue(txtStartDate)))
            {
                ShowError(lblErrMsg, "Enter start date.");
                return;
            }

            dtStartDate = Convert.ToDateTime(Util.GetDataString(Util.getValue(txtStartDate)));


            if (!String.IsNullOrEmpty(Util.getValue(txtEndDate)))
            {

                dtEndDate = Convert.ToDateTime(Util.GetDataString(Util.getValue(txtEndDate)));
                dtStartDate = Convert.ToDateTime(Util.GetDataString(Util.getValue(txtStartDate)));
                tsResult = new TimeSpan(dtEndDate.Ticks - dtStartDate.Ticks);

                #region End date validation

                //In case of Yearly  
                if (ddlRepeat.SelectedIndex == 1)
                {
                    double Years = tsResult.TotalDays / 365;

                    if (Years > 1)
                    {
                        ShowError(lblErrMsg, "End date can't be greater than one year from start date.");
                        return;
                    }
                }

                //In case of Monthly
                if (ddlRepeat.SelectedIndex == 2)
                {

                    //months
                    double months = dtEndDate.Month - dtStartDate.Month;
                    months = months.ToString().IndexOf("-") != -1 ? (tsResult.TotalDays / 30) : months;

                    if (double.Parse(months.ToString("N1")) > 1)
                    {
                        ShowError(lblErrMsg, "End date can't be greater than one month from start date.");
                        return;
                    }
                }

                //In case of Weekly
                if (ddlRepeat.SelectedIndex == 3)
                {
                    double Days = tsResult.TotalDays;

                    if (Days > 7)
                    {
                        ShowError(lblErrMsg, "End date can't be greater than one week from start date.");
                        return;
                    }
                }
                #endregion end of code
            }

            if (!String.IsNullOrEmpty(Util.getValue(txtRepeatUntil)))
            {
                if (ddlRepeat.SelectedIndex > 0 && Util.getValue(txtRepeatUntil) != "01/01/1900")
                {
                    dtRepeatUntil = Convert.ToDateTime(Util.GetDataString(Util.getValue(txtRepeatUntil)));
                    if (dtRepeatUntil < dtToday)
                    {
                        ShowError(lblErrMsg, "Repeat until date should be greater than today's date.");
                        return;
                    }
                }
            }

            if (BlockDateID == 0)
            {
                if (dtStartDate < dtToday)
                {
                    ShowError(lblErrMsg, "Start date should be greater than or equal today's date.");
                    return;
                }

                if (!String.IsNullOrEmpty(Util.getValue(txtEndDate)) && dtStartDate > dtEndDate)
                {
                    ShowError(lblErrMsg, "End date should be greater than or equals to start date");
                    return;
                }

            }
            else
            {
                if (dtStartDate < dtToday)
                {
                    ShowError(lblErrMsg, "Start date can't be past date.");
                    return;
                }
            }
            if (dtStartDate > dtEndDate && !String.IsNullOrEmpty(Util.getValue(txtEndDate)))
            {
                ShowError(lblErrMsg, "End date should be greater than or equals to start date");
                return;
            }

            #region Overlaping concept Removed..
            //else
            //{
            //    Data.BlockDates blockDate = new Data.BlockDates();
            //    Int32 isValidDate = 0;

            //    isValidDate = Convert.ToInt32(blockDate.GetBlockDatesByReseller(Util.GetDataInt32(Util.getValue(ddlReseller)), BlockDateID, dtStartDate, dtEndDate));

            //    if (isValidDate <= 0)
            //    {
            //        ShowError(lblErrMsg, "Start date or End date already exist with this block date range.");
            //        return;
            //    }
            //}
            #endregion End of overlaping concept.

            //Save data to Database
            if (ddlType.SelectedIndex == 3) // Both (Add Mode)
            {
                //Int32 resellerID = Util.GetDataInt32(Util.getValue(ddlReseller));

                //if (!IsExistsResellerAndBlockDateType(resellerID, 1))
                SaveData(1); // Saving record for delivery

                //if (!IsExistsResellerAndBlockDateType(resellerID, 2))
                SaveData(2); //Saving record for pickup
            }
            else if (BlockDateID < 1) // Add Mode
            {
                //Int32 blockDateTypeID = Util.GetDataInt32(Util.getValue(ddlType));
                //Int32 resellerID = Util.GetDataInt32(Util.getValue(ddlReseller));

                //if (!IsExistsResellerAndBlockDateType(resellerID, blockDateTypeID))
                SaveData(0);
            }
            else // Edit Mode
                SaveData(0);
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect("BlockDates.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("BlockDates.aspx");
    }

    protected void ddlRepeat_SelectedIndexChanged(object sender, EventArgs e)
    {
        trRepeatUntil.Visible = ddlRepeat.SelectedIndex > 0 ? true : false;
    }

    private Boolean IsExistsResellerAndBlockDateType(Int32 resellerID, Int32 type)
    {
        Boolean IsExists = false;
        DataSet ds = new DataSet();
        Data.BlockDates bd = new Data.BlockDates();

        ds = bd.IsExistsResellerAndBlockDateType(resellerID, type);

        if (Util.IsValidDataSet(ds))
            IsExists = true;

        return IsExists;
    }

}

﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;
using System.Configuration;

public partial class Admin_SpecialOffers : AppPage
{
    private int rowCount = 0;
    private int totalRowCount = 0;

    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            InitializeControls();
            BindData();
        }
    }

    private void InitializeControls()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlResellers, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlResellers, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlResellers, v.ResellerID);
            //ddlResellers.Enabled = false;
            divShop.Visible = false;
        }

        #region Getting visibility of push notification from config settings.

        Int32 IsActivePushNotification = 0;

        IsActivePushNotification = Util.GetDataInt32(ConfigurationManager.AppSettings["IsPushNotificationActive"].ToString());
        btnSendNotification.Visible = IsActivePushNotification > 0 ? true : false;

        #endregion
    }

    private void BindData()
    {
        Util.setValue(gvSpecialOffers, GetSpecialOfferList());

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvSpecialOffers.Columns[3].Visible = false;
        }
    }

    private DataSet GetSpecialOfferList()
    {
        StringBuilder sbFilter = new StringBuilder();

        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();

        switch (searchOperator)
        {
            case "Begins With":
                sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                break;
            case "Ends With":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                break;
            case "Contains":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                break;
            case "Equals":
                sbFilter.Append(" And " + searchField + " = " + searchString);
                break;
            default:
                break;
        }


        Int32 resellerID = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? Convert.ToInt32(v.ResellerID) : Convert.ToInt32(Util.getValue(ddlResellers));

        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);

        String orderBy = gvSpecialOffers.OrderBy;

        Data.SpecialOffer splSpecialOffer = new Data.SpecialOffer();
        DataSet dsSpecialOffer = splSpecialOffer.FilteredList(gvSpecialOffers.PageIndex, gvSpecialOffers.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvSpecialOffers.VirtualItemCount = totalRowCount;

        // Set Total Count in grid footer
        Util.setValue(lblTotalSpecialOffer, String.Format("{0} Special Offer(s).", totalRowCount));

        return dsSpecialOffer;
    }

    protected void gvSpecialOffers_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSpecialOffers.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvSpecialOffers_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddSpecialOffer.aspx";

        try
        {
            Int32 specialOfferID = Util.GetDataInt32(((ImageButton)sender).CommandArgument);

            String qs = Crypto.EncryptQueryString("SpecialOfferID=" + specialOfferID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int32 specialOfferID = Convert.ToInt32(objButton.CommandArgument.ToString());
            Data.SpecialOffer splSpecialOffer = new Data.SpecialOffer();
            splSpecialOffer.LoadID(specialOfferID);

            Int32 resellerID = splSpecialOffer.ResellerID.value;

            splSpecialOffer.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
            splSpecialOffer.DateDeleted.value = Util.GetServerDate();
            splSpecialOffer.DeletedBy.value = v.UserID;
            splSpecialOffer.Save();

            #region Inactivate/Delete the Relationship table(s) for this user
            splSpecialOffer.InActiveRelationshipTables(specialOfferID, "SpecialOffer");
            #endregion End of inactivate/delete process.

            #region Insert/Update on Server
            //Save in ServerUpdates
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatus(resellerID, "specialoffer");
            #endregion

            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void gvSpecialOffer_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSpecialOffers.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvSpecialOffer_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddRootsplSpecialOffer.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String rootSpecialOfferID = objButton.CommandArgument.ToString();
            String qs = Crypto.EncryptQueryString("RootSpecialOfferID=" + rootSpecialOfferID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void btnSendNotification_Click(object sender, EventArgs e)
    {
        try
        {
            if (IsValidChecked())
            {
                Data.DeviceSetting deviceSetting = new Data.DeviceSetting();
                Data.SpecialOffer so = new Data.SpecialOffer();

                Int32 specialOfferID = 0;
                String deviceToken = String.Empty;
                String deviceID = String.Empty;
                String sourceCertificateFile = ConfigurationManager.AppSettings["iPhoneCertificateFile"].ToString();
                Int32 deviceTypeID = 0;
                Int32 resellerID = 0;
                String certFilePath = Server.MapPath(sourceCertificateFile);
                CheckBox chkNotify = new CheckBox();
                DataSet dsGetToken = new DataSet();

                for (int i = 0; i < gvSpecialOffers.Rows.Count; i++)
                {
                    chkNotify = (CheckBox)gvSpecialOffers.Rows[i].FindControl("chkNotify");

                    if (chkNotify.Checked)
                    {
                        specialOfferID = Convert.ToInt32(chkNotify.Attributes["SpecialOfferID"]);
                        resellerID = Convert.ToInt32(chkNotify.Attributes["ResellerID"]);
                        dsGetToken = deviceSetting.GetListByReseller(resellerID);
                        so.LoadID(specialOfferID);
                        if (so.OfferEndDate.value >= DateTime.Now)
                        {
                            if (Util.IsValidDataSet(dsGetToken))
                            {
                                foreach (DataRow rowToken in dsGetToken.Tables[0].Rows)
                                {
                                    deviceTypeID = Util.GetDataInt32(rowToken["DeviceTypeID"].ToString());
                                    deviceToken = Util.GetDataString(rowToken["DeviceToken"].ToString());
                                    deviceID = Util.GetDataString(rowToken["DeviceID"].ToString());

                                    Int32 logNotification = Util.GetDataInt32(String.IsNullOrEmpty(ConfigurationManager.AppSettings["LogNotification"]) ? "0" : ConfigurationManager.AppSettings["LogNotification"].ToString());
                                    if (logNotification == 1)
                                        Log.WriteLog(1, "SendPushNotification",
                                                string.Format("deviceTypeID:{0}; deviceID:{1}; deviceToken:{2}", deviceTypeID, deviceID, deviceToken),
                                                "Checking PNs", Util.GetServerDate());
                                    if (deviceTypeID == 1) //Device Type ID 1: iPhone 2: Android
                                    {
                                        Int32 isiPhonePushActive = Convert.ToInt32(ConfigurationManager.AppSettings["IsiPhonePushActive"]);
                                        if (isiPhonePushActive == 1)
                                            try
                                            {
                                                string iOS_PN_Message = so.OfferTitle.value;
                                                //if (!string.IsNullOrEmpty(so.OfferDescription.value))
                                                //    iOS_PN_Message += " - " + Server.HtmlDecode(so.OfferDescription.value);
                                                //if (iOS_PN_Message.Length > 170)
                                                //    iOS_PN_Message = iOS_PN_Message.Substring(0, 170) + "...";
                                                //so.SendPushNotification_iPhone(deviceToken, Util.GetDataString(chkNotify.ValidationGroup), certFilePath);
                                                so.SendPushNotification_iPhone(deviceToken, Util.GetDataString(iOS_PN_Message), certFilePath, 1);
                                            }
                                            catch (Exception ex)
                                            {
                                                Data.ErrorLog el = new Data.ErrorLog();
                                                el.ModuleInfo.value = "SaveNotification : Send_iPhoneNotification()";
                                                if (ex.InnerException != null)
                                                    el.Message.value = ex.InnerException.Message;
                                                else
                                                    el.Message.value = ex.ToString();
                                                el.SmallMessage.value = ex.Message;
                                                el.Save();
                                            }
                                    }
                                    else if (deviceTypeID == 2)
                                    {
                                        Int32 isAndroidPushActive = Convert.ToInt32(ConfigurationManager.AppSettings["IsAndroidPushActive"]);
                                        if (isAndroidPushActive == 1)
                                        {
                                            try
                                            {
                                                string android_PN_Message = so.OfferTitle.value;
                                                //if (!string.IsNullOrEmpty(so.OfferDescription.value))
                                                //    android_PN_Message += " - " + Server.HtmlDecode(so.OfferDescription.value);
                                                //if (android_PN_Message.Length > 900)
                                                //    android_PN_Message = android_PN_Message.Substring(0, 900) + "...";
                                                //so.SendPushNotification_Android(deviceToken, Util.GetDataString(chkNotify.ValidationGroup));
                                                so.SendPushNotification_Android(deviceToken, Util.GetDataString(android_PN_Message), 1);
                                            }
                                            catch (Exception ex)
                                            {
                                                Data.ErrorLog el = new Data.ErrorLog();
                                                el.ModuleInfo.value = "SaveNotification : Send_AndroidNotification()";
                                                if (ex.InnerException != null)
                                                    el.Message.value = ex.InnerException.Message;
                                                else
                                                    el.Message.value = ex.ToString();
                                                el.SmallMessage.value = ex.Message;
                                                el.Save();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        so.LoadID(specialOfferID);
                        so.IsNotified.value = true;
                        so.DateNotified.value = Util.GetServerDate();
                        so.NotifiedBy.value = v.UserID;
                        so.Save();
                    }
                }
            }
            else if (IsVisibleChecked())
            {
                ShowError(lblErrMsg, "Select a special offer to send the notification.");
            }
            else
            {
                ShowError(lblErrMsg, "Please enable a special offer to send the notification.");
            }
        }
        catch (Exception ex)
        {
            string msg = ex.StackTrace;
            if (ex.InnerException != null)
                msg += "  ----  " + ex.InnerException.StackTrace;
            Log.WriteLog(101, "Special Offer -> Send PN", ex.Message, msg, DateTime.Now);
            ShowError(lblErrMsg, ex.StackTrace);
            return;
        }

        BindData();
    }

    protected Boolean IsValidChecked()
    {
        Boolean isValid = false;
        CheckBox chkNodify = new CheckBox();

        for (int i = 0; i < gvSpecialOffers.Rows.Count; i++)
        {
            chkNodify = ((CheckBox)gvSpecialOffers.Rows[i].FindControl("chkNotify"));
            if (chkNodify.Visible && chkNodify.Checked)
            {
                isValid = true;
                break;
            }
        }
        return isValid;
    }

    protected Boolean IsVisibleChecked()
    {
        Boolean isVisible = false;
        CheckBox chkNodify = new CheckBox();

        for (int i = 0; i < gvSpecialOffers.Rows.Count; i++)
        {
            chkNodify = ((CheckBox)gvSpecialOffers.Rows[i].FindControl("chkNotify"));
            if (chkNodify.Visible)
            {
                isVisible = true;
                break;
            }
        }
        return isVisible;
    }

    protected void lnkEnable_Click(object sender, EventArgs e)
    {
        try
        {
            String[] strValues = ((ImageButton)sender).CommandArgument.Split(',');

            Int32 specialOfferID = Util.GetDataInt32(strValues[0].ToString());
            Int32 resellerID = Util.GetDataInt32(strValues[1].ToString());

            Data.SpecialOffer so = new Data.SpecialOffer();

            DataSet dsOffer = so.GetSpecialOfferListByReseller(resellerID);
            DataRow[] drOfferStatus = dsOffer.Tables[0].Select("OfferStatusID=1 and ResellerID=" + resellerID);
            Int32 rowSpecialOfferID = 0;

            foreach (DataRow rowOfferStatus in drOfferStatus)
            {
                if (drOfferStatus.Length > 0)
                {
                    rowSpecialOfferID = Convert.ToInt32(rowOfferStatus["SpecialOfferID"].ToString());
                    so.LoadID(rowSpecialOfferID);
                    so.OfferStatusID.value = 0; // Only 1 offer will be enable at a time for a particular reseller.
                    so.Save();
                }
            }
            //until 
            if (rowSpecialOfferID != specialOfferID)
            {
                so.LoadID(specialOfferID);
                if (so.OfferStatusID.value == 0)
                    so.OfferStatusID.value = 1;
                so.Save();
            }

            #region Insert/Update on Server
            //Save in ServerUpdates
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatus(so.ResellerID.value, "specialoffer");
            #endregion

            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }

    }
}

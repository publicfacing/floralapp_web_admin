﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="BlockDates.aspx.cs" Inherits="Admin_BlockDates" Title="floralapp&reg; | Block Dates"
    Theme="GraySkin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12">
                    <tr>
                        <th class="TableHeadingBg TableHeading" colspan="2">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                text-indent: 5px;">
                                <tr>
                                    <td>
                                        Block Dates
                                    </td>
                                    <td style="text-align: right; padding-right: 5px; text-transform: capitalize;">
                                        <asp:HyperLink ID="lnkAddBlockDates" runat="server" NavigateUrl="~/Admin/AddBlockDates.aspx"
                                            ToolTip="Add BlockDates">Add Block Dates</asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label" valign="top">
                                            Search :
                                        </td>
                                        <td valign="top">
                                            <div runat="server" id="divShop">
                                                <label class="label">
                                                    Shop:</label>
                                                <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                            </div>
                                            <asp:DropDownList ID="ddlSearchField" runat="server" AutoPostBack="false">
                                                <asp:ListItem Text="ID" Value="BlockDateID"></asp:ListItem>
                                                <asp:ListItem Text="Shop" Value="ResellerName"></asp:ListItem>
                                                <asp:ListItem Text="Shop ID" Value="ResellerID"></asp:ListItem>
                                                <asp:ListItem Text="Start Time" Value="StartDateTime"></asp:ListItem>
                                                <asp:ListItem Text="End Time" Value="EndDateTime"></asp:ListItem>
                                                <asp:ListItem Text="Message" Value="Message"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchOperator" runat="server">
                                                <asp:ListItem Value="Begins With">Begins With</asp:ListItem>
                                                <asp:ListItem Value="Ends With">Ends With</asp:ListItem>
                                                <asp:ListItem Value="Contains">Contains</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnFilter" class="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click"
                                                CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <asp:DataPager ID="dpTop" runat="server" PageSize="25" PagedControlID="gvBlockDates">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                        ShowLastPageButton="false" ShowNextPageButton="false" />
                                    <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                        NextPreviousButtonCssClass="command" />
                                    <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                        ShowLastPageButton="true" ShowNextPageButton="true" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <mo:ExtGridView ID="gvBlockDates" runat="server" DataKeyNames="BlockDateID" AllowSorting="True"
                                OnSorting="gvBlockDates_OnSorting" orderby="BlockDateID Desc" SkinID="gvskin"
                                OnPageIndexChanging="gvBlockDates_OnPageIndexChanging" EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:BoundField DataField="BlockDateID" SortExpression="BlockDateID" HeaderText="ID"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="ResellerID" SortExpression="ResellerID" HeaderText="Shop ID"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="ResellerName" SortExpression="ResellerName" HeaderText="Shop"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="StartDateTime" SortExpression="StartDateTime" HeaderText="Start Time"
                                        ItemStyle-CssClass="gridContent" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>
                                    <asp:BoundField DataField="EndDateTime" SortExpression="EndDateTime" HeaderText="End Time"
                                        ItemStyle-CssClass="gridContent" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>
                                    <asp:BoundField DataField="Message" SortExpression="Message" HeaderText="Message"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="BlockDateTypeID" SortExpression="BlockDateTypeID" HeaderText="Type"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="RepeatTypeID" SortExpression="RepeatTypeID" HeaderText="Repeat"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Repeat Until" SortExpression="RepeatUntilDate" ItemStyle-CssClass="gridContent">
                                        <HeaderStyle CssClass="gridHeader" Width="80px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblUntilDate" runat="server" Text='<%# Eval("RepeatUntilDate").ToString().IndexOf("1900") != -1? "" : Eval("RepeatUntilDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Order Allowed" SortExpression="OrderAllowed" ItemStyle-CssClass="gridContent"
                                        Visible="false">
                                        <HeaderStyle CssClass="gridHeader" Width="80px" />
                                        <ItemTemplate>
                                            <img id="Img1" alt="" runat="server" src='<%# Eval("OrderAllowed").ToString().Equals("True")?"~/Images/tick.png":"~/Images/tick-gray.png" %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="gridContent">
                                        <HeaderStyle CssClass="gridHeader" Width="80px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("BlockDateID")%>'
                                                CausesValidation="false" OnClick="lnkEdit_Click" ToolTip="Edit" ImageUrl="~/Images/edit-icon.jpg" />
                                            <asp:ImageButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("BlockDateID")%>'
                                                CausesValidation="false" OnClick="lnkDelete_Click" ImageUrl="~/Images/DeleteButton.jpg"
                                                OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotalCategory" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="25" PagedControlID="gvBlockDates">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

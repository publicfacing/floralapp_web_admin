﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;
using System.IO;
using System.Drawing;

public partial class Admin_Reminders : AppPage
{
    AppVars v = new AppVars();
    private int rowCount = 0;
    private int totalRowCount = 0;

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (v.UserRoleID != 1)
        {
            LabelAuthenticateError.Visible = true;
            contentTable.Visible = false;
            LabelAuthenticateError.Text = "Your User does not have enought rights to perform this action";
        }

        if (!IsPostBack)
        {
            BindData(true);
        }
    }

    private void BindData(bool defaultUSA)
    {
        Data.Country country = new Data.Country();
        Util.setValue(ddlCountry, country.List(), "CountryName", "CountryId");
        if(defaultUSA)
            Util.setValue(ddlCountry, 233);
        Util.setValue(gvReminders, GetReminderList());
    }

    private DataSet GetReminderList()
    {
        StringBuilder sbFilter = new StringBuilder();

        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();

        if (!String.IsNullOrEmpty(searchString))
        {
            switch (searchOperator)
            {
                case "Begins With":
                    sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                    break;
                case "Ends With":
                    sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                    break;
                case "Contains":
                    sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                    break;
                case "Equals":
                    sbFilter.Append(" And " + searchField + " = " + searchString);
                    break;
                default:
                    break;
            }
        }

        if (txtStartDate.Text != "" && txtEndDate.Text != "")
            sbFilter.Append(" And  (DateAdded  >='" + Convert.ToDateTime(txtStartDate.Text).ToShortDateString() + "' and DateAdded  <='" + Convert.ToDateTime(txtEndDate.Text).ToShortDateString() + "' )");

        if (ddlCountry.SelectedIndex > 0)
            sbFilter.Append(" And CountryID =" + Util.getValue(ddlCountry));

        String orderBy = gvReminders.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " ReminderDefID asc ";

        Data.ReminderDef p = new Data.ReminderDef();
        DataSet dsReminders = p.GetList(gvReminders.PageIndex, gvReminders.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvReminders.VirtualItemCount = totalRowCount;

        // Set Total Count in grid footer
        Util.setValue(lblTotalReminders, String.Format("{0} Reminder(s).", totalRowCount));

        return dsReminders;
    }

    protected void gvReminders_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData(false);
    }

    protected void gvReminders_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvReminders.PageIndex = e.NewPageIndex;
        BindData(false);
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddReminder.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            string[] value = objButton.CommandArgument.ToString().Split(',');
            String ReminderDefID = value[0].ToString();

            String qs = Crypto.EncryptQueryString("ReminderDefID=" + ReminderDefID);

            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int64 ReminderDefID = Convert.ToInt64(objButton.CommandArgument.ToString());

            Data.ReminderDef p = new Data.ReminderDef(ReminderDefID);
            p.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
            p.DateDeleted.value = Util.GetServerDate();
            p.DeletedBy.value = Convert.ToInt64(v.UserID);
            p.Save();

            BindData(false);
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData(false);
    }

    protected string GetCountryName(string countryID)
    {
        foreach (ListItem item in ddlCountry.Items)
            if (item.Value == countryID)
                return item.Text;
        return "";
    }
}
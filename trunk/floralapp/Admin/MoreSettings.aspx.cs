﻿using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Library.Utility;
using MetaOption.Web.UI.WebControls;
using Library.AppSettings;
using System.Text;
using System.Text.RegularExpressions;

public partial class Admin_MoreSettings : AppPage
{
    private static String filePath
    {
        get;
        set;
    }

    AppVars v = new AppVars();
    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        if (!IsPostBack)
        {
            GenerateMoreSettingLinks();

            //if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
            //{
            //    lbMoreSettings.Visible = false;
            //    gvMoreSettings.Columns[0].Visible = false;
            //}
        }
    }

    private void GenerateMoreSettingLinks()
    {
        filePath = Server.MapPath(@"~/DynFiles\MoreSettings\");
        String appUrl = ConfigurationManager.AppSettings["AppUrl"];
        DirectoryInfo di = new DirectoryInfo(filePath);
        FileInfo[] fi = di.GetFiles("*.*");

        DataTable dtSettings = new DataTable();
        dtSettings.Columns.Add("Url");
        dtSettings.Columns.Add("FileName");
        DataRow drSettings;

        for (int i = 0; i < fi.Length; i++)
            if (fi[i].Name == "PrivacyPolicy_0.htm" || fi[i].Name == "Terms of Use - Service Agreement.htm")
            {
                drSettings = dtSettings.NewRow();
                drSettings["Url"] = appUrl + "DynFiles/MoreSettings/" + fi[i].Name;
                //Regx is used to add space between file name (i.e. [InitCaps] >> [Init Caps])
                drSettings["FileName"] = Regex.Replace(fi[i].Name, "[A-Z]", " $0").Trim().Replace(".htm", "").Replace("_0", "");
                dtSettings.Rows.Add(drSettings);
            }

        gvMoreSettings.DataSource = dtSettings;
        gvMoreSettings.DataBind();
    }

    protected void lbMoreSettings_Click(object sender, EventArgs e)
    {
        RadioButton rdoSelected = new RadioButton();
        HyperLink hlEditSettings = new HyperLink();

        filePath = Server.MapPath("~/DynFiles/MoreSettings/");

        foreach (GridViewRow gvRow in gvMoreSettings.Rows)
        {
            rdoSelected = (RadioButton)gvRow.FindControl("rdoMoreSettings");
            hlEditSettings = (HyperLink)gvRow.FindControl("hlSettings");

            if (rdoSelected.Checked)
            {
                filePath += Path.GetFileName(hlEditSettings.NavigateUrl);
                EditHTMLfile(filePath);
                mpeMoreSettings.Show();
                break;
            }
        }
    }

    private void EditHTMLfile(String path)
    {
        StreamReader sr = new StreamReader(path);
        rteMoreSettings.Content = sr.ReadToEnd();
        sr.Close();
        sr.Dispose();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        StreamWriter sw = new StreamWriter(filePath);
        StringBuilder sb = new StringBuilder();

        sb.Append(String.Format(@"<html xmlns=""http://www.w3.org/1999/xhtml"">
                    <head>
                        <meta http-equiv=""Pragma"" content=""no-cache"" />
                        <meta http-equiv=""Expires"" content=""-1"" />
                        <title>floralapp : More Settings</title>
                    </head>
                    <body>
                        {0}
                    </body>
                    </html>", rteMoreSettings.Content));

        sw.Write(sb.ToString());
        sw.Close();
    }
}


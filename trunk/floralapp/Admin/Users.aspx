﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="Users.aspx.cs" Inherits="Admin_Users" Title="floralapp&reg; | Users"
    Theme="GraySkin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <asp:Label ID="LabelAuthenticateError" runat="server" Text="Label" Visible="false"></asp:Label>
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12" id="contentTable" runat="server">
                    <tr>
                        <th class="TableHeadingBg TableHeading" colspan="2">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                text-indent: 5px;">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                                    </td>
                                    <td style="text-align: right; padding-right: 5px; text-transform: capitalize;">
                                        <asp:LinkButton ID="lnkAdd" runat="server" OnClick="btnAdd_Click"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label">
                                            Search :
                                        </td>
                                        <td>
                                            <label class="label">
                                                Status:</label>
                                            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                <asp:ListItem Value="-1" Text="New" Selected="True"></asp:ListItem>
                                                <%--<asp:ListItem Value="1" Text="Processed"></asp:ListItem>--%>
                                                <asp:ListItem Value="0" Text="InActive"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlUserRole" runat="server" Visible="false">
                                                <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Super Admin"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Shop Admin"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Consumer"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="lblReseller" CssClass="label" Text="Shop" runat="server"></asp:Label>
                                            <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSearchField" runat="server">
                                                <asp:ListItem Value="FirstName" Text="First Name"></asp:ListItem>
                                                <asp:ListItem Value="LastName" Text="Last Name"></asp:ListItem>
                                                <asp:ListItem Value="LoginID" Text="LoginID"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchOperator" runat="server">
                                                <asp:ListItem Value="Begins With">Begins With</asp:ListItem>
                                                <asp:ListItem Value="Ends With">Ends With</asp:ListItem>
                                                <asp:ListItem Value="Contains">Contains</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnFilter" class="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click"
                                                CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <asp:DataPager ID="dpTop" runat="server" PageSize="25" PagedControlID="gvUsers">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                        ShowLastPageButton="false" ShowNextPageButton="false" />
                                    <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                        NextPreviousButtonCssClass="command" />
                                    <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                        ShowLastPageButton="true" ShowNextPageButton="true" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <mo:ExtGridView ID="gvUsers" runat="server" DataKeyNames="UserID" AllowSorting="True"
                                OnSorting="gvUsers_OnSorting" orderby="UserID Desc" SkinID="gvskin" OnPageIndexChanging="gvUsers_OnPageIndexChanging"
                                EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:BoundField DataField="UserID" SortExpression="UserID" HeaderText="User ID" ItemStyle-CssClass="gridContent"
                                        HeaderStyle-Width="50px"></asp:BoundField>
                                    <asp:BoundField DataField="UserName" SortExpression="UserName" HeaderText="User Name"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="LoginID" SortExpression="LoginID" HeaderText="Login ID"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="Role" SortExpression="Role" HeaderText="Role" ItemStyle-CssClass="gridContent"
                                        HeaderStyle-Width="100px"></asp:BoundField>
                                    <asp:BoundField DataField="ResellerName" SortExpression="ResellerName" HeaderText="Shop"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="DateAdded" SortExpression="DateAdded" HeaderText="Added On"
                                        DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="60px" ItemStyle-CssClass="gridContent">
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="gridContent">
                                        <HeaderStyle CssClass="gridHeader" Width="80px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("UserID")+ "," + Eval("ResellerID")%>'
                                                CausesValidation="false" OnClick="lnkEdit_Click" ToolTip="Edit" ImageUrl="~/Images/edit-icon.jpg" />
                                            <asp:ImageButton ID="lnkDelete" runat="server" CommandArgument='<%# Eval("UserID")%>'
                                                CausesValidation="false" OnClick="lnkDelete_Click" ImageUrl="~/Images/DeleteButton.jpg"
                                                OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" ItemStyle-CssClass="gridContent halign-center"
                                        HeaderStyle-Width="60px" SortExpression="IsActive" Visible="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgStatus" runat="server" ImageUrl='<%# Eval("IsActive").ToString().Equals("1")?"~/Images/tick.png":"~/Images/tick-gray.png" %>'
                                                OnClick="ImgStatus_Click" mageAlign="Top" ToolTip='<%# Eval("UserID") %>' CommandArgument='<%# Eval("IsActive") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotalCategory" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="25" PagedControlID="gvUsers">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="Dashboard.aspx.cs" Inherits="Admin_Dashboard" Title="floralapp&reg; | Dashboard" Theme="GraySkin" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false"></asp:Label>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <th class="TableHeadingBg">
                Recent Purchase(s)
            </th>
        </tr>
        <tr>
            <td class="TableBorder">
                <asp:GridView ID="gvRecentPurchase" runat="server" DataKeyNames="PurchaseID" AllowSorting="false"
                    AllowPaging="false" SkinID="gvskin" AutoGenerateColumns="false" EmptyDataText="No record(s) found.">
                    <Columns>
                        <asp:BoundField DataField="User" HeaderText="User" HeaderStyle-CssClass="gridHeader"
                            ItemStyle-CssClass="gridContent" HeaderStyle-Width="200px"></asp:BoundField>
                        <asp:BoundField DataField="Reseller" HeaderText="Shop" HeaderStyle-CssClass="gridHeader"
                            ItemStyle-CssClass="gridContent" HeaderStyle-Width="180px"></asp:BoundField>
                        <asp:TemplateField HeaderStyle-CssClass="gridHeader" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="120px" ItemStyle-CssClass="gridContent" HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                <div style="color: Navy;">
                                    <i>
                                        <%# Eval("AccountNo") %>
                                    </i>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-CssClass="gridHeader" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="60px" ItemStyle-CssClass="gridContent" HeaderText="Card Type">
                            <ItemTemplate>
                                <asp:Label ID="lblCardType" runat="server" Text='<%# getCardName(Eval("CardType").ToString())%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="PurchaseDate" HeaderText="Purchase Date" HeaderStyle-CssClass="gridHeader"
                            DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="100px" ItemStyle-CssClass="gridContent">
                        </asp:BoundField>
                        <asp:TemplateField HeaderStyle-CssClass="gridHeader" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="20px" ItemStyle-CssClass="gridContent">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnPreviewPurchase" runat="server" CommandArgument='<%# Eval("PurchaseID")%>'
                                    CausesValidation="false" OnClick="btnPreviewPurchase_Click" ToolTip="User Profile Preview"
                                    ImageUrl="~/Images/Pop-Up.gif" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:HyperLink ID="lnkViewPurchase" runat="server" NavigateUrl="~/Admin/ViewAllPurchases.aspx"
                    ToolTip="Show More Purchases"> Show More</asp:HyperLink>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <th class="TableHeadingBg">
                Special Offer(s)
            </th>
        </tr>
        <tr>
            <td class="TableBorder">
                <asp:GridView ID="gvSpecialOffer" runat="server" DataKeyNames="SpecialOfferID" SkinID="gvskin"
                    AllowSorting="false" AutoGenerateColumns="False" EmptyDataText="No record(s) found.">
                    <Columns>
                        <asp:BoundField DataField="Reseller" HeaderText="Shop" HeaderStyle-CssClass="gridHeader"
                            ItemStyle-CssClass="gridContent" HeaderStyle-Width="200px"></asp:BoundField>
                        <asp:BoundField DataField="OfferTitle" HeaderText="Offer" HeaderStyle-CssClass="gridHeader"
                            ItemStyle-CssClass="gridContent" HeaderStyle-Width="180px"></asp:BoundField>
                        <asp:BoundField DataField="OfferStartDate" HeaderText="Start Date" HeaderStyle-CssClass="gridHeader"
                            DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="180px" ItemStyle-CssClass="gridContent">
                        </asp:BoundField>
                        <asp:BoundField DataField="OfferEndDate" HeaderText="End Date" HeaderStyle-CssClass="gridHeader"
                            DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="180px" ItemStyle-CssClass="gridContent">
                        </asp:BoundField>
                        <%--  <asp:BoundField DataField="OfferDescription" HeaderText="Description" HeaderStyle-CssClass="gridHeader"
                            ItemStyle-CssClass="gridContent" HeaderStyle-Width="180px"></asp:BoundField>--%>
                        <asp:TemplateField HeaderStyle-Width="20px" HeaderStyle-CssClass="gridHeader" ItemStyle-HorizontalAlign="Center"
                            ItemStyle-CssClass="gridContent">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnPreviewRecentOffers" runat="server" CommandArgument='<%# Eval("SpecialOfferID")%>'
                                    CausesValidation="false" OnClick="btnPreviewRecentOffers_Click" ToolTip="User Profile Preview"
                                    ImageUrl="~/Images/Pop-Up.gif" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:HyperLink ID="lnkViewOffers" runat="server" NavigateUrl="~/Admin/SpecialOffers.aspx"
                    ToolTip="Show More Special Offers"> Show More</asp:HyperLink>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <th class="TableHeadingBg">
                Recent Coupon(s)
            </th>
        </tr>
        <tr>
            <td class="TableBorder">
                <asp:GridView ID="gvCoupons" runat="server" DataKeyNames="CouponID" AllowSorting="false"
                    AllowPaging="false" SkinID="gvskin" AutoGenerateColumns="false" EmptyDataText="No record(s) found.">
                    <Columns>
                        <asp:BoundField DataField="Reseller" HeaderText="Shop" HeaderStyle-Width="200px"
                            HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridContent" />
                        <asp:BoundField DataField="CouponCode" HeaderText="Code" HeaderStyle-Width="200px"
                            HeaderStyle-CssClass="gridHeader" ItemStyle-CssClass="gridContent" />
                        <asp:BoundField DataField="CouponStartDate" HeaderText="Start Date" HeaderStyle-CssClass="gridHeader"
                            DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="180px" ItemStyle-CssClass="gridContent">
                        </asp:BoundField>
                        <asp:BoundField DataField="CouponEndDate" HeaderText="End Date" HeaderStyle-CssClass="gridHeader"
                            DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="180px" ItemStyle-CssClass="gridContent">
                        </asp:BoundField>
                        <asp:TemplateField HeaderStyle-CssClass="gridHeader" HeaderStyle-Width="20px" ItemStyle-HorizontalAlign="Center"
                            ItemStyle-CssClass="gridContent">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnPreviewCoupon" runat="server" CommandArgument='<%# Eval("CouponID")%>'
                                    CausesValidation="false" OnClick="btnPreviewCoupon_Click" ToolTip="Magazine Print Preview"
                                    ImageUrl="~/Images/Pop-Up.gif" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:HyperLink ID="lnkCoupon" runat="server" NavigateUrl="~/Admin/Coupons.aspx" ToolTip="Show More Coupon"> Show More</asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

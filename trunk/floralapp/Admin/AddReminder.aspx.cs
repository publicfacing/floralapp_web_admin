﻿using System;
using Library.AppSettings;
using Library.Utility;
using System.Collections.Specialized;
using System.Data;

public partial class Admin_AddReminder : AppPage
{
    AppVars v = new AppVars();

    private Int32 ReminderDefID
    {
        get;
        set;
    }

    private static DateTime dtToday
    {
        get;
        set;
    }

    AppVars appVar = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        if (v.UserRoleID != 1)
        {
            LabelAuthenticateError.Visible = true;
            contentTable.Visible = false;
            LabelAuthenticateError.Text = "Your User does not have enought rights to perform this action";
        }

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                ReminderDefID = Util.GetDataInt32(nvc["ReminderDefID"]);
            }

            Util.setValue(lblPageTitle, (ReminderDefID > 0) ? "Edit Reminder" : "Add Reminder");
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        Data.Country country = new Data.Country();
        Util.setValue(ddlCountry, country.List(), "CountryName", "CountryId");
        Util.setValue(txtStartDate, String.Format("{0:MM/dd/yyyy}", Util.GetServerDate()));

        if (ReminderDefID <= 0)
        {
            Util.setValue(ddlCountry, 233);
            return;
        }

        Data.ReminderDef reminder = new Data.ReminderDef(ReminderDefID);

        Util.setValue(ddlCountry, reminder.CountryID.value);
        Util.setValue(txtStartDate, String.Format("{0:MM/dd/yyyy}", reminder.Date.value));
        Util.setValue(txtTitle, reminder.Title.value);
        Util.setValue(txtDescription, reminder.Description.value);

        dtToday = Convert.ToDateTime(String.Format("{0:MM/dd/yyyy}", reminder.Date.value));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (String.IsNullOrEmpty(Util.getValue(txtStartDate)))
            {
                ShowError(lblErrMsg, "Enter reminder date.");
                return;
            }

            Data.ReminderDef reminder = new Data.ReminderDef(ReminderDefID);

            reminder.CountryID.value = Int32.Parse(Util.getValue(ddlCountry));
            reminder.Date.value = Convert.ToDateTime(Util.GetDataString(Util.getValue(txtStartDate)));
            reminder.Title.value = Util.getValue(txtTitle);
            reminder.Description.value = Util.getValue(txtDescription);

            if (ReminderDefID <= 0)
            {
                reminder.DateAdded.value = Util.GetServerDate();
                reminder.AddedBy.value = appVar.UserID;
            }
            reminder.DateModified.value = Util.GetServerDate();
            reminder.ModifiedBy.value = appVar.UserID;

            reminder.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);            
            reminder.Save();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect("Reminders.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Reminders.aspx");
    }
}

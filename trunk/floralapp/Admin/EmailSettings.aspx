﻿<%@ Page Title="floralapp&reg; | Email Settings" Language="C#" MasterPageFile="~/Admin/AdminMaster.master"
    AutoEventWireup="true" CodeFile="EmailSettings.aspx.cs" Inherits="Admin_EmailSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <asp:Label ID="LabelAuthenticateError" runat="server" Text="Label" Visible="false"></asp:Label>
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    id="contentTable" runat="server">
                    <tr>
                        <th class="TableHeadingBg TableHeading">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                padding-left: 5px;">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPageTitle" runat="server" Text="Email Settings"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder">
                            <span>Enter email addresses to which Help Request made by users will be sent.</span>
                            <br />
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error">
                            </asp:Label>
                            <asp:ValidationSummary ID="vsReseller" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                HeaderText="" ShowSummary="true" />
                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                <tr>
                                    <td class="col">
                                        Email 1
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtEmail1" runat="server" SkinID="textbox" class="FormItem" MaxLength="150"
                                            Width="400px"></asp:TextBox><br />
                                        <asp:RegularExpressionValidator ID="revEmail1" runat="server" ErrorMessage="Please enter a valid Email 1 address."
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,3}([-.]\w+)*" ControlToValidate="txtEmail1"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Email 2
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtEmail2" runat="server" SkinID="textbox" class="FormItem" MaxLength="150"
                                            Width="400px"></asp:TextBox><br />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Please enter a valid Email 2 address."
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,3}([-.]\w+)*" ControlToValidate="txtEmail2"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Email 3
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtEmail3" runat="server" SkinID="textbox" class="FormItem" MaxLength="150"
                                            Width="400px"></asp:TextBox><br />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Please enter a valid Email 3 address."
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,3}([-.]\w+)*" ControlToValidate="txtEmail3"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Email 4
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtEmail4" runat="server" SkinID="textbox" class="FormItem" MaxLength="150"
                                            Width="400px"></asp:TextBox><br />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Please enter a valid Email 4 address."
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,3}([-.]\w+)*" ControlToValidate="txtEmail4"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Email 5
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtEmail5" runat="server" SkinID="textbox" class="FormItem" MaxLength="150"
                                            Width="400px"></asp:TextBox><br />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Please enter a valid Email 5 address."
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,3}([-.]\w+)*" ControlToValidate="txtEmail5"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                            <div class="actionDiv">
                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save changes."
                                    OnClick="btnSave_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

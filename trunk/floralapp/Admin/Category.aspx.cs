﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;
using System.IO;

public partial class Admin_Category : AppPage
{
    private int rowCount = 0;
    private int totalRowCount = 0;
    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            InitializeControls();
            BindData();
        }
    }

    private void InitializeControls()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            divShop.Visible = false;
            //ddlReseller.Enabled = false;
            ddlSearchField.Items.RemoveAt(2);
            ddlSearchField.Items.RemoveAt(2);
        }
        else
            lnkReorder.Enabled = false;
        //lnkAddCategory.Enabled = false;
    }

    private void BindData()
    {
        Util.setValue(gvCategory, GetCategoryList());

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvCategory.Columns[3].Visible = false;
            gvCategory.Columns[4].Visible = false;
        }
    }

    private DataSet GetCategoryList()
    {

        StringBuilder sbFilter = new StringBuilder();
        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();

        switch (searchOperator)
        {
            case "Begins With":
                sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                break;
            case "Ends With":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                break;
            case "Contains":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                break;
            case "Equals":
                sbFilter.Append(" And " + searchField + " = " + searchString);
                break;
            default:
                break;
        }

        Int32 resellerID = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? Convert.ToInt32(v.ResellerID) : Convert.ToInt32(Util.getValue(ddlReseller));

        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);

        String orderBy = gvCategory.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " Sequence asc";

        Data.Category cat = new Data.Category();
        DataSet dsCategory = cat.FilteredList(gvCategory.PageIndex, gvCategory.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvCategory.VirtualItemCount = totalRowCount;

        #region Binding images ...
        /*
        foreach (DataRow drCat in dsCategory.Tables[0].Rows)
        {
            Byte[] fileByteData = (byte[]) drCat["ImageBinary"];
            String imageExt = Convert.ToString(drCat["ImageExt"]); 
            Int32 catID  = Convert.ToInt32(drCat["CategoryID"]);

            String filePath = Server.MapPath(@"~/DynImages\Category\");
            String fileName = String.Format("CategoryImage_{0}{1}", catID, imageExt);
            filePath += fileName;

            if (File.Exists(filePath))
                File.Delete(filePath);

            if (fileByteData != null)
            {
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteData, 0, fileByteData.Length);
                fs.Close();
            }            
        }*/

        #endregion End of binding image

        // Set Total Count in grid footer
        Util.setValue(lblTotalCategory, String.Format("{0} Categories.", totalRowCount));

        return dsCategory;
    }

    protected void gvCategory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCategory.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvCategory_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddCategory.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String categoryID = objButton.CommandArgument.ToString();
            String qs = Crypto.EncryptQueryString("CategoryID=" + categoryID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int32 categoryID = Convert.ToInt32(objButton.CommandArgument.ToString());
            Data.Category cat = new Data.Category(categoryID);
            cat.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);           
            cat.DateDeleted.value = Util.GetServerDate();
            cat.DeletedBy.value = v.UserID;
            cat.Save();

            //Save in ServerUpdates
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatus(cat.ResellerID.value, "category");

            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void gvCategory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //Response.BinaryWrite((byte[])e.Row.Cells[3]);
        }
    }

    protected void imgIsEnabled_Click(object sender, EventArgs e)
    {
        Boolean isEnabledCategory = false;
        String[] args = Util.GetDataString(((ImageButton)sender).CommandArgument).Split(',');
        Int32 categoryID = Util.GetDataInt32(args[0].ToString());
        Int32 resellerID = Util.GetDataInt32(args[1].ToString());
        Boolean isEnabled = Util.GetDataBool(args[2].ToString());

        Boolean isDisabled= canDisabledCategory(resellerID);

        isEnabledCategory = getIsEnabledCategory(resellerID);

        if (!isEnabledCategory)
        {
            ShowError(lblErrMsg, "Sorry! limit exceed(s) for this shop, it can not be enabled.");
            ((ImageButton)sender).ImageUrl = "~/Images/tick-gray.png";
        }

        if (isEnabled)
        {
            if (!isDisabled)
            {
                ShowError(lblErrMsg, "Sorry! minimum limit reached for this shop, it can not be disabled.");
                ((ImageButton)sender).ImageUrl = "~/Images/tick.png";
                return;
            }
            HideError(lblErrMsg);
        }
        Data.Category cat = new Data.Category(categoryID);
        cat.IsEnabled.value = isEnabled == false ? isEnabledCategory : false;
        cat.Save();

        //Save in ServerUpdates
        Data.ServerUpdates su = new Data.ServerUpdates();
        su.UpdateStatus(cat.ResellerID.value, "category");

        BindData();
    }

    protected void imgIsFeatured_Click(object sender, EventArgs e)
    {   
        String[] args = Util.GetDataString(((ImageButton)sender).CommandArgument).Split(',');
        Int32 categoryID = Util.GetDataInt32(args[0].ToString());
        Int32 resellerID = Util.GetDataInt32(args[1].ToString());
        Boolean isFeatured = Util.GetDataBool(args[2].ToString());
        
        Data.Category cat = new Data.Category(categoryID);
        cat.IsFeatured.value = !isFeatured;
        cat.Save();

        //Save in ServerUpdates
        Data.ServerUpdates su = new Data.ServerUpdates();
        su.UpdateStatus(cat.ResellerID.value, "category");

        BindData();
    }

    protected Boolean getIsEnabledCategory(Int32 resellerID)
    {
        Int32 resellerLimit = getMaxResellerLimitByReseller(resellerID);
        Int32 availableCategory = getAvailableCategoryByReseller(resellerID);
        Boolean isEnabled = false;

        if (resellerLimit > availableCategory)
            isEnabled = true;

        return isEnabled;
    }

    private Int32 getAvailableCategoryByReseller(Int32 resellerID)
    {
        Data.Category cat = new Data.Category();
        DataTable dtCategory = cat.GetCategoryListByReseller(resellerID).Tables[0];
        DataRow[] drCategory = dtCategory.Select("IsEnabled=1");

        return drCategory.Length;
    }

    private Int32 getMaxResellerLimitByReseller(Int32 resellerID)
    {
        Data.ResellerSetting rs = new Data.ResellerSetting();
        DataTable dtReseller = rs.GetSetting(resellerID).Tables[0];

        return Util.GetDataInt32(dtReseller, 0, "maxcatlimit");
    }

    private Boolean canDisabledCategory(Int32 resellerID)
    {
        Int32 resellerMinimumLimit = getMinResellerLimitByReseller(resellerID);
        Int32 availableMinimumCategory = getAvailableCategoryByReseller(resellerID);
        Boolean isDisabled = false;

        if (resellerMinimumLimit < availableMinimumCategory)
            isDisabled = true;

        return isDisabled;
    }

    private Int32 getMinResellerLimitByReseller(Int32 resellerID)
    {
        Data.ResellerSetting rs = new Data.ResellerSetting();
        DataTable dtReseller = rs.GetSetting(resellerID).Tables[0];

        return Util.GetDataInt32(dtReseller, 0, "mincatlimit");
    }

    public String getCategoryImage(String fileExtension)
    {
        String categoryImage = "../DynImages/Category/categoryimage_" + fileExtension;

        if(!File.Exists(Server.MapPath(categoryImage)))
            categoryImage = "~/Images/blank_img.png";

        return categoryImage;
    }
}


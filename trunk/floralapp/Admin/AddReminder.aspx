﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="AddReminder.aspx.cs" Inherits="Admin_AddReminder" Title="floralapp&reg; | Add Reminder"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="../UserControls/HelpMsg.ascx" TagName="HelpMsg" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/javascript">
        function DateTimeFormat(sender, args) {
            var d = sender._selectedDate;
            var now = new Date();
            sender.get_element().value = d.format("MM/dd/yyyy")
        }
    </script>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <asp:Label ID="LabelAuthenticateError" runat="server" Text="Label" Visible="false"></asp:Label>
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    id="contentTable" runat="server">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Add Reminder"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:UpdatePanel ID="upCoupon" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <uc1:HelpMsg ID="HelpMsg1" runat="server" />
                                    <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                                    <asp:ValidationSummary ID="vsBlockDates" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                        HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                                    <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                        <tr>
                                            <td class="col">
                                                Country<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="Select One " Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:RequiredFieldValidator ID="rfvResellerID" runat="server" ControlToValidate="ddlCountry"
                                                    ErrorMessage="Select a Country for this reminder." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col showhh" style="height: 34px">
                                                Date<span class="reqd">*</span>
                                            </td>
                                            <td class="val" style="height: 34px">
                                                <div style="width: 99%; border: none !important; text-decoration: none;">
                                                    <asp:TextBox runat="server" OnKeyUP="this.value=''" CssClass="textbox" ID="txtStartDate"
                                                        Width="70px"></asp:TextBox>
                                                    <ajax:CalendarExtender ID="ceStartDate" OnClientDateSelectionChanged="DateTimeFormat"
                                                        Format="MM/dd/yyyy" runat="server" TargetControlID="txtStartDate" PopupButtonID="imgCalIcon1">
                                                    </ajax:CalendarExtender>
                                                    <asp:Image ImageUrl="~/Images/calender-icon.gif" Style="cursor: hand" ID="imgCalIcon1"
                                                        runat="server" />
                                                    <ajax:TextBoxWatermarkExtender ID="twmStartDate" runat="server" TargetControlID="txtStartDate"
                                                        WatermarkText="MM/DD/YYYY" WatermarkCssClass="watermarked">
                                                    </ajax:TextBoxWatermarkExtender>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col showhh">
                                                Title<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <asp:TextBox ID="txtTitle" runat="server" SkinID="textbox" class="FormItem" MaxLength="100"></asp:TextBox>
                                                <br />
                                                <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
                                                    ErrorMessage="Enter a title." Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col showhh">
                                                Description<span class="reqd">*</span>
                                            </td>
                                            <td class="val">
                                                <mo:ExtTextBox ID="txtDescription" runat="server" TextMode="MultiLine" MaxLength="500"
                                                    SkinID="textarea"></mo:ExtTextBox>
                                                Max. 500 character.
                                                <br />
                                                <asp:RequiredFieldValidator ID="rfvMessage" runat="server" ControlToValidate="txtDescription"
                                                    ErrorMessage="Enter reminder description." Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="actionDiv">
                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this Country."
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                                    CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

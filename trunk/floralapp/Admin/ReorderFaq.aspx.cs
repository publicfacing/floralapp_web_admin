﻿using System;
using System.Data;
using Library.Utility;
using System.Collections.Generic;


public partial class Admin_ReorderFaq : AppPage
{
    private DataTable FAQTable
    {
        get
        {
            if (ViewState["dtFAQ"] == null)
                ViewState["dtFAQ"] = GetCategoryRootData();

            return (DataTable)ViewState["dtFAQ"];
        }
        set { ViewState["dtFAQ"] = value; }
    }

    private string[] ItemOrders
    {
        get
        {
            if (ViewState["itemsOrders"] == null)
                ViewState["itemsOrders"] = GetItemOrders();

            return (string[])ViewState["itemsOrders"];
        }
        set { ViewState["itemsOrders"] = value; }
    }
         
    public override void Page_Load(object sender, EventArgs e  )
    {
        base.HideError(lblErrMsg);
         
        if (!IsPostBack)
        {
            BindReorderList();
        }
    }

    private DataTable GetCategoryRootData()
    {
        Data.Faq faq = new Data.Faq();
        DataSet dsFAQ = faq.List();
        return (Util.IsValidDataSet(dsFAQ)) ? Util.GetDataTable(dsFAQ) : null;
    }    
   
    private void BindReorderList()
    {
        if (Util.IsValidDataTable(FAQTable))
        {
            rlFAQ.DataSource = FAQTable;
            rlFAQ.DataBind();
        }
    }

    private void SaveNewOrderList()
    {
        Data.Faq faq = new Data.Faq();
        DataTable dt = FAQTable;

        for (int i = 0; i < ItemOrders.Length; i++)
        {
            Int64 faqID = Int64.Parse(ItemOrders[i].ToString());
            faq.LoadID(faqID);
            faq.Sequence.value = i + 1;
            faq.Save();
        }

        FAQTable = null;
    }

    private string[] GetItemOrders()
    {
        if (!Util.IsValidDataTable(FAQTable))
            return null;

        DataTable dt = FAQTable; 
        string idList = "";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            String id = Util.GetDataString(dt, i, "FaqID");
            idList += (String.IsNullOrEmpty(idList)) ? id : "," + id;
        }

        string[] ret = idList.Split(',');
        //Array.Reverse(ret);

        return ret;
    }

    protected void rlFAQ_ItemReorder(object sender, AjaxControlToolkit.ReorderListItemReorderEventArgs e)
    {
        string[] tempArray = ItemOrders;

        List<string> list = new List<string>(tempArray); 
        string itemToMove = list[e.OldIndex];
        list.Remove(itemToMove);
        list.Insert(e.NewIndex, itemToMove);
        ItemOrders = list.ToArray();

        SaveNewOrderList();
        BindReorderList();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("FAQ.aspx");
    } 
}


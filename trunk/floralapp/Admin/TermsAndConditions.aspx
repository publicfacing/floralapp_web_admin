﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TermsAndConditions.aspx.cs"
    Inherits="TermsAndConditions" %>

<%@ Register Src="../UserControls/LoginStatus.ascx" TagName="LoginStatus" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>floralapp&reg; | Terms & Conditions</title>
    <link href="/floralapp/Css/Default.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        h2
        {
            color: #000000;
            font-size: 14px;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <center>
        <div class="mainDiv" id="mainDiv">
            <div class="headerDiv" style="border-bottom: 5px solid #194275;">
                <div class="headerLeft">
                    &nbsp;
                </div>
                <div class="headerRight">
                    <uc2:LoginStatus ID="LoginStatus1" runat="server" />
                </div>
            </div>
            <div class="contentDiv">
                <div>
                    <asp:Label ID="lblErrMsg" runat="server" CssClass="errMsg"></asp:Label>
                </div>
                <div style="margin: 10px; width: 60%">
                    <!-- Old code hidden on 09th June 2011 #region: was not displayed in actual format.-->
                    <!--
                    <h3>
                        Terms And Conditions</h3>
                    <div style="overflow-y: scroll; height: 250px; width: 100%; border: 1px solid #c0c0c0;
                         padding: 10px; text-align: justify;" runat="server" >
                    </div>
                    -->
                    <!-- New code changes on 09th June 2011 -->
                    <h3 style="color: #333333;">
                        Terms of Use/ Services Agreement</h3>
                    <div title="Not in use" style="overflow-y: scroll; height: 250px; width: 100%; border: 1px solid #c0c0c0;
                        padding: 10px; text-align: justify; display: none" runat="server" id="divTAndC">
                    </div>
                    <iframe id="iTerms_and_Conditions" src="../DynFiles/MoreSettings/T&C Service Agreement/Terms of Use.html" 
                        height="250px" width="100%" scrolling="auto" frameborder="0" ></iframe>
                    <!-- End of changes -->
                    <asp:Button ID="btnContinue" TabIndex="1" runat="server" Text="Accept and Continue"
                        OnClick="btnContinue_Click"></asp:Button>
                    <asp:Button ID="btnCancel" TabIndex="1" runat="server" Text="Cancel" OnClick="btnCancel_Click">
                    </asp:Button>
                </div>
            </div>
            <div class="hrDiv">
            </div>
            <div class="footerDiv">
                &copy; 2012 floralapp&reg;. All Rights Reserved. Call us at (800) 560-0501 or email us at support@floralapp.com
            </div>
        </div>
    </center>
    </form>

    <script type="text/javascript" language="javascript">
        <!--
        setInterval(metaoption.settings.adjustPage, 500);
        // -->
    </script>

</body>
</html>

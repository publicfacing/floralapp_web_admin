﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

using Library.DAL;
using Library.Utility;
using Library.AppSettings;

public partial class Admin_AdminMasterNoLeftPanel : System.Web.UI.MasterPage
{
    AppVars v = new AppVars();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
        {
            lnkSearchSettings.Visible = true;

            //Binding Reseller into DropDownList
            Data.Reseller reseller = new Data.Reseller();
            Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
            // Set Reseller.
            Util.setValue(ddlReseller, v.ResellerID);

            Data.Reseller r = new Data.Reseller(v.ResellerID);
            String msg = String.Format("You are viewing data for '{0}' shop.", (v.ResellerID > 0) ? r.ResellerName.value : "all");
            Util.setValue(lblFilteredResellerName, msg);
        }
    }

    protected void btnApply_Click(object sender, EventArgs e)
    {
        Int32 resellerID = Convert.ToInt32(Util.getValue(ddlReseller));
        v.ResellerID = resellerID;

        Response.Redirect("../Default.aspx");
    }
}

﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="CreateUser.aspx.cs" Inherits="Admin_CreateUser" Title="BenevaFlowers | Create Administrative User"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/jscript">
        function validateForm() {
            var pwd = document.getElementById('ctl00$cphPageContainer$txtPassword').value;
            var repwd = document.getElementById('ctl00$cphPageContainer$txtConfirmPassword').value;

            if (pwd == '' || pwd.length <= 3) {
                alert('Please enter a valid password with at least 4 characters.');
                return false;
            }

            if (repwd == '') {
                alert('Please confirm your password.');
                return false;
            }

            if (pwd != repwd) {
                alert('Make sure password and confirm password are same.');
                return false;
            }
            else
                return true;
        }
    </script>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <th class="TableHeadingBg">
                <asp:Label ID="lblPageTitle" runat="server" Text="Create Administrative User"></asp:Label>
            </th>
        </tr>
        <tr>
            <td class="TableBorder">
                <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                <asp:ValidationSummary ID="vsReseller" runat="server" CssClass="reqd" DisplayMode="BulletList"
                    HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                    <tr>
                        <td class="col">
                            User Role<span class="reqd">*</span>
                        </td>
                        <td class="val">
                            <asp:DropDownList ID="ddlUserRole" runat="server">
                                <asp:ListItem Value="0" Text="Select ..." Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Super Admin"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Shop Admin"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Consumer"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rdfUserRole" runat="server" ControlToValidate="ddlUserRole"
                                ErrorMessage="Select a valid user role." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="resellerTR" runat="server" visible="false">
                        <td class="col">
                            Shop<span class="reqd">*</span>
                        </td>
                        <td class="val">
                            <asp:DropDownList ID="ddlReseller" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Value="0" Text="Select ..." Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rdfReseller" runat="server" ControlToValidate="ddlReseller"
                                ErrorMessage="Select a valid shop." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Login ID<span class="reqd">*</span>
                        </td>
                        <td class="val">
                            <asp:TextBox ID="txtLoginID" runat="server" MaxLength="60" SkinID="textbox" Width="150px"
                                autocomplete="off"></asp:TextBox>
                            <asp:ImageButton ID="imgCheckAvailibility" Text="Check availibility of LoginId" runat="server"
                                ImageUrl="~/Images/availability.png" Width="16px" Height="16px" ToolTip="Click here to check availability of Login Id."
                                OnClick="imgCheckAvailibility_Click" CausesValidation="false" />
                            <asp:RequiredFieldValidator ID="rfvLoginID" runat="server" ControlToValidate="txtLoginID"
                                ErrorMessage="Enter Login ID." Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Password<span class="reqd">*</span>
                        </td>
                        <td class="val">
                            <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" MaxLength="40" SkinID="textbox"
                                Width="150px" autocomplete="off"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                ErrorMessage="Enter Password." Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regextxtPassword" ControlToValidate="txtPassword"
                                runat="server" ValidationExpression="^[\s\S]{4,40}$" Display="Dynamic" ErrorMessage="Please enter a valid password with at least 4 characters." />
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Confirm Password<span class="reqd">*</span>
                        </td>
                        <td class="val">
                            <asp:TextBox ID="txtConfirmPassword" TextMode="Password" runat="server" MaxLength="40"
                                SkinID="textbox" Width="150px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                                ErrorMessage="Enter Confirm Password." Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                                ControlToCompare="txtPassword" Display="Dynamic" Operator="Equal" ErrorMessage="Make sure password and confirm password are same."></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            First Name<span class="reqd">*</span>
                        </td>
                        <td class="val">
                            <asp:TextBox ID="txtFirstName" runat="server" SkinID="textbox" class="FormItem" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                                ErrorMessage="Enter First Name." Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Last Name<span class="reqd">*</span>
                        </td>
                        <td class="val">
                            <asp:TextBox ID="txtLastName" runat="server" SkinID="textbox" class="FormItem" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                                ErrorMessage="Enter Last Name." Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="col">
                            Email<span class="reqd">*</span>
                        </td>
                        <td class="val">
                            <asp:TextBox ID="txtEmailID" TabIndex="1" runat="server" MaxLength="50" CssClass="txtbox"
                                Text=""></asp:TextBox>
                            <p>
                                <i>(Enter your Email ID for floralapp communication.)</i></p>
                            <asp:RequiredFieldValidator ID="reqdtxtEmail" runat="server" Display="Dynamic" ErrorMessage="Enter your Email ID."
                                ControlToValidate="txtEmailID"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Please enter a valid Email ID."
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,3}([-.]\w+)*" Display="Dynamic"
                                ControlToValidate="txtEmailID">
                            </asp:RegularExpressionValidator>
                        </td>
                    </tr>
                </table>
                <div class="actionDiv">
                    <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save the user."
                        OnClick="btnSave_Click" CausesValidation="true" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false"
                        CssClass="btn" OnClick="btnCancel_Click" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

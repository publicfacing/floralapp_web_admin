﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;
using System.IO;
using System.Web;

public partial class Admin_ViralMarketing : AppPage
{
    private static String RequestMode
    {
        get;
        set;
    }

    private static String[] AppRequestValues
    {
        get;
        set;
    }

    private int rowCount = 0;
    private int totalRowCount = 0;
    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            //if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
            //Response.Redirect("~/Admin/Dashboard.aspx");

            InitializeControls();
            BindData();
        }
    }

    private void InitializeControls()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);
        Util.setValue(txtStartDate, String.Format("{0:MM/dd/yyyy}", DateTime.Now.AddMonths(-1)));
        Util.setValue(txtEndDate, String.Format("{0:MM/dd/yyyy}", DateTime.Now));

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            //ddlReseller.Enabled = false;
            lblShop.Visible = false;
            ddlReseller.Visible = false;
        }
    }

    private void BindData()
    {
        gvViralMarketing.Visible = true;
        lblTotalViral.Text = String.Empty;
        mpeAppDownloads.Hide();
        Util.setValue(gvViralMarketing, GetViralMarketingList());

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvViralMarketing.Columns[0].Visible = false;
        }
    }

    private DataSet GetViralMarketingList()
    {
        StringBuilder sbFilter = new StringBuilder();
        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();

        switch (searchOperator)
        {
            case "Begins With":
                sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                break;
            case "Ends With":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                break;
            case "Contains":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                break;
            case "Equals":
                sbFilter.Append(" And " + searchField + " = " + searchString);
                break;
            default:
                break;
        }

        Int32 resellerID = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? Convert.ToInt32(v.ResellerID) : Convert.ToInt32(Util.getValue(ddlReseller));

        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);

        String orderBy = gvViralMarketing.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " ResellerID desc";

        Data.ViralMarketingRequest ViralMarketing = new Data.ViralMarketingRequest();
        DataSet dsViralMarketing = ViralMarketing.GetFilteredList(gvViralMarketing.PageIndex, gvViralMarketing.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount, Util.GetDataDateTime(Util.getValue(txtStartDate)), Util.GetDataDateTime(Util.getValue(txtEndDate)));
        gvViralMarketing.VirtualItemCount = totalRowCount;

        // Set Total Count in grid footer
        Util.setValue(lblTotalViral, String.Format("{0} Viral Marketing(s).", totalRowCount));

        return dsViralMarketing;
    }

    private String ReadTemplate(String fileName)
    {
        String path = HttpContext.Current.Server.MapPath(@"../EmailTemplate\");

        String cmdAllPath = path + fileName;
        StreamReader sr = new StreamReader(cmdAllPath);
        String emailTemplate = sr.ReadToEnd();
        sr.Close();
        sr.Dispose();

        return emailTemplate;
    }

    protected void gvViralMarketing_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvViralMarketing.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvViralMarketing_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddViralMarketing.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String ViralMarketingRequestID = objButton.CommandArgument.ToString();
            String qs = Crypto.EncryptQueryString("ViralMarketingRequestID=" + ViralMarketingRequestID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int32 ViralMarketingRequestID = Convert.ToInt32(objButton.CommandArgument.ToString());
            Data.ViralMarketingRequest ViralMarketing = new Data.ViralMarketingRequest();

            if (!ViralMarketing.Delete(ViralMarketingRequestID))
            {
                ShowError(lblErrMsg, "An error occured when deleting the ViralMarketing. ");
                return;
            }

            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void lnkSentRequest_Click(object sender, EventArgs e)
    {
        AppRequestValues = ((LinkButton)sender).CommandArgument.Split(',');

        RequestMode = "Request";
        BindReport(RequestMode);
        lblAppDownloads.Text = "App request(s) status";
        mpeAppDownloads.Show();
    }

    protected void lnkDownload_Click(object sender, EventArgs e)
    {
        AppRequestValues = ((LinkButton)sender).CommandArgument.Split(',');

        RequestMode = "Download";
        BindReport(RequestMode);
        lblAppDownloads.Text = "App download(s) status";
        mpeAppDownloads.Show();
    }

    private void BindReport(String appRequest)
    {
        Int32 resellerID = Util.GetDataInt32(AppRequestValues[0].ToString());
        String reseller = Util.GetDataString(AppRequestValues[1].ToString());
        Int64 userID = Util.GetDataInt64(AppRequestValues[2].ToString());
        DateTime dtStartDate = Util.GetDataDateTime(Util.getValue(txtStartDate));
        DateTime dtEndDate = Util.GetDataDateTime(Util.getValue(txtEndDate));

        Data.ViralMarketingRequest vmr = new Data.ViralMarketingRequest();
        DataSet dsAppRequest = vmr.GetRequestByResellerID(resellerID, userID, appRequest, dtStartDate, dtEndDate);

        gvReport.DataSource = Util.GetDataTable(dsAppRequest);
        gvReport.DataBind();
    }

    protected void gvReport_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvReport.PageIndex = e.NewPageIndex;
        BindReport(RequestMode);
        mpeAppDownloads.Show();
    }

    protected void lnkActivate_Click(object sender, EventArgs e)
    {
        AppRequestValues = ((LinkButton)sender).CommandArgument.Split(',');

        RequestMode = "Activated";
        BindReport(RequestMode);
        lblAppDownloads.Text = "App activated status";
        mpeAppDownloads.Show();
    }

    ////NOT WORKING SORTING [POP UP GRID]
    //protected void gvReport_OnSorting(object sender, GridViewSortEventArgs e)
    //{
    //    BindReport(RequestMode);
    //    mpeAppDownloads.Show();
    //}
}
﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.Utility;
using Library.AppSettings;
using System.Collections.Specialized;
using System.Drawing;

public partial class Admin_PurchaseInformation : AppPage
{
    AppVars v = new AppVars();

    private Int64 PurchaseID
    {
        get;
        set;
    }

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                PurchaseID = Convert.ToInt64(nvc["PurchaseID"]);
            }
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            trShop.Visible = false;
        }
        if (PurchaseID > 0)
        {
            Data.Purchase p = new Data.Purchase();
            DataSet dsPurchase = p.GetListByPurchaseID(PurchaseID);

            if (Util.IsValidDataSet(dsPurchase))
            {
                DataTable dtPurchase = Util.GetDataTable(dsPurchase);

                // Purchase  Info

                if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
                {
                    Util.setValue(lblPurchaseID, Util.GetDataString(dtPurchase, 0, "PurchaseID"));
                    trPurchaseID.Visible = true;
                }

                Util.setValue(lblOrderNo, Util.GetDataString(dtPurchase, 0, "OrderID"));
                Util.setValue(lblPurchasedOn, String.Format("{0:MM/dd/yyyy hh:mm:ss tt}",
                           Util.GetDataDateTime(Util.GetDataString(dtPurchase, 0, "DateAdded"))) == "1/1/1900" ? new DateTime(1900, 1, 1) :
                           Util.GetDataDateTime(Util.GetDataString(dtPurchase, 0, "DateAdded")));


                // User Info
                Util.setValue(lblUser, Util.GetDataString(dtPurchase, 0, "User"));
                Util.setValue(lblIsRegistered, Util.GetDataString(dtPurchase, 0, "IsRegisteredUser").Equals("True") ? "Yes" : "No");
                Util.setValue(lblEmail, Util.GetDataString(dtPurchase, 0, "Email"));
                Util.setValue(lblMobilePhone, Util.GetDataString(dtPurchase, 0, "Phone1"));

                #region Get Email ID of this user's
                //Get User's Email ID from [PurchaseAddresses Table]
                //Data.Address a=new Data.Address();
                //DataSet dsEmail = a.List();
                //DataRow[] drEmail=dsEmail.Tables[0].Select("UserID="+ Util.GetDataString(dtPurchase,0,"UserID"),"");
                //Util.setValue(lblEmail,drEmail[0]["Email"]);
                #endregion

                // Reseller Info
                Util.setValue(lblReseller, Util.GetDataString(dtPurchase, 0, "Reseller"));
                Util.setValue(lblResellerID, Util.GetDataString(dtPurchase, 0, "ResellerID"));

                // FEES & Discount
                Util.setValue(lblServiceFee, String.Format("{0:C}", Util.GetDataDecimal(dtPurchase, 0, "ServiceFee")));
                Util.setValue(lblDeliveredFee, String.Format("{0:C}", Util.GetDataDecimal(dtPurchase, 0, "Deliveryfee")));
                Util.setValue(lblSaleTax, String.Format("{0:C}", Util.GetDataDecimal(dtPurchase, 0, "SalesTax")));
                Util.setValue(lblTotalAmount, String.Format("{0:C}", Util.GetDataDecimal(dtPurchase, 0, "TotalAmount")));
                Util.setValue(lblCouponAmount, String.Format("{0:C}", Util.GetDataDecimal(dtPurchase, 0, "DiscountAmount")));

                // Pickup & Delivery 
                Util.setValue(lblIsPickup, Util.GetDataString(dtPurchase, 0, "IsPickUp").Equals("True") ? "Yes" : "No");
                Util.setValue(lblDeliveryDate, String.Format("{0:MM/dd/yyyy}", Util.GetDataDateTime(Util.GetDataString(dtPurchase, 0, "DeliveryDate"))));
                Util.setValue(lblDelveryStatus, Util.GetDataString(dtPurchase, 0, "IsDelivered").Equals("Yes") ? "Yes" : "No");
                Util.setValue(lblActualDeliveryDate, Util.GetDataString(dtPurchase, 0, "ActualDeliveryDate"));

                if (lblDelveryStatus.Text == "Yes")
                    lblDelveryStatus.Font.Bold = true;

                Util.setValue(lblGiftMessage, Util.GetDataString(dtPurchase, 0, "GiftMessage"));
                Util.setValue(lblNotes, Util.GetDataString(dtPurchase, 0, "Notes"));
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(Util.GetDataString(dtPurchase, 0, "OrderStatus")));
                Util.setValue(lblModifiedBy, Util.GetDataString(dtPurchase, 0, "ModifiedBy"));
                if (Util.GetDataDateTime(Util.GetDataString(dtPurchase, 0, "DateModified")).Year <= 1900)
                    Util.setValue(lblModifiedOn, "");
                else
                    Util.setValue(lblModifiedOn, String.Format("{0:MM/dd/yyyy}", Util.GetDataDateTime(Util.GetDataString(dtPurchase, 0, "DateModified"))));
                // Transaction Info
                Util.setValue(lblWorkingMode, Util.GetDataString(dtPurchase, 0, "WorkingMode").Equals("2") ? "Test" : "Live");
                //Util.setValue(lblStatus, Util.GetDataString(dtPurchase, 0, "TransactionStatusID"));
                String transactionStatusID = Util.GetDataString(dtPurchase, 0, "TransactionStatusID");
                String acountNo = Util.GetDataString(dtPurchase, 0, "AccountNo");

                if (transactionStatusID.Equals("102"))
                {
                    Util.setValue(lblTransactionStatus, "Pending");
                    lblTransactionStatus.CssClass = "tran-pending";
                    imgTranStatus.ImageUrl = "../Images/Waiting.png";
                }
                else
                {
                    if (transactionStatusID.Equals("100") || !String.IsNullOrEmpty(acountNo))
                    {
                        Util.setValue(lblTransactionStatus, "Success");
                        lblTransactionStatus.CssClass = "tran-success";
                        imgTranStatus.ImageUrl = "../Images/tick.png";
                    }
                    else
                    {
                        Util.setValue(lblTransactionStatus, "Failed");
                        lblTransactionStatus.CssClass = "tran-failed";
                        imgTranStatus.ImageUrl = "../Images/cancel.png";
                    }
                }

                if (!String.IsNullOrEmpty(acountNo))
                    Util.setValue(lblHouseAcNo, acountNo);

                //Util.setValue(lblResultCode, Util.GetDataString(dtPurchase, 0, "ResultCode"));                 
                Util.setValue(lblDeviceID, Util.GetDataString(dtPurchase, 0, "DeviceID"));
                Int32 deviceTypeID = Util.GetDataInt32(dtPurchase, 0, "DeviceTypeID");

                switch (deviceTypeID)
                {
                    case 0:
                        Util.setValue(lblDeviceTypeID, "Unknown or Cmd Tool");
                        break;

                    case 1:
                        Util.setValue(lblDeviceTypeID, "iPhone");
                        break;

                    case 2:
                        Util.setValue(lblDeviceTypeID, "ANDROID");
                        break;
                }

                Util.setValue(lblCardTypeID, getCardName(Util.GetDataString(dtPurchase, 0, "CardTypeID")));
                Util.setValue(lblCouponNo, Util.GetDataString(dtPurchase, 0, "CouponCode"));
                Int64 couponID = Util.GetDataInt64(Util.GetDataString(dtPurchase, 0, "CouponID"));
                String qs = Crypto.EncryptQueryString("CouponID=" + couponID);
                lnkCouponNo.NavigateUrl = "~/Admin/AddCoupon.aspx?eqs=" + qs;

                //Util.setValue(lblLast4Car, Util.GetDataString(dtPurchase, 0, "CardLast4Char"));
                //Util.setValue(lblCardFee, "$ " + Util.GetDataString(dtPurchase, 0, "CardFee"));
                //Util.setValue(lblCardPercentage, Util.GetDataString(dtPurchase, 0, "CardFeePercentage") + " %");
                //Util.setValue(lblTransactionFeeID, Util.GetDataString(dtPurchase, 0, "TransactionFeeTypeID"));
                //Util.setValue(lblTransactionFee, "$ " + Util.GetDataString(dtPurchase, 0, "TransactionFee"));
                //Util.setValue(lblTransactionFeePercentage, Util.GetDataString(dtPurchase, 0, "TransactionFeePercentage")+" %");

                Int32 gatewayTypeID = Util.GetDataInt32(Util.GetDataString(dtPurchase, 0, "GatewayTypeID"));
                Data.Purchase pur = new Data.Purchase();
                pur.LoadID(PurchaseID);
                String resultXml = pur.ResultXML.value;

                if (gatewayTypeID == Convert.ToInt32(AppEnum.Gateway.AUTHNET))
                {
                    Util.setValue(lblGateway, "MERCURY");

                    try
                    {

                        String[] arrResponse = resultXml.Split('|');

                        Util.setValue(lblTransactionMsg, arrResponse[3]); //3rd -Response Reason Text
                        Util.setValue(lblTransactionID, arrResponse[6]); //7th -Transaction ID, The payment gateway assigned identification number for the transaction                    

                        // Check for Successful Transaction 
                        if (arrResponse[0] == "1")
                            Util.setValue(lblCardNo, arrResponse[50]); //50 - Card no 

                    }
                    catch (Exception ex)
                    {
                        //ShowError(lblErrMsg, ex.Message);
                        //return;
                    }
                }
                else if (gatewayTypeID == Convert.ToInt32(AppEnum.Gateway.PAYPAL))
                {
                    Util.setValue(lblGateway, "PayPal");
                    Util.setValue(lblTransactionID, pur.ResultCode.value);
                    //Util.setValue(lblTransactionID, resultXml);

                }
                else if (gatewayTypeID == Convert.ToInt32(AppEnum.Gateway.HOUSEACCOUNT))
                {
                    Util.setValue(lblGateway, "HOUSEACCOUNT");
                }
                else if (gatewayTypeID == Convert.ToInt32(AppEnum.Gateway.TELEFLORA))
                {
                    Util.setValue(lblGateway, "TELEFLORA");
                    Util.setValue(lblTransactionID, Util.GetDataString(dtPurchase, 0, "TFUserID") + "/" + Util.GetDataString(dtPurchase, 0, "TFTranID"));
                    string transMsg = "";
                    switch (Util.GetDataString(dtPurchase, 0, "TFAuthResp").Trim().ToUpper())
                    {
                        case "1_APPROVED":
                            transMsg = "Authorization is approved";
                            break;
                        case "2_DECLINED":
                            transMsg = "Authorization is declined";
                            break;
                        case "3_TIMEOUT":
                            transMsg = "Some portion of the network was not available, no authorization takes place";
                            break;
                        case "4_INVALID DATA":
                            transMsg = "Some problem was found with the data provided by the web server. Either some data was missing or did not pass validation checks.";
                            break;
                        case "5_VOICE":
                            transMsg = "Voice authorization is required";
                            break;
                        default:
                            break;
                    }
                    if (transMsg.Length > 0)
                        transMsg += " - ";
                    switch (Util.GetDataString(dtPurchase, 0, "TFTranMsg").Trim().ToUpper())
                    {
                        case "M":
                            transMsg += "CVV2/CVC2/CID Match";
                            break;
                        case "N":
                            transMsg += "CVV2/CVC2/CID No Match";
                            break;
                        case "P":
                            transMsg += "Bypassed CC";
                            break;
                        case "S":
                            transMsg += "The CVV2/CID should be on the card but the merchant indicates it’s not (Visa and Discover only)";
                            break;
                        case "U":
                            transMsg += "The issuer is not certified for processing";
                            break;
                        default:
                            break;
                    }
                    if (transMsg == " - ")
                        transMsg = Util.GetDataString(dtPurchase, 0, "TFCcaResp");
                    Util.setValue(lblTransactionMsg, transMsg);
                }
            }

            //Billing
            DataSet dsBillingAddress = p.GetAddress(PurchaseID, Convert.ToInt32(AppEnum.AddressType.BILLING));

            if (Util.IsValidDataSet(dsBillingAddress))
            {
                String city = Util.GetDataString(Util.GetDataTable(dsBillingAddress), 0, "City");

                if (!string.IsNullOrEmpty(city))
                    Util.setValue(gvBillingAddress, dsBillingAddress);
            }

            // Shiping
            if (!lblIsPickup.Text.Equals("Yes"))//if pickup as 'yes' then shipping address can't displayed.
                Util.setValue(gvShipingAddress, p.GetAddress(PurchaseID, Convert.ToInt32(AppEnum.AddressType.SHIPPING)));

            BindProducts();
            BindUpsellProducts();
        }
    }

    private string getCardName(String CardTypeID)
    {
        String CardName = String.Empty;
        switch (CardTypeID)
        {
            case "1":
                CardName = "VISA";
                break;
            case "2":
                CardName = "DISCOVER";
                break;
            case "3":
                CardName = "MASTER CARD";
                break;
            case "4":
                CardName = "AMEX";
                break;
            default:
                CardName = "None";
                break;
        }
        return CardName;
    }

    protected void gvProducts_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindProducts();

    }

    private void BindProducts()
    {
        Data.PurchaseCart pCart = new Data.PurchaseCart();

        DataSet dsCart = pCart.GetProductListByPurchaseID(PurchaseID);
        Util.setValue(gvProducts, dsCart);
        if (Util.IsValidDataSet(dsCart))
            ((Label)gvProducts.FooterRow.FindControl("lblProductTotalAmount")).Text = String.Format("{0:C}", dsCart.Tables[0].Compute("sum(TotalPrice)", "TotalPrice>0"));
    }

    private void BindUpsellProducts()
    {
        Data.UpsellPurchaseCart upCart = new Data.UpsellPurchaseCart();

        DataSet dsCart = upCart.GetUpsellProductListByPurchaseID(PurchaseID);
        Util.setValue(gvUpsellProduct, dsCart);
        if (Util.IsValidDataSet(dsCart))
            ((Label)gvUpsellProduct.FooterRow.FindControl("lblUpsellProductTotalAmount")).Text = String.Format("{0:C}", dsCart.Tables[0].Compute("sum(TotalPrice)", "TotalPrice>0"));
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewAllPurchases.aspx");
    }

    protected void gvUpsellProduct_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindUpsellProducts();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Int32 orderStatus = Util.GetDataInt32(ddlStatus.SelectedItem.Value);
        Int64 userID = v.UserID;

        Data.Purchase purchase = new Data.Purchase();

        purchase.ManageOrderStatusByPurchaseID(PurchaseID, orderStatus, userID);

        lblErrMsg.ForeColor = Color.Green;
        ShowError(lblErrMsg, "Order status saved.");
        BindData();
    }
}


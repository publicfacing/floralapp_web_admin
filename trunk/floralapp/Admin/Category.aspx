﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="Category.aspx.cs" Inherits="Admin_Category" Title="floralapp&reg; | Category"
    Theme="GraySkin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                    class="arial-12">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                text-indent: 5px;">
                                <tr>
                                    <td>
                                        Category
                                    </td>
                                    <td style="text-align: right; padding-right: 5px; text-transform: capitalize;">
                                        <span style="display: none;">
                                            <asp:HyperLink ID="lnkReorder" runat="server" NavigateUrl="~/Admin/ReorderCategory.aspx"
                                                ToolTip="Reorder category">Reorder category</asp:HyperLink>
                                            &nbsp;|&nbsp;</span>
                                        <asp:HyperLink ID="lnkAddCategory" runat="server" NavigateUrl="~/Admin/AddCategory.aspx"
                                            ToolTip="Add Category">Add Category</asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="FilterContentDetails">
                            <div class="filterSection">
                                <table style="width: 100%">
                                    <tr>
                                        <td class="label" valign="top">
                                            Search :
                                        </td>
                                        <td valign="top">
                                            <div runat="server" id="divShop">
                                                <label class="label">
                                                    Shop:</label>
                                                <asp:DropDownList ID="ddlReseller" runat="server" AutoPostBack="false" AppendDataBoundItems="true">
                                                    <asp:ListItem Value="0" Text="Select..." Selected="True"></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                            </div>
                                            <asp:DropDownList ID="ddlSearchField" runat="server" AutoPostBack="false">
                                                <asp:ListItem Text="ID" Value="CategoryID"></asp:ListItem>
                                                <asp:ListItem Text="Category" Value="CategoryName"></asp:ListItem>
                                                <asp:ListItem Text="Shop ID" Value="ResellerID"></asp:ListItem>
                                                <asp:ListItem Text="Shop" Value="ResellerName"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSearchOperator" runat="server">
                                                <asp:ListItem Value="Begins With">Begins With</asp:ListItem>
                                                <asp:ListItem Value="Ends With">Ends With</asp:ListItem>
                                                <asp:ListItem Value="Contains">Contains</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtSearchString" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnFilter" class="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_Click"
                                                CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="PagerContentDetails">
                            <asp:DataPager ID="dpTop" runat="server" PageSize="25" PagedControlID="gvCategory">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                        ShowLastPageButton="false" ShowNextPageButton="false" />
                                    <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                        NextPreviousButtonCssClass="command" />
                                    <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                        RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                        ShowLastPageButton="true" ShowNextPageButton="true" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error"></asp:Label>
                            <mo:ExtGridView ID="gvCategory" runat="server" DataKeyNames="CategoryID" AllowSorting="True"
                                OnSorting="gvCategory_OnSorting" OrderBy="CategoryID Desc" SkinID="gvskin" OnPageIndexChanging="gvCategory_OnPageIndexChanging"
                                OnRowDataBound="gvCategory_RowDataBound" EmptyDataText="No record(s) found.">
                                <Columns>
                                    <asp:BoundField DataField="CategoryID" SortExpression="CategoryID" HeaderText="ID"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Image" ItemStyle-CssClass="gridContent">
                                        <HeaderStyle CssClass="gridHeader" Width="30px" />
                                        <ItemTemplate>
                                            <asp:Image ID="imgCat" runat="server" Visible="true" ImageUrl='<%# getCategoryImage(Eval("CategoryID").ToString() + Eval("ImageExt").ToString()) %>'
                                                mageAlign="Top" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CategoryName" SortExpression="CategoryName" HeaderText="Category"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:BoundField DataField="ResellerID" SortExpression="ResellerID" HeaderText="Shop ID"
                                        ItemStyle-CssClass="gridContent" HeaderStyle-Width="60px"></asp:BoundField>
                                    <asp:BoundField DataField="ResellerName" SortExpression="ResellerName" HeaderText="Shop"
                                        ItemStyle-CssClass="gridContent"></asp:BoundField>
                                    <asp:TemplateField HeaderText="System Defined" SortExpression="IsSystemDefined" ItemStyle-CssClass="gridContent"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridHeader" HeaderStyle-Width="90px">
                                        <ItemTemplate>
                                            <img id="Img1" alt="" runat="server" src='<%# Eval("IsSystemDefined").ToString().Equals("True")?"~/Images/tick.png":"~/Images/blank_img.png" %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is Enabled" SortExpression="IsEnabled" ItemStyle-CssClass="gridContent"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridHeader" HeaderStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgIsEnabled" runat="server" CommandArgument='<%# Eval("CategoryID") + "," + Eval("ResellerID") + "," + Eval("IsEnabled") %>'
                                                CausesValidation="false" OnClick="imgIsEnabled_Click" ImageUrl='<%# Eval("IsEnabled").ToString().Equals("True")?"~/Images/tick.png":"~/Images/tick-gray.png" %>'
                                                ToolTip="Enabled/Disabled category" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is Featured" SortExpression="IsFeatured" ItemStyle-CssClass="gridContent"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridHeader" HeaderStyle-Width="70px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgIsFeatured" runat="server" CommandArgument='<%# Eval("CategoryID") + "," + Eval("ResellerID") + "," + Eval("IsFeatured") %>'
                                                CausesValidation="false" OnClick="imgIsFeatured_Click" ImageUrl='<%# Eval("IsFeatured").ToString().Equals("True")?"~/Images/tick.png":"~/Images/tick-gray.png" %>'
                                                ToolTip="Featured/Not Featured category" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="gridContent" HeaderStyle-CssClass="gridHeader"
                                        HeaderStyle-Width="80px">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkEdit" runat="server" Visible='<%# Eval("IsSystemDefined").ToString().Equals("True")? false:true %>'
                                                CommandArgument='<%# Eval("CategoryID")%>' CausesValidation="false" OnClick="lnkEdit_Click"
                                                ToolTip="Edit" ImageUrl="~/Images/edit-icon.jpg" />
                                            <asp:ImageButton ID="lnkDelete" runat="server" Visible='<%# Eval("IsSystemDefined").ToString().Equals("True")? false:true %>'
                                                CommandArgument='<%# Eval("CategoryID")%>' CausesValidation="false" OnClick="lnkDelete_Click"
                                                ImageUrl="~/Images/DeleteButton.jpg" OnClientClick="return confirm('Are you sure you want to delete this record?');"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </mo:ExtGridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails">
                            <div style="float: left; width: 300px; text-align: left">
                                <asp:Label ID="lblTotalCategory" runat="server" Text=""></asp:Label>
                            </div>
                            <div style="float: right; width: 200px; text-align: right; padding-right: 5px;">
                                <asp:DataPager ID="dpBottom" runat="server" PageSize="25" PagedControlID="gvCategory">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonCssClass="command" FirstPageText="«" PreviousPageText="‹"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="true" ShowPreviousPageButton="true"
                                            ShowLastPageButton="false" ShowNextPageButton="false" />
                                        <asp:NumericPagerField ButtonCount="7" NumericButtonCssClass="command" CurrentPageLabelCssClass="current"
                                            NextPreviousButtonCssClass="command" />
                                        <asp:NextPreviousPagerField ButtonCssClass="command" LastPageText="»" NextPageText="›"
                                            RenderDisabledButtonsAsLabels="true" ShowFirstPageButton="false" ShowPreviousPageButton="false"
                                            ShowLastPageButton="true" ShowNextPageButton="true" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

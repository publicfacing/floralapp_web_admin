﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Drawing;

public partial class Admin_AddCoupon : AppPage
{
    private Int64 CouponID
    {
        get;
        set;
    }

    AppVars v = new AppVars();

    private DateTime StartDateTime
    {
        get;
        set;
    }

    private DateTime EndDateTime
    {
        get;
        set;
    }

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                CouponID = Util.GetDataInt32(nvc["CouponID"]);
            }

            Util.setValue(lblPageTitle, (CouponID > 0) ? "Edit Coupon" : "Add Coupon");
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (ddlReseller.SelectedIndex > 0)
            GenerateCouponCode(Util.GetDataInt32(Util.getValue(ddlReseller)));

        Int32 objTypeID = 8; // To getting the coupon type from [String Map] Table.
        // Binding Coupon type from [String Map] Table.
        Data.StringMap sm = new Data.StringMap();
        DataSet dsSm = sm.GetListByObjectTypeID(objTypeID);
        Util.setValue(ddlCouponType, dsSm, "ObjectText", "ObjectValue");
        Util.setValue(txtStartDate, String.Format("{0:MM/dd/yyyy}", Util.GetServerDate()));

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            GenerateCouponCode(v.ResellerID);
            //ddlReseller.Enabled = false;
            trShop.Visible = false;
        }

        if (CouponID <= 0)
        {
            //Setting initially as blank image.
            imgCoupon.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
            txtCouponCode.ReadOnly = false;
            imgCheckCouponCode.Visible = true;
            return;
        }
        else
        {
            #region Enabled / Disabled on edit mode.

            ddlReseller.Enabled = false;
            txtCouponCode.ReadOnly = true;
            imgCheckCouponCode.Visible = false;
            ddlDiscountType.Enabled = false;
            txtDiscountFixedAmount.Enabled = false;
            txtDiscountPercent.Enabled = false;
            ddlCouponType.Enabled = false;
            // Disable start date in Edit Mode
            txtStartDate.Enabled = false;
            ceStartDate.Enabled = false;

            #endregion Enabled / Disabled on edit mode.
        }

        Data.Coupon coupon = new Data.Coupon();
        coupon.LoadID(CouponID);

        if (!(ddlReseller.SelectedIndex > 0))
            Util.setValue(ddlReseller, coupon.ResellerID.value);

        StartDateTime = Util.GetDataDateTime(coupon.CouponStartDate.value.ToString());
        EndDateTime = Util.GetDataDateTime(coupon.CouponEndDate.value.ToString());

        Util.setValue(txtCouponCode, coupon.CouponCode.value);
        Util.setValue(txtReferral, coupon.Referral.value);
        Util.setValue(ddlDiscountType, coupon.DiscountTypeID.value);
        Util.setValue(txtDiscountFixedAmount, coupon.DiscountFixedAmount.value);
        Util.setValue(txtDiscountPercent, coupon.DiscountPercent.value);
        Util.setValue(chkIsEnabled, coupon.IsEnabled.value);
        if (coupon.CouponUsageTypeID.value == Convert.ToInt32(AppEnum.CouponUsageType.Multiple))
            chkIsVisibleOnPhone.Enabled = true;

        Util.setValue(ddlCouponType, coupon.CouponUsageTypeID.value);

        if (coupon.CouponUsageTypeID.value != Convert.ToInt32(AppEnum.CouponUsageType.AppLaunch))
        {
            Util.setValue(txtStartDate, String.Format("{0:MM/dd/yyyy}", StartDateTime));

            if (!String.IsNullOrEmpty(EndDateTime.ToString()) && String.Format("{0:MM/dd/yyyy}", EndDateTime) != "01/01/1900")
                Util.setValue(txtEndDate, String.Format("{0:MM/dd/yyyy}", EndDateTime));
        }

        rteCouponText.Content = coupon.CouponText.value;

        if (Convert.ToInt32(Util.getValue(ddlDiscountType)) == 2)
        {
            DiscountFixedAmountTR.Visible = true;
            DiscountPercentTR.Visible = false;
        }
        else
        {
            DiscountFixedAmountTR.Visible = false;
            DiscountPercentTR.Visible = true;
        }

        ConditionallyDisplayControls();

        Util.setValue(chkIsVisibleOnPhone, coupon.IsVisibleOnPhone.value);

        #region Populating images ...

        String imageExt = coupon.CouponImageExt.value;
        String fileName = String.Format("CouponImage_{0}{1}", coupon.CouponID.value.ToString(), imageExt);

        if (!File.Exists(Server.MapPath("../DynImages/Coupon/" + fileName)))
            imgCoupon.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        else
            imgCoupon.ImageUrl = String.Format("../DynImages/Coupon/{0}", fileName);

        #endregion End of binding image
    }

    private void GenerateCouponCode(Int32 resellerID)
    {
        if (resellerID > 0)
        {
        ReGenrateRandom:
            string charRandoms = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int length = 6; // Defined max. six digit(s) of coupon code.

            Random rnd = new Random();
            StringBuilder sbRandom = new StringBuilder();
            Data.Coupon coupon = new Data.Coupon();

            DataSet dsCoupon = coupon.List();
            String CouponNumber = string.Empty;

            while ((length--) > 0)
                sbRandom.Append(charRandoms[(int)(rnd.NextDouble() * charRandoms.Length)]);

            CouponNumber = Convert.ToString(sbRandom);

            DataRow[] drCoupon = dsCoupon.Tables[0].Select("CouponCode='" + CouponNumber + "'");

            if (drCoupon.Length > 0)
                goto ReGenrateRandom;
            else
                txtCouponCode.Text = CouponNumber;
        }
        else
            txtCouponCode.Text = String.Empty;
    }

    private void SaveData(byte[] CouponFileByteData, String fileExtn)
    {
        Data.Coupon coupon = new Data.Coupon(CouponID);
        coupon.ResellerID.value = Int32.Parse(Util.getValue(ddlReseller));
        coupon.CouponCode.value = Util.getValue(txtCouponCode);
        coupon.Referral.value = Util.getValue(txtReferral);
        coupon.DiscountTypeID.value = Int32.Parse(Util.getValue(ddlDiscountType));

        if (Int32.Parse(Util.getValue(ddlDiscountType)) == 1)
            coupon.DiscountPercent.value = Convert.ToDouble(Util.getValue(txtDiscountPercent));
        else
            coupon.DiscountFixedAmount.value = Convert.ToDecimal(Util.getValue(txtDiscountFixedAmount));

        if (CouponFileByteData != null)
        {
            coupon.CouponImage.value = CouponFileByteData;
            coupon.CouponImageExt.value = fileExtn;
        }

        StartDateTime = Convert.ToDateTime(Util.getValue(txtStartDate));
        EndDateTime = Util.GetDataDateTime(Util.getValue(txtEndDate));

        coupon.CouponStartDate.value = StartDateTime;
        coupon.CouponEndDate.value = EndDateTime;
        coupon.CouponUsageTypeID.value = Util.GetDataInt32(Util.getValue(ddlCouponType));
        coupon.CouponText.value = rteCouponText.Content;
        coupon.IsEnabled.value = chkIsEnabled.Checked;
        coupon.IsVisibleOnPhone.value = chkIsVisibleOnPhone.Checked;

        if (coupon.CouponID.value <= 0)
        {
            coupon.DateAdded.value = Util.GetServerDate();
            coupon.AddedBy.value = v.UserID;
        }

        coupon.DateModified.value = Util.GetServerDate();
        coupon.ModifiedBy.value = v.UserID;

        coupon.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        coupon.Save();

        coupon.EnableCoupon(coupon.CouponID.value, coupon.ResellerID.value, v.UserID, chkIsEnabled.Checked, coupon.CouponUsageTypeID.value);

        #region Save File in file system...
        String filePath = Server.MapPath(@"~/DynFiles\Coupon\");
        String fileName = String.Format("CouponText_{0}{1}", coupon.CouponID.value.ToString(), ".htm");
        filePath += fileName;

        if (File.Exists(filePath))
            File.Delete(filePath);

        File.WriteAllText(filePath, coupon.CouponText.value);

        #endregion Save File in file System...

        #region Insert/Update on Server
        //Save in ServerUpdates
        Data.ServerUpdates su = new Data.ServerUpdates();
        su.UpdateStatus(coupon.ResellerID.value, "coupon");
        #endregion

        #region Binding images ...

        Byte[] fileByteData = coupon.CouponImage.value;
        String imageExt = coupon.CouponImageExt.value;
        String filePaths = Server.MapPath(@"~/DynImages\Coupon\");
        String fileNames = String.Format("CouponImage_{0}{1}", coupon.CouponID.value.ToString(), imageExt);
        filePaths += fileNames;

        if (File.Exists(filePaths))
            File.Delete(filePaths);

        if (fileByteData != null)
        {
            FileStream fs = new FileStream(filePaths, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(fileByteData, 0, fileByteData.Length);
            fs.Close();
        }

        #endregion End of binding image
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        FileUpload imgCtrl = fuCouponImage;
        String extension = String.Empty;
        String imgType = String.Empty;
        byte[] fileByteData = null;
        byte[] fileImageResizedByteData = null;

        try
        {
            #region Validate Controls

            Int32 discountType = Convert.ToInt32(Util.getValue(ddlDiscountType));

            if (discountType == 1 && String.IsNullOrEmpty(Util.getValue(txtDiscountPercent)))
            {
                ShowError(lblErrMsg, "Enter a discount percentage.");
                return;
            }

            if (discountType == 2 && String.IsNullOrEmpty(Util.getValue(txtDiscountFixedAmount)))
            {
                ShowError(lblErrMsg, "Enter a discount amount.");
                return;
            }
            #endregion Validate Controls


            #region Validation for Duplicate Coupon Code
            if (CouponID <= 0)
            {
                if (IsValidCouponCode())
                    return;
            }
            #endregion Validation for Duplicate Coupon Code

            #region Validation for Image Format & Size
            // Check if file upload ctrl has any files selected or not.
            if (imgCtrl.HasFile)
            {
                extension = Path.GetExtension(imgCtrl.PostedFile.FileName).ToString().ToLower();
                imgType = imgCtrl.PostedFile.ContentType + extension;

                switch (imgType)
                {
                    case "image/jpg.jpg":
                    case "image/jpeg.jpg":
                    case "image/pjpeg.jpg":

                    case "image/jpg.jpeg":
                    case "image/jpeg.jpeg":
                    case "image/pjpeg.jpeg":

                    case "image/png.png":
                    case "image/x-png.png":

                    case "image/gif.gif":
                        break;
                    default:
                        AddErrorMessage("Upload image only *.png, *.jpg, *.jpeg, *.gif format.");
                        break;
                }

                // Check error : if any server side validation error exist.
                if (_errorMsg != String.Empty)
                {
                    ShowError(lblErrMsg, _errorMsg);
                    return;
                }
                else
                {
                    // If the selected file extension is valid then 
                    // convert the selected file into bytes for saving in database 
                    Stream imageStream = imgCtrl.PostedFile.InputStream;
                    int contentLength = imgCtrl.PostedFile.ContentLength;

                    using (BinaryReader reader = new BinaryReader(imageStream))
                    {
                        fileByteData = reader.ReadBytes(contentLength);
                        reader.Close();
                    }

                    // Read the file bytes in MemoryStream for checking it's dimention as desired. 
                    MemoryStream memStream = new MemoryStream(fileByteData);
                    System.Drawing.Image imgObject = System.Drawing.Image.FromStream(memStream);
                    Int32 imgHeight = imgObject.Height;
                    Int32 imgWidth = imgObject.Width;

                    //if (imgHeight > 200 || imgWidth > 320)
                    //{
                    //    ShowError(lblErrMsg, "Uploaded image size is greater than the defined size.");
                    //    return;
                    //}

                    #region Resize Image
                    const int MAX_WIDTH = 200;
                    const int MAX_HEIGHT = 200;

                    int NewHeight = imgHeight * MAX_WIDTH / imgWidth;
                    int NewWidth = MAX_WIDTH;

                    if (NewHeight > MAX_HEIGHT)
                    {
                        // Resize with height instead
                        NewWidth = imgWidth * MAX_HEIGHT / imgHeight;
                        NewHeight = MAX_HEIGHT;
                    }

                    System.Drawing.Image imgResized = imgObject.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

                    MemoryStream mstreamResizedImage = new MemoryStream();

                    imgResized.Save(mstreamResizedImage, System.Drawing.Imaging.ImageFormat.Png);
                    fileImageResizedByteData = new Byte[mstreamResizedImage.Length];
                    mstreamResizedImage.Position = 0;
                    mstreamResizedImage.Read(fileImageResizedByteData, 0, (int)mstreamResizedImage.Length);
                    #endregion
                }
            }
            //else
            //{
            //    if (imgCoupon.ImageUrl == String.Empty)
            //    {
            //        ShowError(lblErrMsg, "Upload an image for coupon.");
            //        return;
            //    }
            //}
            #endregion Validation for Image Format & Size

            #region validation for Start Date and End Date

            Int32 couponType = Convert.ToInt32(Util.getValue(ddlCouponType));

            if (couponType == Convert.ToInt32(AppEnum.CouponUsageType.OneTime) || couponType == Convert.ToInt32(AppEnum.CouponUsageType.Multiple))
            {
                Data.Coupon cou = new Data.Coupon(CouponID);
                DateTime dbStartDate = Convert.ToDateTime(String.Format("{0:MM/dd/yyyy}", cou.CouponStartDate.value));
                DateTime dbEndDate = Convert.ToDateTime(String.Format("{0:MM/dd/yyyy}", cou.CouponStartDate.value));
                DateTime inputStartDate = Convert.ToDateTime(String.Format("{0:MM/dd/yyyy}", Util.GetDataDateTime(Util.getValue(txtStartDate))));
                DateTime inputEndDate = Convert.ToDateTime(String.Format("{0:MM/dd/yyyy}", Util.GetDataDateTime(Util.getValue(txtEndDate))));
                DateTime dtToday = Convert.ToDateTime(String.Format("{0:MM/dd/yyyy}", Util.GetServerDate()));

                if (String.Format("{0:MM/dd/yyyy}", inputStartDate) == "01/01/1900")
                {
                    ShowError(lblErrMsg, "Please select a start date. It should be either today's date or future date.");
                    return;
                }

                if (CouponID > 0) // Edit Mode
                {
                    if (String.Format("{0:MM/dd/yyyy}", inputEndDate) != "01/01/1900")
                    {
                        if (inputEndDate < dbStartDate)
                        {
                            ShowError(lblErrMsg, "End date should be greater than start date.");
                            return;
                        }

                        if (inputEndDate < dbEndDate || (inputEndDate < dtToday && inputEndDate != dbEndDate))
                        {
                            ShowError(lblErrMsg, "Past date should not be input as end date.");
                            return;
                        }
                    }

                }
                else // Add
                {
                    if (inputStartDate < dtToday)
                    {
                        ShowError(lblErrMsg, "Start date should not be past date. It should be either today's date or future date.");
                        return;
                    }

                    if (String.Format("{0:MM/dd/yyyy}", inputEndDate) != "01/01/1900")
                    {
                        if (inputEndDate < dtToday)
                        {
                            ShowError(lblErrMsg, "End date should not be past date. Provide a valid end date.");
                            return;
                        }

                        if (inputEndDate < inputStartDate)
                        {
                            ShowError(lblErrMsg, "Start date should not greater than end date.");
                            return;
                        }
                    }
                }
            }


            #endregion validation for Start Date and End Date

            // Save data to Database
            extension = ".png";
            SaveData(fileImageResizedByteData, extension);
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect("Coupons.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Coupons.aspx");
    }

    protected void ddlDiscountType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Util.getValue(ddlDiscountType)) == 2)
        {
            DiscountFixedAmountTR.Visible = true;
            DiscountPercentTR.Visible = false;
        }
        else
        {
            DiscountFixedAmountTR.Visible = false;
            DiscountPercentTR.Visible = true;
        }
    }

    protected void ddlReseller_SelectedIndexChanged(object sender, EventArgs e)
    {
        GenerateCouponCode(Util.GetDataInt32(Util.getValue(ddlReseller)));
    }

    protected void imgCheckCouponCode_Click(object sender, ImageClickEventArgs e)
    {
        IsValidCouponCode();
    }

    private Boolean IsValidCouponCode()
    {
        Boolean isExistCoupon = false;

        String couponCode = Util.getValue(txtCouponCode);
        Int32 resellerID = Convert.ToInt32(Util.getValue(ddlReseller));
        lblCouponCode.ForeColor = Color.Red;

        if (!String.IsNullOrEmpty(couponCode))
        {
            Data.Coupon cou = new Data.Coupon();
            if (cou.GetIsValidCouponCode(resellerID, couponCode) > 0)
            {
                ShowError(lblCouponCode, "This coupon code is already in use. Please provide another one.");
                isExistCoupon = true;
            }
            else
            {
                ShowError(lblCouponCode, "This coupon code is available.");
                lblCouponCode.ForeColor = Color.Green;
                isExistCoupon = false;
            }
        }
        return isExistCoupon;
    }

    protected void ddlCouponType_SelectedIndexChanged(object sender, EventArgs e)
    {

        ConditionallyDisplayControls();
        ShowEnableMessage();
    }

    protected void chkIsEnabled_OnCheckedChanged(object sender, EventArgs e)
    {
        ShowEnableMessage();
    }

    protected void chkIsVisibleOnPhone_OnCheckedChanged(object sender, EventArgs e)
    {
        ShowEnableMessage();
    }

    private void ConditionallyDisplayControls()
    {
        chkIsVisibleOnPhone.Checked = false;
        chkIsVisibleOnPhone.Enabled = false;

        if (CouponID <= 0)
            Util.setValue(txtStartDate, String.Format("{0:MM/dd/yyyy}", Util.GetServerDate()));

        switch (ddlCouponType.SelectedIndex)
        {
            case 1: // One Time
                trDateRange.Visible = true;
                break;
            case 2: // Multi Use
                trDateRange.Visible = true;
                chkIsVisibleOnPhone.Enabled = true;
                break;
            case 3: // App. Launch
                trDateRange.Visible = false;
                chkIsVisibleOnPhone.Checked = true;
                if (CouponID <= 0)
                    Util.setValue(txtStartDate, String.Format("{0:MM/dd/yyyy}", "1/1/1900"));
                break;
            default:
                trDateRange.Visible = false;
                break;
        }
    }

    private void ShowEnableMessage()
    {
        HideMsg(lblCouponEnableMsg);

        if (Util.getValue(chkIsEnabled) == true)
        {
            Int32 couponType = Convert.ToInt32(Util.getValue(ddlCouponType));

            if (couponType == Convert.ToInt32(AppEnum.CouponUsageType.AppLaunch))
                ShowMsg(lblCouponEnableMsg, "You may have only one app launch coupon enabled at a time. This will disable any other app launch coupon created before.");

            if (couponType == Convert.ToInt32(AppEnum.CouponUsageType.Multiple) && Util.getValue(chkIsVisibleOnPhone) == true)
                ShowMsg(lblCouponEnableMsg, "You may have only one multiple use coupon enabled at a time which is visible on phone. This will disable any other multiple use coupon created before which is visible on phone.");
        }
    }
}


﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Collections.Specialized;
using System.IO;

public partial class Admin_AddColorTheme : AppPage
{
    private Int32 ColorThemeID
    {
        get;
        set;
    }

    AppVars appVar = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                ColorThemeID = Util.GetDataInt32(nvc["ColorThemeID"]);
            }

            Util.setValue(lblPageTitle, (ColorThemeID > 0) ? "Edit Color Theme" : "Add Color Theme");
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        Data.ColorTheme ct = new Data.ColorTheme();

        #region rights for reseller admin that can only change the theme
        if (appVar.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            txtThemeName.Enabled = false;
            txtBgColor.Enabled = false;
            txtNavigationColor.Enabled = false;
            btnSave.Visible = false;
        }
        DataSet dsColorTheme = ct.List();
        //btnSave.Visible = dsColorTheme.Tables[0].Rows.Count < 3 ? true : false;
        btnSave.Visible = true;
        #endregion

        #region Setting blank image.
        //Setting initially as blank image.
        imgNavigation.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        imgBackground.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        imgNaviRightOn.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        imgNaviLeftOn.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        imgNaviRightOff.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        imgNaviLeftOff.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        #endregion

        if (ColorThemeID <= 0)
            return;

        ct.LoadID(ColorThemeID);

        Util.setValue(txtThemeName, ct.ThemeName.value);
        Util.setValue(txtBgColor, ct.BackGroundColor.value);
        Util.setValue(txtNavigationColor, ct.NavigationBarColor.value);

        #region Populating images ...

        #region Populating images for Navigation Bar Image and Navigation Background Image...

        String fileNavigName = String.Format("NavigationBarImage_{0}{1}", ct.ColorThemeID.value.ToString(), ".png");
        String fileBgName = String.Format("BackGroundImage_{0}{1}", ct.ColorThemeID.value.ToString(), ".png");

        imgNavigation.ImageUrl = String.Format("../DynImages/ColorTheme/{0}", fileNavigName);
        imgBackground.ImageUrl = String.Format("../DynImages/ColorTheme/{0}", fileBgName);
        #endregion End of Navigation bar and background images...


        #region Populating images for Navigation Right/Left Image [On] ...

        String fileNavigRightOnName = String.Format("NavRightImgOn_{0}{1}", ct.ColorThemeID.value.ToString(), ".png");
        String fileNavigLeftOnName = String.Format("NavLeftImgOn_{0}{1}", ct.ColorThemeID.value.ToString(), ".png");

        imgNaviRightOn.ImageUrl = String.Format("../DynImages/ColorTheme/{0}", fileNavigRightOnName);
        imgNaviLeftOn.ImageUrl = String.Format("../DynImages/ColorTheme/{0}", fileNavigLeftOnName);

        #endregion End of Binding images for Navigation Right/Left Image [On] ...


        #region Populating images for Navigation Right/Left Image [Off] ...

        String fileNavigRightOffName = String.Format("NavRightImgOff_{0}{1}", ct.ColorThemeID.value.ToString(), ".png");
        String fileNavigLeftOffName = String.Format("NavLeftImgOff_{0}{1}", ct.ColorThemeID.value.ToString(), ".png");

        imgNaviRightOff.ImageUrl = String.Format("../DynImages/ColorTheme/{0}", fileNavigRightOffName);
        imgNaviLeftOff.ImageUrl = String.Format("../DynImages/ColorTheme/{0}", fileNavigLeftOffName);
        #endregion End of Binding images for Navigation Right/Left Image [Off] ...

        #endregion Eng of populating images..
    }

    private void SaveData(
                          byte[] NavigFileByteData, byte[] ImgBgFileByteData,
                          byte[] NavigRightOnByteData, byte[] NavigLeftOnByteData,
                          byte[] NavigRightOffByteData, byte[] NavigLeftOffByteData
                         )
    {
        Data.ColorTheme ct = new Data.ColorTheme(ColorThemeID);

        ct.ThemeName.value = Util.getValue(txtThemeName);
        ct.NavigationBarColor.value = Util.getValue(txtNavigationColor);
        ct.BackGroundColor.value = Util.getValue(txtBgColor);

        if (NavigFileByteData != null)
            ct.NavigationBarImage.value = NavigFileByteData;

        if (ImgBgFileByteData != null)
            ct.BackGroundImage.value = ImgBgFileByteData;

        if (NavigRightOnByteData != null)
            ct.NavRightImgOn.value = NavigRightOnByteData;

        if (NavigLeftOnByteData != null)
            ct.NavLeftImgOn.value = NavigLeftOnByteData;

        if (NavigRightOffByteData != null)
            ct.NavRightImgOff.value = NavigRightOffByteData;

        if (NavigLeftOffByteData != null)
            ct.NavLeftImgOff.value = NavigLeftOffByteData;


        ct.DateAdded.value = Util.GetServerDate();
        ct.AddedBy.value = appVar.UserID;
        ct.DateModified.value = Util.GetServerDate();
        ct.ModifiedBy.value = appVar.UserID;
        ct.DateDeleted.value = Util.GetServerDate();
        ct.DeletedBy.value = appVar.UserID;
        ct.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        ct.Save();

        #region Updating color Theme images...

        #region Binding images for Navigation Bar Image and Navigation Background Image...

        String filePath = Server.MapPath(@"~/DynImages\ColorTheme\");
        String fileNavigName = String.Format("NavigationBarImage_{0}{1}", ct.ColorThemeID.value.ToString(), ".png");
        String fileBgName = String.Format("BackGroundImage_{0}{1}", ct.ColorThemeID.value.ToString(), ".png");

        if (NavigFileByteData != null)
        {
            if (File.Exists(filePath + fileNavigName))
                File.Delete(filePath + fileNavigName);

            FileStream fs = new FileStream(filePath + fileNavigName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(NavigFileByteData, 0, NavigFileByteData.Length);
            fs.Close();
        }

        filePath += fileBgName;

        if (ImgBgFileByteData != null)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(ImgBgFileByteData, 0, ImgBgFileByteData.Length);
            fs.Close();
        }

        #endregion End of Navigation bar and background images...

        #region Binding images for Navigation Right/Left Image [On] ...

        filePath = Server.MapPath(@"~/DynImages\ColorTheme\");
        String fileNavigRightOnName = String.Format("NavRightImgOn_{0}{1}", ct.ColorThemeID.value.ToString(), ".png");
        String fileNavigLeftOnName = String.Format("NavLeftImgOn_{0}{1}", ct.ColorThemeID.value.ToString(), ".png");

        if (NavigRightOnByteData != null)
        {
            if (File.Exists(filePath + fileNavigRightOnName))
                File.Delete(filePath + fileNavigRightOnName);

            FileStream fs = new FileStream(filePath + fileNavigRightOnName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(NavigRightOnByteData, 0, NavigRightOnByteData.Length);
            fs.Close();
        }
        filePath += fileNavigLeftOnName;
        if (NavigLeftOnByteData != null)
        {

            if (File.Exists(filePath))
                File.Delete(filePath);

            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(NavigLeftOnByteData, 0, NavigLeftOnByteData.Length);
            fs.Close();
        }

        #endregion End of Binding images for Navigation Right/Left Image [On] ...

        #region Binding images for Navigation Right/Left Image [Off] ...

        filePath = Server.MapPath(@"~/DynImages\ColorTheme\");
        String fileNavigRightOffName = String.Format("NavRightImgOff_{0}{1}", ct.ColorThemeID.value.ToString(), ".png");
        String fileNavigLeftOffName = String.Format("NavLeftImgOff_{0}{1}", ct.ColorThemeID.value.ToString(), ".png");




        if (NavigRightOffByteData != null)
        {
            if (File.Exists(filePath + fileNavigRightOffName))
                File.Delete(filePath + fileNavigRightOffName);

            FileStream fs = new FileStream(filePath + fileNavigRightOffName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(NavigRightOffByteData, 0, NavigRightOffByteData.Length);
            fs.Close();
        }
        filePath += fileNavigLeftOffName;
        if (NavigLeftOffByteData != null)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(NavigLeftOffByteData, 0, NavigLeftOffByteData.Length);
            fs.Close();
        }

        #endregion End of Binding images for Navigation Right/Left Image [Off] ...

        #endregion Eng of updating images..

        #region  Update on ServerStatus
        //Save in ServerUpdates
        if (ColorThemeID > 0)
        {
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatusColorTheme(ColorThemeID);
        }
        #endregion
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        #region Declaring here file upload control(s).

        FileUpload fuNavImage = this.fuNavigationImage;
        FileUpload fuBgImage = this.fuBgImage;
        FileUpload fuNavRightOn = this.fuNaviRightOn;
        FileUpload fuNavLeftOn = this.fuNaviLeftOn;
        FileUpload fuNavRightOff = this.fuNaviRightOff;
        FileUpload fuNavLeftOff = this.fuNaviLeftOff;

        #endregion

        #region Declaring here byte object

        byte[] NavigFileByteData = null;
        byte[] ImgBgFileByteData = null;
        byte[] NavigRightOnByteData = null;
        byte[] NavigLeftOnByteData = null;
        byte[] NavigRightOffByteData = null;
        byte[] NavigLeftOffByteData = null;

        #endregion

        #region Getting here for given file(s) type

        String strImageNavigType = fuNavImage.PostedFile.ContentType + Path.GetExtension(fuNavImage.PostedFile.FileName).ToLower();
        String strImageBgType = fuBgImage.PostedFile.ContentType + Path.GetExtension(fuBgImage.PostedFile.FileName).ToLower();
        String strImageNavigRightOnType = fuNavRightOn.PostedFile.ContentType + Path.GetExtension(fuNavRightOn.PostedFile.FileName).ToLower();
        String strImageNavigLeftOnType = fuNavLeftOn.PostedFile.ContentType + Path.GetExtension(fuNavLeftOn.PostedFile.FileName).ToLower();
        String strImageNavigRightOffType = fuNavRightOff.PostedFile.ContentType + Path.GetExtension(fuNavRightOff.PostedFile.FileName).ToLower();
        String strImageNavigLeftOffType = fuNavLeftOff.PostedFile.ContentType + Path.GetExtension(fuNavLeftOff.PostedFile.FileName).ToLower();

        #endregion

        #region Verifying here for file(s) format is correct or not.

        Boolean isValidNavigationType = fuNavImage.HasFile ? isValidFileFormat(strImageNavigType) : true;
        Boolean isValidBackgroundType = isValidFileFormat(strImageBgType);
        Boolean isValidNavigRightOnType = fuNavRightOn.HasFile ? isValidFileFormat(strImageNavigRightOnType) : true;
        Boolean isValidNavigLeftOnType = fuNavLeftOn.HasFile ? isValidFileFormat(strImageNavigLeftOnType) : true;
        Boolean isValidNavigRightOffType = fuNavRightOff.HasFile ? isValidFileFormat(strImageNavigRightOffType) : true;
        Boolean isValidNavigLeftOffType = fuNavLeftOff.HasFile ? isValidFileFormat(strImageNavigLeftOffType) : true;

        #endregion

        try
        {
            #region Image Validation
            // Check if file upload ctrl has any files selected or not.
            //if ((fuNavImage.HasFile && fuBgImage.HasFile && fuNaviRightOn.HasFile &&
            //    fuNaviLeftOn.HasFile && fuNaviRightOff.HasFile && fuNaviLeftOff.HasFile)
            //    || ColorThemeID > 0)
            if (fuBgImage.HasFile || ColorThemeID > 0)
            {

                switch (
                         (
                         isValidNavigationType && isValidBackgroundType && isValidNavigRightOnType
                          && isValidNavigLeftOnType && isValidNavigRightOffType && isValidNavigLeftOffType
                         ) || ColorThemeID > 0
                       )
                {
                    case false:
                        AddErrorMessage("All uploaded image should be only *.png format.");
                        break;
                    case true:

                        if (fuNavImage.HasFile && !isValidNavigationType)
                        {
                            AddErrorMessage("Upload image only *.png format.");
                            break;
                        }

                        if (fuBgImage.HasFile && !isValidBackgroundType)
                        {
                            AddErrorMessage("Upload image only *.png format.");
                            break;
                        }

                        if (fuNavRightOn.HasFile && !isValidNavigRightOnType)
                        {
                            AddErrorMessage("Upload image only *.png format.");
                            break;
                        }

                        if (fuNavLeftOn.HasFile && !isValidNavigLeftOnType)
                        {
                            AddErrorMessage("Upload image only *.png format.");
                            break;
                        }

                        if (fuNavRightOff.HasFile && !isValidNavigRightOffType)
                        {
                            AddErrorMessage("Upload image only *.png format.");
                            break;
                        }

                        if (fuNavLeftOff.HasFile && !isValidNavigLeftOffType)
                        {
                            AddErrorMessage("Upload image only *.png format.");
                            break;
                        }

                        break;
                }

                // Check error : if any server side validation error exist.
                if (_errorMsg != String.Empty)
                {
                    ShowError(lblErrMsg, _errorMsg);
                    return;
                }
                else
                {
                    if (fuNavImage.PostedFile.ContentLength > 1)
                    {
                        // If the selected file extension is valid then 
                        // convert the selected file into bytes for saving in database 
                        Stream imgNavigStream = fuNavImage.PostedFile.InputStream;
                        int contentNavigLength = fuNavImage.PostedFile.ContentLength;

                        using (BinaryReader readerNavig = new BinaryReader(imgNavigStream))
                        {
                            NavigFileByteData = readerNavig.ReadBytes(contentNavigLength);
                            readerNavig.Close();
                        }

                        MemoryStream memNavigStream = new MemoryStream(NavigFileByteData);

                        System.Drawing.Image imgNavigObject = System.Drawing.Image.FromStream(memNavigStream);

                        Int32 imgNavigHeight = imgNavigObject.Height;
                        Int32 imgNavigWidth = imgNavigObject.Width;

                        if (imgNavigHeight > 50 || imgNavigWidth > 320)
                        {
                            ShowError(lblErrMsg, "Uploaded navigation image size is different than the defined size.");
                            return;
                        }
                    }

                    if (fuBgImage.PostedFile.ContentLength > 1)
                    {
                        // If the selected file extension is valid then 
                        // convert the selected file into bytes for saving in database 
                        Stream imgImgBgStream = fuBgImage.PostedFile.InputStream;
                        int contentImgBgLength = fuBgImage.PostedFile.ContentLength;

                        using (BinaryReader readerImgBg = new BinaryReader(imgImgBgStream))
                        {
                            ImgBgFileByteData = readerImgBg.ReadBytes(contentImgBgLength);
                            readerImgBg.Close();
                        }

                        // Read the file bytes in MemoryStream for checking it's dimention as desired. 
                        MemoryStream memImgBgStream = new MemoryStream(ImgBgFileByteData);

                        System.Drawing.Image imgBgObject = System.Drawing.Image.FromStream(memImgBgStream);

                        Int32 imgBgHeight = imgBgObject.Height;
                        Int32 imgBgWidth = imgBgObject.Width;

                        if (imgBgHeight > 480 || imgBgWidth > 320)
                        {
                            ShowError(lblErrMsg, "Uploaded background image size is different than the defined size.");
                            return;
                        }
                    }

                    if (fuNaviRightOn.PostedFile.ContentLength > 1)
                    {
                        // If the selected file extension is valid then 
                        // convert the selected file into bytes for saving in database 
                        Stream imgNaviRightOnStream = fuNaviRightOn.PostedFile.InputStream;
                        int contentRightOnLength = fuNaviRightOn.PostedFile.ContentLength;

                        using (BinaryReader readerRightOn = new BinaryReader(imgNaviRightOnStream))
                        {
                            NavigRightOnByteData = readerRightOn.ReadBytes(contentRightOnLength);
                            readerRightOn.Close();
                        }

                        // Read the file bytes in MemoryStream for checking it's dimention as desired. 
                        MemoryStream memRightOnStream = new MemoryStream(NavigRightOnByteData);

                        System.Drawing.Image imgRightOnObject = System.Drawing.Image.FromStream(memRightOnStream);

                        Int32 imgRightOnHeight = imgRightOnObject.Height;
                        Int32 imgRightOnWidth = imgRightOnObject.Width;

                        if (imgRightOnHeight != Convert.ToInt32(AppEnum.ColorThemeHeightWidth.Height) && imgRightOnWidth != Convert.ToInt32(AppEnum.ColorThemeHeightWidth.Width))
                        {
                            ShowError(lblErrMsg, "Uploaded image [RightImageOn] size should be (ht. x wt.) 75 x 30 pixel(s).");
                            return;
                        }
                    }

                    if (fuNaviLeftOn.PostedFile.ContentLength > 1)
                    {
                        // If the selected file extension is valid then 
                        // convert the selected file into bytes for saving in database 
                        Stream imgNaviLeftOnStream = fuNaviLeftOn.PostedFile.InputStream;
                        int contentLeftOnLength = fuNaviLeftOn.PostedFile.ContentLength;

                        using (BinaryReader readerLeftOn = new BinaryReader(imgNaviLeftOnStream))
                        {
                            NavigLeftOnByteData = readerLeftOn.ReadBytes(contentLeftOnLength);
                            readerLeftOn.Close();
                        }

                        // Read the file bytes in MemoryStream for checking it's dimention as desired. 
                        MemoryStream memLeftOnStream = new MemoryStream(NavigLeftOnByteData);

                        System.Drawing.Image imgLeftOnObject = System.Drawing.Image.FromStream(memLeftOnStream);

                        Int32 imgLeftOnHeight = imgLeftOnObject.Height;
                        Int32 imgLeftOnWidth = imgLeftOnObject.Width;

                        if (imgLeftOnHeight != Convert.ToInt32(AppEnum.ColorThemeHeightWidth.Height) && imgLeftOnWidth != Convert.ToInt32(AppEnum.ColorThemeHeightWidth.Width))
                        {
                            ShowError(lblErrMsg, "Uploaded image [LeftImageOn] size should be (ht. x wt.) 75 x 30 pixel(s).");
                            return;
                        }
                    }

                    if (fuNaviRightOff.PostedFile.ContentLength > 1)
                    {
                        // If the selected file extensiOff is valid then 
                        // cOffvert the selected file into bytes for saving in database 
                        Stream imgNaviRightOffStream = fuNaviRightOff.PostedFile.InputStream;
                        int cOfftentRightOffLength = fuNaviRightOff.PostedFile.ContentLength;

                        using (BinaryReader readerRightOff = new BinaryReader(imgNaviRightOffStream))
                        {
                            NavigRightOffByteData = readerRightOff.ReadBytes(cOfftentRightOffLength);
                            readerRightOff.Close();
                        }

                        // Read the file bytes in MemoryStream for checking it's dimentiOff as desired. 
                        MemoryStream memRightOffStream = new MemoryStream(NavigRightOffByteData);

                        System.Drawing.Image imgRightOffObject = System.Drawing.Image.FromStream(memRightOffStream);

                        Int32 imgRightOffHeight = imgRightOffObject.Height;
                        Int32 imgRightOffWidth = imgRightOffObject.Width;

                        if (imgRightOffHeight != Convert.ToInt32(AppEnum.ColorThemeHeightWidth.Height) && imgRightOffWidth != Convert.ToInt32(AppEnum.ColorThemeHeightWidth.Width))
                        {
                            ShowError(lblErrMsg, "Uploaded image [RightImageOff] size should be (ht. x wt.) 75 x 30 pixel(s).");
                            return;
                        }
                    }

                    if (fuNaviLeftOff.PostedFile.ContentLength > 1)
                    {
                        // If the selected file extensiOff is valid then 
                        // cOffvert the selected file into bytes for saving in database 
                        Stream imgNaviLeftOffStream = fuNaviLeftOff.PostedFile.InputStream;
                        int cOfftentLeftOffLength = fuNaviLeftOff.PostedFile.ContentLength;

                        using (BinaryReader readerLeftOff = new BinaryReader(imgNaviLeftOffStream))
                        {
                            NavigLeftOffByteData = readerLeftOff.ReadBytes(cOfftentLeftOffLength);
                            readerLeftOff.Close();
                        }

                        // Read the file bytes in MemoryStream for checking it's dimentiOff as desired. 
                        MemoryStream memLeftOffStream = new MemoryStream(NavigLeftOffByteData);

                        System.Drawing.Image imgLeftOffObject = System.Drawing.Image.FromStream(memLeftOffStream);

                        Int32 imgLeftOffHeight = imgLeftOffObject.Height;
                        Int32 imgLeftOffWidth = imgLeftOffObject.Width;

                        if (imgLeftOffHeight != Convert.ToInt32(AppEnum.ColorThemeHeightWidth.Height) && imgLeftOffWidth != Convert.ToInt32(AppEnum.ColorThemeHeightWidth.Width))
                        {
                            ShowError(lblErrMsg, "Uploaded image [LeftImageOff] size should be (ht. x wt.) 75 x 30 pixel(s).");
                            return;
                        }
                    }
                }
            }
            else
            {
                //if (
                //    (imgBackground.ImageUrl.IndexOf("blank_img.png") > 0 || imgNavigation.ImageUrl.IndexOf("blank_img.png") > 0 ||
                //    imgNaviRightOn.ImageUrl.IndexOf("blank_img.png") > 0 || imgNaviLeftOn.ImageUrl.IndexOf("blank_img.png") > 0 ||
                //    imgNaviRightOff.ImageUrl.IndexOf("blank_img.png") > 0 || imgNaviLeftOff.ImageUrl.IndexOf("blank_img.png") > 0)
                //    ||
                //    (imgBackground.ImageUrl == String.Empty || imgNavigation.ImageUrl == String.Empty ||
                //    imgNaviRightOn.ImageUrl == String.Empty || imgNaviLeftOn.ImageUrl == String.Empty ||
                //    imgNaviRightOff.ImageUrl == String.Empty || imgNaviLeftOff.ImageUrl == String.Empty)
                //   )
                //{
                //    ShowError(lblErrMsg, "Upload an image for all navigations.");
                //    return;
                //}
                if (imgBackground.ImageUrl.IndexOf("blank_img.png") > 0
                    || imgBackground.ImageUrl == String.Empty)
                {
                    ShowError(lblErrMsg, "Upload an image for Background Image.");
                    return;
                }
            }

            #endregion End of Image Validation

            // Save data to Database
            SaveData(NavigFileByteData, ImgBgFileByteData, NavigRightOnByteData, NavigLeftOnByteData, NavigRightOffByteData, NavigLeftOffByteData);
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect("ColorTheme.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ColorTheme.aspx");
    }

    private Boolean isValidFileFormat(String fileFormat)
    {
        Boolean isValidFormat = true;

        switch (fileFormat)
        {
            case "image/png.png":
            case "image/x-png.png":
                break;
            default:
                isValidFormat = false;
                break;
        }
        return isValidFormat;
    }
}
﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Drawing;
using Library.Utility;
using Library.AppSettings;
using System.Collections.Specialized;

public partial class Admin_CreateUser : AppPage
{
    AppVars v = new AppVars();

    private String PageTitle
    {
        get;
        set;
    }

    private Int64 ResellerUserMappingID
    {
        get;
        set;
    }

    private Int32 RoleID
    {
        get;
        set;
    }

    private Int64 ResellerID
    {
        get;
        set;
    }

    private Int64 UserID
    {
        get
        {
            if (ViewState["UserID"] == null)
                ViewState["UserID"] = 0;

            return Convert.ToInt64(ViewState["UserID"]);
        }
        set { ViewState["UserID"] = value; }
    }

    public override void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (v.UserRoleID == 1)
                PageTitle = "Super Admin";
            else if (v.UserRoleID == 2)
                PageTitle = "Shop Admin";

            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                UserID = Convert.ToInt32(nvc["UserID"]);
                RoleID = Convert.ToInt32(nvc["RoleID"]);
                ResellerID = Convert.ToInt32(nvc["ResellerID"]);
            }

            switch (RoleID.ToString())
            {
                case "1":
                    PageTitle = "Super Admin";
                    break;
                case "2":
                    PageTitle = "Shop Admin";
                    break;
                case "3":
                    PageTitle = "Consumer";
                    break;
            }

            Util.setValue(lblPageTitle, (UserID > 0) ? "Edit " + PageTitle : "Add " + PageTitle);

            //if (UserID > 0)
            //{
            //    txtLoginID.ReadOnly = true;
            //    imgCheckAvailibility.Visible = false;
            //}
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            IntitializeControl();
            BindData();
        }
    }

    private void IntitializeControl()
    {
        Util.setValue(ddlUserRole, RoleID);


        if (RoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
            ddlUserRole.Enabled = false;

        else if (RoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            ddlUserRole.Enabled = false;
            resellerTR.Visible = v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN);
            Data.Reseller ressler = new Data.Reseller();
            Util.setValue(ddlReseller, ressler.List(), "ResellerName", "ResellerID");
            Util.setValue(ddlReseller, v.ResellerID);
        }
        else if (RoleID == Convert.ToInt32(AppEnum.UserRole.CONSUMER))
        {
            btnSave.Enabled = false;
            resellerTR.Visible = false;
        }
    }

    private void BindData()
    {
        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            ddlReseller.Enabled = false;
            Util.setValue(ddlReseller, v.ResellerID);
        }

        if (UserID <= 0)
            return;

        Data.Users user = new Data.Users();
        user.LoadID(UserID);

        Util.setValue(ddlUserRole, user.RoleID.value);
        Util.setValue(txtFirstName, user.FirstName.value);
        Util.setValue(txtLastName, user.LastName.value);
        Util.setValue(txtLoginID, user.LoginID.value);
        Util.setValue(ddlReseller, ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
        {
            Util.setValue(txtPassword, user.Password.value);
            Util.setValue(txtConfirmPassword, user.Password.value);
            txtPassword.TextMode = TextBoxMode.SingleLine;
            txtConfirmPassword.TextMode = TextBoxMode.SingleLine;
        }
        else
        {
            txtPassword.Attributes.Add("value", "nicetry!");
            txtConfirmPassword.Attributes.Add("value", "nicetry!");
        }

        Int64 addressID = 0;
        Int32 addressTypeID = Convert.ToInt32(AppEnum.AddressType.BILLING);
        Data.Address address = new Data.Address();
        
        DataTable dtAddress = null;
        if (address.IsExist(UserID, addressTypeID, ref dtAddress))
        {
            addressID = Util.GetDataInt64(dtAddress, 0, "AddressID");
            address.LoadID(addressID);
        }

        Util.setValue(txtEmailID, address.Email.value);
    }

    private void SaveData()
    {
        AppVars v = new AppVars();

        Data.Users u = new Data.Users(UserID);
        u.RoleID.value = Int32.Parse(Util.getValue(ddlUserRole));
        u.LoginID.value = Util.getValue(txtLoginID);
        u.FirstName.value = Util.getValue(txtFirstName);
        u.LastName.value = Util.getValue(txtLastName);

        String password = Util.getValue(txtPassword);
        if (password != "nicetry!")
            u.Password.value = Util.getValue(txtPassword);

        u.AddedBy.value = v.UserID;
        u.DateAdded.value = Util.GetServerDate();
        u.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        u.Save();

        Int64 addressID = 0;
        Int32 addressTypeID = Convert.ToInt32(AppEnum.AddressType.BILLING);
        Data.Address address = new Data.Address();

        // Cross check if a billing address exist for this userID 
        DataTable dtAddress = null;
        if (address.IsExist(UserID, addressTypeID, ref dtAddress))
        {
            addressID = Util.GetDataInt64(dtAddress, 0, "AddressID");
            address.LoadID(addressID);
        }

        address.UserID.value = u.UserID.value;
        address.AddressTypeID.value = addressTypeID;
        address.FirstName.value = u.FirstName.value;
        address.LastName.value = u.LastName.value;
        address.Email.value = Util.getValue(txtEmailID);
        #region Commented
        //address.Address1.value = Util.GetDataString(nvc["adr1"]);
        //address.Address2.value = Util.GetDataString(nvc["adr2"]);
        //address.City.value = Util.GetDataString(nvc["city"]);
        //address.OtherState.value = Util.GetDataString(nvc["ost"]);
        //address.StateID.value = Util.GetDataInt32(nvc["stid"]);
        //address.CountryID.value = Util.GetDataInt32(nvc["cnid"]);
        //address.Zip.value = Util.GetDataString(nvc["zip"]);
        //address.Phone1.value = Util.GetDataString(nvc["ph1"]);
        //address.Phone1Ext.value = Util.GetDataString(nvc["ph1ext"]);
        //address.Phone2.value = Util.GetDataString(nvc["ph2"]);
        //address.Phone2Ext.value = Util.GetDataString(nvc["ph2ext"]);
        //address.Mobile.value = Util.GetDataString(nvc["mobile"]);
        //address.Fax.value = Util.GetDataString(nvc["fax"]);
        //address.AltEmail.value = Util.GetDataString(nvc["altemail"]);
        #endregion Commented

        if (UserID <= 0)
        {
            address.DateAdded.value = Util.GetServerDate();
            address.AddedBy.value = v.UserID;
        }
        address.DateModified.value = Util.GetServerDate();
        address.ModifiedBy.value = v.UserID;
        address.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        address.Save();

        //Insert into ResellerUserMapping table
        if (Convert.ToInt32(Util.getValue(ddlUserRole)) == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Int32 resellerID = Int32.Parse(Util.getValue(ddlReseller));
            Data.ResellerUserMapping rum = new Data.ResellerUserMapping();
            ResellerUserMappingID = rum.GetActiveResellerUserMappingID(u.UserID.value);
            rum.LoadID(ResellerUserMappingID);

            rum.ResellerID.value = resellerID;
            rum.UserID.value = u.UserID.value;
            rum.IsEnabled.value = true;
            rum.AddedBy.value = v.UserID;
            rum.DateAdded.value = Util.GetServerDate();
            rum.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
            rum.Save();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Data.Users u = new Data.Users();
            if (u.CanInsertUpdate(Util.getValue(txtLoginID), UserID))
            {
                SaveData();
            }
            else
            {
                ShowError(lblErrMsg, "Login ID already exist(s), please provide another one.");
                return;
            }
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect("Users.aspx?RoleID=" + RoleID);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Users.aspx?RoleID=" + RoleID);
    }

    private void isLoginUserExist(String loginID)
    {
        Data.Users u = new Data.Users();
        u.CanInsertUpdate(loginID, v.UserID);
    }

    protected void imgCheckAvailibility_Click(object sender, EventArgs e)
    {
        if (Util.getValue(txtLoginID) != "")
        {
            Data.Users u = new Data.Users();
            lblErrMsg.ForeColor = Color.Red;

            if (!u.CanInsertUpdate(Util.getValue(txtLoginID), UserID))
            {
                ShowError(lblErrMsg, "Login ID is already exist(s). Please provide another one.");
            }
            else
            {
                ShowError(lblErrMsg, "Login ID is available for you.");
                lblErrMsg.ForeColor = Color.Green;
            }
        }
    }
}


﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;
using System.IO;

public partial class Admin_UpsellCategory : AppPage
{
    private int rowCount = 0;
    private int totalRowCount = 0;
    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            InitializeControls();
            BindData();
        }
    }

    private void InitializeControls()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            //ddlReseller.Enabled = false;
            divShop.Visible = false;
            ddlSearchField.Items.RemoveAt(2);
            ddlSearchField.Items.RemoveAt(2);
        }
        //else
        //lnkAddUpsellCategory.Enabled = false;
    }

    private void BindData()
    {
        Util.setValue(gvUpsellCategory, GetCategoryList());

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvUpsellCategory.Columns[3].Visible = false;
            gvUpsellCategory.Columns[4].Visible = false;
        }
    }

    private DataSet GetCategoryList()
    {

        StringBuilder sbFilter = new StringBuilder();
        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();

        switch (searchOperator)
        {
            case "Begins With":
                sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                break;
            case "Ends With":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                break;
            case "Contains":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                break;
            case "Equals":
                sbFilter.Append(" And " + searchField + " = " + searchString);
                break;
            default:
                break;
        }


        Int32 resellerID = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? Convert.ToInt32(v.ResellerID) : Convert.ToInt32(Util.getValue(ddlReseller));

        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);


        String orderBy = gvUpsellCategory.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " Sequence asc";

        Data.UpsellCategory uc = new Data.UpsellCategory();
        DataSet dsCategory = uc.FilteredList(gvUpsellCategory.PageIndex, gvUpsellCategory.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvUpsellCategory.VirtualItemCount = totalRowCount;

        #region Binding images ...
        /*
        foreach (DataRow drCat in dsCategory.Tables[0].Rows)
        {
            Byte[] fileByteData = (byte[])drCat["ImageBinary"];
            String imageExt = Convert.ToString(drCat["ImageExt"]);
            Int32 catID = Convert.ToInt32(drCat["UpsellCategoryID"]);

            String filePath = Server.MapPath(@"~/DynImages\UpsellCategory\");
            String fileName = String.Format("CategoryImage_{0}{1}", catID, imageExt);
            filePath += fileName;

            if (File.Exists(filePath))
                File.Delete(filePath);

            if (fileByteData != null)
            {
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteData, 0, fileByteData.Length);
                fs.Close();
            }
        }
         */
        #endregion End of binding image

        // Set Total Count in grid footer
        Util.setValue(lblTotalCategory, String.Format("{0} Upsell Categories.", totalRowCount));

        return dsCategory;
    }

    protected void gvUpsellCategory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUpsellCategory.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvUpsellCategory_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddUpsellCategory.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String UpsellCategoryID = objButton.CommandArgument.ToString();
            String qs = Crypto.EncryptQueryString("UpsellCategoryID=" + UpsellCategoryID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int32 UpsellCategoryID = Convert.ToInt32(objButton.CommandArgument.ToString());
            Data.UpsellCategory uc = new Data.UpsellCategory(UpsellCategoryID);
            uc.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
            uc.Save();

            #region Save in ServerUpdates
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatus(uc.ResellerID.value, "upsellcategory");
            #endregion

            BindData();

        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void gvUpsellCategory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //Response.BinaryWrite((byte[])e.Row.Cells[3]);
        }
    }

    public String getUpsellCategoryImage(String fileExtension)
    {
        String imgPath = "../DynImages/UpsellCategory/categoryimage_" + fileExtension;

        if (!File.Exists(Server.MapPath(imgPath)))
            imgPath = "~/Images/blank_img.png";

        return imgPath;
    }
}
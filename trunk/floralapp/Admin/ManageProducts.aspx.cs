﻿using System;
using Library.Utility;
using System.IO;
using System.Collections.Specialized;
public partial class Admin_ManageProducts : AppPage
{
    private Int64 ProductID
    {
        get;
        set;
    }
     
    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                ProductID = Convert.ToInt64(nvc["ProductID"]);                
            }
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    private void BindData()
    {
        Data.Product prod = new Data.Product(ProductID);
        Data.Reseller res = new Data.Reseller(prod.ResellerID.value);
         
        Util.setValue(lblCategory, prod.GetCategoryList((int)ProductID));
        Util.setValue(lblResellerName, res.ResellerName.value);
        Util.setValue(lblProductName, prod.ProductName.value);
        Util.setValue(lblDescription, prod.Description.value);
        Util.setValue(lblPrice, String.Format("{0:C}",prod.ProductPrice.value));

        imgIsUpCharge.ImageUrl=prod.IsUpchargeActive.value==true?"~/Images/tick.png":"~/Images/tick-gray.png";
        imgPublished.ImageUrl = prod.IsPublished.value == true ? "~/Images/tick.png" : "~/Images/tick-gray.png";

        #region Binding Images...
        Byte[] productFileByteData = prod.ProductImage.value;
        Byte[] ThumbFileByteData = prod.ProductThumb.value;
        String filePath = Server.MapPath(@"~/DynImages\Product\").Replace("Admin\\","");
        String fileProductName = String.Format("ProductImage_{0}{1}", prod.ProductID.value.ToString(), prod.ProductImageExt.value);
        String fileThumbName = String.Format("ProductThumb_{0}{1}", prod.ProductID.value.ToString(), ".png");


        if (File.Exists(filePath + fileProductName))
            File.Delete(filePath + fileProductName);
        if (File.Exists(filePath + fileThumbName))
            File.Delete(filePath + fileThumbName);

        if (productFileByteData != null)
        {
            FileStream fs = new FileStream(filePath + fileProductName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(productFileByteData, 0, productFileByteData.Length);
            fs.Close();
        }

        if (ThumbFileByteData != null)
        {
            FileStream fs = new FileStream(filePath + fileThumbName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(ThumbFileByteData, 0, ThumbFileByteData.Length);
            fs.Close();
        }

        imgProduct.ImageUrl = String.Format("../DynImages/Product/{0}", fileProductName);
        imgThumb.ImageUrl = String.Format("../DynImages/Product/{0}", fileThumbName);
        #endregion Eng of Binding images..
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Products.aspx");
    }
}

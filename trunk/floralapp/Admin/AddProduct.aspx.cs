﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.IO;
using System.Collections.Specialized;
using System.Collections;
using Library.DAL;
using System.Text;

public partial class Admin_AddProduct : AppPage
{
    private Int64 ProductID
    {
        get;
        set;
    }

    private Int32 ProductVariationID
    {
        get { return ViewState["ProductVariationID"] != null ? (int)ViewState["ProductVariationID"] : 0; }
        set { ViewState["ProductVariationID"] = value; }
    }

    private Int64 ProductUpchargeID
    {
        get;
        set;
    }

    private Int64 ProductVariationUpchargeID
    {
        get { return ViewState["ProductVariationUpchargeID"] != null ? (Int64)ViewState["ProductVariationUpchargeID"] : 0; }
        set { ViewState["ProductVariationUpchargeID"] = value; }
    }

    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);
        lblErrorVar.Text = "";
        //Page.MaintainScrollPositionOnPostBack = true;

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                ProductID = Util.GetDataInt64(nvc["ProductID"]);
                ProductUpchargeID = Util.GetDataInt64(nvc["ProdUpchargeID"]);
            }

            Util.setValue(lblPageTitle, (ProductID > 0) ? "Edit Product" : "Add Product");
            if (Session["prodsuccess"] != null)
            {
                Session["prodsuccess"] = null;
                ShowMsg(lblErrMsg, "Product saved successfully.");
            }
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {

            BindData();
        }
    }

    private void BindData()
    {
        if (ProductID <= 0)
            trProductVariation.Visible = false;

        if (!CanAddNewProduct() && ProductID <= 0)
        {
            btnSave.Visible = false;
            btnAddVariation.Visible = false;
        }

        // product DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        FillCategory(Convert.ToInt32(Util.getValue(ddlReseller)));

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            trShop.Visible = false;
            //ddlReseller.Enabled = false;
        }

        //Setting initially as blank image.
        imgProductImage.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");

        if (ProductID <= 0)
            return;

        Data.Product product = new Data.Product();
        product.LoadID(ProductID);

        if (!(ddlReseller.SelectedIndex > 0))
        {
            Util.setValue(ddlReseller, product.ResellerID.value);
            FillCategory(Int32.Parse(Util.getValue(ddlReseller)));
            //FillSpecialOffer(Int32.Parse(Util.getValue(ddlReseller)));  Removed concept for special offer : Jnan
        }

        Data.Category cat = new Data.Category();
        DataSet dsCat = cat.GetCategoryListByProduct(ProductID);

        if (dsCat != null && dsCat.Tables.Count > 0)
            foreach (DataRow dr in dsCat.Tables[0].Rows)
                foreach (GridViewRow gvr in gvCategories.Rows)
                    if (dr["CategoryID"].ToString() == gvCategories.DataKeys[gvr.RowIndex].Value.ToString())
                    {
                        ((CheckBox)gvr.FindControl("cbChecked")).Checked = true;
                    }

        Util.setValue(txtProductName, product.ProductName.value);
        Util.setValue(txtColor, product.Color.value);
        chkShowColorInDevice.Checked = product.ShowColorInDevice.value;
        Util.setValue(txtDescription, product.Description.value);
        Util.setValue(txtSkuNumber, product.SKUNo.value);

        #region Populating images ...

        String imgExtn = product.ProductImageExt.value;
        String imgProductPath = String.Format("ProductImage_{0}{1}", product.ProductID.value.ToString(), imgExtn);

        if (!File.Exists(Server.MapPath("../DynImages/Product/" + imgProductPath)))
            imgProductImage.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        else
            imgProductImage.ImageUrl = String.Format("../DynImages/Product/{0}", imgProductPath);

        #endregion End of binding image

        Util.setValue(txtProductPrice, product.ProductPrice.value);
        Util.setValue(chkIsUpChargeActive, Convert.ToBoolean(product.IsUpchargeActive.value));
        //Util.setValue(chkIsFloristToFlorist, Convert.ToBoolean(product.IsFloristToFlorist.value));
        Util.setValue(chkIsDirectShip, Convert.ToBoolean(product.IsDirectShip.value));
        Util.setValue(chkIsPublished, Convert.ToBoolean(product.IsPublished.value));

        Data.ProductUpcharge pu = new Data.ProductUpcharge();
        pu.LoadID(ProductUpchargeID);

        Util.setValue(txtUpchargePercent, pu.UpchargePercentage.value);
        //Util.setValue(etDescription, pu.Description.value);
        if (chkIsUpChargeActive.Checked)
            trUpercent.Style.Add("display", "");

        BindVariations();
    }

    private void SaveData(byte[] ProductFileByteData, byte[] ThumbFileByteData, string extnProduct, string extnThumb)
    {
        AppVars appVar = new AppVars();
        Data.Product product = new Data.Product(ProductID);
        product.ResellerID.value = Int32.Parse(Util.getValue(ddlReseller));
        product.ProductName.value = Util.getValue(txtProductName);
        product.Color.value = Util.getValue(txtColor);
        product.ShowColorInDevice.value = chkShowColorInDevice.Checked;
        product.Description.value = Util.getValue(txtDescription);

        if (ProductFileByteData != null && ThumbFileByteData != null)
        {
            product.ProductImage.value = ProductFileByteData;
            product.ProductImageExt.value = extnProduct;
            product.ProductThumb.value = ThumbFileByteData;
            product.ProductThumbExt.value = extnThumb;
        }
        product.ProductPrice.value = txtProductPrice.Text == String.Empty ? 0 : Convert.ToDecimal(Util.getValue(txtProductPrice));
        product.IsUpchargeActive.value = chkIsUpChargeActive.Checked;

        Boolean isFloristToFlorist = true;
        if (Util.getValue(chkIsDirectShip))
            isFloristToFlorist = false;

        product.IsFloristToFlorist.value = isFloristToFlorist; //Util.getValue(chkIsFloristToFlorist);
        product.IsDirectShip.value = Util.getValue(chkIsDirectShip);
        product.IsPublished.value = chkIsPublished.Checked;

        if (ProductID <= 0)
        {
            product.DateAdded.value = Util.GetServerDate();
            product.AddedBy.value = appVar.UserID;
        }
        product.DateModified.value = Util.GetServerDate();
        product.ModifiedBy.value = appVar.UserID;

        //product.DateDeleted.value = Util.GetServerDate();
        //product.DeletedBy.value = appVar.UserID;

        product.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        product.SKUNo.value = Util.getValue(txtSkuNumber);
        product.Save();

        #region Updating Images...


        String filePath = Server.MapPath(@"~/DynImages\Product\");
        String fileProductImagePath = String.Format("ProductImage_{0}{1}", product.ProductID.value.ToString(), product.ProductImageExt.value);
        String fileProductThumbPath = String.Format("ProductThumb_{0}{1}", product.ProductID.value.ToString(), product.ProductThumbExt.value);

        if (ProductFileByteData != null)
        {
            if (File.Exists(filePath + fileProductImagePath))
                File.Delete(filePath + fileProductImagePath);

            FileStream fs = new FileStream(filePath + fileProductImagePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(ProductFileByteData, 0, ProductFileByteData.Length);
            fs.Close();
        }

        filePath += fileProductThumbPath;

        if (ThumbFileByteData != null)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Write(ThumbFileByteData, 0, ThumbFileByteData.Length);
            fs.Close();
        }

        #endregion Eng of Binding images..

        // Delete from ProductCategory by ProductID
        Data.ProductCategory pcat = new Data.ProductCategory();
        pcat.DeleteByProduct(ProductID);
        // Save selected Categories [ProductCategory] table.
        foreach (GridViewRow gvr in gvCategories.Rows)
            if (((CheckBox)gvr.FindControl("cbChecked")).Checked)
            {
                pcat = new Data.ProductCategory();
                pcat.ProductID.value = product.ProductID.value;
                pcat.CategoryID.value = int.Parse(gvCategories.DataKeys[gvr.RowIndex].Value.ToString());
                pcat.DateAdded.value = Util.GetServerDate();
                pcat.AddedBy.value = appVar.UserID;
                pcat.DateModified.value = Util.GetServerDate();
                pcat.ModifiedBy.value = appVar.UserID;
                pcat.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
                pcat.Save();
            }

        // Save or modify data into [ProductUpcharge] table.
        Data.ProductUpcharge prodUpcharge = new Data.ProductUpcharge();
        prodUpcharge.LoadID(ProductUpchargeID);
        prodUpcharge.ProductID.value = product.ProductID.value;
        prodUpcharge.UpchargePercentage.value = txtUpchargePercent.Text == String.Empty ? 0 : Convert.ToDouble(txtUpchargePercent.Text);
        prodUpcharge.ProductPrice.value = product.ProductPrice.value;
        prodUpcharge.NewProductPrice.value = Product_GetNewPrice(Util.GetDataDecimal(product.ProductPrice.value.ToString()));
        prodUpcharge.Description.value = String.Empty;//Util.getValue(etDescription); No issue

        if (ProductUpchargeID <= 0)
        {
            prodUpcharge.DateAdded.value = Util.GetServerDate();
            prodUpcharge.AddedBy.value = appVar.UserID;
        }

        prodUpcharge.DateModified.value = Util.GetServerDate();
        prodUpcharge.ModifiedBy.value = appVar.UserID;
        prodUpcharge.IsActive.value = chkIsUpChargeActive.Checked ? Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE) : Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
        prodUpcharge.Save();

        #region Log in ServerUpdates
        //Save in ServerUpdates
        Data.ServerUpdates su = new Data.ServerUpdates();
        su.UpdateStatus(product.ResellerID.value, "product");
        #endregion

        ProductID = product.ProductID.value;
        ProductUpchargeID = prodUpcharge.ProductUpchargeID.value;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        FileUpload fuProductImage = this.fuProductImage;

        byte[] fileThumbByteData = null;
        byte[] fileImageResizedByteData = null;
        String extn = Path.GetExtension(fuProductImage.PostedFile.FileName).ToLower();
        String strProductImgType = fuProductImage.PostedFile.ContentType + extn;
        String filePath = String.Empty;

        try
        {
            #region Validation for Product Limit
            if (!CanAddNewProduct() && ProductID <= 0 && chkIsPublished.Checked)
            {
                ShowError(lblErrMsg, "You cann't add new product as maximum limit is reached. Please contact floralapp team for increasing the max limit for adding new product.");
                return;
            }
            else if (chkIsPublished.Checked && ProductID > 0)
            {
                Data.Product product = new Data.Product(ProductID);
                if (!product.IsPublished.value && !CanAddNewProduct())
                {
                    ShowError(lblErrMsg, "You cann't add new product as maximum limit is reached. Please contact floralapp team for increasing the max limit for adding new product.");
                    return;
                }
            }
            #endregion End of validation for Product Limit

            #region Validate at least one Category is selected

            int nrCategs = 0;
            foreach (GridViewRow gvr in gvCategories.Rows)
                if (((CheckBox)gvr.FindControl("cbChecked")).Checked)
                    nrCategs++;
            if (nrCategs == 0)
            {
                ShowError(lblErrMsg, "You must select at least one Category!");
                return;
            }
            if (chkIsPublished.Checked)
            {
                StringBuilder errors = new StringBuilder();
                foreach (GridViewRow gvr in gvCategories.Rows)
                    if (((CheckBox)gvr.FindControl("cbChecked")).Checked)
                    {
                        if (((HiddenField)gvr.FindControl("wasPublished")).Value == "0"
                            && ((HiddenField)gvr.FindControl("canPublish")).Value == "0")
                        {
                            errors.Append(string.Format("Category: '{0}' has exceeded Maximum allowed products published limit!</br>", ((HiddenField)gvr.FindControl("hdnCategName")).Value));
                        }
                    }

                if (errors.Length > 0)
                {
                    ShowError(lblErrMsg, errors.ToString());
                    return;
                }
            }

            #endregion

            if (fuProductImage.HasFile)
            {
                Stream imgProductStream = fuProductImage.PostedFile.InputStream;
                int contentProductLength = fuProductImage.PostedFile.ContentLength;
                string imgValidation = ImageCreate(strProductImgType, imgProductStream, contentProductLength,
                                        ref fileImageResizedByteData, ref fileThumbByteData);
                if (!string.IsNullOrEmpty(imgValidation))
                {
                    ShowError(lblErrMsg, imgValidation);
                    return;
                }
            }
            else
            {
                if ((imgProductImage.ImageUrl.IndexOf("blank_img.png") > 0) || (imgProductImage.ImageUrl == String.Empty))
                {
                    ShowError(lblErrMsg, "Upload an image for product.");
                    return;
                }
            }

            SaveData(fileImageResizedByteData, fileThumbByteData, extn, ".png");
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message + " " + ex.StackTrace);
            return;
        }
        if (((Button)sender).ID == "btnSave")
            Response.Redirect("Products.aspx");
        try
        {
            String redirectUrl = "AddProduct.aspx";
            String qs = Crypto.EncryptQueryString("ProductID=" + ProductID.ToString() + " &ProdUpchargeID=" + ProductUpchargeID.ToString());

            redirectUrl += "?eqs=" + qs + "&#varr";
            Session["prodsuccess"] = 1;
            Response.Redirect(redirectUrl);
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }
    }

    private string ImageCreate(string strProductImgType, Stream imgProductStream, int contentProductLength,
                ref byte[] fileImageResizedByteData, ref byte[] fileThumbByteData)
    {
        byte[] ProductFileByteData = null;
        switch (strProductImgType)
        {
            case "image/jpg.jpg":
            case "image/jpeg.jpg":
            case "image/pjpeg.jpg":

            case "image/jpg.jpeg":
            case "image/jpeg.jpeg":
            case "image/pjpeg.jpeg":

            case "image/png.png":
            case "image/x-png.png":

            case "image/gif.gif":
            case "image/bmp.bmp":
            case "image/ico.ico":
                break;
            default:
                AddErrorMessage("Upload image only *.png, *.jpg, *.jpeg, *.gif, *.bmp, *.ico format.");
                break;
        }

        // Check error : if any server side validation error exist.
        if (_errorMsg != String.Empty)
        {
            return _errorMsg;
        }
        else
        {

            // If the selected file extension is valid then 
            // convert the selected file into bytes for saving in database             
            using (BinaryReader readerProduct = new BinaryReader(imgProductStream))
            {
                ProductFileByteData = readerProduct.ReadBytes(contentProductLength);
                readerProduct.Close();
            }

            // Read the file bytes in MemoryStream for checking it's dimention as desired. 
            MemoryStream memProductStream = new MemoryStream(ProductFileByteData);
            System.Drawing.Image imgProductObject = System.Drawing.Image.FromStream(memProductStream);

            // Prevent using images internal thumbnail
            imgProductObject.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            imgProductObject.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            Int32 imgProductHeight = imgProductObject.Height;
            Int32 imgProductWidth = imgProductObject.Width;

            //if (imgProductHeight > 200 || imgProductWidth > 200)
            //{
            //    ShowError(lblErrMsg, "Uploaded image size should not be greater than the defined size.");
            //    return;
            //}

            #region Resize Image
            const int MAX_WIDTH = 600;
            const int MAX_HEIGHT = 600;

            int NewHeight = imgProductHeight * MAX_WIDTH / imgProductWidth;
            int NewWidth = MAX_WIDTH;

            if (NewHeight > MAX_HEIGHT)
            {
                // Resize with height instead
                NewWidth = imgProductWidth * MAX_HEIGHT / imgProductHeight;
                NewHeight = MAX_HEIGHT;
            }

            System.Drawing.Image imgResized = imgProductObject.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

            MemoryStream mstreamResizedImage = new MemoryStream();

            imgResized.Save(mstreamResizedImage, System.Drawing.Imaging.ImageFormat.Png);
            fileImageResizedByteData = new Byte[mstreamResizedImage.Length];
            mstreamResizedImage.Position = 0;
            mstreamResizedImage.Read(fileImageResizedByteData, 0, (int)mstreamResizedImage.Length);
            #endregion

            #region Create Thumb image...

            const int MAX_THUMB_WIDTH = 75;
            const int MAX_THUMB_HEIGHT = 75;

            int newThumbHeight = imgProductHeight * MAX_THUMB_WIDTH / imgProductWidth;
            int newThumbWidth = MAX_THUMB_HEIGHT;

            if (newThumbHeight > MAX_THUMB_HEIGHT)
            {
                // Resize with height instead
                newThumbWidth = imgProductWidth * MAX_THUMB_HEIGHT / imgProductHeight;
                newThumbHeight = MAX_THUMB_HEIGHT;
            }

            System.Drawing.Image imgThumbs = imgProductObject.GetThumbnailImage(newThumbWidth, newThumbHeight, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);
            MemoryStream mstreamThumb = new MemoryStream();

            imgThumbs.Save(mstreamThumb, System.Drawing.Imaging.ImageFormat.Png);
            fileThumbByteData = new Byte[mstreamThumb.Length];
            mstreamThumb.Position = 0;
            mstreamThumb.Read(fileThumbByteData, 0, (int)mstreamThumb.Length);
            #endregion End of byte
        }
        return "";
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Products.aspx");
    }

    ///  <summary>
    /// Required, but not used
    /// </summary>
    /// <returns>true</returns>
    public bool ThumbnailCallback()
    {
        return true;
    }

    protected void ddlReseller_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCategory(Int32.Parse(Util.getValue(ddlReseller)));
        //FillSpecialOffer(Int32.Parse(Util.getValue(ddlReseller))); Removed concept for special offer : Jnan
    }

    private void FillCategory(Int32 resellerID)
    {
        Data.Category cat = new Data.Category();
        DataSet dsCat = cat.Category_GetListByResellerAndProductId(resellerID, (int)ProductID);

        gvCategories.DataSource = dsCat;
        gvCategories.DataBind();
    }

    private Boolean CanAddNewProduct()
    {
        Int32 resellerID = Int32.Parse(Util.getValue(ddlReseller));
        Data.Reseller rs = new Data.Reseller();
        return rs.CanAddNewProduct(resellerID);
    }

    protected void chkIsUpChargeActive_CheckedChanged(object sender, EventArgs e)
    {
        //pnlPercent.Visible = chkIsUpChargeActive.Checked ? true : false;
    }

    private Decimal Product_GetNewPrice(Decimal currentPrice)
    {
        Decimal newPrice = 0;
        Decimal percent = Util.GetDataDecimal(txtUpchargePercent.Text);
        newPrice = currentPrice * (percent / 100);
        newPrice += currentPrice;
        return Util.GetDataDecimal(newPrice.ToString("N2"));
    }

    protected void btnCreateProdVar_Click(object sender, EventArgs e)
    {
        try
        {
            byte[] fileThumbByteData = null;
            byte[] fileImageResizedByteData = null;
            String extn = Path.GetExtension(fuVariationImage.PostedFile.FileName).ToLower();
            String strProductImgType = fuVariationImage.PostedFile.ContentType + extn;
            String filePath = String.Empty;

            if (fuVariationImage.HasFile)
            {
                Stream imgProductStream = fuVariationImage.PostedFile.InputStream;
                int contentProductLength = fuVariationImage.PostedFile.ContentLength;
                string imgValidation = ImageCreate(strProductImgType, imgProductStream, contentProductLength,
                                        ref fileImageResizedByteData, ref fileThumbByteData);
                if (!string.IsNullOrEmpty(imgValidation))
                {
                    ShowError(lblErrorVar, imgValidation);
                    return;
                }
            }

            AppVars appVar = new AppVars();
            Data.Product parentProduct = new Data.Product(ProductID);
            Data.Product variationProd = new Data.Product(ProductVariationID);

            variationProd.ParentProductID.value = (int)ProductID;
            variationProd.ProductName.value = Util.getValue(txtProductNameVar);
            variationProd.ProductPrice.value = txtProductPriceVar.Text == String.Empty ? 0 : Convert.ToDecimal(Util.getValue(txtProductPriceVar));
            variationProd.SKUNo.value = Util.getValue(txtSkuNumberVar);

            variationProd.ResellerID.value = Int32.Parse(Util.getValue(ddlReseller));
            variationProd.Color.value = parentProduct.Color.value;
            variationProd.ShowColorInDevice.value = parentProduct.ShowColorInDevice.value;
            variationProd.Description.value = parentProduct.Description.value;
            variationProd.IsUpchargeActive.value = parentProduct.IsUpchargeActive.value;
            variationProd.IsFloristToFlorist.value = parentProduct.IsFloristToFlorist.value;
            variationProd.IsDirectShip.value = parentProduct.IsDirectShip.value;
            variationProd.IsPublished.value = parentProduct.IsPublished.value;

            if (ProductVariationID == 0)
            {
                variationProd.DateAdded.value = Util.GetServerDate();
                variationProd.AddedBy.value = appVar.UserID;
            }
            else
            {
                variationProd.DateModified.value = Util.GetServerDate();
                variationProd.ModifiedBy.value = appVar.UserID;
            }
            variationProd.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);

            if (fileImageResizedByteData == null && fileThumbByteData == null &&
                    ProductVariationID == 0)
            {
                variationProd.ProductImage.value = parentProduct.ProductImage.value;
                variationProd.ProductImageExt.value = parentProduct.ProductImageExt.value;
                variationProd.ProductThumb.value = parentProduct.ProductThumb.value;
                variationProd.ProductThumbExt.value = parentProduct.ProductThumbExt.value;
            }

            variationProd.Save();

            #region Updating Images...

            if (fileImageResizedByteData == null && fileThumbByteData == null &&
                    ProductVariationID == 0)
            {
                filePath = Server.MapPath(@"~/DynImages\Product\");
                String fileProductImagePath = String.Format("ProductImage_{0}{1}", parentProduct.ProductID.value.ToString(), parentProduct.ProductImageExt.value);
                String fileProductThumbPath = String.Format("ProductThumb_{0}{1}", parentProduct.ProductID.value.ToString(), parentProduct.ProductThumbExt.value);

                String fileProductImagePathVar = String.Format("ProductImage_{0}{1}", variationProd.ProductID.value.ToString(), variationProd.ProductImageExt.value);
                String fileProductThumbPathVar = String.Format("ProductThumb_{0}{1}", variationProd.ProductID.value.ToString(), variationProd.ProductThumbExt.value);


                if (File.Exists(filePath + fileProductImagePath) &&
                    !File.Exists(filePath + fileProductImagePathVar))
                {
                    File.Copy(filePath + fileProductImagePath, filePath + fileProductImagePathVar);
                }

                if (File.Exists(filePath + fileProductThumbPath) &&
                    !File.Exists(filePath + fileProductThumbPathVar))
                {
                    File.Copy(filePath + fileProductThumbPath, filePath + fileProductThumbPathVar);
                }
            }

            if (fileImageResizedByteData != null && fileThumbByteData != null)
            {
                variationProd.ProductImage.value = fileImageResizedByteData;
                variationProd.ProductThumb.value = fileThumbByteData;
                variationProd.ProductImageExt.value = extn;
                variationProd.ProductThumbExt.value = ".png";

                variationProd.Save();

                filePath = Server.MapPath(@"~/DynImages\Product\");
                String fileProductImagePathProdVar = String.Format("ProductImage_{0}{1}", variationProd.ProductID.value.ToString(), variationProd.ProductImageExt.value);
                String fileProductThumbPathProdVar = String.Format("ProductThumb_{0}{1}", variationProd.ProductID.value.ToString(), variationProd.ProductThumbExt.value);

                if (fileImageResizedByteData != null)
                {
                    if (File.Exists(filePath + fileProductImagePathProdVar))
                        File.Delete(filePath + fileProductImagePathProdVar);

                    FileStream fs = new FileStream(filePath + fileProductImagePathProdVar, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs.Write(fileImageResizedByteData, 0, fileImageResizedByteData.Length);
                    fs.Close();
                }

                filePath += fileProductThumbPathProdVar;

                if (fileThumbByteData != null)
                {
                    if (File.Exists(filePath))
                        File.Delete(filePath);

                    FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs.Write(fileThumbByteData, 0, fileThumbByteData.Length);
                    fs.Close();
                }
            }

            #endregion Eng of Binding images..

            // Save or modify data into [ProductUpcharge] table.
            Data.ProductUpcharge prodUpcharge = new Data.ProductUpcharge();
            prodUpcharge.LoadID(ProductVariationUpchargeID);
            prodUpcharge.ProductID.value = variationProd.ProductID.value;
            prodUpcharge.UpchargePercentage.value = txtUpchargePercent.Text == String.Empty ? 0 : Convert.ToDouble(txtUpchargePercent.Text);
            prodUpcharge.ProductPrice.value = txtProductPriceVar.Text == String.Empty ? 0 : Convert.ToDecimal(Util.getValue(txtProductPriceVar));
            prodUpcharge.NewProductPrice.value = Product_GetNewPrice(Util.GetDataDecimal(variationProd.ProductPrice.value.ToString()));
            prodUpcharge.Description.value = String.Empty;
            if (ProductVariationUpchargeID == 0)
            {
                prodUpcharge.DateAdded.value = Util.GetServerDate();
                prodUpcharge.AddedBy.value = appVar.UserID;
            }
            else
            {
                prodUpcharge.DateModified.value = Util.GetServerDate();
                prodUpcharge.ModifiedBy.value = appVar.UserID;
            }
            prodUpcharge.IsActive.value = chkIsUpChargeActive.Checked ? Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE) : Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
            prodUpcharge.Save();

            if (ProductVariationID > 0)
                ShowError(lblErrorVar, "Product Variation Updated Successfully.");
            else
                ShowError(lblErrorVar, "Product Variation Created Successfully.");

            BindVariations();

            btnCancelVar_Click(null, null);
        }
        catch (Exception ex)
        {
            ShowError(lblErrorVar, "Error Creating/Updating Product Variation.");
            ShowError(lblErrorVar, ex.Message + ex.StackTrace);
        }
    }

    protected void gvCategories_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                ((HiddenField)e.Row.FindControl("hdnCategName")).Value = drv["CategoryName"].ToString();
                ((HiddenField)e.Row.FindControl("wasPublished")).Value = drv["wasPublished"].ToString();
                ((HiddenField)e.Row.FindControl("canPublish")).Value = drv["canPublish"].ToString();
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void BindVariations()
    {
        imgVariationImage.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");

        Data.Product p = new Data.Product();
        Util.setValue(gvVariations, p.GetVariationListByProductID((int)ProductID));
    }

    protected void btnEditVariation_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            string[] value = objButton.CommandArgument.ToString().Split(',');
            ProductVariationID = Convert.ToInt32(value[0]);
            ProductVariationUpchargeID = Convert.ToInt64(value[1]);

            Data.Product variation = new Data.Product(ProductVariationID);
            Util.setValue(txtProductNameVar, variation.ProductName.value);
            Util.setValue(txtProductPriceVar, variation.ProductPrice.value);
            Util.setValue(txtSkuNumberVar, variation.SKUNo.value);

            btnCreateProdVar.Text = "Update Product Variation";
            btnCancelVar.Visible = true;

            String imgExtn = variation.ProductImageExt.value;
            String imgProductPath = String.Format("ProductImage_{0}{1}", variation.ProductID.value.ToString(), imgExtn);
            if (!File.Exists(Server.MapPath("../DynImages/Product/" + imgProductPath)))
                imgVariationImage.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
            else
                imgVariationImage.ImageUrl = String.Format("../DynImages/Product/{0}", imgProductPath);
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message + ex.StackTrace);
        }
    }

    protected void btnDeleteVariation_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int64 productID = Convert.ToInt64(objButton.CommandArgument.ToString());

            Data.Product p = new Data.Product(productID);
            p.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
            p.DateDeleted.value = Util.GetServerDate();
            p.DeletedBy.value = Convert.ToInt64(v.UserID);
            p.Save();

            #region Inactivate/Delete the Relationship table(s).

            p.InActiveRelationshipTables(productID, "Product");

            #endregion End of inactivate/delete process.

            #region Insert/Update on Server
            //Save in ServerUpdates
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatus(p.ResellerID.value, "product");
            #endregion

            BindVariations();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnCancelVar_Click(object sender, EventArgs e)
    {
        try
        {
            Util.setValue(txtProductNameVar, "");
            Util.setValue(txtProductPriceVar, "");
            Util.setValue(txtSkuNumberVar, "");
            ProductVariationID = 0;
            btnCancelVar.Visible = false;
            btnCreateProdVar.Text = "Create Product Variation";

            imgVariationImage.ImageUrl = String.Format("~/Images/{0}", "blank_img.png");
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    public String getThumbImage(String fileExtension)
    {
        String thumbPath = "../DynImages/Product/ProductThumb_" + fileExtension;

        if (!File.Exists(Server.MapPath(thumbPath)))
            thumbPath = "~/Images/blank_img.png";

        return thumbPath;
    }
}

﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;

public partial class Admin_CouponUsageReport : AppPage
{
    private static Int32 resellerID
    {
        get;
        set;
    }

    private static String couponCode
    {
        get;
        set;
    }

    private int rowCount = 0;
    private int totalRowCount = 0;
    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            InitializeControls();
            BindData();
        }
    }

    private void InitializeControls()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            //ddlReseller.Enabled = false;
            divShop.Visible = false;

            ddlSearchField.Items.RemoveAt(2);
        }
    }

    private void BindData()
    {
        Util.setValue(gvCouponUsage, GetCouponList());

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvCouponUsage.Columns[2].Visible = false;
        }
    }

    private DataSet GetCouponList()
    {

        StringBuilder sbFilter = new StringBuilder();
        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();

        switch (searchOperator)
        {
            case "Begins With":
                sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                break;
            case "Ends With":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                break;
            case "Contains":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                break;
            case "Equals":
                sbFilter.Append(" And " + searchField + " = " + searchString);
                break;
            default:
                break;
        }

        Int32 resellerID = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? Convert.ToInt32(v.ResellerID) : Convert.ToInt32(Util.getValue(ddlReseller));

        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);


        String orderBy = gvCouponUsage.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " ResellerName asc";

        Data.PurchaseCoupon pc = new Data.PurchaseCoupon();
        DataSet dsCoupon = pc.GetCouponUsageReport(gvCouponUsage.PageIndex, gvCouponUsage.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvCouponUsage.VirtualItemCount = totalRowCount;

        #region Binding images ...
        /*
        foreach (DataRow drCoupon in dsCoupon.Tables[0].Rows)
        {
            Byte[] fileByteData = (byte[])drCoupon["CouponImage"];
            String imageExt = Convert.ToString(drCoupon["CouponImageExt"]);
            Int64 couponID = Convert.ToInt32(drCoupon["CouponID"]);

            String filePath = Server.MapPath(@"~/DynImages\Coupon\");
            String fileName = String.Format("CouponImage_{0}{1}", couponID, imageExt);
            filePath += fileName;

            if (File.Exists(filePath))
                File.Delete(filePath);

            if (fileByteData != null)
            {
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteData, 0, fileByteData.Length);
                fs.Close();
            }
        } 
         */
        #endregion End of binding image


        // Set Total Count in grid footer
        Util.setValue(lblTotal, String.Format("{0} record(s).", totalRowCount));

        return dsCoupon;
    }

    protected void gvCouponUsage_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCouponUsage.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvCouponUsage_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void lnkViewPurchase_Click(object sender, EventArgs e)
    {
        String[] values = ((ImageButton)sender).CommandArgument.Split(',');
        resellerID = Util.GetDataInt32(values[0].ToString());
        couponCode = Util.GetDataString(values[1].ToString());
        BindCouponReport();
        mpeCouponReport.Show();
    }

    private void BindCouponReport()
    {
        Int32 resID = resellerID;
        String coCode = couponCode;

        Data.PurchaseCoupon pc = new Data.PurchaseCoupon();
        DataSet dsCouponReport = pc.GetCouponReportByReseller(resID, coCode);
        gvReport.DataSource = dsCouponReport.Tables[0].DefaultView;
        gvReport.DataBind();

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
            gvReport.Columns[0].Visible = true;
        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
            gvReport.Columns[3].Visible = false;
    }

    protected void gvReport_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvReport.PageIndex = e.NewPageIndex;
        BindCouponReport();
        mpeCouponReport.Show();
    }

    protected void gvReport_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindCouponReport();
        mpeCouponReport.Show();
    }

    protected void lnkPurchase_Click(object sender, EventArgs e)
    {
        String redirectUrl = "PurchaseInformation.aspx";

        try
        {
            String purchaseID = ((LinkButton)sender).CommandArgument.ToString();

            String qs = Crypto.EncryptQueryString("PurchaseID=" + purchaseID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

}


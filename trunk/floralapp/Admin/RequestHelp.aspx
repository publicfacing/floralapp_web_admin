﻿<%@ Page Title="floralapp&reg; | Request Help" Language="C#" MasterPageFile="~/Admin/AdminMaster.master"
    AutoEventWireup="true" CodeFile="RequestHelp.aspx.cs" Inherits="Admin_RequestHelp"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg TableHeading">
                            <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse;
                                padding-left: 5px;">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPageTitle" runat="server" Text="Request Help"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false" CssClass="error">
                            </asp:Label>
                            <asp:ValidationSummary ID="vsReseller" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                <tr>
                                    <td class="col" style="width: 160px">
                                        Name<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtName" runat="server" SkinID="textbox" class="FormItem" MaxLength="150"
                                            Width="400px"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                            ErrorMessage="Enter Name." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Shop Name<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtShopName" runat="server" SkinID="textbox" class="FormItem" MaxLength="150"
                                            Width="400px"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtShopName"
                                            ErrorMessage="Enter Shop Name." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Phone<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtPhone" runat="server" SkinID="textbox" class="FormItem" MaxLength="150"
                                            Width="100px"></asp:TextBox><br />
                                        <ajax:MaskedEditExtender runat="server" ID="mEPhone1" TargetControlID="txtPhone"
                                            Mask="999-999-9999" MaskType="Number" AcceptNegative="Right" ErrorTooltipEnabled="True"
                                            ClearMaskOnLostFocus="False" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                            CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                            CultureTimePlaceholder="" Enabled="True">
                                        </ajax:MaskedEditExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPhone"
                                            ErrorMessage="Enter Phone number." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Email<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtEmail" runat="server" SkinID="textbox" class="FormItem" MaxLength="150"
                                            Width="400px"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmail"
                                            ErrorMessage="Enter Email address." Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revEmail1" runat="server" ErrorMessage="Please enter a valid Email address."
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,3}([-.]\w+)*" ControlToValidate="txtEmail"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Web address of where you are seeing the problem
                                    </td>
                                    <td class="val">
                                        <asp:TextBox ID="txtUrl" runat="server" SkinID="textbox" class="FormItem" MaxLength="150"
                                            Width="400px"></asp:TextBox><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col">
                                        Description of the problem<span class="reqd">*</span>
                                    </td>
                                    <td class="val">
                                        <mo:ExtTextBox ID="txtDescription" runat="server" TextMode="MultiLine" MaxLength="500"
                                            SkinID="textarea" Height="80px" Width="400px"></mo:ExtTextBox><br />
                                        Max. 500 character.<br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDescription"
                                            ErrorMessage="Enter Description of the problem." Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="actionDiv">
                    <asp:Button ID="btnSend" runat="server" Text="  Send  " CssClass="btn" ToolTip="Click here to request help."
                        OnClick="btnSend_Click" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

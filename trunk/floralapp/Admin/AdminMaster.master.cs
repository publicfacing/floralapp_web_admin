﻿using System;
using Library.Utility;
using Library.AppSettings;
using System.Web.UI.WebControls;
using System.Web.UI;

public partial class Admin_AdminMaster : System.Web.UI.MasterPage
{
    AppVars v = new AppVars();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitializeTableStatus();
            BindData();
        }
    }

    private void InitializeTableStatus()
    {
        tblStatus.Visible = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? false : true;

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            imgResellerLogo.Visible = true;
            imgResellerLogo.ImageUrl = "../DynImages/Reseller/ResellerLogo_" + v.ResellerID + ".png";
        }
        //else
        //{
        //    tblStatus.Visible = true;
        //}

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
        {
            lnkSearchSettings.Visible = true;

            //Binding Reseller into DropDownList
            Data.Reseller reseller = new Data.Reseller();
            Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        }

    }

    private void BindData()
    {
        //Data.LoginUser lu = new Data.LoginUser();
        //Util.setValue(lblUserCount, lu.GetCount());

        Data.Users userCount = new Data.Users();
        Util.setValue(lblUserCount, userCount.GetCount());

        Data.UsersInSession uis = new Data.UsersInSession();
        Util.setValue(lbliPhoneInSession, uis.GetCount(Convert.ToInt32(AppEnum.DeviceType.IPHONE)));
        Util.setValue(lblAndroidInSession, uis.GetCount(Convert.ToInt32(AppEnum.DeviceType.ANDROID)));
        
        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
        {
            // Set Reseller.
            Util.setValue(ddlReseller, v.ResellerID);

            Data.Reseller r = new Data.Reseller(v.ResellerID);
            String msg = String.Format("You are viewing data for '{0}' shop.", (v.ResellerID > 0) ? r.ResellerName.value : "all");
            Util.setValue(lblFilteredResellerName, msg);
        }

    }

    protected void btnApply_Click(object sender, EventArgs e)
    {
        Int32 resellerID = Convert.ToInt32(Util.getValue(ddlReseller));
        v.ResellerID = resellerID;

        Response.Redirect("../Default.aspx");
    } 
}

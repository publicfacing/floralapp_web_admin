﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;
using System.IO;

public partial class Admin_Coupons : AppPage
{
    private int rowCount = 0;
    private int totalRowCount = 0;
    AppVars v = new AppVars();

    public override void Page_Load(object sender, EventArgs e)
    {
        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            InitializeControls();
            BindData();
        }
    }

    private void InitializeControls()
    {
        //Binding Reseller and id into DropDownList
        Data.Reseller reseller = new Data.Reseller();
        Util.setValue(ddlReseller, reseller.List(), "ResellerName", "ResellerID");
        Util.setValue(ddlReseller, v.ResellerID);

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Util.setValue(ddlReseller, v.ResellerID);
            //ddlReseller.Enabled = false;
            divShop.Visible = false;

            ddlSearchField.Items.RemoveAt(3);
        }
    }

    private void BindData()
    {
        Util.setValue(gvCoupon, GetCouponList());

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            gvCoupon.Columns[4].Visible = false;
        }
    }

    private DataSet GetCouponList()
    {

        StringBuilder sbFilter = new StringBuilder();
        String searchField = ddlSearchField.SelectedValue;
        String searchOperator = ddlSearchOperator.SelectedValue;
        String searchString = txtSearchString.Text.Trim();

        switch (searchOperator)
        {
            case "Begins With":
                sbFilter.Append(" And " + searchField + " like  '" + searchString + "%'");
                break;
            case "Ends With":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "'");
                break;
            case "Contains":
                sbFilter.Append(" And " + searchField + " like  '%" + searchString + "%'");
                break;
            case "Equals":
                sbFilter.Append(" And " + searchField + " = " + searchString);
                break;
            default:
                break;
        }

        Int32 resellerID = (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN)) ? Convert.ToInt32(v.ResellerID) : Convert.ToInt32(Util.getValue(ddlReseller));

        if (resellerID > 0)
            sbFilter.Append(" And ResellerID =" + resellerID);


        String orderBy = gvCoupon.OrderBy;
        if (String.IsNullOrEmpty(orderBy))
            orderBy = " CouponID asc";

        Data.Coupon coupon = new Data.Coupon();
        DataSet dsCoupon = coupon.FilteredList(gvCoupon.PageIndex, gvCoupon.PageSize, orderBy, sbFilter.ToString(), ref rowCount, ref totalRowCount);
        gvCoupon.VirtualItemCount = totalRowCount;

        #region Binding images ...
        /*
        foreach (DataRow drCoupon in dsCoupon.Tables[0].Rows)
        {
            Byte[] fileByteData = (byte[])drCoupon["CouponImage"];
            String imageExt = Convert.ToString(drCoupon["CouponImageExt"]);
            Int64 couponID = Convert.ToInt32(drCoupon["CouponID"]);

            String filePath = Server.MapPath(@"~/DynImages\Coupon\");
            String fileName = String.Format("CouponImage_{0}{1}", couponID, imageExt);
            filePath += fileName;

            if (File.Exists(filePath))
                File.Delete(filePath);

            if (fileByteData != null)
            {
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteData, 0, fileByteData.Length);
                fs.Close();
            }
        } 
         */
        #endregion End of binding image


        // Set Total Count in grid footer
        Util.setValue(lblTotalCoupon, String.Format("{0} coupon(s).", totalRowCount));

        return dsCoupon;
    }

    protected void gvCoupon_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCoupon.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvCoupon_OnSorting(object sender, GridViewSortEventArgs e)
    {
        BindData();
    }

    protected void lnkEnable_Click(object sender, EventArgs e)
    {
        try
        {

            String[] args = Util.GetDataString(((ImageButton)sender).CommandArgument).Split(',');
            Int64 couponID = Util.GetDataInt64(args[0].ToString());
            Boolean isEnabled = Util.GetDataBool(args[1].ToString());
            Int32 couponUsageTypeID = Util.GetDataInt32(args[2].ToString());

            if (isEnabled) //if checked user want to deactivate it.
                isEnabled = false;
            else
                isEnabled = true;

            Data.Coupon coupon = new Data.Coupon();
            coupon.LoadID(couponID);

            //Only one coupon can be enabled for any Reseller.
            coupon.EnableCoupon(couponID, coupon.ResellerID.value, v.UserID, isEnabled, couponUsageTypeID);
            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }

    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        String redirectUrl = "AddCoupon.aspx";

        try
        {
            ImageButton objButton = (ImageButton)sender;
            String couponID = objButton.CommandArgument.ToString();
            String qs = Crypto.EncryptQueryString("CouponID=" + couponID);
            redirectUrl += "?eqs=" + qs;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        Response.Redirect(redirectUrl);
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton objButton = (ImageButton)sender;
            Int32 couponID = Convert.ToInt32(objButton.CommandArgument.ToString());
            Data.Coupon coupon = new Data.Coupon(couponID);
            Int32 resellerID = coupon.ResellerID.value;

            coupon.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.INACTIVE);
            coupon.DateDeleted.value = Util.GetServerDate();
            coupon.DeletedBy.value = v.UserID;
            coupon.Save();

            #region Inactivate/Delete the Relationship table(s) for this user

            coupon.InActiveRelationshipTables(couponID, "Coupon");

            #endregion End of inactivate/delete process.

            #region Insert/Update on Server
            //Save in ServerUpdates
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatus(resellerID, "coupon");
            #endregion

            BindData();
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        BindData();
    }

    public string GetImage(bool allowed)
    {
        if (allowed)
            return "~/Images/tick.PNG";
        else
            return "~/Images/tick-gray.PNG";
    }

    public String getCouponImage(String fileExtension)
    {
        String imgPath = "../DynImages/Coupon/Couponimage_" + fileExtension;

        if (!File.Exists(Server.MapPath(imgPath)))
            imgPath = "~/Images/blank_img.png";

        return imgPath;
    }
}


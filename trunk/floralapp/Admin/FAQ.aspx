﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="FAQ.aspx.cs" Inherits="Admin_FAQ" Title="BenevaFlowers | FAQ" Theme="GraySkin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" Runat="Server">
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"> 
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse" class="arial-12">
                    <tr>
                        <th class="TableHeadingBg TableHeading" >
                           <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse; padding-left: 5px;">
                                <tr>
                                    <td>
                                        FAQ
                                    </td>
                                    <td style="text-align: right; padding-right: 5px; text-transform: capitalize;"> 
                                       <asp:HyperLink ID="lnkAdd" runat="server" NavigateUrl="~/Admin/AddFaq.aspx" ToolTip="Add FAQ">Add FAQ</asp:HyperLink>
                                    </td>
                                </tr>
                            </table> 
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder">
                            <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false"></asp:Label>
                            <asp:GridView ID="gvFaq" runat="server" DataKeyNames="FaqID" 
				                AllowSorting="True" SkinID="gvskin" OnPageIndexChanging="gvFaq_OnPageIndexChanging" EmptyDataText="No record(s) found.">
				                <Columns>
					                <asp:BoundField DataField="Question" HeaderText="Question"  ItemStyle-CssClass="gridContent" > </asp:BoundField>
					                <asp:BoundField DataField="Answer" HeaderText="Answer"  ItemStyle-CssClass="gridContent" > </asp:BoundField>
					                 
					                <asp:TemplateField HeaderText="Action" ItemStyle-CssClass="gridContent">
						                <HeaderStyle CssClass="gridHeader" Width="80px" />
						                <ItemTemplate>							
							                <asp:ImageButton ID="LnkBtnEdit" runat="server" CommandArgument='<%# Eval("FaqID")%>' CausesValidation="false" OnClick="lnkBtn_Edit" ToolTip="Edit" ImageUrl="~/Images/edit-icon.jpg" />
							                <asp:ImageButton ID="LnkBtnDelete" runat="server" CommandArgument='<%# Eval("FaqID")%>' CausesValidation="false" OnClick="lnkBtn_Delete" ImageUrl="~/Images/DeleteButton.jpg" OnClientClick="return confirm('Are you sure you want to delete this record?');" ToolTip="Delete" />
						                </ItemTemplate>
					                </asp:TemplateField>
				                </Columns>
			                </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="FooterContentDetails" >&nbsp;
                        </td>
                    </tr>
                 </table>
            </td>
        </tr> 
    </table>   
        
</asp:Content>


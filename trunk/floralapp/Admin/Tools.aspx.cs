﻿using System;
using Library.Utility;
using System.Drawing;

public partial class Admin_Tools : AppPage
{ 
     
    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);
          
    }
     

    private void GenerateImagesByReseller()
    {
        Int32 resellerID =  Util.GetDataInt32(Util.getValue(txtResellerID));

        Data.Reseller res = new Data.Reseller(resellerID);         
        String resellerDirPath = Server.MapPath(@"~/DynImages\Reseller\");
        String categoryDirPath = Server.MapPath(@"~/DynImages\Category\");
        String productDirPath = Server.MapPath(@"~/DynImages\Product\");
        String upsellCategoryDirPath = Server.MapPath(@"~/DynImages\UpsellCategory\");

        if (res.ResellerID.value > 0)
        {
            res.GenerateImagesByReseller(res.ResellerID.value, resellerDirPath, categoryDirPath, productDirPath, upsellCategoryDirPath);
        }
    }

    protected void btnGenerateImagesByReseller_Click(object sender, EventArgs e)
    {
        try
        {
            GenerateImagesByReseller();
            ShowError(lblErrMsg, "Images are generated successfully.");
            lblErrMsg.ForeColor = Color.Green;
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            lblErrMsg.ForeColor = Color.Red;
            return;
        }

        //Response.Redirect("Tools.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Tools.aspx");
    }
}


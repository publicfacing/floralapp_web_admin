﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.IO;
using System.Collections.Specialized;

public partial class Admin_AddDefaultCategory : AppPage
{
    private Int32 DefaultCategoryID
    {
        get;
        set;
    } 

    public override void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);

        try
        {
            if ((Request.QueryString["eqs"] != null) && (Request.QueryString["eqs"].ToString() != ""))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptQueryString(Request.QueryString["eqs"].ToString()));
                DefaultCategoryID = Util.GetDataInt32(nvc["DefaultCategoryID"]); 
            }

            Util.setValue(lblPageTitle, (DefaultCategoryID > 0) ? "Edit Default Category" : "Add Default Category");
        }
        catch (Exception ex)
        {
            base.ShowError(lblErrMsg, ex.Message);
            return;
        }

        if (!IsPostBack)
        {
            initialigeControls();
            BindData();
        }
    }

    private void initialigeControls()
    {
        Data.DefaultCategory dc = new Data.DefaultCategory();
        DataSet dsDefCat = dc.List();
        rptImage.DataSource = dsDefCat.Tables[0].DefaultView;
        rptImage.DataBind();
        foreach (DataRow drCat in dsDefCat.Tables[0].Rows)
        {
            Byte[] fileByteData = (byte[])drCat["ImageBinary"];
            String imageExt = Convert.ToString(drCat["ImageExt"]);
            String ID = drCat["DefaultCategoryID"].ToString();
            String filePath = Server.MapPath(@"~/DynImages\");
            String fileName = String.Format("defCatImg_{0}{1}",ID, imageExt);
            filePath += fileName;

            if (File.Exists(filePath))
                File.Delete(filePath);

            if (fileByteData != null)
            {
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(fileByteData, 0, fileByteData.Length);
                fs.Close();
            }
        }
    }

    private void BindData()
    { 
        if (DefaultCategoryID <= 0)
            return;

        Data.DefaultCategory dc = new Data.DefaultCategory();
        dc.LoadID(DefaultCategoryID);

        Util.setValue(this.txtCategoryName, dc.CategoryName.value);

    }

    private void SaveData(byte[] categoryImgByteData, String fileExtn)
    { 
        AppVars appVar = new AppVars();

        Int32 dcatID = Util.GetDataInt32(Util.getValue(txtCatID));

        Data.DefaultCategory dc = new Data.DefaultCategory(dcatID);

        //dc.CategoryName.value = Util.getValue(txtCategoryName);

        if (categoryImgByteData != null) 
            dc.ImageBinary.value = categoryImgByteData;
        dc.ImageExt.value = fileExtn;
        //dc.Sequence.value = 0;
        dc.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
        dc.Save();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        FileUpload imgCtrl = this.imgCtrl;
        byte[] fileByteData = null;
        String extension = Path.GetExtension(imgCtrl.PostedFile.FileName).ToString().ToLower();
        String imgType = imgCtrl.PostedFile.ContentType + extension;

        try
        {
            // Check if file upload ctrl has any files selected or not.
            if (imgCtrl.HasFile)
            {
                switch (imgType)
                {
                    case "image/jpg.jpg":
                    case "image/jpeg.jpg":
                    case "image/pjpeg.jpg":

                    case "image/jpg.jpeg":
                    case "image/jpeg.jpeg":
                    case "image/pjpeg.jpeg":

                    case "image/png.png":
                    case "image/x-png.png":

                    case "image/gif.gif":
                    case "image/bmp.bmp":
                    case "image/ico.ico":
                        break;
                    default:
                        AddErrorMessage("Upload image only *.png, *.jpg, *.jpeg, *.gif, *.bmp, *.ico format.");
                        break;
                }

                // Check error : if any server side validation error exist.
                if (_errorMsg != String.Empty)
                {
                    ShowError(lblErrMsg, _errorMsg);
                    return;
                }
                else
                {
                    // If the selected file extension is valid then 
                    // convert the selected file into bytes for saving in database 
                    Stream imageStream = imgCtrl.PostedFile.InputStream;
                    int contentLength = imgCtrl.PostedFile.ContentLength;

                    using (BinaryReader reader = new BinaryReader(imageStream))
                    {
                        fileByteData = reader.ReadBytes(contentLength);
                        reader.Close();
                    }

                    // Read the file bytes in MemoryStream for checking it's dimention as desired. 
                    MemoryStream memStream = new MemoryStream(fileByteData);
                    System.Drawing.Image imgObject = System.Drawing.Image.FromStream(memStream);
                    Int32 imgHeight = imgObject.Height;
                    Int32 imgWidth = imgObject.Width;

                    if (imgHeight != 50 && imgWidth != 50)
                    {
                        ShowError(lblErrMsg, "Uploaded image size is greater or less than the defined size.");
                        return;
                    }
                }
            }

            // Save data to Database
            SaveData(fileByteData, extension);
        }
        catch (Exception ex)
        {
            ShowError(lblErrMsg, ex.Message);
            return;
        }

        //Response.Redirect("DefaultCategory.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("DefaultCategory.aspx");
    }
 
}

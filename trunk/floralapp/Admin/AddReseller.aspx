﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMasterNoLeftPanel.master" AutoEventWireup="true"
    CodeFile="AddReseller.aspx.cs" Inherits="Admin_AddReseller" Title="floralapp&reg; | Add Shop"
    Theme="GraySkin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="../UserControls/HelpMsg.ascx" TagName="HelpMsg" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <script language="javascript" type="text/javascript">
        function DateTimeFormat(sender, args) {
            var d = sender._selectedDate;
            var now = new Date();
            sender.get_element().value = d.format("MM/dd/yyyy")
        }

        function DisabledControl(obj) {
            var servicefee = document.getElementById("<%=txtServiceFee.ClientID %>");
            var deliveryfee = document.getElementById("<%=txtDeliveryFee.ClientID %>");

            servicefee.value = "0";
            deliveryfee.value = "0";
            servicefee.disabled = obj.checked ? true : false;
            deliveryfee.disabled = obj.checked ? true : false;
        }
        function showViralIncentive() {
            var cbxIsViralMarketing = document.getElementById("<%=chkIsViralMktOn.ClientID %>");
            var viralIncentiveMesageControls = document.getElementById("<%=viralIncentiveMesageControls.ClientID %>");
            var viralIncentiveMesageLabel = document.getElementById("<%=viralIncentiveMesageLabel.ClientID %>");
            var customEmailTextControls = document.getElementById("<%=divCustomEmailTextControls.ClientID %>");
            var customEmailTextLabel = document.getElementById("<%=divCustomEmailTextLabel.ClientID %>");

            if (cbxIsViralMarketing.checked) {
                viralIncentiveMesageControls.style.display = 'block';
                viralIncentiveMesageLabel.style.display = 'block';

                customEmailTextControls.style.display = 'block';
                customEmailTextLabel.style.display = 'block';
            }
            else {
                viralIncentiveMesageControls.style.display = 'none';
                viralIncentiveMesageLabel.style.display = 'none';

                customEmailTextControls.style.display = 'none';
                customEmailTextLabel.style.display = 'none';
            }

        }

        function textCounter(field, countfield, maxlimit) {
            if (field.value.length > maxlimit)
                field.value = field.value.substring(0, maxlimit);
            else
                countfield.value = maxlimit - field.value.length;
        }
    </script>
    <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
        <tr>
            <td valign="top" align="left">
                <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
                    <tr>
                        <th class="TableHeadingBg" colspan="2">
                            <asp:Label ID="lblPageTitle" runat="server" Text="Shop Information"></asp:Label>
                        </th>
                    </tr>
                    <tr>
                        <td class="TableBorder" colspan="2">
                            <uc1:HelpMsg ID="HelpMsg1" runat="server" />
                            &nbsp;<asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="False" CssClass="error"></asp:Label>
                            <asp:ValidationSummary ID="vsReseller" runat="server" CssClass="reqd" DisplayMode="BulletList"
                                HeaderText="Please fill out all the required fields:" ShowSummary="true" />
                            <ajax:TabContainer ID="tcContainer" runat="server" ActiveTabIndex="0" Width="100%">
                                <ajax:TabPanel ID="tpReseller" CssClass="TableHeadingBg" TabIndex="0" runat="server"
                                    HeaderText="Shop">
                                    <HeaderTemplate>
                                        Shop
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                            <tr>
                                                <td class="col showhh">
                                                    Shop<span class="reqd">*</span>
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtResellerName" runat="server" SkinID="textbox" class="FormItem"
                                                        MaxLength="300"></asp:TextBox><br />
                                                    <asp:RequiredFieldValidator ID="rfvResellerName" runat="server" ControlToValidate="txtResellerName"
                                                        ErrorMessage="Enter shop name." Display="Dynamic"></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="col showhh">
                                                    Florist Code<span class="reqd">*</span>
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtResellerCode" runat="server" SkinID="textbox" class="FormItem"
                                                        MaxLength="14" Width="100px" Enabled="false"></asp:TextBox><br />
                                                    <asp:RequiredFieldValidator ID="rfvResellerCode" runat="server" ControlToValidate="txtResellerCode"
                                                        ErrorMessage="Enter florist code." Display="Dynamic"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Shop Logo<span class="reqd">*</span>
                                                </td>
                                                <td class="val">
                                                    <asp:FileUpload ID="fuResellerLogo" runat="server" Width="185px"></asp:FileUpload>
                                                    <div class="desc">
                                                        <br />
                                                        Supported image format: *.png.
                                                        <%--<br />
                                                        Suggested image size (ht x wt) is 100 x 320 pixel(s).--%>
                                                        <br />
                                                        Max. image size(ht x wt) should not exceed 100 x 320 pixel(s).
                                                        <br />
                                                    </div>
                                                    <div>
                                                        <asp:Image ID="imgResellerLogo" runat="server"></asp:Image>
                                                    </div>
                                                </td>
                                                <td class="col showhh">
                                                    Splash Image With Logo<span class="reqd">*</span>
                                                </td>
                                                <td class="val">
                                                    <asp:FileUpload ID="fuSplashImage" runat="server" Width="185px"></asp:FileUpload>
                                                    <div class="desc">
                                                        <br />
                                                        Supported image format: *.png.
                                                        <%-- <br />
                                                        Suggested image size (ht x wt) is 480 x 320 pixel(s).--%>
                                                        <br />
                                                        Max. image size(ht x wt) should not exceed 480 x 320 pixel(s).
                                                        <br />
                                                    </div>
                                                    <div>
                                                        <asp:Image ID="imgSplashImage" runat="server" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col showhh">
                                                    Address 1<span class="reqd">*</span>
                                                </td>
                                                <td class="val">
                                                    <mo:ExtTextBox ID="txtAddress1" runat="server" TextMode="MultiLine" MaxLength="500"
                                                        SkinID="textarea" Height="40px"></mo:ExtTextBox><br />
                                                    Max. 500 character.<br />
                                                    <asp:RequiredFieldValidator ID="rfvAddress1" runat="server" ControlToValidate="txtAddress1"
                                                        ErrorMessage="Enter your address." Display="Dynamic"></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="col showhh">
                                                    Address 2
                                                </td>
                                                <td class="val">
                                                    <mo:ExtTextBox ID="txtAddress2" runat="server" TextMode="MultiLine" MaxLength="500"
                                                        SkinID="textarea" Height="40px"></mo:ExtTextBox><br />
                                                    Max. 500 character.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col showhh">
                                                    City<span class="reqd">*</span>
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtCity" runat="server" class="FormItem" MaxLength="200"></asp:TextBox>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="rftxtCity" runat="server" ControlToValidate="txtCity"
                                                        ErrorMessage="Enter city name." Display="Dynamic"></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="col showhh">
                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Label ID="lblZip" runat="server"></asp:Label><span class="reqd">*</span>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtZip" runat="server" SkinID="textbox" class="FormItem" MaxLength="7"
                                                        Width="122px"></asp:TextBox><br />
                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                        <ContentTemplate>
                                                            <asp:RequiredFieldValidator ID="rfvZip" runat="server" ControlToValidate="txtZip"
                                                                ErrorMessage="Enter post code." Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col showhh">
                                                    Country<span class="reqd">*</span>
                                                </td>
                                                <td class="val">
                                                    <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" AppendDataBoundItems="True"
                                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                                        <asp:ListItem Value="0" Text="Select One " Selected="True"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="rfvddlCountry" runat="server" ControlToValidate="ddlCountry"
                                                        ErrorMessage="Select a country." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="col showhh">
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Label ID="lblState" runat="server"></asp:Label><span class="reqd">*</span>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td class="val">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlState" runat="server" AppendDataBoundItems="True">
                                                                <asp:ListItem Value="0" Text="Select One " Selected="True"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlCountry" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="rfvddlState" runat="server" ControlToValidate="ddlState"
                                                        ErrorMessage="Select a state." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col showhh">
                                                    Phone<span class="reqd">*</span>
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtPhone1" runat="server" SkinID="textbox" class="FormItem" MaxLength="12"
                                                        Width="122px"></asp:TextBox>
                                                    <ajax:MaskedEditExtender runat="server" ID="mEPhone1" TargetControlID="txtPhone1"
                                                        Mask="999-999-9999" MaskType="Number" AcceptNegative="Right" ErrorTooltipEnabled="True"
                                                        ClearMaskOnLostFocus="False" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                        CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                        CultureTimePlaceholder="" Enabled="True">
                                                    </ajax:MaskedEditExtender>
                                                </td>
                                                <td class="col showhh">
                                                    Toll Free Number<span class="reqd">*</span>
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtPhone2" runat="server" SkinID="textbox" class="FormItem" MaxLength="12"
                                                        Width="122px"></asp:TextBox>
                                                    <ajax:MaskedEditExtender runat="server" ID="mEPhone2" TargetControlID="txtPhone2"
                                                        Mask="999-999-9999" MaskType="Number" AcceptNegative="Right" ErrorTooltipEnabled="True"
                                                        ClearMaskOnLostFocus="False" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                        CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                        CultureTimePlaceholder="" Enabled="True">
                                                    </ajax:MaskedEditExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Cell
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtCell" runat="server" SkinID="textbox" class="FormItem" MaxLength="11"
                                                        Width="122px"></asp:TextBox>
                                                    <ajax:MaskedEditExtender runat="server" ID="meCell" TargetControlID="txtCell" Mask="999-999-9999"
                                                        MaskType="Number" AcceptNegative="Right" ErrorTooltipEnabled="True" ClearMaskOnLostFocus="False"
                                                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                        CultureTimePlaceholder="" Enabled="True">
                                                    </ajax:MaskedEditExtender>
                                                </td>
                                                <td class="col showhh">
                                                    Fax
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtFax" runat="server" SkinID="textbox" class="FormItem" MaxLength="15"
                                                        Width="122px"></asp:TextBox>
                                                    <ajax:MaskedEditExtender runat="server" ID="meFax" TargetControlID="txtFax" Mask="999-999-9999"
                                                        MaskType="Number" AcceptNegative="Right" ErrorTooltipEnabled="True" ClearMaskOnLostFocus="False"
                                                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                        CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                        CultureTimePlaceholder="" Enabled="True">
                                                    </ajax:MaskedEditExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col showhh">
                                                    Email<span class="reqd">*</span>
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtEmail1" runat="server" SkinID="textbox" class="FormItem" MaxLength="50"></asp:TextBox>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail1"
                                                        ErrorMessage="Enter email ID." Display="Dynamic"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revEmail1" runat="server" ErrorMessage="Please enter a valid Email ID."
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,3}([-.]\w+)*" ControlToValidate="txtEmail1"></asp:RegularExpressionValidator>
                                                </td>
                                                <td class="col">
                                                    Alternate Email
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtEmail2" runat="server" SkinID="textbox" class="FormItem" MaxLength="50"></asp:TextBox>
                                                    <br />
                                                    <asp:RegularExpressionValidator ID="revEmail2" runat="server" ErrorMessage="Please enter a valid alternate Email ID."
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,3}([-.]\w+)*" ControlToValidate="txtEmail2"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Latitude
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtLat" runat="server" SkinID="textbox" class="FormItem" MaxLength="50"
                                                        Width="122px"></asp:TextBox>
                                                    <br />
                                                    <asp:RegularExpressionValidator ID="revLat" runat="server" ControlToValidate="txtLat"
                                                        ValidationExpression="^\d{0,15}($|\.\d{0,15}$)" ErrorMessage="Enter a valid latitude."
                                                        Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                                </td>
                                                <td class="col">
                                                    Longitude
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtLng" runat="server" SkinID="textbox" class="FormItem" MaxLength="50"
                                                        Width="122px"></asp:TextBox>
                                                    <br />
                                                    <asp:RegularExpressionValidator ID="revLng" runat="server" ControlToValidate="txtLng"
                                                        ValidationExpression="^\d{0,15}($|\.\d{0,15}$)" ErrorMessage="Enter a valid longitude."
                                                        Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Website Url
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtWebsiteUrl" runat="server" SkinID="textbox" class="FormItem"
                                                        MaxLength="50"></asp:TextBox>
                                                </td>
                                                <td class="col">
                                                </td>
                                                <td class="val">
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </ajax:TabPanel>
                                <ajax:TabPanel ID="tpResellerSettings" TabIndex="1" CssClass="TableHeadingBg" runat="server"
                                    HeaderText="Shop Settings">
                                    <ContentTemplate>
                                        <asp:Label ID="lblError" runat="server" Text="Error" Visible="False" CssClass="error"></asp:Label>
                                        <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                            <tr>
                                                <td class="col showhh">
                                                    Color Theme<span class="reqd">*</span>
                                                </td>
                                                <td class="val">
                                                    <asp:DropDownList ID="ddlColorTheme" runat="server" AppendDataBoundItems="True">
                                                        <asp:ListItem Value="0" Text="Select One " Selected="True"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="rfvColorTheme" runat="server" ControlToValidate="ddlColorTheme"
                                                        ErrorMessage="Select color theme." Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                                </td>
                                                <td class="col">
                                                    Is All Fee Inclusive
                                                </td>
                                                <td class="val">
                                                    <asp:CheckBox ID="chkIsAllFeeInclusive" runat="server" SkinID="checkbox" class="FormItem"
                                                        OnClick="javascript:return DisabledControl(this);" />
                                                </td>
                                                <td class="col">
                                                    Receive new member emails
                                                </td>
                                                <td class="val">
                                                    <asp:CheckBox ID="cbxReceiveUserSignInEmail" runat="server" SkinID="checkbox" class="FormItem" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Is Sales Tax On
                                                </td>
                                                <td class="val">
                                                    <asp:CheckBox ID="chkIsSaleTaxOn" runat="server" SkinID="checkbox" class="FormItem" />
                                                </td>
                                                <td class="col showhh">
                                                    Service Fee
                                                </td>
                                                <td class="val" colspan="3">
                                                    &#36;
                                                    <asp:TextBox ID="txtServiceFee" runat="server" SkinID="textbox" class="FormItem"
                                                        MaxLength="8" Width="40px"></asp:TextBox>
                                                    <br />
                                                    <asp:RegularExpressionValidator ID="rfvServiceFee" runat="server" ControlToValidate="txtServiceFee"
                                                        ValidationExpression="^\d{0,8}($|\.\d{0,2}$)" ErrorMessage="Enter a valid Service fee."
                                                        Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col showhh">
                                                    Sales Tax (%)
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtSaleTax" runat="server" SkinID="textbox" class="FormItem" MaxLength="8"
                                                        Width="40px"></asp:TextBox>
                                                    <br />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDeliveryFee"
                                                        ValidationExpression="^\d{0,8}($|\.\d{0,2}$)" ErrorMessage="Enter a valid sales tax."
                                                        Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                                </td>
                                                <td class="col showhh">
                                                    Delivery Fee
                                                </td>
                                                <td class="val" colspan="3">
                                                    &#36;
                                                    <asp:TextBox ID="txtDeliveryFee" runat="server" SkinID="textbox" class="FormItem"
                                                        MaxLength="8" Width="40px"></asp:TextBox>
                                                    <asp:CheckBox ID="chkDeliveryFeeTax" runat="server" SkinID="checkbox" class="FormItem"
                                                        Text="Would you like to tax delivery?" />
                                                    <br />
                                                    <asp:RegularExpressionValidator ID="rfvDeliveryFee" runat="server" ControlToValidate="txtDeliveryFee"
                                                        ValidationExpression="^\d{0,8}($|\.\d{0,2}$)" ErrorMessage="Enter a valid delivery fee."
                                                        Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    Support Phone<span class="reqd">*</span>
                                                </td>
                                                <td class="val" id="tdSpan" runat="server">
                                                    <asp:TextBox ID="txtSupportPhone" runat="server" SkinID="textbox" class="FormItem"
                                                        MaxLength="100" Width="100px"></asp:TextBox>
                                                    <ajax:MaskedEditExtender runat="server" ID="meSupportPhone" TargetControlID="txtSupportPhone"
                                                        Mask="999-999-9999" MessageValidatorTip="true" UserDateFormat="None" UserTimeFormat="None"
                                                        MaskType="Number" InputDirection="LeftToRight" AcceptNegative="Right" DisplayMoney="None"
                                                        ErrorTooltipEnabled="True" ClearMaskOnLostFocus="false">
                                                    </ajax:MaskedEditExtender>
                                                </td>
                                                <td class="col" id="tdVmCol" runat="server">
                                                    <span>Is Viral Marketing On</span>
                                                </td>
                                                <td class="val" id="tdVmVal" runat="server" colspan="3">
                                                    <span>
                                                        <asp:CheckBox ID="chkIsViralMktOn" runat="server" SkinID="checkbox" class="FormItem"
                                                            OnCheckedChanged="chkIsViralMktOn_CheckedChanged" AutoPostBack="true" />
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col showhh">
                                                    <div id="viralIncentiveMesageLabel" runat="server">
                                                        <div id="viralIncentiveMesageLabelC" runat="server">
                                                            <asp:Label ID="lblViralIncentiveMessage" runat="server" Text="Viral Incentive Message"></asp:Label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="val">
                                                    <div id="viralIncentiveMesageControls" runat="server">
                                                        <div id="viralIncentiveMesageControlsC" runat="server">
                                                            <asp:TextBox ID="txtViralIncentiveMessage" runat="server" TextMode="MultiLine" Rows="4"
                                                                onKeyDown="textCounter(this,this.form.txtViralIncentiveMessageCounter,250)" onKeyUp="textCounter(this,this.form.txtViralIncentiveMessageCounter,250)"> </asp:TextBox><br />
                                                            You have
                                                            <input type="text" value="250" size="3" name="countdown" readonly="" id="txtViralIncentiveMessageCounter"
                                                                style="width: 20px;">
                                                            characters left.<br />
                                                            <span class="checkbox">
                                                                <asp:CheckBox ID="cbxShowViralIncentiveMessage" runat="server" Text="Show Viral Incentive Message" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td id="tdZipCode" class="col">
                                                    <asp:Label ID="lblZipCodes" runat="server" Text="Label"></asp:Label><br />
                                                    <br />
                                                    <br />
                                                    Delivery Zip Code Limit:
                                                    <br />
                                                    <asp:TextBox ID="txtZipCodeLimit" runat="server" MaxLength="4" Text="1000" Width="30px"
                                                        Enabled="false"> </asp:TextBox>
                                                    <ajax:MaskedEditExtender runat="server" ID="MaskedEditExtender1" TargetControlID="txtZipCodeLimit"
                                                        Mask="9999" MessageValidatorTip="true" UserDateFormat="None" UserTimeFormat="None"
                                                        MaskType="Number" InputDirection="LeftToRight" AcceptNegative="None" DisplayMoney="None"
                                                        ErrorTooltipEnabled="True" ClearMaskOnLostFocus="false" PromptCharacter="0">
                                                    </ajax:MaskedEditExtender>
                                                </td>
                                                <td class="val" colspan="3">
                                                    <mo:ExtTextBox ID="txtDeliveryZipCode" runat="server" TextMode="MultiLine" MaxLength="1000"
                                                        SkinID="textarea"></mo:ExtTextBox><br />
                                                    Max.
                                                    <asp:Label ID="lblZipCodeDisplayMsg" runat="server" Text="1000"></asp:Label>
                                                    characters. Use only zip codes or post codes, separated by a comma. NO SPACES
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col showhh">
                                                    <div id="divCustomEmailTextLabel" runat="server">
                                                        <asp:Label ID="Label4" runat="server" Text="Custom Email Text"></asp:Label>
                                                    </div>
                                                </td>
                                                <td class="val">
                                                    <div id="divCustomEmailTextControls" runat="server">
                                                        <asp:TextBox ID="txtCustomEmail" runat="server" TextMode="MultiLine" Rows="4" onKeyDown="textCounter(this,this.form.txtCustomEmailCounter,100)"
                                                            onKeyUp="textCounter(this,this.form.txtCustomEmailCounter,100)"> </asp:TextBox><br />
                                                        You have
                                                        <input type="text" value="100" size="3" name="countdown" readonly="" id="txtCustomEmailCounter"
                                                            style="width: 20px;">
                                                        characters left.<br />
                                                        <span class="checkbox">
                                                            <asp:CheckBox ID="cbEnableCustomEmail" runat="server" Text="Enable Custom Email Text" />
                                                        </span>
                                                    </div>
                                                </td>
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col showhh">
                                                    Pick-up Message
                                                </td>
                                                <td class="val">
                                                    <mo:ExtTextBox ID="txtPickUpMsg" runat="server" TextMode="MultiLine" MaxLength="500"
                                                        SkinID="textarea"></mo:ExtTextBox><br />
                                                    Max. 500 characters.
                                                </td>
                                                <td class="col showhh">
                                                    Profile Message
                                                </td>
                                                <td class="val" colspan="3">
                                                    <mo:ExtTextBox ID="etbProfileMessage" runat="server" TextMode="MultiLine" MaxLength="250"
                                                        SkinID="textarea"></mo:ExtTextBox><br />
                                                    Max. 250 characters.<br />
                                                    <asp:CheckBox ID="chkShowProfMsg" runat="server" SkinID="checkbox" class="FormItem"
                                                        Text="Show Profile Message" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col showhh">
                                                    Florist To Florist Message
                                                </td>
                                                <td class="val">
                                                    <mo:ExtTextBox ID="txtFloristToFloristMsg" runat="server" TextMode="MultiLine" MaxLength="500"
                                                        SkinID="textarea"></mo:ExtTextBox><br />
                                                    Max. 500 characters.
                                                </td>
                                                <td class="col showhh">
                                                    Direct Ship Message
                                                </td>
                                                <td class="val" colspan="3">
                                                    <mo:ExtTextBox ID="txtDirectShipMsg" runat="server" TextMode="MultiLine" MaxLength="500"
                                                        SkinID="textarea"></mo:ExtTextBox><br />
                                                    Max. 500 characters.
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </ajax:TabPanel>
                                <ajax:TabPanel ID="tpResellerMerchantSettings" TabIndex="2" CssClass="TableHeadingBg"
                                    runat="server" HeaderText="Shop Merchant Settings">
                                    <ContentTemplate>
                                        <asp:Label ID="Label1" runat="server" Text="Error" Visible="False" CssClass="error"></asp:Label>
                                        <div style="text-align: right; color: #21618C; margin-bottom: 10px;">
                                            Need Help? <a target="_blank" href="http://www.beneva.com/highres/paypal-floralapp.wmv">
                                                Click Here</a></div>
                                        <%--<table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                            <tr>
                                                <th colspan="2" class="val">
                                                    <label style="color: #FF8800; font-weight: bold;">
                                                        Payment Type
                                                    </label>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td class="col" style="width: 200px">
                                                    Payment Type<span class="reqd">*</span>
                                                </td>
                                                <td class="val">
                                                    <asp:DropDownList runat="server" ID="ddlPaymentType" AutoPostBack="true" OnSelectedIndexChanged="ddlPaymentType_SelectedIndexChanged">
                                                        <asp:ListItem Selected="True" Value="1" Text="PayPal"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Mercury"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <br />--%>
                                        <asp:Panel runat="server" ID="pnlMercury" Visible="false">
                                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                                <tr>
                                                    <th colspan="2" class="val">
                                                        <label style="color: #FF8800; font-weight: bold;">
                                                            Mercury Payment Settings
                                                        </label>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td class="col" style="width: 200px">
                                                        Mercury Email Address<span class="reqd">*</span>
                                                    </td>
                                                    <td class="val">
                                                        <asp:TextBox ID="txtMercuryEmail" runat="server" SkinID="textbox" class="FormItem"
                                                            MaxLength="250" Width="250px"></asp:TextBox>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMercuryEmail"
                                                            ErrorMessage='Please provide an email address for Mercury Email Address.' Display="Dynamic"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Please enter a valid Mercury Email Address."
                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w{2,3}([-.]\w+)*" ControlToValidate="txtMercuryEmail"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col" style="width: 200px">
                                                        FTD Encryption Key<span class="reqd">*</span>
                                                        <br />
                                                        <span class="reqd" style="font-weight: normal">Must Use "DES" Encryption</span>
                                                    </td>
                                                    <td class="val">
                                                        <asp:TextBox ID="txtFTDEncryptionKey" runat="server" SkinID="textbox" class="FormItem"
                                                            MaxLength="8" Width="250px"></asp:TextBox>
                                                        Needs to be exactly 8 characters and/or digits.
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFTDEncryptionKey"
                                                            ErrorMessage='Please provide an encryption key.' Display="Dynamic"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Please provide an encryption key of exactly 8 characters and/or digits."
                                                            ValidationExpression="^[a-zA-Z0-9]{8}$" ControlToValidate="txtFTDEncryptionKey"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="pnlPayPall">
                                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                                <tr>
                                                    <th colspan="2" class="val">
                                                        <label style="color: #FF8800; font-weight: bold;">
                                                            To Accept ONLY PayPal payments:
                                                        </label>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td class="col" style="width: 200px">
                                                        PayPal Merchant Login ID<span runat="server" id="spnMerchantLogIDReq" class="reqd">*</span>
                                                    </td>
                                                    <td class="val">
                                                        <asp:TextBox ID="txtMerchantLogID" runat="server" SkinID="textbox" class="FormItem"
                                                            MaxLength="250" Width="250px"></asp:TextBox>
                                                        This is your ID for your account where you RECEIVE funds.
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="rfvMerchantLoginID" runat="server" ControlToValidate="txtMerchantLogID"
                                                            ErrorMessage='Please provide the missing PayPal information from the "Shop Merchant Settings" Tab (Login ID).'
                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <br />
                                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                                <tr>
                                                    <th colspan="2" class="val">
                                                        <label style="color: #FF8800; font-weight: bold;">
                                                            To Accept PayPal AND Credit Cards:
                                                        </label>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td class="col" style="width: 200px">
                                                        Exp. Check-out Login ID<span runat="server" id="spnExpCheckoutLogIDReq" class="reqd">*</span>
                                                    </td>
                                                    <td class="val">
                                                        <asp:TextBox ID="txtExpCheckoutLogID" runat="server" SkinID="textbox" class="FormItem"
                                                            MaxLength="250" Width="400px"></asp:TextBox>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="rfvExpCheckoutLogID" runat="server" ControlToValidate="txtExpCheckoutLogID"
                                                            ErrorMessage='Please provide the missing PayPal information from the "Shop Merchant Settings" Tab (Exp. checkout login ID).'
                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col">
                                                        Merchant Trans Key<span runat="server" id="spnMerchantTransKeyReq" class="reqd">*</span>
                                                    </td>
                                                    <td class="val">
                                                        <asp:TextBox ID="txtMerchantTransKey" runat="server" SkinID="textbox" class="FormItem"
                                                            MaxLength="250" Width="400px"></asp:TextBox>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="rfvMerchantTransKey" runat="server" ControlToValidate="txtMerchantTransKey"
                                                            ErrorMessage='Please provide the missing PayPal information from the "Shop Merchant Settings" Tab (Merchant Key).'
                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col">
                                                        Signature<span runat="server" id="spnSignatureReq" class="reqd">*</span>
                                                    </td>
                                                    <td class="val">
                                                        <asp:TextBox ID="txtSignature" runat="server" SkinID="textbox" class="FormItem" MaxLength="250"
                                                            Width="400px"></asp:TextBox>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="rfvSignature" runat="server" ControlToValidate="txtSignature"
                                                            ErrorMessage='Please provide the missing PayPal information from the "Shop Merchant Settings" Tab (Signature).'
                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <br />
                                            <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                                <tr>
                                                    <th colspan="2" class="val">
                                                        <label style="color: #FF8800; font-weight: bold;">
                                                            Use Teleflora
                                                            <asp:CheckBox runat="server" ID="cbUseTeleflora" AutoPostBack="true" OnCheckedChanged="cbUseTeleflora_CheckChanged" />
                                                        </label>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td class="col" style="width: 200px">
                                                        Teleflora ID<span runat="server" id="spnTfTelefloraIDReq" class="reqd">*</span>
                                                    </td>
                                                    <td class="val">
                                                        <asp:TextBox ID="txtTfTelefloraID" runat="server" SkinID="textbox" class="FormItem"
                                                            MaxLength="100" Width="400px"></asp:TextBox>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="rfvTfTelefloraID" runat="server" ControlToValidate="txtTfTelefloraID"
                                                            ErrorMessage='Please provide the missing Teleflora information from the "Shop Merchant Settings" Tab (Teleflora ID).'
                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col">
                                                        Merchant Account ID
                                                    </td>
                                                    <td class="val">
                                                        <asp:TextBox ID="txtTfMerchantAccountID" runat="server" SkinID="textbox" class="FormItem"
                                                            MaxLength="100" Width="400px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col">
                                                        Terminal ID
                                                    </td>
                                                    <td class="val">
                                                        <asp:TextBox ID="txtTfTerminalID" runat="server" SkinID="textbox" class="FormItem"
                                                            MaxLength="100" Width="400px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </ajax:TabPanel>
                                <ajax:TabPanel ID="tpAppDistribution" TabIndex="3" CssClass="TableHeadingBg" runat="server"
                                    HeaderText="App. Distribution" Visible="false">
                                    <ContentTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="Error" Visible="False" CssClass="error"></asp:Label>
                                        <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                            <tr>
                                                <td class="col">
                                                    Android
                                                </td>
                                                <td class="val">
                                                    <asp:HyperLink ID="lnkDownloadAndroidApp" runat="server"></asp:HyperLink>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col">
                                                    iPhone
                                                </td>
                                                <td class="val">
                                                    <asp:HyperLink ID="lnkDownloadiPhoneApp" runat="server"></asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </ajax:TabPanel>
                                <ajax:TabPanel ID="tpSettingsBySuperAdmin" TabIndex="4" CssClass="TableHeadingBg"
                                    runat="server" HeaderText="System Settings">
                                    <ContentTemplate>
                                        <asp:Label ID="Label3" runat="server" Text="Error" Visible="False" CssClass="error"></asp:Label>
                                        <table border="0" cellspacing="0" cellpadding="0" class="detailTable">
                                            <tr>
                                                <td class="col" style="white-space: nowrap;">
                                                    Supported Device(s)
                                                </td>
                                                <td class="val">
                                                    <asp:CheckBox ID="chkiPhone" runat="server" Text="iPhone" SkinID="checkbox" class="FormItem"
                                                        Enabled="false" />
                                                    <asp:CheckBox ID="chkAndroid" runat="server" Text="Android" SkinID="checkbox" class="FormItem"
                                                        Enabled="false" />
                                                </td>
                                                <td class="col" style="white-space: nowrap;">
                                                    Active Product Limit
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtActiveProductLimit" runat="server" SkinID="textbox" class="FormItem"
                                                        MaxLength="4" Width="60px" Text="1000" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col" style="white-space: nowrap;">
                                                    Category Enabled
                                                </td>
                                                <td class="val" colspan="3">
                                                    <span style="font-weight: bold;">Min.</span><asp:TextBox ID="txtMinCategoryLimit"
                                                        runat="server" SkinID="textbox" class="FormItem" MaxLength="4" Width="20px" Text="8"
                                                        Enabled="false"></asp:TextBox>
                                                    &nbsp;&nbsp;<span style="font-weight: bold;">Max.</span><asp:TextBox ID="txtMaxCategoryLimit"
                                                        runat="server" SkinID="textbox" Text="10" class="FormItem" MaxLength="4" Width="20px"
                                                        Enabled="false"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtMinCategoryLimit"
                                                        ValidationExpression="^[0-9]*$" ErrorMessage="Enter a valid minimum category limit."
                                                        Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtMaxCategoryLimit"
                                                        ValidationExpression="^[0-9]*$" ErrorMessage="Enter a valid maximum category limit."
                                                        Display="Dynamic" InitialValue="0"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col" style="white-space: nowrap;">
                                                    Products Per Category
                                                </td>
                                                <td class="val">
                                                    <asp:TextBox ID="txtCategMaxLimit" runat="server" SkinID="textbox" class="FormItem"
                                                        MaxLength="4" Width="60px" Text="20" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col" style="white-space: nowrap;">
                                                    Shop Time Zone
                                                </td>
                                                <td class="val" colspan="3">
                                                    <asp:DropDownList runat="server" ID="ddlShopTimeZone" Width="100px">
                                                        <asp:ListItem Value="1" Text="Eastern Time"></asp:ListItem>
                                                        <asp:ListItem Value="0" Text="Central Time"></asp:ListItem>
                                                        <asp:ListItem Value="-1" Text="Mountain Time"></asp:ListItem>
                                                        <asp:ListItem Value="-2" Text="Pacific Time"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col" style="white-space: nowrap;">
                                                    RSS Feed Url
                                                </td>
                                                <td class="val" colspan="3">
                                                    <asp:TextBox ID="txtRssFeed" runat="server" SkinID="textbox" class="FormItem" MaxLength="100"
                                                        Width="600px" Text="" Enabled="false"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtRssFeed"
                                                        ErrorMessage="Enter valid Url for RSS Feed." ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"
                                                        Display="Dynamic" InitialValue=""></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col" style="white-space: nowrap;">
                                                    Media Url
                                                </td>
                                                <td class="val" colspan="3">
                                                    <asp:TextBox ID="txtMediaUrl" runat="server" SkinID="textbox" class="FormItem" MaxLength="100"
                                                        Width="600px" Text=""></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtMediaUrl"
                                                        ErrorMessage="Enter valid Url for Media." ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"
                                                        Display="Dynamic" InitialValue=""></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </ajax:TabPanel>
                            </ajax:TabContainer>
                            <div class="actionDiv">
                                <asp:Button ID="btnSave" runat="server" Text="  Save  " CssClass="btn" ToolTip="Click here to save this shop."
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click"
                                    CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

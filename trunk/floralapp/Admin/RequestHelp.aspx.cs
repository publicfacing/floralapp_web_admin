﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Configuration;
using System.Net.Mail;
using System.IO;

public partial class Admin_RequestHelp : AppPage
{
    AppVars v = new AppVars();

    protected void Page_Load(object sender, EventArgs e)
    {
        HideError(lblErrMsg);
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        try
        {
            String strMailMessage = String.Empty;
            strMailMessage = ReadTemplate("RequestHelp.htm");
            strMailMessage = strMailMessage.Replace("{name}", txtName.Text);
            strMailMessage = strMailMessage.Replace("{shopname}", txtShopName.Text);
            strMailMessage = strMailMessage.Replace("{phone}", txtPhone.Text);
            strMailMessage = strMailMessage.Replace("{email}", txtEmail.Text);
            strMailMessage = strMailMessage.Replace("{url}", txtUrl.Text);
            strMailMessage = strMailMessage.Replace("{description}", txtDescription.Text);

            MailMessage objMailMsg = new MailMessage();
            SmtpClient SmtpMail = new SmtpClient();
            SmtpMail.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
            //SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;

            MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["FromEmail"].ToLower());

            objMailMsg.To.Add(ReadEmails());

            objMailMsg.From = fromAddress;
            objMailMsg.Subject = "New Support Ticket";
            objMailMsg.IsBodyHtml = true;
            objMailMsg.Body = strMailMessage;
            SmtpMail.Host = ConfigurationManager.AppSettings["MailServer"].ToLower();
            SmtpMail.Send(objMailMsg);

            ShowError(lblErrMsg, "Your request was sent successfully.");
        }
        catch (Exception ex)
        {
            Data.ErrorLog el = new Data.ErrorLog();
            el.SmallMessage.value = "Request Email Sending Error";
            el.Message.value = ex.Message;
            el.ModuleInfo.value = "floralapp Request Help : Email.Send()";
            el.ApplicationType.value = 11;
            el.DateAdded.value = Util.GetServerDate();
            el.Save();
            ShowError(lblErrMsg, "Error sending request!");
        }
    }

    private String ReadTemplate(String fileName)
    {
        String path = HttpContext.Current.Server.MapPath(@"../EmailTemplate\");

        String cmdAllPath = path + fileName;
        StreamReader sr = new StreamReader(cmdAllPath);
        String emailTemplate = sr.ReadToEnd();
        sr.Close();
        sr.Dispose();

        return emailTemplate;
    }

    private String ReadEmails()
    {
        try
        {
            String path = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EmailSettingsFile"]);

            StreamReader sr = new StreamReader(path);
            String emailTemplate = sr.ReadToEnd();
            sr.Close();
            sr.Dispose();

            return emailTemplate;
        }
        catch (Exception ex)
        {
            return "";
        }
    }
}
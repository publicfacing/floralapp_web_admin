﻿using System;
using System.Data;
using Library.AppSettings;
using Library.Utility;
using System.Collections.Generic;

public partial class Admin_ReorderProducts : AppPage 
{
    AppVars v = new AppVars();

    private DataTable ProductTable
    {
        get
        {
            if (ViewState["dtProducts"] == null)
                ViewState["dtProducts"] = GetProductData();

            return (DataTable)ViewState["dtProducts"];
        }
        set { ViewState["dtProducts"] = value; }
    }

    private string[] ProductOrders
    {
        get
        {
            if (ViewState["itemsOrders"] == null)
                ViewState["itemsOrders"] = GetProductOrders();

            return (string[])ViewState["itemsOrders"];
        }
        set { ViewState["itemsOrders"] = value; }
    }

    public override void Page_Load(object sender, EventArgs e)
    {
        //rlProducts.ItemDataBound += new EventHandler<AjaxControlToolkit.ReorderListItemEventArgs>(rlProducts_ItemDataBound);

        base.HideError(lblErrMsg);

        if (!IsPostBack)
        {
            BindReorderList();
        }
    }

    private DataTable GetProductData()
    {
        Data.Product  prod = new Data.Product();
        DataSet dsProducts = prod.List();
        return (Util.IsValidDataSet(dsProducts)) ? Util.GetDataTable(dsProducts) : null;
    }

    //protected void btnPreview_Click(object sender, EventArgs e)
    //{
    //    String redirectUrl = "ManageMagazinePrint.aspx";

    //    try
    //    {
    //        ImageButton objButton = (ImageButton)sender;
    //        String ProductID = objButton.CommandArgument.ToString();
    //        String qs = Crypto.EncryptQueryString("ProductID=" + ProductID);
    //        redirectUrl += "?eqs=" + qs;
    //    }
    //    catch (Exception ex)
    //    {
    //        ShowError(lblErrMsg, ex.Message);
    //        return;
    //    }

    //    Response.Redirect(redirectUrl);
    //}

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Products.aspx");
    }

    private void BindReorderList()
    {
        if (Util.IsValidDataTable(ProductTable))
        {
            rlProduct.DataSource = ProductTable;
            rlProduct.DataBind();
        }
    }

    private void SaveNewOrderList()
    {
        Data.Product prod = new Data.Product();
        DataTable dt = ProductTable;

        for (int i = 0; i < ProductOrders.Length; i++)
        {
            Int64 ProductID = Int64.Parse(ProductOrders[i].ToString());
            prod.LoadID(ProductID);
            prod.Save();
        }

        ProductTable = null;
    }

    private string[] GetProductOrders()
    {
        if (!Util.IsValidDataTable(ProductTable))
            return null;

        DataTable dt = ProductTable;

        string idList = "";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            String id = Util.GetDataString(dt, i, "ProductID");
            idList += (String.IsNullOrEmpty(idList)) ? id : "," + id;
        }

        string[] ret = idList.Split(',');
        //Array.Reverse(ret);

        return ret;
    }

    protected void rlProduct_ItemReorder(object sender, AjaxControlToolkit.ReorderListItemReorderEventArgs e)
    {
        string[] tempArray = ProductOrders;

        List<string> list = new List<string>(tempArray);//using a list for the reordering (convienience)
        string itemToMove = list[e.OldIndex];
        list.Remove(itemToMove);
        list.Insert(e.NewIndex, itemToMove);
        ProductOrders = list.ToArray();

        SaveNewOrderList();
        BindReorderList();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("Products.aspx");
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true"
    CodeFile="ReorderFaq.aspx.cs" Inherits="Admin_ReorderFaq" Title="floralapp&reg; | Reorder FAQ"
    Theme="GraySkin" %>

<%@ Register Namespace="MetaOption.Web.UI.WebControls" TagPrefix="mo" %> 

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPageContainer" runat="Server">
    <asp:UpdatePanel ID="upNewPayment" runat="server">
        <ContentTemplate>
            <table border="0" width="100%" cellpadding="0" stle="border-collapse: collapse">
                <tr>
                    <td valign="top" align="left">
                        <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse"
                            class="arial-12">
                            <tr>
                                <th class="TableHeadingBg TableHeading" >
                                    Reorder FAQ
                                </th>
                            </tr>
                            <tr>
                                <td class="TableBorder"  >
                                    <asp:Label ID="lblErrMsg" runat="server" Text="Error" Visible="false"></asp:Label>
                                     
                                    <table style="width:100%;border:0px;border-collapse: collapse" cellpadding="0" class="grid">
                                        <tr class="gridHeader">
                                            <th style="width:16px;">&nbsp;</th>
                                            <th style="padding-left:8px;width:240px;">Question</th> 
                                            <th style="padding-left:8px;" >Answer</th>
                                        </tr>
                                    </table> 
                                    <mo:ExtReorderList runat="server" ID="rlFAQ" PostBackOnReorder="true" CssClass="reorderList"
                                        CallbackCssStyle="callbackStyle" DragHandleAlignment="Left" ItemInsertLocation="End"
                                        DataKeyField="FaqID" SortOrderField="Question" OnItemReorder="rlFAQ_ItemReorder">
                                        <ItemTemplate>
                                            <div class="itemArea">
                                                <table style="width:100%;border:0px;border-collapse: collapse" cellpadding="0">
                                                    <tr>
                                                        <td style="width:250px;"><%# HttpUtility.HtmlEncode(Convert.ToString(Eval("Question")))%></td> 
                                                        <td style="padding-left:8px;" ><%# HttpUtility.HtmlEncode(Convert.ToString(Eval("Answer")))%></td>
                                                    </tr>
                                                </table> 
                                            </div>
                                        </ItemTemplate> 
                                        <ReorderTemplate>
                                            <asp:Panel ID="Panel2" runat="server" CssClass="reorderCue" > 
                                                <%# HttpUtility.HtmlEncode(Convert.ToString(Eval("Question")))%>
                                            </asp:Panel> 
                                        </ReorderTemplate>
                                        <DragHandleTemplate>
                                            <div class="dragHandle"> 
                                            </div>
                                        </DragHandleTemplate> 
                                    </mo:ExtReorderList>  
                                </td>
                            </tr> 
                        </table>
                        <div class="actionDiv"> 
                            <asp:Button ID="btnBack" runat="server" Text="Back" ToolTip="Click here to go back to category root list." CssClass="btn" OnClick="btnBack_Click" />
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

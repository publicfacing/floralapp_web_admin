﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using Library.Utility;
using Library.AppSettings;
using System.Net;
using System.IO;
using System.Text;

public partial class TestGravity : System.Web.UI.Page
{
    public int ResellerID { get; set; }
    public long UserID { get; set; }
    public int CategoryID { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Unnamed1_Click(object sender, EventArgs e)
    {
        try
        {
            ResellerID = 308;
            UserID = 111;
            CategoryID = 3682;
            //XmlTextReader reader = new XmlTextReader("http://www.peoplesflowers.com/floralAppFeed.cfm");
            XmlReader reader = new XmlTextReader(Server.MapPath("~/test_cfm.xml"));

            DataSet ds = new DataSet();
            ds.ReadXml(reader);

            StringBuilder failed = new StringBuilder();
            int k = 0;
            int nrImported = 0;
            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow dr in ds.Tables["item"].Select("item_group_id=5177"))
                {
                    k++;
                    string tmp = SaveProduct(dr);
                    if (tmp.Length > 0)
                    {
                        failed.Append(tmp);
                        failed.Append("</br>");
                    }
                    else
                    {
                        nrImported++;
                    }
                    if (k == 20)
                        break;
                }
            }
            string message = string.Format("{0} products out of {1} were imported successfully.", nrImported, ds.Tables["item"].Rows.Count);
            if (failed.Length > 0)
            {
                //lblImportMsg.Text = message + "</br>Some of the products were not imported:</br>" + failed.ToString();
            }
            else
            {
                //lblImportMsg.Text = message;
            }

        }
        catch (Exception ex)
        {
            //base.ShowError(lblErrMsg, ex.Message);
            return;
        }
    }

    private string SaveProduct(DataRow dr)
    {
        StringBuilder retVal = new StringBuilder();
        try
        {
            string sourceID = "";
            try
            {
                sourceID = dr["id"].ToString();
                retVal.Append("id=" + sourceID);
            }
            catch { }
            try
            {
                retVal.Append("  sku=" + dr["sku"].ToString());
            }
            catch { }
            try
            {
                retVal.Append("  title=" + Server.HtmlDecode(dr["title"].ToString()));
            }
            catch { }

            if (retVal.Length == 0)
                return "Unable to identify Product.";

            byte[] ProductFileByteData = null;
            byte[] ThumbFileByteData = null;
            string productSKU = dr["sku"].ToString().Split('|')[0];
            string variationSKU = dr["sku"].ToString().Split('|')[1];
            if (variationSKU.Trim().Length == 0)
                variationSKU = productSKU;
            Data.Product product = new Data.Product();
            DataSet dsProd = product.Product_GetByResellerAndSourceId(ResellerID, "", productSKU);
            Int64 productID = 0;
            try
            {
                productID = Int64.Parse(dsProd.Tables[0].Rows[0]["ProductID"].ToString());
            }
            catch { }
            if (productID > 0)
            {
                // this is a duplicate product
                return "Duplicate product SKUNo=" + dr["sku"].ToString() + "  - Not Imported!";
            }
            string productSourceId = sourceID.Split('-')[0];
            dsProd = product.Product_GetByResellerAndSourceId(ResellerID, productSourceId, productSKU);
            try
            {
                productID = Int64.Parse(dsProd.Tables[0].Rows[0]["ProductID"].ToString());
            }
            catch { }

            BuildPhoto(dr["image_link"].ToString(), ref ProductFileByteData, ref ThumbFileByteData);

            if (productID > 0)
                product.LoadID(productID);
            product.ParentProductID.value = 0;
            product.ResellerID.value = ResellerID;
            product.SourceId.value = productSourceId;
            product.ProductSource.value = 1;
            product.ProductName.value = Server.HtmlDecode(dr["title"].ToString().Split('-')[0]);
            product.Color.value = "";
            product.ShowColorInDevice.value = false;
            product.Description.value = Server.HtmlDecode(dr["description"].ToString());

            if (ProductFileByteData != null && ThumbFileByteData != null)
            {
                product.ProductImage.value = ProductFileByteData;
                product.ProductImageExt.value = ".png";
                product.ProductThumb.value = ThumbFileByteData;
                product.ProductThumbExt.value = ".png";
            }

            product.ProductPrice.value = Convert.ToDecimal(dr["price"].ToString().ToLower().Replace("usd", ""));
            product.IsUpchargeActive.value = false;

            product.IsFloristToFlorist.value = false;
            product.IsDirectShip.value = false;
            if (product.ProductID.value <= 0)
                product.IsPublished.value = false;

            if (productID <= 0)
            {
                product.DateAdded.value = Util.GetServerDate();
                product.AddedBy.value = UserID;
            }
            product.DateModified.value = Util.GetServerDate();
            product.ModifiedBy.value = UserID;

            if (product.ProductID.value <= 0)
                product.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
            product.SKUNo.value = productSKU;
            product.Save();

            #region Updating Images...

            String filePath = Server.MapPath(@"~/DynImages\Product\");
            String fileProductImagePath = String.Format("ProductImage_{0}{1}", product.ProductID.value.ToString(), product.ProductImageExt.value);
            String fileProductThumbPath = String.Format("ProductThumb_{0}{1}", product.ProductID.value.ToString(), product.ProductThumbExt.value);

            if (ProductFileByteData != null)
            {
                if (File.Exists(filePath + fileProductImagePath))
                    File.Delete(filePath + fileProductImagePath);

                FileStream fs = new FileStream(filePath + fileProductImagePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(ProductFileByteData, 0, ProductFileByteData.Length);
                fs.Close();
            }

            filePath += fileProductThumbPath;

            if (ThumbFileByteData != null)
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);

                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(ThumbFileByteData, 0, ThumbFileByteData.Length);
                fs.Close();
            }

            #endregion Eng of Binding images..

            // Delete from ProductCategory by ProductID
            Data.ProductCategory pcat = new Data.ProductCategory();
            pcat.DeleteByProductAndCategory(productID, CategoryID);

            // Save selected Categories [ProductCategory] table.
            pcat = new Data.ProductCategory();
            pcat.ProductID.value = product.ProductID.value;
            pcat.CategoryID.value = CategoryID;
            pcat.DateAdded.value = Util.GetServerDate();
            pcat.AddedBy.value = UserID;
            pcat.DateModified.value = Util.GetServerDate();
            pcat.ModifiedBy.value = UserID;
            pcat.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
            pcat.Save();

            #region Log in ServerUpdates
            //Save in ServerUpdates
            Data.ServerUpdates su = new Data.ServerUpdates();
            su.UpdateStatus(product.ResellerID.value, "product");
            #endregion

            #region Product Variation

            Data.Product variation = new Data.Product();
            Int64 variationID = 0;
            string variationSourceId = sourceID.Split('-')[1];
            dsProd = variation.Variation_GetByResellerAndSourceId(ResellerID, variationSourceId, variationSKU);
            try
            {
                variationID = Int64.Parse(dsProd.Tables[0].Rows[0]["ProductID"].ToString());
            }
            catch { }

            if (variationID > 0)
                variation.LoadID(variationID);
            variation.ParentProductID.value = (int)product.ProductID.value;
            variation.ResellerID.value = ResellerID;
            variation.SourceId.value = variationSourceId;
            variation.ProductSource.value = 1;
            variation.ProductName.value = Server.HtmlDecode(dr["title"].ToString().Split('-')[1]);
            variation.Color.value = "";
            variation.ShowColorInDevice.value = false;
            variation.Description.value = Server.HtmlDecode(dr["description"].ToString());

            if (ProductFileByteData != null && ThumbFileByteData != null)
            {
                variation.ProductImage.value = ProductFileByteData;
                variation.ProductImageExt.value = ".png";
                variation.ProductThumb.value = ThumbFileByteData;
                variation.ProductThumbExt.value = ".png";
            }

            variation.ProductPrice.value = Convert.ToDecimal(dr["price"].ToString().ToLower().Replace("usd", ""));
            variation.IsUpchargeActive.value = false;

            variation.IsFloristToFlorist.value = false;
            variation.IsDirectShip.value = false;
            if (variation.ProductID.value <= 0)
                variation.IsPublished.value = false;

            if (variationID <= 0)
            {
                variation.DateAdded.value = Util.GetServerDate();
                variation.AddedBy.value = UserID;
            }
            variation.DateModified.value = Util.GetServerDate();
            variation.ModifiedBy.value = UserID;

            if (variation.ProductID.value <= 0)
                variation.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
            variation.SKUNo.value = variationSKU;
            variation.Save();

            #region Updating Images...

            filePath = Server.MapPath(@"~/DynImages\Product\");
            fileProductImagePath = String.Format("ProductImage_{0}{1}", variation.ProductID.value.ToString(), variation.ProductImageExt.value);
            fileProductThumbPath = String.Format("ProductThumb_{0}{1}", variation.ProductID.value.ToString(), variation.ProductThumbExt.value);

            if (ProductFileByteData != null)
            {
                if (File.Exists(filePath + fileProductImagePath))
                    File.Delete(filePath + fileProductImagePath);

                FileStream fs = new FileStream(filePath + fileProductImagePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(ProductFileByteData, 0, ProductFileByteData.Length);
                fs.Close();
            }

            filePath += fileProductThumbPath;

            if (ThumbFileByteData != null)
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);

                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(ThumbFileByteData, 0, ThumbFileByteData.Length);
                fs.Close();
            }

            #endregion Eng of Binding images..

            #endregion

            return "";
        }
        catch (Exception ex)
        {
            retVal.Append(" not Imported! - Image Link problem.");
            Data.ErrorLog el = new Data.ErrorLog();
            el.ModuleInfo.value = "Gravity Import Product: SaveProduct()";
            el.Message.value = "Message=" + ex.Message + " Stack=" + ex.StackTrace;
            el.SmallMessage.value = "Error importing product.";
            el.ApplicationType.value = Convert.ToInt32(AppEnum.ApplicationType.WEBSITE);
            el.Save();
        }
        return retVal.ToString();
    }

    private void BuildPhoto(string link, ref byte[] fileImageResizedByteData, ref byte[] fileThumbByteData)
    {
        byte[] ProductFileByteData = null;
        WebClient webClient = new WebClient();
        ProductFileByteData = webClient.DownloadData(link);

        MemoryStream memProductStream = new MemoryStream(ProductFileByteData);
        System.Drawing.Image imgProductObject = System.Drawing.Image.FromStream(memProductStream);

        // Prevent using images internal thumbnail
        imgProductObject.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
        imgProductObject.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

        Int32 imgProductHeight = imgProductObject.Height;
        Int32 imgProductWidth = imgProductObject.Width;

        #region Resize Image
        const int MAX_WIDTH = 200;
        const int MAX_HEIGHT = 200;

        int NewHeight = imgProductHeight * MAX_WIDTH / imgProductWidth;
        int NewWidth = MAX_WIDTH;

        if (NewHeight > MAX_HEIGHT)
        {
            // Resize with height instead
            NewWidth = imgProductWidth * MAX_HEIGHT / imgProductHeight;
            NewHeight = MAX_HEIGHT;
        }

        System.Drawing.Image imgResized = imgProductObject.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);

        MemoryStream mstreamResizedImage = new MemoryStream();

        imgResized.Save(mstreamResizedImage, System.Drawing.Imaging.ImageFormat.Png);
        fileImageResizedByteData = new Byte[mstreamResizedImage.Length];
        mstreamResizedImage.Position = 0;
        mstreamResizedImage.Read(fileImageResizedByteData, 0, (int)mstreamResizedImage.Length);
        #endregion

        #region Create Thumb image...

        const int MAX_THUMB_WIDTH = 50;
        const int MAX_THUMB_HEIGHT = 50;

        int newThumbHeight = imgProductHeight * MAX_THUMB_WIDTH / imgProductWidth;
        int newThumbWidth = MAX_THUMB_HEIGHT;

        if (newThumbHeight > MAX_THUMB_HEIGHT)
        {
            // Resize with height instead
            newThumbWidth = imgProductWidth * MAX_THUMB_HEIGHT / imgProductHeight;
            newThumbHeight = MAX_THUMB_HEIGHT;
        }

        System.Drawing.Image imgThumbs = imgProductObject.GetThumbnailImage(newThumbWidth, newThumbHeight, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);
        MemoryStream mstreamThumb = new MemoryStream();

        imgThumbs.Save(mstreamThumb, System.Drawing.Imaging.ImageFormat.Png);
        fileThumbByteData = new Byte[mstreamThumb.Length];
        mstreamThumb.Position = 0;
        mstreamThumb.Read(fileThumbByteData, 0, (int)mstreamThumb.Length);
        #endregion End of byte
    }

    private bool ThumbnailCallback()
    {
        return true;
    }

    private Decimal Product_GetNewPrice(Decimal currentPrice, string strPercent)
    {
        Decimal newPrice = 0;
        Decimal percent = Util.GetDataDecimal(strPercent);
        newPrice = currentPrice * (percent / 100);
        newPrice += currentPrice;
        return Util.GetDataDecimal(newPrice.ToString("N2"));
    }
}
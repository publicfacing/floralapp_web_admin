﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.AppSettings;
using Library.Utility;
using System.Text;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblResult.Visible = false;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        lblResult.Visible = true;
        Data.DeviceSetting deviceSetting = new Data.DeviceSetting();
        DataSet dsGetToken = new DataSet();
        dsGetToken = deviceSetting.GetListByReseller(101);

        String deviceToken = String.Empty;
        String deviceID = String.Empty;
        Int32 deviceTypeID = 0;
        String notificationText = String.Empty;
        


        foreach (DataRow rowToken in dsGetToken.Tables[0].Rows)
        {
            deviceTypeID = Util.GetDataInt32(rowToken["DeviceTypeID"].ToString());
            deviceToken = Util.GetDataString(rowToken["DeviceToken"].ToString());
            deviceID = Util.GetDataString(rowToken["DeviceID"].ToString());
            lblResult.Text += deviceID + rowToken["DeviceID"].ToString()+ "<br>";
            lblResult.Text += deviceToken + "<br>";
            if (deviceID.Trim() == tbxDeviceID.Text.Trim() && deviceTypeID == 1)// GABEs "352212040196482" 
            {
                Data.SpecialOffer so = new Data.SpecialOffer();
                notificationText = "Test Special";

                String sourceCertificateFile = ConfigurationManager.AppSettings["iPhoneCertificateFile"].ToString();
                String certFilePath = Server.MapPath(sourceCertificateFile);

                so.SendPushNotification_iPhone(deviceToken, notificationText, certFilePath,1);

                
                lblResult.Text += String.Format("Sent notification to {0} with token {2} the notification text {1}", deviceID, notificationText, deviceToken);
            }
        }
        //if (lblResult.Text.Length < 10) lblResult.Text = String.Format("The deviceID:{0} is not sign up to Developments shop", deviceID);
    }
}
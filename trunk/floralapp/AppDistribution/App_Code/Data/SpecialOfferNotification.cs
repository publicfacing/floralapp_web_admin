using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for SpecialOfferNotification
	/// </summary>
	public class SpecialOfferNotification : Data.Base.SpecialOfferNotification
	{
	    #region Standard Methods
		public SpecialOfferNotification(){}
		public SpecialOfferNotification(int loadID){LoadID(loadID);}
	    #endregion
	}
}

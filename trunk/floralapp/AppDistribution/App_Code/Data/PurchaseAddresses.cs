using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for PurchaseAddresses
	/// </summary>
	public class PurchaseAddresses : Data.Base.PurchaseAddresses
	{
	    #region Standard Methods
		public PurchaseAddresses(){}
		public PurchaseAddresses(int loadID){LoadID(loadID);}
	    #endregion
	}
}

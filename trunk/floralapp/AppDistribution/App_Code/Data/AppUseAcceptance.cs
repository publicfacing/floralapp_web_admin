using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;
using Library.Utility;

namespace Data
{
	/// <summary>
	/// Summary description for AppUseAcceptance
	/// </summary>
	public class AppUseAcceptance : Data.Base.AppUseAcceptance
	{
	    #region Standard Methods
		public AppUseAcceptance(){}
		public AppUseAcceptance(Int64 loadID){LoadID(loadID);}
	    #endregion

        public Boolean IsAcceptTerms_n_Conditions(Int64 userID)
        {
            Boolean IsAcceptTerms = false;
            
            SqlParameter[] param = new SqlParameter[2];
            
            param[0] = new SqlParameter("@UserID", SqlDbType.BigInt);
            param[0].Value = userID;

            DataSet ds = SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "IsAcceptTerms_n_Conditions", param);
            if (Util.IsValidDataSet(ds))
            {
                IsAcceptTerms = true;
            }
            return IsAcceptTerms;
        }
    }
}

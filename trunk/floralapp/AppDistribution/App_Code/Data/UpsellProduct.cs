using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for UpsellProduct
	/// </summary>
	public class UpsellProduct : Data.Base.UpsellProduct
	{
	    #region Standard Methods
		public UpsellProduct(){}
		public UpsellProduct(Int64 loadID){LoadID(loadID);}
	    #endregion
        
        public DataSet GetList(Int32 pageIndex, Int32 pageSize, String sortExpression, String filterClause, ref Int32 rowCount, ref Int32 totalRowCount)
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@pageIndex", SqlDbType.Int);
            param[0].Value = pageIndex;
            param[1] = new SqlParameter("@pageSize", SqlDbType.Int);
            param[1].Value = pageSize;
            param[2] = new SqlParameter("@sortExpression", SqlDbType.NVarChar, 100);
            param[2].Value = sortExpression;
            param[3] = new SqlParameter("@filterClause", SqlDbType.NVarChar, 4000);
            param[3].Value = filterClause;
            param[4] = new SqlParameter("@rowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, rowCount);
            param[5] = new SqlParameter("@totalRowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, totalRowCount);

            DataSet ds = SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "UpsellProduct_GetList", param);
            rowCount = (Int32)param[4].Value;
            totalRowCount = (Int32)param[5].Value;
            return ds;
        }

        public void InActiveRelationshipTables(Int64 upsellProductID, String strTableName)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ObjectID", SqlDbType.BigInt);
            param[0].Value = upsellProductID;
            param[1] = new SqlParameter("@TableName", SqlDbType.NVarChar, 50);
            param[1].Value = strTableName;

            Int32 count = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "usp_InActiveRelationships", param);
        }
    }
}

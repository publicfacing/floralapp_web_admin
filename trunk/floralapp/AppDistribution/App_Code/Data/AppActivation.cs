using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for AppActivation
	/// </summary>
	public class AppActivation : Data.Base.AppActivation
	{
	    #region Standard Methods
		public AppActivation(){}
		public AppActivation(int loadID){LoadID(loadID);}

        public Int32 Activate(String referralCode, String deviceID, Int32 deviceTypeID)
        {

            Int32 apActivationID = 0;
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@ReferralCode", SqlDbType.NVarChar);
            param[0].Value = referralCode;
            param[1] = new SqlParameter("@DeviceID", SqlDbType.NVarChar);
            param[1].Value = deviceID;
            param[2] = new SqlParameter("@DeviceTypeID", SqlDbType.Int);
            param[2].Value = deviceTypeID;
            param[3] = new SqlParameter("@retAppActivationID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, apActivationID);
            apActivationID = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "ssp_ActivateApp", param);
            apActivationID =  Convert.ToInt32(String.IsNullOrEmpty(param[3].Value.ToString()) ? "0" : param[3].Value.ToString());

            return apActivationID;
        }

	    #endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for ProductCategory
	/// </summary>
	public class ProductCategory : Data.Base.ProductCategory
	{
	    #region Standard Methods
		public ProductCategory(){}
		public ProductCategory(Int64 loadID){LoadID(loadID);}
	    #endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for AppDownloads
	/// </summary>
	public class AppDownloads : Data.Base.AppDownloads
	{
	    #region Standard Methods
		public AppDownloads(){}
		public AppDownloads(int loadID){LoadID(loadID);}
	    #endregion

        public DataSet getAppDownloads4(DateTime dtStartDate, DateTime dtEndDate, string p, long p_4)
        {
            throw new NotImplementedException();
        }

        public DataSet getAppDownloads1(DateTime dtStartDate, DateTime dtEndDate, Int32 resellerID, Int64 userID)
        {
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@StartDate", SqlDbType.DateTime);
            param[0].Value = dtStartDate;

            param[1] = new SqlParameter("@EndDate", SqlDbType.DateTime);
            param[1].Value = dtEndDate;

            param[2] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[2].Value = resellerID;

            param[3] = new SqlParameter("@UserID", SqlDbType.BigInt);
            param[3].Value = userID;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "AppDownloads_GetAppDownloads", param);
        }

        public DataSet getAppDownloads(String searchQueryString)
        {
            SqlParameter[] param = new SqlParameter[1];

            param[0] = new SqlParameter("@SearchQuery", SqlDbType.NVarChar);
            param[0].Value = searchQueryString;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "AppDownloads_GetAppDownloads", param);
        }
    }
}

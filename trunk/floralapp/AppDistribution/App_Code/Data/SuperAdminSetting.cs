using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for SuperAdminSetting
	/// </summary>
	public class SuperAdminSetting : Data.Base.SuperAdminSetting
	{
	    #region Standard Methods
		public SuperAdminSetting(){}
		public SuperAdminSetting(int loadID){LoadID(loadID);}
	    #endregion
	}
}

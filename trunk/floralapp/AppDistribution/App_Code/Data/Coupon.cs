using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for Coupon
	/// </summary>
	public class Coupon : Data.Base.Coupon
	{
	    #region Standard Methods
		public Coupon(){}
		public Coupon(long loadID){LoadID(loadID);}
	    #endregion

        public void EnableCoupon(Int64 CouponID, Int32 ResellerID, Int64 ModifiedBy, Boolean isEnabled, Int32 couponUsagesTypeID)
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@CouponID", SqlDbType.BigInt);
            param[0].Value = CouponID;
            param[1] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[1].Value = ResellerID;
            param[2] = new SqlParameter("@ModifiedBy", SqlDbType.BigInt);
            param[2].Value = ModifiedBy;
            param[3] = new SqlParameter("@IsEnabled", SqlDbType.Bit);
            param[3].Value = isEnabled;
            param[4] = new SqlParameter("@CouponUsaseTypeID", SqlDbType.Int);
            param[4].Value = couponUsagesTypeID;

            Int32 count = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "Coupon_Enable", param);
        }

        public Int32 GetCouponDateStatus(Int32 ResellerID,Int64 CouponID,DateTime dtNewStartDate, DateTime dtNewEndDate)
        {
            Int32 existRows = 0;
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = ResellerID;

            param[1] = new SqlParameter("@CouponID", SqlDbType.BigInt);
            param[1].Value = CouponID;

            param[2] = new SqlParameter("@NewStartDate", SqlDbType.DateTime);
            param[2].Value = dtNewStartDate;

            param[3] = new SqlParameter("@NewEndDate", SqlDbType.DateTime);
            param[3].Value = dtNewEndDate;

            param[4] = new SqlParameter("@Return", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, existRows);

            existRows = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "Coupon_IsValidDateRange", param);
            existRows = Convert.ToInt32(param[4].Value);

            return existRows;
        }

        public DataSet GetDashboardList(Int32 resellerID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            if (resellerID > 0)
                param[0].Value = resellerID;
            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Coupon_GetDashboardList", param);
        }

        public void InActiveRelationshipTables(Int64 couponID, String strTableName)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ObjectID", SqlDbType.BigInt);
            param[0].Value = couponID;
            param[1] = new SqlParameter("@TableName", SqlDbType.NVarChar, 50);
            param[1].Value = strTableName;

            Int32 count = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "usp_InActiveRelationships", param);
        }


        public Int32 GetIsValidCouponCode(Int32 resellerID, String couponCode)
        {
            Int32 existCoupon = 0;
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;
            param[1] = new SqlParameter("@CouponCode", SqlDbType.NVarChar);
            param[1].Value = couponCode;
            param[2] = new SqlParameter("@Return", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, existCoupon);

            existCoupon = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "Coupon_GetIsValidCouponCode", param);
            existCoupon = Convert.ToInt32(param[2].Value);
            return existCoupon;
        }

        public DataSet GetCouponListByReseller(Int32 resellerID)
        {  
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Coupon_GetCouponListByReseller", param);
        }
    }
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for BlockDates
	/// </summary>
	public class BlockDates : Data.Base.BlockDates
	{
	    #region Standard Methods
		public BlockDates(){}
		public BlockDates(int loadID){LoadID(loadID);}
	    #endregion

        public Int32 GetBlockDatesByReseller(Int32 ResellerID, Int32 BlockDateID, DateTime dtNewStartDate, DateTime dtNewEndDate)
        {
            Int32 existRows = 0;
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = ResellerID;

            param[1] = new SqlParameter("@BlockDateID", SqlDbType.Int);
            param[1].Value = BlockDateID;

            param[2] = new SqlParameter("@NewStartDate", SqlDbType.DateTime);
            param[2].Value = dtNewStartDate;

            param[3] = new SqlParameter("@NewEndDate", SqlDbType.DateTime);
            param[3].Value = dtNewEndDate;

            param[4] = new SqlParameter("@Return", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, existRows);

            existRows = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "BlockDates_GetBlockDatesByReseller", param);
            existRows = Convert.ToInt32(param[4].Value);

            return existRows;
        }

        public DataSet IsExistsResellerAndBlockDateType(Int32 resellerID, Int32 type)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;
            param[1] = new SqlParameter("@BlockDateTypeID", SqlDbType.Int);
            param[1].Value = type;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "usp_IsExistsResellerAndBlockDateType", param);
        }
    }
}

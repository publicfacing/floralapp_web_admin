using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;
using Library.Utility;

namespace Data
{
	/// <summary>
	/// Summary description for Users
	/// </summary>
	public class Users : Data.Base.Users
	{
	    #region Standard Methods
		public Users(){}
		public Users(long loadID){LoadID(loadID);}
	    #endregion

        public Int32 GetCount()
        {
            Int32 rowCount = 0;

            DataSet ds = SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "LoginUser_GetCount", null);
            if (Util.IsValidDataSet(ds))
                rowCount = Util.GetDataInt32(Util.GetDataTable(ds), 0, "RowCount");

            return rowCount;
        }

        public Boolean IsValidLogin(String loginID, String password, ref DataTable dtUserInfo)
        {
            Boolean isValidUser = false;

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@loginID", SqlDbType.NVarChar);
            param[0].Value = loginID;
            param[1] = new SqlParameter("@password", SqlDbType.NVarChar);
            param[1].Value = password;
            param[2] = new SqlParameter("@returnCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, _returnCode);

            if (_returnCode == 0)
            {
                DataSet dsUserInfo = SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Users_Login", param);

                if (Util.IsValidDataSet(dsUserInfo))
                {
                    dtUserInfo = Util.GetDataTable(dsUserInfo);
                    isValidUser = true; 
                }
            }

            return isValidUser;
        }

        public Boolean UserExists(String loginID, Int64 UserID)
        {
            Boolean userExists = false;
            Int32 result = -1;
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@LoginID", SqlDbType.NVarChar);
            param[0].Value = loginID;
            param[1] = new SqlParameter("@UserID", SqlDbType.BigInt);
            param[1].Value = UserID;
            param[2] = new SqlParameter("@Exists", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, result);
            result = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "User_Exists", param);
            result = Util.GetDataInt32(param[2].Value.ToString());
            return userExists = result == 0 ? false : true;
        }
        
        public Boolean CanInsertUpdate(String loginID, Int64 UserID)
        { 
            Int32 spReturn = -1;

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@LoginID", SqlDbType.NVarChar);
            param[0].Value = loginID;
            param[1] = new SqlParameter("@UserID", SqlDbType.BigInt);
            param[1].Value = UserID;
            param[2] = new SqlParameter("@CanInsertUpdate", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, spReturn);
            
            Int32 result = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "LoginUser_CanInsertUpdate", param);
            spReturn = Util.GetDataInt32(param[2].Value.ToString());
            return  (spReturn == 0)? false : true;
        }

        public DataSet GetList(Int32 pageIndex, Int32 pageSize, String sortExpression, String filterClause, ref Int32 rowCount, ref Int32 totalRowCount)
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@pageIndex", SqlDbType.Int);
            param[0].Value = pageIndex;
            param[1] = new SqlParameter("@pageSize", SqlDbType.Int);
            param[1].Value = pageSize;
            param[2] = new SqlParameter("@sortExpression", SqlDbType.NVarChar, 100);
            param[2].Value = sortExpression;
            param[3] = new SqlParameter("@filterClause", SqlDbType.NVarChar, 4000);
            param[3].Value = filterClause;
            param[4] = new SqlParameter("@rowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, rowCount);
            param[5] = new SqlParameter("@totalRowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, totalRowCount);

            DataSet ds = SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Users_GetList", param);
            rowCount = (Int32)param[4].Value;
            totalRowCount = (Int32)param[5].Value;
            return ds;
        }

        public void InActiveRelationshipTables(Int64 userID, String strTableName)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ObjectID", SqlDbType.BigInt);
            param[0].Value = userID;
            param[1] = new SqlParameter("@TableName", SqlDbType.NVarChar, 50);
            param[1].Value = strTableName;

            Int32 count = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "usp_InActiveRelationships", param);
        }

        public DataSet getLostPassword(String loginID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@LoginID", SqlDbType.NVarChar);
            param[0].Value = loginID;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "User_LostPassword", param);
        }

        public void InactiveUserByID(Int64 userID, Int32 isActive)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@UserID", SqlDbType.BigInt);
            param[0].Value = userID;
            param[1] = new SqlParameter("@IsActive", SqlDbType.Int);
            param[1].Value = isActive;

            Int32 rows = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "Users_InactiveUserByID", param);
        }
    }
}

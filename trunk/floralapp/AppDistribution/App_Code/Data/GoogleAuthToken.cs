﻿using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;
using Library.Utility;

namespace Data
{
    /// <summary>
    /// Summary description for GoogleAuthToken
    /// </summary>
    public class GoogleAuthToken : Data.Base.GoogleAuthToken
    {
        #region Standard Methods
		public GoogleAuthToken(){}
	    #endregion

        public String GetToken()
        {
            String token = String.Empty;

            DataSet ds = SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "GoogleAuthToken_Get", null);
            if (Util.IsValidDataSet(ds))
                token = Util.GetDataString(Util.GetDataTable(ds), 0, "GoogleLoginAuthToken");

            return token; 
        }

        public void SetToken(String token)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@GoogleLoginAuthToken", SqlDbType.NVarChar);
            param[0].Value = token;

            SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "GoogleAuthToken_Save", param);
        }
    }
}
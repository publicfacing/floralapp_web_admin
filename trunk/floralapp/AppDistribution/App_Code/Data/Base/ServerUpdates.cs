using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for ServerUpdates
	/// </summary>
	public class ServerUpdates : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql ServerUpdatesID = new PKIntSql("ServerUpdatesID",0,SqlDbType.Int );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public DateTimeSql ResellerSettingsUpdatedDate = new DateTimeSql("ResellerSettingsUpdatedDate",SqlDbType.DateTime );
		public DateTimeSql BlockDatesUpdatedDate = new DateTimeSql("BlockDatesUpdatedDate",SqlDbType.DateTime );
		public DateTimeSql CouponUpdatedDate = new DateTimeSql("CouponUpdatedDate",SqlDbType.DateTime );
		public DateTimeSql SpecialOfferUpdatedDate = new DateTimeSql("SpecialOfferUpdatedDate",SqlDbType.DateTime );
		public DateTimeSql ColorThemeDate = new DateTimeSql("ColorThemeDate",SqlDbType.DateTime );
		public DateTimeSql CategoryUpdatedDate = new DateTimeSql("CategoryUpdatedDate",SqlDbType.DateTime );
		public DateTimeSql ProductUpdatedDate = new DateTimeSql("ProductUpdatedDate",SqlDbType.DateTime );
		public DateTimeSql UpsellCategoryUpdatedDate = new DateTimeSql("UpsellCategoryUpdatedDate",SqlDbType.DateTime );
		public DateTimeSql UpsellProductUpdatedDate = new DateTimeSql("UpsellProductUpdatedDate",SqlDbType.DateTime );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "ServerUpdates";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 12;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ServerUpdatesID;
			_fields[1] = ResellerID;
			_fields[2] = ResellerSettingsUpdatedDate;
			_fields[3] = BlockDatesUpdatedDate;
			_fields[4] = CouponUpdatedDate;
			_fields[5] = SpecialOfferUpdatedDate;
			_fields[6] = ColorThemeDate;
			_fields[7] = CategoryUpdatedDate;
			_fields[8] = ProductUpdatedDate;
			_fields[9] = UpsellCategoryUpdatedDate;
			_fields[10] = UpsellProductUpdatedDate;
			_fields[11] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public ServerUpdates()
		{
		}
		public ServerUpdates(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for AppUseAcceptance
	/// </summary>
	public class AppUseAcceptance : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql AppUseAcceptanceID = new PKBigIntSql("AppUseAcceptanceID",0,SqlDbType.BigInt );
		public BigIntSql UserID = new BigIntSql("UserID",0,SqlDbType.BigInt );
		public BitSql AcceptTermsAndConditions = new BitSql("AcceptTermsAndConditions",false,SqlDbType.Bit );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "AppUseAcceptance";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 10;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = AppUseAcceptanceID;
			_fields[1] = UserID;
			_fields[2] = AcceptTermsAndConditions;
			_fields[3] = DateAdded;
			_fields[4] = AddedBy;
			_fields[5] = DateModified;
			_fields[6] = ModifiedBy;
			_fields[7] = DateDeleted;
			_fields[8] = DeletedBy;
			_fields[9] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public AppUseAcceptance()
		{
		}
		public AppUseAcceptance(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

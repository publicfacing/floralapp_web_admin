using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for StringMap
	/// </summary>
	public class StringMap : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql StringMapID = new PKIntSql("StringMapID",0,SqlDbType.Int );
		public IntSql ObjectTypeID = new IntSql("ObjectTypeID",0,SqlDbType.Int );
		public StringSql ObjectValue = new StringSql("ObjectValue",100,"",SqlDbType.NVarChar );
		public StringSql ObjectText = new StringSql("ObjectText",100,"",SqlDbType.NVarChar );
		public IntSql DisplayOrder = new IntSql("DisplayOrder",0,SqlDbType.Int );
		public StringSql DevNote = new StringSql("DevNote",500,"",SqlDbType.NVarChar );
		public IntSql ActiveStatus = new IntSql("ActiveStatus",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "StringMap";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 7;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = StringMapID;
			_fields[1] = ObjectTypeID;
			_fields[2] = ObjectValue;
			_fields[3] = ObjectText;
			_fields[4] = DisplayOrder;
			_fields[5] = DevNote;
			_fields[6] = ActiveStatus;


			Reset();
		}
		
		#region Standard Methods
		public StringMap()
		{
		}
		public StringMap(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

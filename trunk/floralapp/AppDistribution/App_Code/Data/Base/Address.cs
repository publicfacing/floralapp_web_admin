using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for Address
	/// </summary>
	public class Address : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql AddressID = new PKBigIntSql("AddressID",0,SqlDbType.BigInt );
		public IntSql AddressTypeID = new IntSql("AddressTypeID",0,SqlDbType.Int );
		public BigIntSql UserID = new BigIntSql("UserID",0,SqlDbType.BigInt );
		public StringSql FirstName = new StringSql("FirstName",60,"",SqlDbType.NVarChar );
		public StringSql LastName = new StringSql("LastName",60,"",SqlDbType.NVarChar );
		public StringSql Address1 = new StringSql("Address1",50,"",SqlDbType.NVarChar );
		public StringSql Address2 = new StringSql("Address2",50,"",SqlDbType.NVarChar );
		public StringSql City = new StringSql("City",50,"",SqlDbType.NVarChar );
		public StringSql OtherState = new StringSql("OtherState",50,"",SqlDbType.NVarChar );
		public IntSql StateID = new IntSql("StateID",0,SqlDbType.Int );
		public IntSql CountryID = new IntSql("CountryID",0,SqlDbType.Int );
		public StringSql Zip = new StringSql("Zip",50,"",SqlDbType.NVarChar );
		public StringSql Phone1 = new StringSql("Phone1",50,"",SqlDbType.NVarChar );
		public StringSql Phone1Ext = new StringSql("Phone1Ext",50,"",SqlDbType.NVarChar );
		public StringSql Phone2 = new StringSql("Phone2",50,"",SqlDbType.NVarChar );
		public StringSql Phone2Ext = new StringSql("Phone2Ext",50,"",SqlDbType.NVarChar );
		public StringSql Mobile = new StringSql("Mobile",50,"",SqlDbType.NVarChar );
		public StringSql Fax = new StringSql("Fax",50,"",SqlDbType.NVarChar );
		public StringSql Email = new StringSql("Email",50,"",SqlDbType.NVarChar );
		public StringSql AltEmail = new StringSql("AltEmail",50,"",SqlDbType.NVarChar );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "Address";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 27;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = AddressID;
			_fields[1] = AddressTypeID;
			_fields[2] = UserID;
			_fields[3] = FirstName;
			_fields[4] = LastName;
			_fields[5] = Address1;
			_fields[6] = Address2;
			_fields[7] = City;
			_fields[8] = OtherState;
			_fields[9] = StateID;
			_fields[10] = CountryID;
			_fields[11] = Zip;
			_fields[12] = Phone1;
			_fields[13] = Phone1Ext;
			_fields[14] = Phone2;
			_fields[15] = Phone2Ext;
			_fields[16] = Mobile;
			_fields[17] = Fax;
			_fields[18] = Email;
			_fields[19] = AltEmail;
			_fields[20] = DateAdded;
			_fields[21] = AddedBy;
			_fields[22] = DateModified;
			_fields[23] = ModifiedBy;
			_fields[24] = DateDeleted;
			_fields[25] = DeletedBy;
			_fields[26] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public Address()
		{
		}
		public Address(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

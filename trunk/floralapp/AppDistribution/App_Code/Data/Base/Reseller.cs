using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for Reseller
	/// </summary>
	public class Reseller : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql ResellerID = new PKIntSql("ResellerID",0,SqlDbType.Int );
		public StringSql ResellerName = new StringSql("ResellerName",150,"",SqlDbType.NVarChar );
		public StringSql ResellerCode = new StringSql("ResellerCode",14,"",SqlDbType.NVarChar );
		public BinarySql ResellerLogo = new BinarySql("ResellerLogo",SqlDbType.VarBinary );
		public BinarySql SplashImage = new BinarySql("SplashImage",SqlDbType.VarBinary );
		public StringSql Address1 = new StringSql("Address1",250,"",SqlDbType.NVarChar );
		public StringSql Address2 = new StringSql("Address2",250,"",SqlDbType.NVarChar );
		public StringSql City = new StringSql("City",100,"",SqlDbType.NVarChar );
		public StringSql State = new StringSql("State",100,"",SqlDbType.NVarChar );
		public IntSql CountryID = new IntSql("CountryID",0,SqlDbType.Int );
		public StringSql Zip = new StringSql("Zip",50,"",SqlDbType.NVarChar );
		public StringSql Phone1 = new StringSql("Phone1",20,"",SqlDbType.NVarChar );
		public StringSql Phone2 = new StringSql("Phone2",20,"",SqlDbType.NVarChar );
		public StringSql Cell = new StringSql("Cell",20,"",SqlDbType.NVarChar );
		public StringSql Fax = new StringSql("Fax",50,"",SqlDbType.NVarChar );
		public StringSql Email = new StringSql("Email",100,"",SqlDbType.NVarChar );
		public StringSql Email2 = new StringSql("Email2",100,"",SqlDbType.NVarChar );
		public FloatSql Lat = new FloatSql("Lat",0,SqlDbType.Float );
		public FloatSql Lng = new FloatSql("Lng",0,SqlDbType.Float );
		public StringSql WebsiteURL = new StringSql("WebsiteURL",350,"",SqlDbType.NVarChar );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "Reseller";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 27;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ResellerID;
			_fields[1] = ResellerName;
			_fields[2] = ResellerCode;
			_fields[3] = ResellerLogo;
			_fields[4] = SplashImage;
			_fields[5] = Address1;
			_fields[6] = Address2;
			_fields[7] = City;
			_fields[8] = State;
			_fields[9] = CountryID;
			_fields[10] = Zip;
			_fields[11] = Phone1;
			_fields[12] = Phone2;
			_fields[13] = Cell;
			_fields[14] = Fax;
			_fields[15] = Email;
			_fields[16] = Email2;
			_fields[17] = Lat;
			_fields[18] = Lng;
			_fields[19] = WebsiteURL;
			_fields[20] = DateAdded;
			_fields[21] = AddedBy;
			_fields[22] = DateModified;
			_fields[23] = ModifiedBy;
			_fields[24] = DateDeleted;
			_fields[25] = DeletedBy;
			_fields[26] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public Reseller()
		{
		}
		public Reseller(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

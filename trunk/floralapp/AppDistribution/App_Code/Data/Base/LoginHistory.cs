using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for LoginHistory
	/// </summary>
	public class LoginHistory : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql LoginHistoryID = new PKBigIntSql("LoginHistoryID",0,SqlDbType.BigInt );
		public BigIntSql UserID = new BigIntSql("UserID",0,SqlDbType.BigInt );
		public DateTimeSql LastLogin = new DateTimeSql("LastLogin",SqlDbType.DateTime );
		public StringSql IPAddress = new StringSql("IPAddress",50,"",SqlDbType.NVarChar );
		public BitSql IsSuccess = new BitSql("IsSuccess",false,SqlDbType.Bit );
		public StringSql LoginID = new StringSql("LoginID",50,"",SqlDbType.NVarChar );
		public StringSql Password = new StringSql("Password",50,"",SqlDbType.NVarChar );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "LoginHistory";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 8;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = LoginHistoryID;
			_fields[1] = UserID;
			_fields[2] = LastLogin;
			_fields[3] = IPAddress;
			_fields[4] = IsSuccess;
			_fields[5] = LoginID;
			_fields[6] = Password;
			_fields[7] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public LoginHistory()
		{
		}
		public LoginHistory(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

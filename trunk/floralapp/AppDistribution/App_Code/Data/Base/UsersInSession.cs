using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for UsersInSession
	/// </summary>
	public class UsersInSession : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql ID = new PKBigIntSql("ID",0,SqlDbType.BigInt );
		public StringSql DeviceID = new StringSql("DeviceID",50,"",SqlDbType.NVarChar );
		public IntSql DeviceTypeID = new IntSql("DeviceTypeID",0,SqlDbType.Int );
		public BigIntSql UserID = new BigIntSql("UserID",0,SqlDbType.BigInt );
		public DateTimeSql LastUpdateTime = new DateTimeSql("LastUpdateTime",SqlDbType.DateTime );
		public DateTimeSql SessionStartTime = new DateTimeSql("SessionStartTime",SqlDbType.DateTime );
		public DateTimeSql SessionEndTime = new DateTimeSql("SessionEndTime",SqlDbType.DateTime );
		public BitSql IsSessionEnded = new BitSql("IsSessionEnded",false,SqlDbType.Bit );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "UsersInSession";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 9;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ID;
			_fields[1] = DeviceID;
			_fields[2] = DeviceTypeID;
			_fields[3] = UserID;
			_fields[4] = LastUpdateTime;
			_fields[5] = SessionStartTime;
			_fields[6] = SessionEndTime;
			_fields[7] = IsSessionEnded;
			_fields[8] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public UsersInSession()
		{
		}
		public UsersInSession(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

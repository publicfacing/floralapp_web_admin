using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for EmailLog
	/// </summary>
	public class EmailLog : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql EmailLogID = new PKBigIntSql("EmailLogID",0,SqlDbType.BigInt );
		public IntSql ObjectTypeID = new IntSql("ObjectTypeID",0,SqlDbType.Int );
		public BigIntSql ObjectID = new BigIntSql("ObjectID",0,SqlDbType.BigInt );
		public StringSql EmailTo = new StringSql("EmailTo",150,"",SqlDbType.NVarChar );
		public StringSql EmailCC = new StringSql("EmailCC",150,"",SqlDbType.NVarChar );
		public StringSql EmailBCC = new StringSql("EmailBCC",150,"",SqlDbType.NVarChar );
		public StringSql Subject = new StringSql("Subject",250,"",SqlDbType.NVarChar );
		public StringSql EmailMsg = new StringSql("EmailMsg",-1,"",SqlDbType.NVarChar );
		public IntSql MailMsgFormatID = new IntSql("MailMsgFormatID",0,SqlDbType.Int );
		public BinarySql Attachment = new BinarySql("Attachment",SqlDbType.VarBinary );
		public DateTimeSql SentDate = new DateTimeSql("SentDate",SqlDbType.DateTime );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "EmailLog";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 12;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = EmailLogID;
			_fields[1] = ObjectTypeID;
			_fields[2] = ObjectID;
			_fields[3] = EmailTo;
			_fields[4] = EmailCC;
			_fields[5] = EmailBCC;
			_fields[6] = Subject;
			_fields[7] = EmailMsg;
			_fields[8] = MailMsgFormatID;
			_fields[9] = Attachment;
			_fields[10] = SentDate;
			_fields[11] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public EmailLog()
		{
		}
		public EmailLog(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

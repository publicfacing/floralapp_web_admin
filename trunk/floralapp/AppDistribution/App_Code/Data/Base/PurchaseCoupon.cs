using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for PurchaseCoupon
	/// </summary>
	public class PurchaseCoupon : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql PurchaseCouponID = new PKBigIntSql("PurchaseCouponID",0,SqlDbType.BigInt );
		public BigIntSql PurchaseID = new BigIntSql("PurchaseID",0,SqlDbType.BigInt );
		public BigIntSql CouponID = new BigIntSql("CouponID",0,SqlDbType.BigInt );
		public StringSql CouponCode = new StringSql("CouponCode",50,"",SqlDbType.NVarChar );
		public DecimalSql DiscountAmount = new DecimalSql("DiscountAmount",8,2,0,SqlDbType.Decimal );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "PurchaseCoupon";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 12;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = PurchaseCouponID;
			_fields[1] = PurchaseID;
			_fields[2] = CouponID;
			_fields[3] = CouponCode;
			_fields[4] = DiscountAmount;
			_fields[5] = DateAdded;
			_fields[6] = AddedBy;
			_fields[7] = DateModified;
			_fields[8] = ModifiedBy;
			_fields[9] = DateDeleted;
			_fields[10] = DeletedBy;
			_fields[11] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public PurchaseCoupon()
		{
		}
		public PurchaseCoupon(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for ProductUpcharge
	/// </summary>
	public class ProductUpcharge : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql ProductUpchargeID = new PKBigIntSql("ProductUpchargeID",0,SqlDbType.BigInt );
		public BigIntSql ProductID = new BigIntSql("ProductID",0,SqlDbType.BigInt );
		public FloatSql UpchargePercentage = new FloatSql("UpchargePercentage",0,SqlDbType.Float );
		public DecimalSql ProductPrice = new DecimalSql("ProductPrice",8,2,0,SqlDbType.Decimal );
		public DecimalSql NewProductPrice = new DecimalSql("NewProductPrice",8,2,0,SqlDbType.Decimal );
		public StringSql Description = new StringSql("Description",4000,"",SqlDbType.NVarChar );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "ProductUpcharge";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 13;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ProductUpchargeID;
			_fields[1] = ProductID;
			_fields[2] = UpchargePercentage;
			_fields[3] = ProductPrice;
			_fields[4] = NewProductPrice;
			_fields[5] = Description;
			_fields[6] = DateAdded;
			_fields[7] = AddedBy;
			_fields[8] = DateModified;
			_fields[9] = ModifiedBy;
			_fields[10] = DateDeleted;
			_fields[11] = DeletedBy;
			_fields[12] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public ProductUpcharge()
		{
		}
		public ProductUpcharge(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for State
	/// </summary>
	public class State : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql StateID = new PKIntSql("StateID",0,SqlDbType.Int );
		public IntSql CountryID = new IntSql("CountryID",0,SqlDbType.Int );
		public StringSql StateCode = new StringSql("StateCode",50,"",SqlDbType.NVarChar );
		public StringSql StateName = new StringSql("StateName",50,"",SqlDbType.NVarChar );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "State";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 5;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = StateID;
			_fields[1] = CountryID;
			_fields[2] = StateCode;
			_fields[3] = StateName;
			_fields[4] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public State()
		{
		}
		public State(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

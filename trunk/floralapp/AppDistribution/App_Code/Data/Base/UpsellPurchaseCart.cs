using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for UpsellPurchaseCart
	/// </summary>
	public class UpsellPurchaseCart : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql UpsellPurchaseCartID = new PKBigIntSql("UpsellPurchaseCartID",0,SqlDbType.BigInt );
		public BigIntSql PurchaseID = new BigIntSql("PurchaseID",0,SqlDbType.BigInt );
		public BigIntSql UpsellProductID = new BigIntSql("UpsellProductID",0,SqlDbType.BigInt );
		public DecimalSql ProductPrice = new DecimalSql("ProductPrice",8,2,0,SqlDbType.Decimal );
		public IntSql Quantity = new IntSql("Quantity",0,SqlDbType.Int );
		public DecimalSql TotalPrice = new DecimalSql("TotalPrice",8,2,0,SqlDbType.Decimal );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "UpsellPurchaseCart";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 13;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = UpsellPurchaseCartID;
			_fields[1] = PurchaseID;
			_fields[2] = UpsellProductID;
			_fields[3] = ProductPrice;
			_fields[4] = Quantity;
			_fields[5] = TotalPrice;
			_fields[6] = DateAdded;
			_fields[7] = AddedBy;
			_fields[8] = DateModified;
			_fields[9] = ModifiedBy;
			_fields[10] = DateDeleted;
			_fields[11] = DeletedBy;
			_fields[12] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public UpsellPurchaseCart()
		{
		}
		public UpsellPurchaseCart(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for Country
	/// </summary>
	public class Country : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql CountryID = new PKIntSql("CountryID",0,SqlDbType.Int );
		public StringSql CountryCode = new StringSql("CountryCode",50,"",SqlDbType.NVarChar );
		public StringSql CountryName = new StringSql("CountryName",50,"",SqlDbType.NVarChar );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );
		public DateTimeSql LastEditDate = new DateTimeSql("LastEditDate",SqlDbType.DateTime );
		public DateTimeSql CreationDate = new DateTimeSql("CreationDate",SqlDbType.DateTime );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "Country";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 6;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = CountryID;
			_fields[1] = CountryCode;
			_fields[2] = CountryName;
			_fields[3] = IsActive;
			_fields[4] = LastEditDate;
			_fields[5] = CreationDate;


			Reset();
		}
		
		#region Standard Methods
		public Country()
		{
		}
		public Country(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

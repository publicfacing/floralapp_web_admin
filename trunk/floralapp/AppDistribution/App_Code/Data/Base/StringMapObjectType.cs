using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for StringMapObjectType
	/// </summary>
	public class StringMapObjectType : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql ObjectTypeID = new PKIntSql("ObjectTypeID",0,SqlDbType.Int );
		public StringSql ObjectTypeName = new StringSql("ObjectTypeName",100,"",SqlDbType.NVarChar );
		public StringSql Description = new StringSql("Description",500,"",SqlDbType.NVarChar );
		public StringSql DevNote = new StringSql("DevNote",500,"",SqlDbType.NVarChar );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "StringMapObjectType";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 5;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ObjectTypeID;
			_fields[1] = ObjectTypeName;
			_fields[2] = Description;
			_fields[3] = DevNote;
			_fields[4] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public StringMapObjectType()
		{
		}
		public StringMapObjectType(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for UpsellProductCategory
	/// </summary>
	public class UpsellProductCategory : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql UpsellProductCategoryID = new PKBigIntSql("UpsellProductCategoryID",0,SqlDbType.BigInt );
		public BigIntSql UpsellProductID = new BigIntSql("UpsellProductID",0,SqlDbType.BigInt );
		public IntSql UpsellCategoryID = new IntSql("UpsellCategoryID",0,SqlDbType.Int );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "UpsellProductCategory";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 8;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = UpsellProductCategoryID;
			_fields[1] = UpsellProductID;
			_fields[2] = UpsellCategoryID;
			_fields[3] = DateAdded;
			_fields[4] = AddedBy;
			_fields[5] = DateModified;
			_fields[6] = ModifiedBy;
			_fields[7] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public UpsellProductCategory()
		{
		}
		public UpsellProductCategory(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

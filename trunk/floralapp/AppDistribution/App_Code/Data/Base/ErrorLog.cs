using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for ErrorLog
	/// </summary>
	public class ErrorLog : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql ErrorLogID = new PKBigIntSql("ErrorLogID",0,SqlDbType.BigInt );
		public StringSql SmallMessage = new StringSql("SmallMessage",500,"",SqlDbType.NVarChar );
		public StringSql Message = new StringSql("Message",-1,"",SqlDbType.NVarChar );
		public StringSql ModuleInfo = new StringSql("ModuleInfo",250,"",SqlDbType.NVarChar );
		public IntSql ApplicationType = new IntSql("ApplicationType",0,SqlDbType.Int );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "ErrorLog";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 6;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ErrorLogID;
			_fields[1] = SmallMessage;
			_fields[2] = Message;
			_fields[3] = ModuleInfo;
			_fields[4] = ApplicationType;
			_fields[5] = DateAdded;


			Reset();
		}
		
		#region Standard Methods
		public ErrorLog()
		{
		}
		public ErrorLog(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

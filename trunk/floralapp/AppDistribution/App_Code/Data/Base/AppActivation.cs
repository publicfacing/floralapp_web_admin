using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for AppActivation
	/// </summary>
	public class AppActivation : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql AppActivationID = new PKBigIntSql("AppActivationID",0,SqlDbType.BigInt );
		public BigIntSql ViralMarketingRequestID = new BigIntSql("ViralMarketingRequestID",0,SqlDbType.BigInt );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public BigIntSql ReferralUserID = new BigIntSql("ReferralUserID",0,SqlDbType.BigInt );
		public StringSql ResellerCode = new StringSql("ResellerCode",50,"",SqlDbType.NVarChar );
		public StringSql ReferralCode = new StringSql("ReferralCode",50,"",SqlDbType.NVarChar );
		public DateTimeSql ActivationDate = new DateTimeSql("ActivationDate",SqlDbType.DateTime );
		public FloatSql Lat = new FloatSql("Lat",0,SqlDbType.Float );
		public FloatSql Lng = new FloatSql("Lng",0,SqlDbType.Float );
		public StringSql DeviceID = new StringSql("DeviceID",50,"",SqlDbType.NVarChar );
		public IntSql DeviceTypeID = new IntSql("DeviceTypeID",0,SqlDbType.Int );
		public BitSql IsSuccess = new BitSql("IsSuccess",false,SqlDbType.Bit );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "AppActivation";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 13;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = AppActivationID;
			_fields[1] = ViralMarketingRequestID;
			_fields[2] = ResellerID;
			_fields[3] = ReferralUserID;
			_fields[4] = ResellerCode;
			_fields[5] = ReferralCode;
			_fields[6] = ActivationDate;
			_fields[7] = Lat;
			_fields[8] = Lng;
			_fields[9] = DeviceID;
			_fields[10] = DeviceTypeID;
			_fields[11] = IsSuccess;
			_fields[12] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public AppActivation()
		{
		}
		public AppActivation(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for DeviceSetting
	/// </summary>
	public class DeviceSetting : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql DeviceSettingID = new PKBigIntSql("DeviceSettingID",0,SqlDbType.BigInt );
		public StringSql DeviceID = new StringSql("DeviceID",50,"",SqlDbType.NVarChar );
		public IntSql DeviceTypeID = new IntSql("DeviceTypeID",0,SqlDbType.Int );
		public StringSql DeviceToken = new StringSql("DeviceToken",500,"",SqlDbType.NVarChar );
        public StringSql GoogleRegistrationID = new StringSql("GoogleRegistrationID", 500, "", SqlDbType.NVarChar);
        
		public BitSql IsPushNotificationEnabled = new BitSql("IsPushNotificationEnabled",false,SqlDbType.Bit );
		public StringSql Lat = new StringSql("Lat",50,"",SqlDbType.NVarChar );
		public StringSql Lng = new StringSql("Lng",50,"",SqlDbType.NVarChar );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "DeviceSetting";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 15;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = DeviceSettingID;
			_fields[1] = DeviceID;
			_fields[2] = DeviceTypeID;
			_fields[3] = DeviceToken;
            _fields[4] = GoogleRegistrationID;
			_fields[5] = IsPushNotificationEnabled;
			_fields[6] = Lat;
			_fields[7] = Lng;
			_fields[8] = DateAdded;
			_fields[9] = AddedBy;
			_fields[10] = DateModified;
			_fields[11] = ModifiedBy;
			_fields[12] = DateDeleted;
			_fields[13] = DeletedBy;
			_fields[14] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public DeviceSetting()
		{
		}
		public DeviceSetting(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

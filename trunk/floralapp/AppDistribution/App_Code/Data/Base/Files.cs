using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for Files
	/// </summary>
	public class Files : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql FileID = new PKBigIntSql("FileID",0,SqlDbType.BigInt );
		public IntSql FileObjectTypeID = new IntSql("FileObjectTypeID",0,SqlDbType.Int );
		public IntSql FileObjectID = new IntSql("FileObjectID",0,SqlDbType.Int );
		public BinarySql FileBinary = new BinarySql("FileBinary",SqlDbType.VarBinary );
		public StringSql FileExt = new StringSql("FileExt",50,"",SqlDbType.NVarChar );
		public IntSql FileTypeID = new IntSql("FileTypeID",0,SqlDbType.Int );
		public StringSql FileName = new StringSql("FileName",250,"",SqlDbType.NVarChar );
		public StringSql FileDescription = new StringSql("FileDescription",500,"",SqlDbType.NVarChar );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "Files";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 13;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = FileID;
			_fields[1] = FileObjectTypeID;
			_fields[2] = FileObjectID;
			_fields[3] = FileBinary;
			_fields[4] = FileExt;
			_fields[5] = FileTypeID;
			_fields[6] = FileName;
			_fields[7] = FileDescription;
			_fields[8] = DateAdded;
			_fields[9] = AddedBy;
			_fields[10] = DateModified;
			_fields[11] = ModifiedBy;
			_fields[12] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public Files()
		{
		}
		public Files(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

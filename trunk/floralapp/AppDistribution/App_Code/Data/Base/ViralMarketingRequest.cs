using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for ViralMarketingRequest
	/// </summary>
	public class ViralMarketingRequest : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql ViralMarketingRequestID = new PKBigIntSql("ViralMarketingRequestID",0,SqlDbType.BigInt );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public BigIntSql RequestedBy = new BigIntSql("RequestedBy",0,SqlDbType.BigInt );
		public StringSql ReferralCode = new StringSql("ReferralCode",60,"",SqlDbType.NVarChar );
		public StringSql EmailIDList = new StringSql("EmailIDList",1000,"",SqlDbType.NVarChar );
		public StringSql RequestLink = new StringSql("RequestLink",500,"",SqlDbType.NVarChar );
		public StringSql Message = new StringSql("Message",4000,"",SqlDbType.NVarChar );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "ViralMarketingRequest";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 12;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ViralMarketingRequestID;
			_fields[1] = ResellerID;
			_fields[2] = RequestedBy;
			_fields[3] = ReferralCode;
			_fields[4] = EmailIDList;
			_fields[5] = RequestLink;
			_fields[6] = Message;
			_fields[7] = DateAdded;
			_fields[8] = AddedBy;
			_fields[9] = DateModified;
			_fields[10] = ModifiedBy;
			_fields[11] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public ViralMarketingRequest()
		{
		}
		public ViralMarketingRequest(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

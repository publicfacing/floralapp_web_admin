using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for Category
	/// </summary>
	public class Category : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql CategoryID = new PKIntSql("CategoryID",0,SqlDbType.Int );
		public IntSql ClonedCategoryID = new IntSql("ClonedCategoryID",0,SqlDbType.Int );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public IntSql DefaultCategoryID = new IntSql("DefaultCategoryID",0,SqlDbType.Int );
		public StringSql CategoryName = new StringSql("CategoryName",50,"",SqlDbType.NVarChar );
		public BinarySql ImageBinary = new BinarySql("ImageBinary",SqlDbType.VarBinary );
		public StringSql ImageExt = new StringSql("ImageExt",10,"",SqlDbType.NVarChar );
		public IntSql Sequence = new IntSql("Sequence",0,SqlDbType.Int );
		public BitSql IsEnabled = new BitSql("IsEnabled",false,SqlDbType.Bit );
		public BitSql IsSystemDefined = new BitSql("IsSystemDefined",false,SqlDbType.Bit );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "Category";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 17;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = CategoryID;
			_fields[1] = ClonedCategoryID;
			_fields[2] = ResellerID;
			_fields[3] = DefaultCategoryID;
			_fields[4] = CategoryName;
			_fields[5] = ImageBinary;
			_fields[6] = ImageExt;
			_fields[7] = Sequence;
			_fields[8] = IsEnabled;
			_fields[9] = IsSystemDefined;
			_fields[10] = DateAdded;
			_fields[11] = AddedBy;
			_fields[12] = DateModified;
			_fields[13] = ModifiedBy;
			_fields[14] = DateDeleted;
			_fields[15] = DeletedBy;
			_fields[16] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public Category()
		{
		}
		public Category(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for ResellerSetting
	/// </summary>
	public class ResellerSetting : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql ResellerSettingID = new PKIntSql("ResellerSettingID",0,SqlDbType.Int );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public IntSql ColorThemeID = new IntSql("ColorThemeID",0,SqlDbType.Int );
		public IntSql ActiveProductLimit = new IntSql("ActiveProductLimit",0,SqlDbType.Int );
		public BitSql IsAllFeeInclusive = new BitSql("IsAllFeeInclusive",false,SqlDbType.Bit );
		public DecimalSql DeliveryFee = new DecimalSql("DeliveryFee",8,2,0,SqlDbType.Decimal );
		public DecimalSql ServiceFee = new DecimalSql("ServiceFee",8,2,0,SqlDbType.Decimal );
		public FloatSql SalesTax = new FloatSql("SalesTax",0,SqlDbType.Float );
		public BitSql IsSalesTaxOn = new BitSql("IsSalesTaxOn",false,SqlDbType.Bit );
		public BitSql IsViralMarketingOn = new BitSql("IsViralMarketingOn",false,SqlDbType.Bit );
		public StringSql FloristToFloristMsg = new StringSql("FloristToFloristMsg",4000,"",SqlDbType.NVarChar );
		public StringSql DirectShipMsg = new StringSql("DirectShipMsg",4000,"",SqlDbType.NVarChar );
		public StringSql SupportPhone = new StringSql("SupportPhone",50,"",SqlDbType.NVarChar );
		public StringSql PickupMessage = new StringSql("PickupMessage",250,"",SqlDbType.NVarChar );
		public StringSql BlockOutDatesMessage = new StringSql("BlockOutDatesMessage",250,"",SqlDbType.NVarChar );
		public StringSql DeliveryZipCode = new StringSql("DeliveryZipCode",-1,"",SqlDbType.NVarChar );
		public BigIntSql DeliveryZipCodeLimit = new BigIntSql("DeliveryZipCodeLimit",0,SqlDbType.BigInt );
		public IntSql MinCategoryEnabled = new IntSql("MinCategoryEnabled",0,SqlDbType.Int );
		public IntSql MaxCategoryEnabled = new IntSql("MaxCategoryEnabled",0,SqlDbType.Int );
		public StringSql SupportedDeviceTypeID = new StringSql("SupportedDeviceTypeID",50,"",SqlDbType.NVarChar );
		public BitSql ShowProfileMsg = new BitSql("ShowProfileMsg",false,SqlDbType.Bit );
		public StringSql ProfileMsg = new StringSql("ProfileMsg",250,"",SqlDbType.NVarChar );
		public BitSql IsDeliveryTaxOn = new BitSql("IsDeliveryTaxOn",false,SqlDbType.Bit );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );
        public IntSql ReceiveUserSignInEmail = new IntSql("ReceiveUserSignInEmail", 0, SqlDbType.Int);
        public StringSql ViralIncentiveMessage = new StringSql("ViralIncentiveMessage", 250, "", SqlDbType.NVarChar);
        public IntSql IsViralIncentiveMessageOn = new IntSql("IsViralIncentiveMessageOn", 0, SqlDbType.Int);
		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "ResellerSetting";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 31;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ResellerSettingID;
			_fields[1] = ResellerID;
			_fields[2] = ColorThemeID;
			_fields[3] = ActiveProductLimit;
			_fields[4] = IsAllFeeInclusive;
			_fields[5] = DeliveryFee;
			_fields[6] = ServiceFee;
			_fields[7] = SalesTax;
			_fields[8] = IsSalesTaxOn;
			_fields[9] = IsViralMarketingOn;
			_fields[10] = FloristToFloristMsg;
			_fields[11] = DirectShipMsg;
			_fields[12] = SupportPhone;
			_fields[13] = PickupMessage;
			_fields[14] = BlockOutDatesMessage;
			_fields[15] = DeliveryZipCode;
			_fields[16] = DeliveryZipCodeLimit;
			_fields[17] = MinCategoryEnabled;
			_fields[18] = MaxCategoryEnabled;
			_fields[19] = SupportedDeviceTypeID;
			_fields[20] = ShowProfileMsg;
			_fields[21] = ProfileMsg;
			_fields[22] = IsDeliveryTaxOn;
			_fields[23] = DateAdded;
			_fields[24] = AddedBy;
			_fields[25] = DateModified;
			_fields[26] = ModifiedBy;
			_fields[27] = IsActive;
            _fields[28] = ReceiveUserSignInEmail;
            _fields[29] = ViralIncentiveMessage;
            _fields[30] = IsViralIncentiveMessageOn;

			Reset();
		}
		
		#region Standard Methods
		public ResellerSetting()
		{
		}
		public ResellerSetting(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

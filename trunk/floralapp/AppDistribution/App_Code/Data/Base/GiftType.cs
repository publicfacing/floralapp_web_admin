using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for GiftType
	/// </summary>
	public class GiftType : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql GiftTypeID = new PKIntSql("GiftTypeID",0,SqlDbType.Int );
		public StringSql GiftTypeName = new StringSql("GiftTypeName",50,"",SqlDbType.NVarChar );
		public IntSql Sequence = new IntSql("Sequence",0,SqlDbType.Int );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "GiftType";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 4;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = GiftTypeID;
			_fields[1] = GiftTypeName;
			_fields[2] = Sequence;
			_fields[3] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public GiftType()
		{
		}
		public GiftType(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for UserSetting
	/// </summary>
	public class UserSetting : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql UserSettingID = new PKBigIntSql("UserSettingID",0,SqlDbType.BigInt );
		public BigIntSql UserID = new BigIntSql("UserID",0,SqlDbType.BigInt );
		public BitSql IsSpecialOfferEnabled = new BitSql("IsSpecialOfferEnabled",false,SqlDbType.Bit );
		public BitSql OptInEmail = new BitSql("OptInEmail",false,SqlDbType.Bit );
		public BitSql OptInText = new BitSql("OptInText",false,SqlDbType.Bit );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "UserSetting";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 6;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = UserSettingID;
			_fields[1] = UserID;
			_fields[2] = IsSpecialOfferEnabled;
			_fields[3] = OptInEmail;
			_fields[4] = OptInText;
			_fields[5] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public UserSetting()
		{
		}
		public UserSetting(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for ResellerMerchantSetting
	/// </summary>
	public class ResellerMerchantSetting : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql ResellerMerchantID = new PKIntSql("ResellerMerchantID",0,SqlDbType.Int );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public StringSql MerchantLoginID = new StringSql("MerchantLoginID",250,"",SqlDbType.NVarChar );
		public StringSql ExpCkeckoutLoginID = new StringSql("ExpCkeckoutLoginID",250,"",SqlDbType.NVarChar );
		public StringSql MerchantTransKey = new StringSql("MerchantTransKey",250,"",SqlDbType.NVarChar );
		public StringSql APISignature = new StringSql("APISignature",500,"",SqlDbType.NVarChar );
		public BitSql IsMerchantVerified = new BitSql("IsMerchantVerified",false,SqlDbType.Bit );
		public DateTimeSql DateMechantVerified = new DateTimeSql("DateMechantVerified",SqlDbType.DateTime );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "ResellerMerchantSetting";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 15;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ResellerMerchantID;
			_fields[1] = ResellerID;
			_fields[2] = MerchantLoginID;
			_fields[3] = ExpCkeckoutLoginID;
			_fields[4] = MerchantTransKey;
			_fields[5] = APISignature;
			_fields[6] = IsMerchantVerified;
			_fields[7] = DateMechantVerified;
			_fields[8] = DateAdded;
			_fields[9] = AddedBy;
			_fields[10] = DateModified;
			_fields[11] = ModifiedBy;
			_fields[12] = DateDeleted;
			_fields[13] = DeletedBy;
			_fields[14] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public ResellerMerchantSetting()
		{
		}
		public ResellerMerchantSetting(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for AppDownloads
	/// </summary>
	public class AppDownloads : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql AppDownloadID = new PKBigIntSql("AppDownloadID",0,SqlDbType.BigInt );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public StringSql Link = new StringSql("Link",250,"",SqlDbType.NVarChar );
		public StringSql ReferralCode = new StringSql("ReferralCode",50,"",SqlDbType.NVarChar );
		public BigIntSql UserID = new BigIntSql("UserID",0,SqlDbType.BigInt );
		public StringSql UserIP = new StringSql("UserIP",50,"",SqlDbType.NVarChar );
		public StringSql UserEmail = new StringSql("UserEmail",50,"",SqlDbType.NVarChar );
		public StringSql BrowserXML = new StringSql("BrowserXML",4000,"",SqlDbType.NVarChar );
		public DateTimeSql DownloadDate = new DateTimeSql("DownloadDate",SqlDbType.DateTime );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "AppDownloads";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 10;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = AppDownloadID;
			_fields[1] = ResellerID;
			_fields[2] = Link;
			_fields[3] = ReferralCode;
			_fields[4] = UserID;
			_fields[5] = UserIP;
			_fields[6] = UserEmail;
			_fields[7] = BrowserXML;
			_fields[8] = DownloadDate;
			_fields[9] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public AppDownloads()
		{
		}
		public AppDownloads(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

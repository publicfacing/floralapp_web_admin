using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for DeviceMapping
	/// </summary>
	public class DeviceMapping : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql DeviceMappingID = new PKBigIntSql("DeviceMappingID",0,SqlDbType.BigInt );
		public BigIntSql DeviceSettingID = new BigIntSql("DeviceSettingID",0,SqlDbType.BigInt );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public IntSql UserID = new IntSql("UserID",0,SqlDbType.Int );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "DeviceMapping";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 11;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = DeviceMappingID;
			_fields[1] = DeviceSettingID;
			_fields[2] = ResellerID;
			_fields[3] = UserID;
			_fields[4] = DateAdded;
			_fields[5] = AddedBy;
			_fields[6] = DateModified;
			_fields[7] = ModifiedBy;
			_fields[8] = DateDeleted;
			_fields[9] = DeletedBy;
			_fields[10] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public DeviceMapping()
		{
		}
		public DeviceMapping(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

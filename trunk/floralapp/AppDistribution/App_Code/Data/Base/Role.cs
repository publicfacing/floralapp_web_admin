using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for Role
	/// </summary>
	public class Role : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql RoleID = new PKIntSql("RoleID",0,SqlDbType.Int );
		public StringSql RoleName = new StringSql("RoleName",50,"",SqlDbType.NVarChar );
		public StringSql Description = new StringSql("Description",250,"",SqlDbType.NVarChar );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "Role";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 4;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = RoleID;
			_fields[1] = RoleName;
			_fields[2] = Description;
			_fields[3] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public Role()
		{
		}
		public Role(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

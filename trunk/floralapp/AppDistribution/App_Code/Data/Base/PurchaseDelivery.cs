using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for PurchaseDelivery
	/// </summary>
	public class PurchaseDelivery : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql PurchaseDeliveryID = new PKBigIntSql("PurchaseDeliveryID",0,SqlDbType.BigInt );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public BigIntSql PurchaseID = new BigIntSql("PurchaseID",0,SqlDbType.BigInt );
		public DateTimeSql ActualDeliveryDate = new DateTimeSql("ActualDeliveryDate",SqlDbType.DateTime );
		public BitSql IsDelivered = new BitSql("IsDelivered",false,SqlDbType.Bit );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "PurchaseDelivery";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 12;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = PurchaseDeliveryID;
			_fields[1] = ResellerID;
			_fields[2] = PurchaseID;
			_fields[3] = ActualDeliveryDate;
			_fields[4] = IsDelivered;
			_fields[5] = DateAdded;
			_fields[6] = AddedBy;
			_fields[7] = DateModified;
			_fields[8] = ModifiedBy;
			_fields[9] = DateDeleted;
			_fields[10] = DeletedBy;
			_fields[11] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public PurchaseDelivery()
		{
		}
		public PurchaseDelivery(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

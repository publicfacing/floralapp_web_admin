using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for Product
	/// </summary>
	public class Product : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql ProductID = new PKBigIntSql("ProductID",0,SqlDbType.BigInt );
		public BigIntSql ClonedProductID = new BigIntSql("ClonedProductID",0,SqlDbType.BigInt );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public StringSql ProductName = new StringSql("ProductName",250,"",SqlDbType.NVarChar );
		public StringSql Color = new StringSql("Color",250,"",SqlDbType.NVarChar );
		public BitSql ShowColorInDevice = new BitSql("ShowColorInDevice",false,SqlDbType.Bit );
		public StringSql Description = new StringSql("Description",2000,"",SqlDbType.NVarChar );
		public BinarySql ProductImage = new BinarySql("ProductImage",SqlDbType.VarBinary );
		public StringSql ProductImageExt = new StringSql("ProductImageExt",10,"",SqlDbType.NVarChar );
		public BinarySql ProductThumb = new BinarySql("ProductThumb",SqlDbType.VarBinary );
		public StringSql ProductThumbExt = new StringSql("ProductThumbExt",10,"",SqlDbType.NVarChar );
		public DecimalSql ProductPrice = new DecimalSql("ProductPrice",8,2,0,SqlDbType.Decimal );
		public StringSql SKUNo = new StringSql("SKUNo",20,"",SqlDbType.NVarChar );
		public BitSql IsUpchargeActive = new BitSql("IsUpchargeActive",false,SqlDbType.Bit );
		public BitSql IsFloristToFlorist = new BitSql("IsFloristToFlorist",false,SqlDbType.Bit );
		public BitSql IsDirectShip = new BitSql("IsDirectShip",false,SqlDbType.Bit );
		public BitSql IsPublished = new BitSql("IsPublished",false,SqlDbType.Bit );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "Product";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 24;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ProductID;
			_fields[1] = ClonedProductID;
			_fields[2] = ResellerID;
			_fields[3] = ProductName;
			_fields[4] = Color;
			_fields[5] = ShowColorInDevice;
			_fields[6] = Description;
			_fields[7] = ProductImage;
			_fields[8] = ProductImageExt;
			_fields[9] = ProductThumb;
			_fields[10] = ProductThumbExt;
			_fields[11] = ProductPrice;
			_fields[12] = SKUNo;
			_fields[13] = IsUpchargeActive;
			_fields[14] = IsFloristToFlorist;
			_fields[15] = IsDirectShip;
			_fields[16] = IsPublished;
			_fields[17] = DateAdded;
			_fields[18] = AddedBy;
			_fields[19] = DateModified;
			_fields[20] = ModifiedBy;
			_fields[21] = DateDeleted;
			_fields[22] = DeletedBy;
			_fields[23] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public Product()
		{
		}
		public Product(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for PurchaseAddresses
	/// </summary>
	public class PurchaseAddresses : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql PurchaseAddressID = new PKBigIntSql("PurchaseAddressID",0,SqlDbType.BigInt );
		public BigIntSql PurchaseID = new BigIntSql("PurchaseID",0,SqlDbType.BigInt );
		public IntSql AddressTypeID = new IntSql("AddressTypeID",0,SqlDbType.Int );
		public BigIntSql AddressID = new BigIntSql("AddressID",0,SqlDbType.BigInt );
		public StringSql FirstName = new StringSql("FirstName",50,"",SqlDbType.NVarChar );
		public StringSql LastName = new StringSql("LastName",50,"",SqlDbType.NVarChar );
		public StringSql Name = new StringSql("Name",150,"",SqlDbType.NVarChar );
		public StringSql Street = new StringSql("Street",50,"",SqlDbType.NVarChar );
		public StringSql Suite = new StringSql("Suite",50,"",SqlDbType.NVarChar );
		public StringSql City = new StringSql("City",50,"",SqlDbType.NVarChar );
		public StringSql OtherState = new StringSql("OtherState",50,"",SqlDbType.NVarChar );
		public IntSql StateID = new IntSql("StateID",0,SqlDbType.Int );
		public IntSql CountryID = new IntSql("CountryID",0,SqlDbType.Int );
		public StringSql Zip = new StringSql("Zip",50,"",SqlDbType.NVarChar );
		public StringSql Phone1 = new StringSql("Phone1",50,"",SqlDbType.NVarChar );
		public StringSql Phone1Ext = new StringSql("Phone1Ext",50,"",SqlDbType.NVarChar );
		public StringSql Phone2 = new StringSql("Phone2",50,"",SqlDbType.NVarChar );
		public StringSql Phone2Ext = new StringSql("Phone2Ext",50,"",SqlDbType.NVarChar );
		public StringSql Fax = new StringSql("Fax",50,"",SqlDbType.NVarChar );
		public StringSql Email1 = new StringSql("Email1",50,"",SqlDbType.NVarChar );
		public StringSql Email2 = new StringSql("Email2",50,"",SqlDbType.NVarChar );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "PurchaseAddresses";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 24;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = PurchaseAddressID;
			_fields[1] = PurchaseID;
			_fields[2] = AddressTypeID;
			_fields[3] = AddressID;
			_fields[4] = FirstName;
			_fields[5] = LastName;
			_fields[6] = Name;
			_fields[7] = Street;
			_fields[8] = Suite;
			_fields[9] = City;
			_fields[10] = OtherState;
			_fields[11] = StateID;
			_fields[12] = CountryID;
			_fields[13] = Zip;
			_fields[14] = Phone1;
			_fields[15] = Phone1Ext;
			_fields[16] = Phone2;
			_fields[17] = Phone2Ext;
			_fields[18] = Fax;
			_fields[19] = Email1;
			_fields[20] = Email2;
			_fields[21] = DateAdded;
			_fields[22] = AddedBy;
			_fields[23] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public PurchaseAddresses()
		{
		}
		public PurchaseAddresses(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for UpsellCategory
	/// </summary>
	public class UpsellCategory : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql UpsellCategoryID = new PKIntSql("UpsellCategoryID",0,SqlDbType.Int );
		public IntSql ClonedUpsellCategoryID = new IntSql("ClonedUpsellCategoryID",0,SqlDbType.Int );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public StringSql CategoryName = new StringSql("CategoryName",50,"",SqlDbType.NVarChar );
		public BinarySql ImageBinary = new BinarySql("ImageBinary",SqlDbType.VarBinary );
		public StringSql ImageExt = new StringSql("ImageExt",10,"",SqlDbType.NVarChar );
		public IntSql Sequence = new IntSql("Sequence",0,SqlDbType.Int );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "UpsellCategory";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 8;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = UpsellCategoryID;
			_fields[1] = ClonedUpsellCategoryID;
			_fields[2] = ResellerID;
			_fields[3] = CategoryName;
			_fields[4] = ImageBinary;
			_fields[5] = ImageExt;
			_fields[6] = Sequence;
			_fields[7] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public UpsellCategory()
		{
		}
		public UpsellCategory(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

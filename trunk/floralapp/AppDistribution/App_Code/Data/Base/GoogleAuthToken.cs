﻿using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for Address
	/// </summary>
	public class GoogleAuthToken : Library.DAL.TableSql  
	{
        public StringSql GoogleAuthTokenCode = new StringSql("GoogleLoginAuthToken",300,"",SqlDbType.NVarChar );
	    
        public override void SetFields()
		{
			
			_tableName = "GoogleAuthToken";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 1;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = GoogleAuthTokenCode;

			Reset();
		}
		
		#region Standard Methods
        public GoogleAuthToken()
		{
		}

		#endregion
    }
}
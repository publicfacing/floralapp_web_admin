using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for SpecialOffer
	/// </summary>
	public class SpecialOffer : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql SpecialOfferID = new PKIntSql("SpecialOfferID",0,SqlDbType.Int );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public StringSql OfferTitle = new StringSql("OfferTitle",250,"",SqlDbType.NVarChar );
		public StringSql OfferDescription = new StringSql("OfferDescription",500,"",SqlDbType.NVarChar );
		public StringSql OfferHtml = new StringSql("OfferHtml",4000,"",SqlDbType.NVarChar );
		public IntSql OfferStatusID = new IntSql("OfferStatusID",0,SqlDbType.Int );
		public DateTimeSql OfferStartDate = new DateTimeSql("OfferStartDate",SqlDbType.DateTime );
		public DateTimeSql OfferEndDate = new DateTimeSql("OfferEndDate",SqlDbType.DateTime );
		public BitSql IsNotified = new BitSql("IsNotified",false,SqlDbType.Bit );
		public DateTimeSql DateNotified = new DateTimeSql("DateNotified",SqlDbType.DateTime );
		public BigIntSql NotifiedBy = new BigIntSql("NotifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "SpecialOffer";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 18;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = SpecialOfferID;
			_fields[1] = ResellerID;
			_fields[2] = OfferTitle;
			_fields[3] = OfferDescription;
			_fields[4] = OfferHtml;
			_fields[5] = OfferStatusID;
			_fields[6] = OfferStartDate;
			_fields[7] = OfferEndDate;
			_fields[8] = IsNotified;
			_fields[9] = DateNotified;
			_fields[10] = NotifiedBy;
			_fields[11] = DateAdded;
			_fields[12] = AddedBy;
			_fields[13] = DateModified;
			_fields[14] = ModifiedBy;
			_fields[15] = DateDeleted;
			_fields[16] = DeletedBy;
			_fields[17] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public SpecialOffer()
		{
		}
		public SpecialOffer(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

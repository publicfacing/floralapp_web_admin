using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for Users
	/// </summary>
	public class Users : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql UserID = new PKBigIntSql("UserID",0,SqlDbType.BigInt );
		public IntSql RoleID = new IntSql("RoleID",0,SqlDbType.Int );
		public StringSql LoginID = new StringSql("LoginID",50,"",SqlDbType.NVarChar );
		public StringSql Password = new StringSql("Password",50,"",SqlDbType.NVarChar );
		public StringSql FirstName = new StringSql("FirstName",60,"",SqlDbType.NVarChar );
		public StringSql LastName = new StringSql("LastName",60,"",SqlDbType.NVarChar );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "Users";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 13;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = UserID;
			_fields[1] = RoleID;
			_fields[2] = LoginID;
			_fields[3] = Password;
			_fields[4] = FirstName;
			_fields[5] = LastName;
			_fields[6] = DateAdded;
			_fields[7] = AddedBy;
			_fields[8] = DateModified;
			_fields[9] = ModifiedBy;
			_fields[10] = DateDeleted;
			_fields[11] = DeletedBy;
			_fields[12] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public Users()
		{
		}
		public Users(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

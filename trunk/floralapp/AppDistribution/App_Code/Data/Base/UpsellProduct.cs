using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for UpsellProduct
	/// </summary>
	public class UpsellProduct : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql UpsellProductID = new PKBigIntSql("UpsellProductID",0,SqlDbType.BigInt );
		public BigIntSql ClonedUpsellProductID = new BigIntSql("ClonedUpsellProductID",0,SqlDbType.BigInt );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public StringSql ProductName = new StringSql("ProductName",250,"",SqlDbType.NVarChar );
		public StringSql Description = new StringSql("Description",2000,"",SqlDbType.NVarChar );
		public DecimalSql ProductPrice = new DecimalSql("ProductPrice",8,2,0,SqlDbType.Decimal );
		public StringSql SKUNo = new StringSql("SKUNo",20,"",SqlDbType.NVarChar );
		public BitSql IsPublished = new BitSql("IsPublished",false,SqlDbType.Bit );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "UpsellProduct";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 15;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = UpsellProductID;
			_fields[1] = ClonedUpsellProductID;
			_fields[2] = ResellerID;
			_fields[3] = ProductName;
			_fields[4] = Description;
			_fields[5] = ProductPrice;
			_fields[6] = SKUNo;
			_fields[7] = IsPublished;
			_fields[8] = DateAdded;
			_fields[9] = AddedBy;
			_fields[10] = DateModified;
			_fields[11] = ModifiedBy;
			_fields[12] = DateDeleted;
			_fields[13] = DeletedBy;
			_fields[14] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public UpsellProduct()
		{
		}
		public UpsellProduct(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

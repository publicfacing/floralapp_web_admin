using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for ColorTheme
	/// </summary>
	public class ColorTheme : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql ColorThemeID = new PKIntSql("ColorThemeID",0,SqlDbType.Int );
		public StringSql ThemeName = new StringSql("ThemeName",50,"",SqlDbType.NVarChar );
		public StringSql NavigationBarColor = new StringSql("NavigationBarColor",10,"",SqlDbType.NVarChar );
		public BinarySql NavigationBarImage = new BinarySql("NavigationBarImage",SqlDbType.VarBinary );
		public BinarySql BackGroundImage = new BinarySql("BackGroundImage",SqlDbType.VarBinary );
		public StringSql BackGroundColor = new StringSql("BackGroundColor",10,"",SqlDbType.NVarChar );
		public BinarySql NavLeftImgOn = new BinarySql("NavLeftImgOn",SqlDbType.VarBinary );
		public BinarySql NavLeftImgOff = new BinarySql("NavLeftImgOff",SqlDbType.VarBinary );
		public BinarySql NavRightImgOn = new BinarySql("NavRightImgOn",SqlDbType.VarBinary );
		public BinarySql NavRightImgOff = new BinarySql("NavRightImgOff",SqlDbType.VarBinary );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "ColorTheme";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 17;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ColorThemeID;
			_fields[1] = ThemeName;
			_fields[2] = NavigationBarColor;
			_fields[3] = NavigationBarImage;
			_fields[4] = BackGroundImage;
			_fields[5] = BackGroundColor;
			_fields[6] = NavLeftImgOn;
			_fields[7] = NavLeftImgOff;
			_fields[8] = NavRightImgOn;
			_fields[9] = NavRightImgOff;
			_fields[10] = DateAdded;
			_fields[11] = AddedBy;
			_fields[12] = DateModified;
			_fields[13] = ModifiedBy;
			_fields[14] = DateDeleted;
			_fields[15] = DeletedBy;
			_fields[16] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public ColorTheme()
		{
		}
		public ColorTheme(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

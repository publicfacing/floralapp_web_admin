using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for ProductCategory
	/// </summary>
	public class ProductCategory : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql ProductCategoryID = new PKBigIntSql("ProductCategoryID",0,SqlDbType.BigInt );
		public BigIntSql ProductID = new BigIntSql("ProductID",0,SqlDbType.BigInt );
		public IntSql CategoryID = new IntSql("CategoryID",0,SqlDbType.Int );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "ProductCategory";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 8;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ProductCategoryID;
			_fields[1] = ProductID;
			_fields[2] = CategoryID;
			_fields[3] = DateAdded;
			_fields[4] = AddedBy;
			_fields[5] = DateModified;
			_fields[6] = ModifiedBy;
			_fields[7] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public ProductCategory()
		{
		}
		public ProductCategory(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

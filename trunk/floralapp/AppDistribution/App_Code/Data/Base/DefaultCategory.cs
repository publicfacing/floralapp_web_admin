using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for DefaultCategory
	/// </summary>
	public class DefaultCategory : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql DefaultCategoryID = new PKIntSql("DefaultCategoryID",0,SqlDbType.Int );
		public StringSql CategoryName = new StringSql("CategoryName",50,"",SqlDbType.NVarChar );
		public BinarySql ImageBinary = new BinarySql("ImageBinary",SqlDbType.VarBinary );
		public StringSql ImageExt = new StringSql("ImageExt",10,"",SqlDbType.NVarChar );
		public IntSql Sequence = new IntSql("Sequence",0,SqlDbType.Int );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "DefaultCategory";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 6;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = DefaultCategoryID;
			_fields[1] = CategoryName;
			_fields[2] = ImageBinary;
			_fields[3] = ImageExt;
			_fields[4] = Sequence;
			_fields[5] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public DefaultCategory()
		{
		}
		public DefaultCategory(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for Purchase
	/// </summary>
	public class Purchase : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql PurchaseID = new PKBigIntSql("PurchaseID",0,SqlDbType.BigInt );
		public BigIntSql OrderID = new BigIntSql("OrderID",0,SqlDbType.BigInt );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public BigIntSql UserID = new BigIntSql("UserID",0,SqlDbType.BigInt );
		public StringSql MerchantLoginID = new StringSql("MerchantLoginID",50,"",SqlDbType.NVarChar );
		public IntSql GatewayTypeID = new IntSql("GatewayTypeID",0,SqlDbType.Int );
		public BitSql IsRegisteredUser = new BitSql("IsRegisteredUser",false,SqlDbType.Bit );
		public BitSql IsPickUp = new BitSql("IsPickUp",false,SqlDbType.Bit );
		public DateTimeSql DeliveryDate = new DateTimeSql("DeliveryDate",SqlDbType.DateTime );
		public StringSql Notes = new StringSql("Notes",1000,"",SqlDbType.NVarChar );
		public DecimalSql ServiceFee = new DecimalSql("ServiceFee",8,2,0,SqlDbType.Decimal );
		public DecimalSql Deliveryfee = new DecimalSql("Deliveryfee",8,2,0,SqlDbType.Decimal );
		public DecimalSql SalesTax = new DecimalSql("SalesTax",8,2,0,SqlDbType.Decimal );
		public IntSql DiscountTypeID = new IntSql("DiscountTypeID",0,SqlDbType.Int );
		public DecimalSql TotalAmount = new DecimalSql("TotalAmount",8,2,0,SqlDbType.Decimal );
		public StringSql GiftMessage = new StringSql("GiftMessage",1000,"",SqlDbType.NVarChar );
		public IntSql TransactionStatusID = new IntSql("TransactionStatusID",0,SqlDbType.Int );
		public StringSql ResultCode = new StringSql("ResultCode",50,"",SqlDbType.NVarChar );
		public StringSql ResultXML = new StringSql("ResultXML",-1,"",SqlDbType.NVarChar );
		public StringSql AccountNo = new StringSql("AccountNo",50,"",SqlDbType.NVarChar );
		public IntSql WorkingMode = new IntSql("WorkingMode",0,SqlDbType.Int );
		public StringSql DeviceID = new StringSql("DeviceID",50,"",SqlDbType.NVarChar );
		public IntSql DeviceTypeID = new IntSql("DeviceTypeID",0,SqlDbType.Int );
		public IntSql CardTypeID = new IntSql("CardTypeID",0,SqlDbType.Int );
		public StringSql CardLast4Char = new StringSql("CardLast4Char",50,"",SqlDbType.NVarChar );
		public DecimalSql CardFee = new DecimalSql("CardFee",8,2,0,SqlDbType.Decimal );
		public FloatSql CardFeePercentage = new FloatSql("CardFeePercentage",0,SqlDbType.Float );
		public IntSql TransactionFeeTypeID = new IntSql("TransactionFeeTypeID",0,SqlDbType.Int );
		public DecimalSql TransactionFee = new DecimalSql("TransactionFee",8,2,0,SqlDbType.Decimal );
		public FloatSql TransactionFeePercentage = new FloatSql("TransactionFeePercentage",0,SqlDbType.Float );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "Purchase";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 37;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = PurchaseID;
			_fields[1] = OrderID;
			_fields[2] = ResellerID;
			_fields[3] = UserID;
			_fields[4] = MerchantLoginID;
			_fields[5] = GatewayTypeID;
			_fields[6] = IsRegisteredUser;
			_fields[7] = IsPickUp;
			_fields[8] = DeliveryDate;
			_fields[9] = Notes;
			_fields[10] = ServiceFee;
			_fields[11] = Deliveryfee;
			_fields[12] = SalesTax;
			_fields[13] = DiscountTypeID;
			_fields[14] = TotalAmount;
			_fields[15] = GiftMessage;
			_fields[16] = TransactionStatusID;
			_fields[17] = ResultCode;
			_fields[18] = ResultXML;
			_fields[19] = AccountNo;
			_fields[20] = WorkingMode;
			_fields[21] = DeviceID;
			_fields[22] = DeviceTypeID;
			_fields[23] = CardTypeID;
			_fields[24] = CardLast4Char;
			_fields[25] = CardFee;
			_fields[26] = CardFeePercentage;
			_fields[27] = TransactionFeeTypeID;
			_fields[28] = TransactionFee;
			_fields[29] = TransactionFeePercentage;
			_fields[30] = DateAdded;
			_fields[31] = AddedBy;
			_fields[32] = DateModified;
			_fields[33] = ModifiedBy;
			_fields[34] = DateDeleted;
			_fields[35] = DeletedBy;
			_fields[36] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public Purchase()
		{
		}
		public Purchase(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

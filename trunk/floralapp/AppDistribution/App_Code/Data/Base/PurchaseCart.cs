using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for PurchaseCart
	/// </summary>
	public class PurchaseCart : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql PurchaseCartID = new PKBigIntSql("PurchaseCartID",0,SqlDbType.BigInt );
		public BigIntSql PurchaseID = new BigIntSql("PurchaseID",0,SqlDbType.BigInt );
		public BigIntSql ProductID = new BigIntSql("ProductID",0,SqlDbType.BigInt );
		public IntSql OfferID = new IntSql("OfferID",0,SqlDbType.Int );
		public BigIntSql ProductUpchargeID = new BigIntSql("ProductUpchargeID",0,SqlDbType.BigInt );
		public FloatSql UpchargePercentage = new FloatSql("UpchargePercentage",0,SqlDbType.Float );
		public DecimalSql ProductPrice = new DecimalSql("ProductPrice",8,2,0,SqlDbType.Decimal );
		public IntSql Quantity = new IntSql("Quantity",0,SqlDbType.Int );
		public DecimalSql TotalPrice = new DecimalSql("TotalPrice",8,2,0,SqlDbType.Decimal );
		public StringSql ProductColor = new StringSql("ProductColor",250,"",SqlDbType.NVarChar );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "PurchaseCart";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 17;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = PurchaseCartID;
			_fields[1] = PurchaseID;
			_fields[2] = ProductID;
			_fields[3] = OfferID;
			_fields[4] = ProductUpchargeID;
			_fields[5] = UpchargePercentage;
			_fields[6] = ProductPrice;
			_fields[7] = Quantity;
			_fields[8] = TotalPrice;
			_fields[9] = ProductColor;
			_fields[10] = DateAdded;
			_fields[11] = AddedBy;
			_fields[12] = DateModified;
			_fields[13] = ModifiedBy;
			_fields[14] = DateDeleted;
			_fields[15] = DeletedBy;
			_fields[16] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public PurchaseCart()
		{
		}
		public PurchaseCart(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

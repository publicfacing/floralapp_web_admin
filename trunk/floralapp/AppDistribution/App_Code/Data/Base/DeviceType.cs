using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for DeviceType
	/// </summary>
	public class DeviceType : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql DeviceTypeID = new PKIntSql("DeviceTypeID",0,SqlDbType.Int );
		public StringSql DeviceTypeName = new StringSql("DeviceTypeName",50,"",SqlDbType.NVarChar );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "DeviceType";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 3;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = DeviceTypeID;
			_fields[1] = DeviceTypeName;
			_fields[2] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public DeviceType()
		{
		}
		public DeviceType(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

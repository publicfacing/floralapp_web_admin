using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for Coupon
	/// </summary>
	public class Coupon : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql CouponID = new PKBigIntSql("CouponID",0,SqlDbType.BigInt );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public StringSql CouponCode = new StringSql("CouponCode",50,"",SqlDbType.NVarChar );
		public StringSql CouponText = new StringSql("CouponText",4000,"",SqlDbType.NVarChar );
		public StringSql Referral = new StringSql("Referral",4000,"",SqlDbType.NVarChar );
		public BitSql IsEnabled = new BitSql("IsEnabled",false,SqlDbType.Bit );
		public BinarySql CouponImage = new BinarySql("CouponImage",SqlDbType.VarBinary );
		public StringSql CouponImageExt = new StringSql("CouponImageExt",50,"",SqlDbType.NVarChar );
		public IntSql DiscountTypeID = new IntSql("DiscountTypeID",0,SqlDbType.Int );
		public FloatSql DiscountPercent = new FloatSql("DiscountPercent",0,SqlDbType.Float );
		public DecimalSql DiscountFixedAmount = new DecimalSql("DiscountFixedAmount",8,2,0,SqlDbType.Decimal );
		public DateTimeSql CouponStartDate = new DateTimeSql("CouponStartDate",SqlDbType.DateTime );
		public DateTimeSql CouponEndDate = new DateTimeSql("CouponEndDate",SqlDbType.DateTime );
		public IntSql CouponUsageTypeID = new IntSql("CouponUsageTypeID",0,SqlDbType.Int );
		public BitSql IsVisibleOnPhone = new BitSql("IsVisibleOnPhone",false,SqlDbType.Bit );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "Coupon";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 22;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = CouponID;
			_fields[1] = ResellerID;
			_fields[2] = CouponCode;
			_fields[3] = CouponText;
			_fields[4] = Referral;
			_fields[5] = IsEnabled;
			_fields[6] = CouponImage;
			_fields[7] = CouponImageExt;
			_fields[8] = DiscountTypeID;
			_fields[9] = DiscountPercent;
			_fields[10] = DiscountFixedAmount;
			_fields[11] = CouponStartDate;
			_fields[12] = CouponEndDate;
			_fields[13] = CouponUsageTypeID;
			_fields[14] = IsVisibleOnPhone;
			_fields[15] = DateAdded;
			_fields[16] = AddedBy;
			_fields[17] = DateModified;
			_fields[18] = ModifiedBy;
			_fields[19] = DateDeleted;
			_fields[20] = DeletedBy;
			_fields[21] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public Coupon()
		{
		}
		public Coupon(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for SpecialOfferNotification
	/// </summary>
	public class SpecialOfferNotification : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql SpecialOfferNotificationID = new PKBigIntSql("SpecialOfferNotificationID",0,SqlDbType.BigInt );
		public BigIntSql SpecialOfferID = new BigIntSql("SpecialOfferID",0,SqlDbType.BigInt );
		public BigIntSql UserID = new BigIntSql("UserID",0,SqlDbType.BigInt );
		public StringSql DeviceToken = new StringSql("DeviceToken",500,"",SqlDbType.NVarChar );
		public IntSql DeviceTypeID = new IntSql("DeviceTypeID",0,SqlDbType.Int );
		public StringSql DeviceID = new StringSql("DeviceID",50,"",SqlDbType.NVarChar );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "SpecialOfferNotification";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 13;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = SpecialOfferNotificationID;
			_fields[1] = SpecialOfferID;
			_fields[2] = UserID;
			_fields[3] = DeviceToken;
			_fields[4] = DeviceTypeID;
			_fields[5] = DeviceID;
			_fields[6] = DateAdded;
			_fields[7] = AddedBy;
			_fields[8] = DateModified;
			_fields[9] = ModifiedBy;
			_fields[10] = DateDeleted;
			_fields[11] = DeletedBy;
			_fields[12] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public SpecialOfferNotification()
		{
		}
		public SpecialOfferNotification(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

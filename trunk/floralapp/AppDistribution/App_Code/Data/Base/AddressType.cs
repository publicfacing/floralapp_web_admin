using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for AddressType
	/// </summary>
	public class AddressType : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql AddressTypeID = new PKIntSql("AddressTypeID",0,SqlDbType.Int );
		public StringSql AddressTypeName = new StringSql("AddressTypeName",50,"",SqlDbType.NVarChar );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "AddressType";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 3;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = AddressTypeID;
			_fields[1] = AddressTypeName;
			_fields[2] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public AddressType()
		{
		}
		public AddressType(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

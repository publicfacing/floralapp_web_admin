using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for Faq
	/// </summary>
	public class Faq : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql FaqID = new PKIntSql("FaqID",0,SqlDbType.Int );
		public StringSql Question = new StringSql("Question",250,"",SqlDbType.NVarChar );
		public StringSql Answer = new StringSql("Answer",4000,"",SqlDbType.NVarChar );
		public IntSql Sequence = new IntSql("Sequence",0,SqlDbType.Int );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "Faq";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 11;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = FaqID;
			_fields[1] = Question;
			_fields[2] = Answer;
			_fields[3] = Sequence;
			_fields[4] = DateAdded;
			_fields[5] = AddedBy;
			_fields[6] = DateModified;
			_fields[7] = ModifiedBy;
			_fields[8] = DateDeleted;
			_fields[9] = DeletedBy;
			_fields[10] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public Faq()
		{
		}
		public Faq(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

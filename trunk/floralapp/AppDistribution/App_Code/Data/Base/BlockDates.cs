using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for BlockDates
	/// </summary>
	public class BlockDates : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql BlockDateID = new PKIntSql("BlockDateID",0,SqlDbType.Int );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public DateTimeSql StartDateTime = new DateTimeSql("StartDateTime",SqlDbType.DateTime );
		public DateTimeSql EndDateTime = new DateTimeSql("EndDateTime",SqlDbType.DateTime );
		public BitSql OrderAllowed = new BitSql("OrderAllowed",false,SqlDbType.Bit );
		public StringSql Message = new StringSql("Message",500,"",SqlDbType.NVarChar );
		public IntSql BlockDateTypeID = new IntSql("BlockDateTypeID",0,SqlDbType.Int );
		public IntSql RepeatTypeID = new IntSql("RepeatTypeID",0,SqlDbType.Int );
		public DateTimeSql RepeatUntilDate = new DateTimeSql("RepeatUntilDate",SqlDbType.DateTime );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateDeleted = new DateTimeSql("DateDeleted",SqlDbType.DateTime );
		public BigIntSql DeletedBy = new BigIntSql("DeletedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "BlockDates";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 16;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = BlockDateID;
			_fields[1] = ResellerID;
			_fields[2] = StartDateTime;
			_fields[3] = EndDateTime;
			_fields[4] = OrderAllowed;
			_fields[5] = Message;
			_fields[6] = BlockDateTypeID;
			_fields[7] = RepeatTypeID;
			_fields[8] = RepeatUntilDate;
			_fields[9] = DateAdded;
			_fields[10] = AddedBy;
			_fields[11] = DateModified;
			_fields[12] = ModifiedBy;
			_fields[13] = DateDeleted;
			_fields[14] = DeletedBy;
			_fields[15] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public BlockDates()
		{
		}
		public BlockDates(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

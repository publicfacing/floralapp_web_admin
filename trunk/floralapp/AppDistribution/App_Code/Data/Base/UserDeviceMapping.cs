using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for UserDeviceMapping
	/// </summary>
	public class UserDeviceMapping : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql UserDeviceMappingID = new PKBigIntSql("UserDeviceMappingID",0,SqlDbType.BigInt );
		public BigIntSql UserID = new BigIntSql("UserID",0,SqlDbType.BigInt );
		public StringSql DeviceID = new StringSql("DeviceID",50,"",SqlDbType.NVarChar );
		public StringSql DeviceToken = new StringSql("DeviceToken",500,"",SqlDbType.NVarChar );
		public IntSql DeviceTypeID = new IntSql("DeviceTypeID",0,SqlDbType.Int );
		public FloatSql Lat = new FloatSql("Lat",0,SqlDbType.Float );
		public FloatSql Lng = new FloatSql("Lng",0,SqlDbType.Float );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "UserDeviceMapping";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 9;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = UserDeviceMappingID;
			_fields[1] = UserID;
			_fields[2] = DeviceID;
			_fields[3] = DeviceToken;
			_fields[4] = DeviceTypeID;
			_fields[5] = Lat;
			_fields[6] = Lng;
			_fields[7] = DateAdded;
			_fields[8] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public UserDeviceMapping()
		{
		}
		public UserDeviceMapping(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

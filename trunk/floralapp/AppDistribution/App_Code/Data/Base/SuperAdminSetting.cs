using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for SuperAdminSetting
	/// </summary>
	public class SuperAdminSetting : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKIntSql SuperAdminSettingID = new PKIntSql("SuperAdminSettingID",0,SqlDbType.Int );
		public StringSql ResellersLimit = new StringSql("ResellersLimit",250,"",SqlDbType.NVarChar );
		public BinarySql SplashImage = new BinarySql("SplashImage",SqlDbType.VarBinary );
		public BinarySql AppIcon = new BinarySql("AppIcon",SqlDbType.VarBinary );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "SuperAdminSetting";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 4;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = SuperAdminSettingID;
			_fields[1] = ResellersLimit;
			_fields[2] = SplashImage;
			_fields[3] = AppIcon;


			Reset();
		}
		
		#region Standard Methods
		public SuperAdminSetting()
		{
		}
		public SuperAdminSetting(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

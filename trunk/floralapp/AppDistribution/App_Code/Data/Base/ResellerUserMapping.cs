using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data.Base
{
	/// <summary>
	/// Summary description for ResellerUserMapping
	/// </summary>
	public class ResellerUserMapping : Library.DAL.TableSql  
	{
		//<<START FIELDS>>
		public PKBigIntSql ResellerUserMappingID = new PKBigIntSql("ResellerUserMappingID",0,SqlDbType.BigInt );
		public IntSql ResellerID = new IntSql("ResellerID",0,SqlDbType.Int );
		public BigIntSql UserID = new BigIntSql("UserID",0,SqlDbType.BigInt );
		public BitSql IsEnabled = new BitSql("IsEnabled",false,SqlDbType.Bit );
		public DateTimeSql DateAdded = new DateTimeSql("DateAdded",SqlDbType.DateTime );
		public BigIntSql AddedBy = new BigIntSql("AddedBy",0,SqlDbType.BigInt );
		public DateTimeSql DateModified = new DateTimeSql("DateModified",SqlDbType.DateTime );
		public BigIntSql ModifiedBy = new BigIntSql("ModifiedBy",0,SqlDbType.BigInt );
		public IntSql IsActive = new IntSql("IsActive",0,SqlDbType.Int );

		//<<END FIELDS>>

		public override void SetFields()
		{
			
			_tableName = "ResellerUserMapping";			
			_pkField= 0;
		     _rowverField= -1;
			_fieldCount = 9;
		    _fields = new TypeSql [_fieldCount];
			_parametersSql = new SqlParameter [_fieldCount+2];
			
			_fields[0] = ResellerUserMappingID;
			_fields[1] = ResellerID;
			_fields[2] = UserID;
			_fields[3] = IsEnabled;
			_fields[4] = DateAdded;
			_fields[5] = AddedBy;
			_fields[6] = DateModified;
			_fields[7] = ModifiedBy;
			_fields[8] = IsActive;


			Reset();
		}
		
		#region Standard Methods
		public ResellerUserMapping()
		{
		}
		public ResellerUserMapping(int loadID)
		{
			LoadID(loadID);
		}
		#endregion
	}
}

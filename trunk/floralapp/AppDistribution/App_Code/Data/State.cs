using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for State
	/// </summary>
	public class State : Data.Base.State
	{
	    #region Standard Methods
		public State(){}
		public State(int loadID){LoadID(loadID);}
	    #endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
    /// <summary>
    /// Summary description for UpsellCategory
    /// </summary>
    public class UpsellCategory : Data.Base.UpsellCategory
    {
        #region Standard Methods
        public UpsellCategory() { }
        public UpsellCategory(int loadID) { LoadID(loadID); }
        #endregion

        public DataSet GetUpsellCategoryListByReseller(Int32 resellerID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "UpsellCategory_GetListByReseller", param);
        }         
    }
}

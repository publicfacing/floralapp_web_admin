using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
    /// <summary>
    /// Summary description for ViralMarketingRequest
    /// </summary>
    public class ViralMarketingRequest : Data.Base.ViralMarketingRequest
    {
        #region Standard Methods
        public ViralMarketingRequest() { }
        public ViralMarketingRequest(int loadID) { LoadID(loadID); }
        #endregion

        public DataSet GetRequestByResellerID(Int32 resellerID, Int64 userID, String method, DateTime dtStartDate, DateTime dtEndDate)
        {
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;
            param[1] = new SqlParameter("@UserID", SqlDbType.BigInt);
            param[1].Value = userID;
            param[2] = new SqlParameter("@Method", SqlDbType.NVarChar);
            param[2].Value = method;
            param[3] = new SqlParameter("@StartDate", SqlDbType.DateTime);
            param[3].Value = dtStartDate;
            param[4] = new SqlParameter("@EndDate", SqlDbType.DateTime);
            param[4].Value = dtEndDate;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "ViralMarketing_GeUsageByReseller", param);
        }

        public DataSet GetFilteredList(Int32 pageIndex, Int32 pageSize, String sortExpression, String filterClause, ref Int32 rowCount, ref Int32 totalRowCount, DateTime startDate, DateTime endDate)
        {
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@pageIndex", SqlDbType.Int);
            param[0].Value = pageIndex;
            param[1] = new SqlParameter("@pageSize", SqlDbType.Int);
            param[1].Value = pageSize;
            param[2] = new SqlParameter("@sortExpression", SqlDbType.NVarChar, 100);
            param[2].Value = sortExpression;
            param[3] = new SqlParameter("@filterClause", SqlDbType.NVarChar, 4000);
            param[3].Value = filterClause;
            param[4] = new SqlParameter("@rowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, rowCount);
            param[5] = new SqlParameter("@totalRowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, totalRowCount);
            param[6] = new SqlParameter("@StartDate", SqlDbType.DateTime);
            param[6].Value = startDate;
            param[7] = new SqlParameter("@EndDate", SqlDbType.DateTime);
            param[7].Value = endDate;

            DataSet ds = SqlPipe.ExecuteDataset(_connectionString, _tableName + "_GetList", param);
            rowCount = (Int32)param[4].Value;
            totalRowCount = (Int32)param[5].Value;
            return ds;
        }
    }
}

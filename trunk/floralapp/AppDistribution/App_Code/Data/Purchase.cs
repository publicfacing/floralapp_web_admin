using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
    /// <summary>
    /// Summary description for Purchase
    /// </summary>
    public class Purchase : Data.Base.Purchase
    {
        #region Standard Methods
        public Purchase() { }
        public Purchase(Int64 loadID) { LoadID(loadID); }
        #endregion

        #region Add methods for displaying purchase information...

        public DataSet GetList(Int32 pageIndex, Int32 pageSize, String sortExpression, String filterClause, ref Int32 rowCount, ref Int32 totalRowCount)
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@pageIndex", SqlDbType.Int);
            param[0].Value = pageIndex;
            param[1] = new SqlParameter("@pageSize", SqlDbType.Int);
            param[1].Value = pageSize;
            param[2] = new SqlParameter("@sortExpression", SqlDbType.NVarChar, 100);
            param[2].Value = sortExpression;
            param[3] = new SqlParameter("@filterClause", SqlDbType.NVarChar, 4000);
            param[3].Value = filterClause;
            param[4] = new SqlParameter("@rowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, rowCount);
            param[5] = new SqlParameter("@totalRowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, totalRowCount);

            DataSet ds = SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Purchase_GetList", param);
            rowCount = (Int32)param[4].Value;
            totalRowCount = (Int32)param[5].Value;
            return ds;
        }

        public DataSet GetListByPurchaseID(Int64 purchaseID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@PurchaseID", SqlDbType.BigInt);
            param[0].Value = purchaseID;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Purchase_GetListByPurchaseID", param);

        }

        public DataSet GetAddress(Int64 purchaseID, Int32 addressTypeID)
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@PurchaseID", SqlDbType.BigInt);
            param[0].Value = purchaseID;
            param[1] = new SqlParameter("@AddressTypeID", SqlDbType.Int);
            param[1].Value = addressTypeID;
            param[2] = new SqlParameter("@returnCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, _returnCode);

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Purchase_GetAddress", param);
        }

        public DataSet GetDashboardList(Int32 resellerID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            if (resellerID > 0)
                param[0].Value = resellerID;
            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Purchase_GetDashboardList", param);
        }

        public DataSet GetSummaryReport(Int32 pageIndex, Int32 pageSize, String sortExpression, String filterClause, ref Int32 rowCount, ref Int32 totalRowCount)
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@pageIndex", SqlDbType.Int);
            param[0].Value = pageIndex;
            param[1] = new SqlParameter("@pageSize", SqlDbType.Int);
            param[1].Value = pageSize;
            param[2] = new SqlParameter("@sortExpression", SqlDbType.NVarChar, 100);
            param[2].Value = sortExpression;
            param[3] = new SqlParameter("@filterClause", SqlDbType.NVarChar, 4000);
            param[3].Value = filterClause;
            param[4] = new SqlParameter("@rowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, rowCount);
            param[5] = new SqlParameter("@totalRowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, totalRowCount);

            DataSet ds = SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Purchase_SummaryReport", param);
            rowCount = (Int32)param[4].Value;
            totalRowCount = (Int32)param[5].Value;
            return ds;
        }

        #endregion End of displaying purchase methods.

        public void ManageOrderStatusByPurchaseID(Int64 purchaseID, Int32 isActive, Int64 userID)
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@PurchaseID", SqlDbType.BigInt);
            param[0].Value = purchaseID;
            param[1] = new SqlParameter("@OrderStatus", SqlDbType.Int);
            param[1].Value = isActive;
            param[2] = new SqlParameter("@UserID", SqlDbType.BigInt);
            param[2].Value = userID;

            Int32 rows = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "Purchase_ManageOrderStatusByPurchaseID", param);
        }
    }
}

using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Library.DAL;
using Library.Utility;

namespace Data
{
    /// <summary>
    /// Summary description for Reseller
    /// </summary>
    public class Reseller : Data.Base.Reseller
    {
        #region Standard Methods
        public Reseller() { }
        public Reseller(Int64 loadID) { LoadID(loadID); }
        #endregion

        public Boolean CanAddNewProduct(Int32 resellerID)
        {
            Int32 result = 0;
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;
            param[1] = new SqlParameter("@Result", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, result);

            Int32 retVal = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "Reseller_CanAddNewProduct", param);
            result = Convert.ToInt32(param[1].Value);

            return (result == 1);
        }

        public Boolean CanAddNewUpsellProduct(Int32 resellerID)
        {
            Int32 result = 0;
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;
            param[1] = new SqlParameter("@Result", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, result);

            Int32 retVal = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "Reseller_CanAddNewUpsellProduct", param);
            result = Convert.ToInt32(param[1].Value);

            return (result == 1);
        }

        public Int32 GetResellerByUser(Int64 userID)
        {
            Int32 resellerID = 0;
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@UserID", SqlDbType.Int);
            param[0].Value = userID;
            param[1] = new SqlParameter("@ResellerID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, resellerID);

            Int32 retVal = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "Reseller_GetByUser", param);
            resellerID = Convert.ToInt32(param[1].Value);

            return resellerID;
        }

        public void InActiveRelationshipTables(Int32 resellerID, String strTableName)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ObjectID", SqlDbType.BigInt);
            param[0].Value = resellerID;
            param[1] = new SqlParameter("@TableName", SqlDbType.NVarChar, 50);
            param[1].Value = strTableName;

            Int32 count = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "usp_InActiveRelationships", param);
        }

        public Int32 isValidFloristCodeByReseller(String newFloristCode, Int32 resellerID)
        {
            Int32 isExistCode = 0;
            SqlParameter[] param = new SqlParameter[3];

            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;
            param[1] = new SqlParameter("@FloristCode", SqlDbType.NVarChar);
            param[1].Value = newFloristCode;

            param[2] = new SqlParameter("@ExistCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, isExistCode);

            isExistCode = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "Reseller_IsValidFloristCodeByReseller", param);
            isExistCode = Convert.ToInt32(param[2].Value);

            return isExistCode;
        }

        public Int32 CreateResellerClone(Int32 resellerID, String newFloristCode, String newReselerName, Int64 userID)
        {
            Int32 newResellerID = 0;
            SqlParameter[] param = new SqlParameter[5];

            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;
            param[1] = new SqlParameter("@NewFloristCode", SqlDbType.NVarChar);
            param[1].Value = newFloristCode;
            param[2] = new SqlParameter("@NewResellerName", SqlDbType.NVarChar);
            param[2].Value = newReselerName;
            param[3] = new SqlParameter("@UserID", SqlDbType.BigInt);
            param[3].Value = userID;
            param[4] = new SqlParameter("@NewResellerID", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, newResellerID);

            newResellerID = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "Reseller_Clone", param);
            newResellerID = Convert.ToInt32(param[4].Value);

            return newResellerID;
        }

        public void GenerateImagesByReseller(Int32 resellerID, String resellerDirPath, String categoryDirPath, String productDirPath,String  upsellCategoryDirPath)
        {

            if (resellerID <= 0)
                return;

            Data.Reseller reseller = new Data.Reseller(resellerID);

            if (reseller.ResellerID.value < 0)
                return;

            Byte[] ResellerFileByteData = (byte[])reseller.ResellerLogo.value;
            Byte[] SplashFileByteData = (byte[])reseller.SplashImage.value;

            #region Generating new reseller images...
 
            String fileResellerName = String.Format("ResellerLogo_{0}{1}", reseller.ResellerID.value.ToString(), ".png");
            String fileSplashName = String.Format("SplashImage_{0}{1}", reseller.ResellerID.value.ToString(), ".png");

            if (ResellerFileByteData != null)
            {
                if (File.Exists(resellerDirPath + fileResellerName))
                    File.Delete(resellerDirPath + fileResellerName);

                FileStream fs = new FileStream(resellerDirPath + fileResellerName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(ResellerFileByteData, 0, ResellerFileByteData.Length);
                fs.Close();
            }
                        
            if (SplashFileByteData != null)
            {
                if (File.Exists(resellerDirPath + fileSplashName))
                    File.Delete(resellerDirPath + fileSplashName);

                FileStream fs = new FileStream(resellerDirPath + fileSplashName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fs.Write(SplashFileByteData, 0, SplashFileByteData.Length);
                fs.Close();
            }

            #endregion Eng of Binding images..

            #region Generating new category images ...

            Data.Category cat = new Data.Category();
            DataSet dsCategory = cat.GetCategoryListByReseller(reseller.ResellerID.value);

            if (Util.IsValidDataSet(dsCategory))
            {
                foreach (DataRow drCat in dsCategory.Tables[0].Rows)
                {
                    Byte[] fileByteData = (byte[])drCat["ImageBinary"];
                    String imageExt = Convert.ToString(drCat["ImageExt"]);
                    Int32 catID = Convert.ToInt32(drCat["CategoryID"]);

                    String catfilePath = categoryDirPath;
                    String fileName = String.Format("CategoryImage_{0}{1}", catID, imageExt);
                     
                    if (File.Exists(catfilePath + fileName))
                        File.Delete(catfilePath + fileName);

                    if (fileByteData != null)
                    {
                        FileStream fs = new FileStream(catfilePath + fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                        fs.Write(fileByteData, 0, fileByteData.Length);
                        fs.Close();
                    }
                }
            }

            #endregion End of binding image

            #region Generating new product images...

            Data.Product product = new Data.Product();
            DataSet dsProducts = product.GetProductListByReseller(reseller.ResellerID.value);

            if (Util.IsValidDataSet(dsProducts))
            {
                #region Binding thumb image...
                
                foreach (DataRow drProd in dsProducts.Tables[0].Rows)
                {
                    Byte[] fileByteData = (byte[])drProd["ProductThumb"];
                    String imgThumbExt = Convert.ToString(drProd["ProductThumbExt"]);
                    Int32 prodID = Convert.ToInt32(drProd["ProductID"]);

                    String productFilePath = productDirPath;
                    String fileName = String.Format("ProductThumb_{0}{1}", prodID, imgThumbExt);

                    if (File.Exists(productFilePath + fileName))
                        File.Delete(productFilePath + fileName);

                    if (fileByteData != null)
                    {
                        FileStream fs = new FileStream(productFilePath + fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                        fs.Write(fileByteData, 0, fileByteData.Length);
                        fs.Close();
                    }
                }

                #endregion End of binding image

                #region Binding product original image...
                foreach (DataRow drProd in dsProducts.Tables[0].Rows)
                {
                    Byte[] fileByteData = (byte[])drProd["ProductImage"];
                    String imgThumbExt = Convert.ToString(drProd["ProductImageExt"]);
                    Int32 prodID = Convert.ToInt32(drProd["ProductID"]);

                    String productFilePath = productDirPath;
                    String fileName = String.Format("ProductImage_{0}{1}", prodID, imgThumbExt);

                    if (File.Exists(productFilePath + fileName))
                        File.Delete(productFilePath + fileName);

                    if (fileByteData != null)
                    {
                        FileStream fs = new FileStream(productFilePath + fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                        fs.Write(fileByteData, 0, fileByteData.Length);
                        fs.Close();
                    }
                }
                #endregion End of binding image
            }
            #endregion

            #region Generate UpsellCategory images ...

            Data.UpsellCategory uc = new Data.UpsellCategory();
            DataSet dsUpsellCategory = uc.GetUpsellCategoryListByReseller(resellerID);

            if (Util.IsValidDataSet(dsUpsellCategory))
            {
                foreach (DataRow drCat in dsUpsellCategory.Tables[0].Rows)
                {
                    Byte[] fileByteData = (byte[])drCat["ImageBinary"];
                    String imageExt = Convert.ToString(drCat["ImageExt"]);
                    Int32 catID = Convert.ToInt32(drCat["UpsellCategoryID"]);                      
                    String fileName = String.Format("CategoryImage_{0}{1}", catID, imageExt);
                    
                    if (File.Exists(upsellCategoryDirPath + fileName))
                        File.Delete(upsellCategoryDirPath + fileName);

                    if (fileByteData != null)
                    {
                        FileStream fs = new FileStream(upsellCategoryDirPath + fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                        fs.Write(fileByteData, 0, fileByteData.Length);
                        fs.Close();
                    }
                }
            }
            #endregion End of binding image
        }

    }
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;
using Library.Utility;
using Library.AppSettings;

namespace Data
{
	/// <summary>
	/// Summary description for ServerUpdates
	/// </summary>
	public class ServerUpdates : Data.Base.ServerUpdates
	{
	    #region Standard Methods
		public ServerUpdates(){}
		public ServerUpdates(int loadID){LoadID(loadID);}
	    #endregion


        public void UpdateStatus(Int32 resellerID, String entityName)
        {
            Data.ServerUpdates su = new ServerUpdates();

            if (resellerID > 0)
            {
                DataSet ds = su.GetUpdatesByReseller(resellerID);

                su.ResellerID.value = resellerID;
                if (Util.IsValidDataSet(ds))
                {
                    Int32 serverUpdatesID = Util.GetDataInt32(Util.GetDataTable(ds), 0, "serverupdatesid");
                    su.LoadID(serverUpdatesID);
                }

                switch (entityName.ToLower().Trim())
                {
                    case "resellersettings":
                        su.ResellerSettingsUpdatedDate.value = Util.GetServerDate();
                        break;

                    case "blockdates":
                        su.BlockDatesUpdatedDate.value = Util.GetServerDate();
                        break;

                    case "coupon":
                        su.CouponUpdatedDate.value = Util.GetServerDate();
                        break;

                    case "specialoffer":
                        su.SpecialOfferUpdatedDate.value = Util.GetServerDate();
                        break;

                    case "colortheme":
                        su.ColorThemeDate.value = Util.GetServerDate();
                        break;

                    case "category":
                        su.CategoryUpdatedDate.value = Util.GetServerDate();
                        break;

                    case "product":
                        su.ProductUpdatedDate.value = Util.GetServerDate();
                        break; 
  
                    case "upsellcategory":
                        su.UpsellCategoryUpdatedDate.value = Util.GetServerDate();
                        break;

                    case "upsellproduct":
                        su.UpsellProductUpdatedDate.value = Util.GetServerDate();
                        break;  

                    default:
                        su.ResellerSettingsUpdatedDate.value = Util.GetServerDate();
                        su.BlockDatesUpdatedDate.value = Util.GetServerDate();
                        su.CouponUpdatedDate.value = Util.GetServerDate();
                        su.ColorThemeDate.value = Util.GetServerDate();
                        break;
                }

                su.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
                su.Save();
            }
        }


        public DataSet GetUpdatesByReseller(Int32 resellerID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;
            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "ServerUpdates_GetByReseller", param);
        }

        public void UpdateStatusColorTheme(Int32 colorThemeID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ColorThemeID", SqlDbType.Int);
            param[0].Value = colorThemeID;

            Int32 ret = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "ServerUpdates_UpdateStatusColorTheme", param);
        }
    }
}

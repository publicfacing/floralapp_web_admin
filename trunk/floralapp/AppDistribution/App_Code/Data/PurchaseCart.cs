using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for PurchaseCart
	/// </summary>
	public class PurchaseCart : Data.Base.PurchaseCart
	{
	    #region Standard Methods
		public PurchaseCart(){}
		public PurchaseCart(int loadID){LoadID(loadID);}
	    #endregion

        public DataSet GetProductListByPurchaseID(Int64 purchaseID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@PurchaseID", SqlDbType.BigInt);
            param[0].Value = purchaseID;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "PurchaseCart_GetProductListByPurchaseID", param);

        }
    }
}

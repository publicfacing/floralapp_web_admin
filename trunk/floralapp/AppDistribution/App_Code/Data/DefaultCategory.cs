using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for DefaultCategory
	/// </summary>
	public class DefaultCategory : Data.Base.DefaultCategory
	{
	    #region Standard Methods
		public DefaultCategory(){}
		public DefaultCategory(int loadID){LoadID(loadID);}
	    #endregion
	}
}

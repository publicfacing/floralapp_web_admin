using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;
using Library.Utility;

namespace Data
{
	/// <summary>
	/// Summary description for Address
	/// </summary>
	public class Address : Data.Base.Address
	{
	    #region Standard Methods
		public Address(){}
		public Address(Int64 loadID){LoadID(loadID);}
	    #endregion

        public Boolean IsExist(Int64 userID, Int32 addressTypeID, ref DataTable dtResult)
        {
            Boolean isExist = false;

            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@UserID", SqlDbType.BigInt);
            param[0].Value = userID;
            param[1] = new SqlParameter("@AddressTypeID", SqlDbType.Int);
            param[1].Value = addressTypeID;

            DataSet ds = SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Address_IsExist", param);
            if (Util.IsValidDataSet(ds))
            {
                dtResult = Util.GetDataTable(ds);
                isExist = true;
            }
            return isExist;
        }
	}
}

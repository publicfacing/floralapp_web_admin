using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for AddressType
	/// </summary>
	public class AddressType : Data.Base.AddressType
	{
	    #region Standard Methods
		public AddressType(){}
		public AddressType(int loadID){LoadID(loadID);}
	    #endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for Category
	/// </summary>
	public class Category : Data.Base.Category
	{
	    #region Standard Methods
		public Category(){}
		public Category(int loadID){LoadID(loadID);}
	    #endregion

        public Boolean PopulateDefaultCategory(Int32 resellerID)
        {
            Int32 returnCode = 0;
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;            
            param[1] = new SqlParameter("@returnCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, returnCode);
                        
            Int32 retVal = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "Category_PopulateDefaultCategory", param);
            returnCode = (Int32)param[1].Value;
            
            return (returnCode == 0); 
        }

        public DataSet GetCategoryListByReseller(Int32 resellerID)
        { 
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;
           
            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Category_GetListByReseller", param);
             
        }         
    }
}

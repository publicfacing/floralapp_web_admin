using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for UsersInSession
	/// </summary>
	public class UsersInSession : Data.Base.UsersInSession
	{
	    #region Standard Methods
		public UsersInSession(){}
		public UsersInSession(Int64 loadID){LoadID(loadID);}
	    #endregion

        public Int32 GetCount(Int32 deviceTypeID)
        {
            Int32 rowCount = 0; 
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@DeviceTypeID", SqlDbType.Int);
            param[0].Value = deviceTypeID;
            param[1] = new SqlParameter("@rowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, rowCount);

            Int32 retVal = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "UsersInSession_GetDeviceCount", param);
            rowCount = (Int32)param[1].Value;
 
            return rowCount;
        }
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for DeviceSetting
	/// </summary>
	public class DeviceSetting : Data.Base.DeviceSetting
	{
	    #region Standard Methods
		public DeviceSetting(){}
		public DeviceSetting(int loadID){LoadID(loadID);}
	    #endregion

        public DataSet GetListByReseller(Int32 resellerID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "DeviceSetting_GetListByReseller", param);
        }
        
        public DataSet GetRegisterGoogleRegistrationID(string DeviceID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@DeviceID", SqlDbType.NVarChar);
            param[0].Value = DeviceID;
            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "DeviceSetting_GetRegisterGoogleRegistrationID", param);
        }

    }
}

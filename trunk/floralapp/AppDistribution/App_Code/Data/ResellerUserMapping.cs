using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;
using Library.Utility;

namespace Data
{
	/// <summary>
	/// Summary description for ResellerUserMapping
	/// </summary>
	public class ResellerUserMapping : Data.Base.ResellerUserMapping
	{
	    #region Standard Methods
		public ResellerUserMapping(){}
		public ResellerUserMapping(long loadID){LoadID(loadID);}
	    #endregion

        public Int64 GetActiveResellerUserMappingID(Int64 userID)
        {
            Int64 resellerUserMappingID = 0;
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@UserID", SqlDbType.BigInt);
            param[0].Value = userID;

            DataSet ds = SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "ResellerUserMapping_GetByUser", param);

            if (Util.IsValidDataSet(ds))
            {
                resellerUserMappingID = Util.GetDataInt64(Util.GetDataTable(ds), 0, "ResellerUserMappingID");
            }

            return resellerUserMappingID;
        }
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;
using System.IO;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Security.Authentication;
using System.Net;
using System.Text;
using System.Web;
using Library.Utility;
using System.Configuration;

namespace Data
{
    /// <summary>
    /// Summary description for SpecialOffer
    /// </summary>
    public class SpecialOffer : Data.Base.SpecialOffer
    {
        #region Standard Methods
        public SpecialOffer() { }
        public SpecialOffer(int loadID) { LoadID(loadID); }
        #endregion

        public DataSet GetSpecialOfferListByReseller(Int32 resellerID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "SpecialOffer_GetListByReseller", param);
        }

        public DataSet GetDashboardList(Int32 resellerID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            if (resellerID > 0)
                param[0].Value = resellerID;
            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "SpecialOffer_GetDashboardList", param);
        }

        public void InActiveRelationshipTables(Int32 specialOfferID, String strTableName)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ObjectID", SqlDbType.BigInt);
            param[0].Value = specialOfferID;
            param[1] = new SqlParameter("@TableName", SqlDbType.NVarChar, 50);
            param[1].Value = strTableName;

            Int32 count = SqlPipe.ExecuteNonQuery(SqlConnect.GetConnectionString(), "usp_InActiveRelationships", param);
        }

        public void SendPushNotification_iPhone(String deviceTokenID, String message, String certFilePath)
        {
            try
            {// Create a TCP/IP client socket.
                using (TcpClient client = new TcpClient())
                {
                    String iPhonePushGateway = ConfigurationManager.AppSettings["iPhonePushGateway"]; //gateway.sandbox.push.apple.com
                    String certPassword = ConfigurationManager.AppSettings["iPhoneCertificatePassword"];
                    client.Connect(iPhonePushGateway, 2195);
                    String certficate = certFilePath;

                    using (NetworkStream networkStream = client.GetStream())
                    {
                        //X509Certificate clientCertificate = new X509Certificate(certficate, String.Empty);
                        X509Certificate2 clientCertificate = new X509Certificate2(certficate, certPassword, X509KeyStorageFlags.MachineKeySet);
                        X509Certificate2Collection clientCertificateCollection = new X509Certificate2Collection(new X509Certificate2[1] { clientCertificate });

                        // Create an SSL stream that will close the client's stream.
                        SslStream sslStream = new SslStream(
                            client.GetStream(),
                            false,
                            new RemoteCertificateValidationCallback(ValidateServerCertificate),
                            null
                            );

                        String logMsg = String.Empty;

                        try
                        {
                            sslStream.AuthenticateAsClient(iPhonePushGateway, clientCertificateCollection, SslProtocols.Default, false);
                            logMsg = "iPhone Notification - Sent Successfully.";
                        }
                        catch (Exception e)
                        {
                            logMsg = String.IsNullOrEmpty(e.Message) ? "iPhone Notification - Error" : String.Format("iPhone Notification - Error: {0}", e.Message);
                            client.Close();
                            return;
                        }

                        Int32 logNotification = Util.GetDataInt32(String.IsNullOrEmpty(ConfigurationManager.AppSettings["LogNotification"]) ? "0" : ConfigurationManager.AppSettings["LogNotification"].ToString());
                        if (logNotification == 1)
                            Log.WriteLog(1, "SendPushNotification_iPhone", String.Format("Special Offer :  {0}", message), logMsg, Util.GetServerDate());

                        String cToken = deviceTokenID;
                        String cAlert = message;
                        int iBadge = 1;

                        // Ready to create the push notification
                        byte[] buf = new byte[256];
                        MemoryStream ms = new MemoryStream();
                        BinaryWriter bw = new BinaryWriter(ms);
                        bw.Write(new byte[] { 0, 0, 32 });

                        byte[] deviceToken = HexToData(cToken);
                        bw.Write(deviceToken);

                        bw.Write((byte)0);

                        // Create the APNS payload - new.caf is an audio file saved in the application bundle on the device
                        //string msg = "{\"aps\":{\"alert\":\"" + cAlert + "\",\"badge\":" + iBadge.ToString() + ",\"sound\":\"new.caf\"}}";

                        string msg = "{\"aps\":{\"badge\":" + iBadge.ToString() + "}}";

                        //                String msg = @"{ ""aps"": { 
                        //                                     ""badge"": 10, 
                        //                                     ""alert"": ""Let me know if you get this alert! dhananjay..."", 
                        //                                     ""sound"": ""cat.caf"" 
                        //                                } 
                        //                            }";

                        // Write the data out to the stream
                        bw.Write((byte)msg.Length);
                        bw.Write(msg.ToCharArray());
                        bw.Flush();

                        if (sslStream != null)
                        {
                            sslStream.Write(ms.ToArray());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Data.ErrorLog el = new Data.ErrorLog();
                el.ModuleInfo.value = "SaveNotification : Send_iPhoneNotification()";
                if (ex.InnerException != null)
                    el.Message.value = ex.InnerException.Message;
                else
                    el.Message.value = ex.ToString();
                el.SmallMessage.value = ex.Message;
                el.Save();
            }
        }

        // Used to convert device token from string to byte[]
        private static byte[] HexToData(string hexString)
        {
            if (hexString == null)
                return null;

            if (hexString.Length % 2 == 1)
                hexString = '0' + hexString; // Up to you whether to pad the first or last byte

            byte[] data = new byte[hexString.Length / 2];

            for (int i = 0; i < data.Length; i++)
                data[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);

            return data;
        }

        public static bool ValidateServerCertificate(object sender,
             X509Certificate certificate, X509Chain chain,
             SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors ==
               SslPolicyErrors.RemoteCertificateChainErrors)
            {
                return false;
            }
            else if (sslPolicyErrors ==
               SslPolicyErrors.RemoteCertificateNameMismatch)
            {
                System.Security.Policy.Zone z =
                   System.Security.Policy.Zone.CreateFromUrl
                   (((HttpWebRequest)sender).RequestUri.ToString());
                if (z.SecurityZone ==
                   System.Security.SecurityZone.Intranet ||
                   z.SecurityZone ==
                   System.Security.SecurityZone.MyComputer)
                {
                    return true;
                }
                return false;
            }
            return true;
        }

        public void SendPushNotification_Android(String registrationID, String data)
        {
            ServicePointManager.ServerCertificateValidationCallback += delegate(
                                           object sender,
                                           X509Certificate certificate,
                                           X509Chain chain,
                                           SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

            string collapseKey = Guid.NewGuid().ToString("n");
            String Auth = GetGoogleAuthCode();

            string url = ConfigurationManager.AppSettings["AndroidPushGateway"]; //"https://android.apis.google.com/c2dm/send";
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Headers.Add("Authorization", "GoogleLogin auth=" + Auth); //registrationID);//Auth);

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("registration_id={0}", registrationID);//metaoptionllc@gmail.com
            sb.AppendFormat("&collapse_key={0}", collapseKey);
            sb.AppendFormat("&data.payload={0}", data);

            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] buffer = encoding.GetBytes(sb.ToString());

            Stream newStream = request.GetRequestStream();
            newStream.Write(buffer, 0, buffer.Length);
            newStream.Close();
            String logMsg = String.Empty;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
               
                Stream resStream = response.GetResponseStream();
                StreamReader sr = new StreamReader(resStream);
                String strResponse = sr.ReadToEnd();
                sr.Close();
                resStream.Close();
                logMsg = "Notification sent succesfully" + strResponse;
            }
            else { logMsg = "There was an error"; }
            Int32 logNotification = Util.GetDataInt32(String.IsNullOrEmpty(ConfigurationManager.AppSettings["LogNotification"]) ? "0" : ConfigurationManager.AppSettings["LogNotification"].ToString());
            if (logNotification == 1)
                Log.WriteLog(1, "SendPushNotification_Android", String.Format("Special Offer :  {0}", data), logMsg, Util.GetServerDate());
        }

        public String GetGoogleAuthCode() {
           
            string authUrl = "https://www.google.com/accounts/ClientLogin";

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(authUrl);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("accountType={0}", "GOOGLE");
            sb.AppendFormat("&Email={0}", ConfigurationManager.AppSettings["AndroidPushEmail"]);
            sb.AppendFormat("&Passwd={0}", ConfigurationManager.AppSettings["AndroidPushEmailPassword"]);
            sb.AppendFormat("&service={0}", "ac2dm");
            sb.AppendFormat("&source={0}", "floralapp");

            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] buffer = encoding.GetBytes(sb.ToString());

            Stream newStream = request.GetRequestStream();
            newStream.Write(buffer, 0, buffer.Length);
            newStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream resStream = response.GetResponseStream();
                StreamReader sr = new StreamReader(resStream);
                string line;
                String authToken = String.Empty; 
                while ((line = sr.ReadLine()) != null)
                {
                    if(line.Contains("Auth="))
                        authToken = line.Substring(5);
                }
                sr.Close();
                resStream.Close();
                GoogleAuthToken gat = new GoogleAuthToken();
                gat.SetToken(authToken);

                return authToken;
            }
            return String.Empty;
        }
    }
}

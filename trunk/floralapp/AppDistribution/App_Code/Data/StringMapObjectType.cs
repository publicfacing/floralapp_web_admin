using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for StringMapObjectType
	/// </summary>
	public class StringMapObjectType : Data.Base.StringMapObjectType
	{
	    #region Standard Methods
		public StringMapObjectType(){}
		public StringMapObjectType(int loadID){LoadID(loadID);}
	    #endregion
	}
}

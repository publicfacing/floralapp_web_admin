using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for PurchaseCoupon
	/// </summary>
	public class PurchaseCoupon : Data.Base.PurchaseCoupon
	{
	    #region Standard Methods
		public PurchaseCoupon(){}
		public PurchaseCoupon(int loadID){LoadID(loadID);}
	    #endregion

        /*
        public DataSet GetCouponUsageByReseller(Int32 resellerID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Report_GetCouponUsage", param);
        }*/

        public DataSet GetCouponUsageReport(Int32 pageIndex, Int32 pageSize, String sortExpression, String filterClause, ref Int32 rowCount, ref Int32 totalRowCount)
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@pageIndex", SqlDbType.Int);
            param[0].Value = pageIndex;
            param[1] = new SqlParameter("@pageSize", SqlDbType.Int);
            param[1].Value = pageSize;
            param[2] = new SqlParameter("@sortExpression", SqlDbType.NVarChar, 100);
            param[2].Value = sortExpression;
            param[3] = new SqlParameter("@filterClause", SqlDbType.NVarChar, 4000);
            param[3].Value = filterClause;
            param[4] = new SqlParameter("@rowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, rowCount);
            param[5] = new SqlParameter("@totalRowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, totalRowCount);

            DataSet ds = SqlPipe.ExecuteDataset(_connectionString, "Report_GetCouponUsage", param);
            rowCount = (Int32)param[4].Value;
            totalRowCount = (Int32)param[5].Value;
            return ds;
        }

        public DataSet GetCouponReportByReseller(Int32 resellerID, String couponCode)
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;
            param[1] = new SqlParameter("@CouponCode", SqlDbType.NVarChar);
            param[1].Value = couponCode;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "Report_GetCouponReportByReseller", param);
        }
    }
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for Country
	/// </summary>
	public class Country : Data.Base.Country
	{
	    #region Standard Methods
		public Country(){}
		public Country(int loadID){LoadID(loadID);}
	    #endregion
	}
}

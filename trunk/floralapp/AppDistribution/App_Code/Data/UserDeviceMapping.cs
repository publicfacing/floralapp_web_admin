using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for UserDeviceMapping
	/// </summary>
	public class UserDeviceMapping : Data.Base.UserDeviceMapping
	{
	    #region Standard Methods
		public UserDeviceMapping(){}
		public UserDeviceMapping(int loadID){LoadID(loadID);}
	    #endregion
	}
}

using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for DeviceType
	/// </summary>
	public class DeviceType : Data.Base.DeviceType
	{
	    #region Standard Methods
		public DeviceType(){}
		public DeviceType(int loadID){LoadID(loadID);}
	    #endregion
	}
}

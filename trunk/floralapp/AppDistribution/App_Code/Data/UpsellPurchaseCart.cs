using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for UpsellPurchaseCart
	/// </summary>
	public class UpsellPurchaseCart : Data.Base.UpsellPurchaseCart
	{
	    #region Standard Methods
		public UpsellPurchaseCart(){}
		public UpsellPurchaseCart(Int64 loadID){LoadID(loadID);}
	    #endregion

        public DataSet GetUpsellProductListByPurchaseID(Int64 purchaseID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@PurchaseID", SqlDbType.BigInt);
            param[0].Value = purchaseID;

            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "UpsellPurchaseCart_GetProductListByPurchaseID", param);

        }
    }
}

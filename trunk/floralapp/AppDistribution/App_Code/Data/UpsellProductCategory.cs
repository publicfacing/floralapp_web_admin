using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for UpsellProductCategory
	/// </summary>
	public class UpsellProductCategory : Data.Base.UpsellProductCategory
	{
	    #region Standard Methods
		public UpsellProductCategory(){}
		public UpsellProductCategory(int loadID){LoadID(loadID);}
	    #endregion
	}
}

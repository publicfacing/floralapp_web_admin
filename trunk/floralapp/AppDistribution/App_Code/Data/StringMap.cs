using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for StringMap
	/// </summary>
	public class StringMap : Data.Base.StringMap
	{
	    #region Standard Methods
		public StringMap(){}
		public StringMap(int loadID){LoadID(loadID);}
	    #endregion

        public DataSet GetListByObjectTypeID(Int32 objTypeID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ObjectTypeID", SqlDbType.Int);
            param[0].Value = objTypeID;
            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "StringMap_GetListByObjectTypeID", param);
        }
    }
}

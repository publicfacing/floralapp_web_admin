﻿using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
    /// <summary>
    /// Summary description for ResellerSetting
    /// </summary>
    public class ResellerMerchantSetting : Data.Base.ResellerMerchantSetting
    {
        #region Standard Methods
        public ResellerMerchantSetting() { }
        public ResellerMerchantSetting(int loadID) { LoadID(loadID); }
        #endregion
    }
}

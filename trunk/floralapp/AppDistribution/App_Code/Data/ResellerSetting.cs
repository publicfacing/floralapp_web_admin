using System;
using System.Data;
using System.Data.SqlClient;
using Library.DAL;

namespace Data
{
	/// <summary>
	/// Summary description for ResellerSetting
	/// </summary>
	public class ResellerSetting : Data.Base.ResellerSetting
	{
	    #region Standard Methods
		public ResellerSetting(){}
		public ResellerSetting(int loadID){LoadID(loadID);}
	    #endregion

        public DataSet GetSetting(Int32 resellerID)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ResellerID", SqlDbType.Int);
            param[0].Value = resellerID;
            return SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "ssp_GetResellerSetting", param);
        }
	}
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Net;
using System.IO;
using System.Configuration;

/// <summary>
/// Summary description for PaymentGateway
/// </summary>
/// 
namespace PaymentGateway
{
    public class AuthorizeNet
    {
        public AuthorizeNet()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public String Pay(Hashtable post_values)
        {
            // By default, this code is designed to post to test server for
            // developer accounts: https://test.authorize.net/gateway/transact.dll
            // for real accounts (even in test mode), please make sure that you are
            // posting to: https://secure.authorize.net/gateway/transact.dll

            String authorizeNetWorkingMode = ConfigurationManager.AppSettings["AuthorizeNetWorkingMode"].ToString();
            String authorizeNetUrl = ConfigurationManager.AppSettings["AuthorizeNetUrlTest"].ToString();
            String authorizeNetLogin = ConfigurationManager.AppSettings["AuthorizeNetLoginTest"].ToString();
            String authorizeNetTranKey = ConfigurationManager.AppSettings["AuthorizeNetTranKeyTest"].ToString();

            if (Convert.ToInt32(authorizeNetWorkingMode) == 1)
            {
                authorizeNetUrl = ConfigurationManager.AppSettings["AuthorizeNetUrl"].ToString();
                authorizeNetLogin = ConfigurationManager.AppSettings["AuthorizeNetLogin"].ToString();
                authorizeNetTranKey = ConfigurationManager.AppSettings["AuthorizeNetTranKey"].ToString();
            }

            String post_url = authorizeNetUrl;

            //the API Login ID and Transaction Key must be replaced with valid values
            post_values.Add("x_login", authorizeNetLogin);
            post_values.Add("x_tran_key", authorizeNetTranKey);

            post_values.Add("x_delim_data", "TRUE");
            post_values.Add("x_delim_char", '|');
            post_values.Add("x_relay_response", "FALSE");

            post_values.Add("x_type", "AUTH_CAPTURE");
            post_values.Add("x_method", "CC");


            // This section takes the input fields and converts them to the proper format
            // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
            String post_string = "";
            foreach (DictionaryEntry field in post_values)
            {
                post_string += field.Key + "=" + field.Value + "&";
            }
            post_string = post_string.TrimEnd('&');

            // create an HttpWebRequest object to communicate with Authorize.net

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(post_url);
            objRequest.Method = "POST";
            objRequest.ContentLength = post_string.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(post_string);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            String post_response;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                post_response = responseStream.ReadToEnd();
                responseStream.Close();
            }

            // the response string is broken into an array
            // The split character specified here must match the delimiting character specified above
            Array response_array = post_response.Split('|');

            return post_response;
        }

        public String VoidTransaction(Hashtable post_values)
        {
            // By default, this code is designed to post to test server for
            // developer accounts: https://test.authorize.net/gateway/transact.dll
            // for real accounts (even in test mode), please make sure that you are
            // posting to: https://secure.authorize.net/gateway/transact.dll


            String authorizeNetWorkingMode = ConfigurationManager.AppSettings["AuthorizeNetWorkingMode"].ToString();
            String authorizeNetUrl = ConfigurationManager.AppSettings["AuthorizeNetUrlTest"].ToString();
            String authorizeNetLogin = ConfigurationManager.AppSettings["AuthorizeNetLoginTest"].ToString();
            String authorizeNetTranKey = ConfigurationManager.AppSettings["AuthorizeNetTranKeyTest"].ToString();

            if (Convert.ToInt32(authorizeNetWorkingMode) == 1)
            {
                authorizeNetUrl = ConfigurationManager.AppSettings["AuthorizeNetUrl"].ToString();
                authorizeNetLogin = ConfigurationManager.AppSettings["AuthorizeNetLogin"].ToString();
                authorizeNetTranKey = ConfigurationManager.AppSettings["AuthorizeNetTranKey"].ToString();
            }

            String post_url = authorizeNetUrl;

            //the API Login ID and Transaction Key must be replaced with valid values
            post_values.Add("x_login", authorizeNetLogin);
            post_values.Add("x_tran_key", authorizeNetTranKey);

            post_values.Add("x_delim_data", "TRUE");
            post_values.Add("x_delim_char", '|');
            post_values.Add("x_relay_response", "FALSE");

            //For Cancellation
            post_values.Add("x_type", "VOID");
            //post_values.Add("x_trans_id", transactionId);


            // This section takes the input fields and converts them to the proper format
            // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
            String post_string = "";
            foreach (DictionaryEntry field in post_values)
            {
                post_string += field.Key + "=" + field.Value + "&";
            }
            post_string = post_string.TrimEnd('&');

            // create an HttpWebRequest object to communicate with Authorize.net

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(post_url);
            objRequest.Method = "POST";
            objRequest.ContentLength = post_string.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(post_string);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            String post_response;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                post_response = responseStream.ReadToEnd();
                responseStream.Close();
            }

            // the response string is broken into an array
            // The split character specified here must match the delimiting character specified above
            Array response_array = post_response.Split('|');

            return post_response;
        }
    }
}

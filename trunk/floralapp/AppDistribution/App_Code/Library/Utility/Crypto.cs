using System;
using System.Collections.Specialized;
using System.Text;
using System.Security.Cryptography;
using System.IO;

/// <summary>
/// Summary description for Crypto
/// </summary>

namespace Library.Utility
{
    public class Crypto
    {
        public Crypto() { }

        #region DES Encrypt/Decrypt
        /// <summary>
        ///    Decrypts  a particular string with a specific Key
        /// </summary>
        public string DecryptDes(string stringToDecrypt, string sEncryptionKey)
        {
            stringToDecrypt = stringToDecrypt.Replace("plus", "+");
            byte[] key = { };
            byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
            byte[] inputByteArray = new byte[stringToDecrypt.Length];
            try
            {
                key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(stringToDecrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                Encoding encoding = Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch
            {
                return (string.Empty);
            }
        }

        /// <summary>
        ///   Encrypts  a particular string with a specific Key
        /// </summary>
        /// <param name="stringToEncrypt"></param>
        /// <param name="sEncryptionKey"></param>
        /// <returns></returns>
        public string EncryptDes(string stringToEncrypt, string sEncryptionKey)
        {
            byte[] key = { };
            byte[] IV = { 10, 20, 30, 40, 50, 60, 70, 80 };
            byte[] inputByteArray; //Convert.ToByte(stringToEncrypt.Length) 

            try
            {
                key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                //string retstr = Convert.ToBase64String(ms.ToArray());
                //retstr = retstr.Replace("+", "plus");
                //return retstr;

                string retstr = ToHex(ms.ToArray());
                //retstr = retstr.Replace("+", "plus");
                return retstr;

            }
            catch
            {
                return (string.Empty);
            }
        }

        public static string EncryptDesToHex(string stringToEncrypt, string sEncryptionKey)
        {
            byte[] key = { };
            byte[] IV = { 0x08, 0x07, 0x06, 0x05, 0x01, 0x02, 0x03, 0x04 };

            byte[] inputByteArray;
            byte[] outputByteArray;

            try
            {
                key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                outputByteArray = ms.ToArray();
                string retstr = ToHex(outputByteArray);
                return retstr;
            }
            catch
            {
                return (string.Empty);
            }
        }

        public static string DecryptDesFromHex(string stringToDecrypt, string sEncryptionKey)
        {
            stringToDecrypt = Convert.ToBase64String(FromHex(stringToDecrypt));
            byte[] key = { };
            byte[] IV = { 0x08, 0x07, 0x06, 0x05, 0x01, 0x02, 0x03, 0x04 };
            byte[] inputByteArray = new byte[stringToDecrypt.Length];

            try
            {
                key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(stringToDecrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                Encoding encoding = Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch
            {
                return (string.Empty);
            }
        }

        #endregion

        #region Encrypt using MD5
        public string Md5Encrypt(string stringToEncrypt)
        {
            byte[] inputByteArray;
            byte[] outputByteArray;

            try
            {
                inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                MemoryStream ms = new MemoryStream();

                string retstr = string.Empty;
                MD5 objMD5 = MD5.Create();
                outputByteArray = objMD5.ComputeHash(inputByteArray);
                retstr = ToHex(outputByteArray);
                return retstr;
            }
            catch
            {
                return (string.Empty);
            }
        }
        #endregion

        #region AES Encrypt/Decrypt
        public static string EncryptAes(string stringToEncrypt, string sEncryptionKey)
        {
            if ((stringToEncrypt == null) || (stringToEncrypt == "") || (sEncryptionKey == null) || (sEncryptionKey == ""))
                return "";


            byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
            byte[] outputByteArray;

            PasswordDeriveBytes pdb = new PasswordDeriveBytes(sEncryptionKey, Encoding.UTF8.GetBytes("Salt"));
            RijndaelManaged rm = new RijndaelManaged();
            rm.Padding = PaddingMode.ISO10126;
            ICryptoTransform encryptor = rm.CreateEncryptor(pdb.GetBytes(16), pdb.GetBytes(16));
            using (MemoryStream msEncrypt = new MemoryStream())
            using (CryptoStream encStream = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
            {
                encStream.Write(inputByteArray, 0, inputByteArray.Length);
                encStream.FlushFinalBlock();
                outputByteArray = msEncrypt.ToArray();
            }

            return Convert.ToBase64String(outputByteArray);
        }

        public static string DecryptAes(string stringToDecrypt, string sEncryptionKey)
        {
            if ((stringToDecrypt == null) || (stringToDecrypt == "") || (sEncryptionKey == null) || (sEncryptionKey == ""))
                return "";

            byte[] inputByteArray = Convert.FromBase64String(stringToDecrypt);
            byte[] outputByteArray;

            PasswordDeriveBytes pdb = new PasswordDeriveBytes(sEncryptionKey, Encoding.UTF8.GetBytes("Salt"));
            RijndaelManaged rm = new RijndaelManaged();
            rm.Padding = PaddingMode.ISO10126;
            ICryptoTransform decryptor = rm.CreateDecryptor(pdb.GetBytes(16), pdb.GetBytes(16));
            using (MemoryStream msDecrypt = new MemoryStream(inputByteArray))
            using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
            {
                byte[] fromEncrypt = new byte[inputByteArray.Length];

                int read = csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
                if (read < fromEncrypt.Length)
                {
                    byte[] clearBytes = new byte[read];
                    Buffer.BlockCopy(fromEncrypt, 0, clearBytes, 0, read);
                    outputByteArray = clearBytes;
                }
                outputByteArray = fromEncrypt;
            }
            return Encoding.UTF8.GetString(outputByteArray);
        }

        #endregion

        private static string ToHex(Byte[] inputByte)
        {
            if ((inputByte == null) || (inputByte.Length == 0))
            {
                return "";
            }

            String HexFormat = "{0:X2}";
            StringBuilder sb = new StringBuilder();

            foreach (Byte b in inputByte)
            {
                sb.Append(String.Format(HexFormat, b));
            }
            return sb.ToString();
        }

        private static Byte[] FromHex(String hexEncoded)
        {
            Byte[] retByte = null;

            if ((hexEncoded == null) || (hexEncoded.Length == 0))
                return null;

            try
            {
                Int32 l = Convert.ToInt32(hexEncoded.Length / 2);
                int iTemp = l - 1;
                Byte[] b = new Byte[iTemp + 1];

                for (int i = 0; i <= iTemp; i++)
                {
                    b[i] = Convert.ToByte(hexEncoded.Substring(i * 2, 2), 16);
                }
                retByte = b;
            }
            catch
            {
            }

            return retByte;
        }

        public static string EncryptQueryString(String qs)
        {
            return Crypto.EncryptDesToHex(qs, "myquery1234");
        }

        public static string DecryptQueryString(String qs)
        {
            return Crypto.DecryptDesFromHex(qs, "myquery1234");
        }

        public static string EncryptString(String str)
        {
            return Crypto.EncryptDesToHex(str, "mystring1234");
        }

        public static string DecryptString(String str)
        {
            return Crypto.DecryptDesFromHex(str, "mystring1234");
        }

        public static NameValueCollection ConvertNVC(string qs)
        {
            NameValueCollection nvc = new NameValueCollection();
            string[] nameValuePairs = qs.Split('&');

            for (int i = 0; i < nameValuePairs.Length; i++)
            {
                string[] nameValue = nameValuePairs[i].Split('=');

                if (nameValue.Length == 2)
                    nvc.Add(nameValue[0], nameValue[1]);
            }

            return nvc;
        }

    }
}
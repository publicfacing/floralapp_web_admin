﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;

using System.IO;
using Library.DAL;
using Library.Utility;
using Library.AppSettings;

/// <summary>
/// Summary description for Utility
/// </summary>

namespace Library.Utility
{
    public class Log
    {
        public Log()
        {
            //
            // TODO: Add constructor logic here
            //
        }
         
        /// <summary>
        /// Create Log 
        /// </summary>
        /// <param name="moduleInfo"></param>
        /// <param name="smallMessage"></param>
        /// <param name="message"></param> 
        public static void ErrorLog(String moduleInfo, String smallMessage, String message)
        {
            try
            {
                DateTime dtCurrent = Util.GetServerDate();

                //Add Log in FileSystem
                WriteLog(0, moduleInfo, smallMessage, message, dtCurrent);

                // Add Log in DB
                Data.ErrorLog el = new Data.ErrorLog();
                el.ApplicationType.value = (Int32) AppEnum.ApplicationType.WEBSITE;
                el.ModuleInfo.value = moduleInfo;
                el.SmallMessage.value = smallMessage;
                el.Message.value = message;
                el.DateAdded.value = Util.GetServerDate();
                el.Save();
            }
            catch { }
        }

        public static void WriteLog(Int32 type, String moduleInfo, String smallMessage, String message, DateTime dtCurrent)
        {
            try
            {
                String suffix = (type == 0) ? "Error-" : "Log-";
                String fileName = suffix + DateTime.Today.ToString("dd-MM-yy") + ".txt";
                String path = "~/Log/" + fileName;

                if (!File.Exists(System.Web.HttpContext.Current.Server.MapPath(path)))
                {
                    File.Create(System.Web.HttpContext.Current.Server.MapPath(path)).Close();
                }

                using (StreamWriter w = File.AppendText(System.Web.HttpContext.Current.Server.MapPath(path)))
                {
                    w.WriteLine("\r\n Log Entry On: {0}", DateTime.Now.ToString(CultureInfo.InvariantCulture));
                    w.WriteLine("\r\n Server Time: {0}", dtCurrent);
                    w.WriteLine("\r\n Log from : " + System.Web.HttpContext.Current.Request.Url.ToString());
                    w.WriteLine("\r\n Module Info: {0}", moduleInfo);

                    w.WriteLine("\r\n Small Message: {0}", smallMessage);
                    w.WriteLine("\r\n Message: {0}", message);

                    w.WriteLine("___________________________________________________");
                    w.Flush();
                    w.Close();
                }
            }
            catch { }
        }
    }
}

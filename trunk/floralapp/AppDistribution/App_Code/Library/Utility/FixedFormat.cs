﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using System.Text;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Summary description for FixedFormat
/// </summary>

namespace Library.Utility.Files
{
    public class FixedFormat
    {
        //public FixedFormat()
        //{
        //    //
        //    // TODO: Add constructor logic here
        //    //
        //}

        public static void Create(System.Xml.Linq.XElement CommandNode, DataTable Table, Stream outputStream, String fullfillmentCom)
        {
            StreamWriter Output = new StreamWriter(outputStream);
            //try
            //{
            int StartAt = CommandNode.Attribute("StartAt") != null ? int.Parse(CommandNode.Attribute("StartAt").Value) : 0;
            //Namespaces.Integration +

            var positions = from c in CommandNode.Descendants("Position")
                            orderby int.Parse(c.Attribute("Start").Value) ascending
                            select new
                            {
                                Name = c.Attribute("Name").Value.ToString(),
                                Start = int.Parse(c.Attribute("Start").Value) - StartAt,
                                Length = int.Parse(c.Attribute("Length").Value),
                                Justification = c.Attribute("Justification") != null ? c.Attribute("Justification").Value.ToLower() : "left",
                                PaddingChar = c.Attribute("Justification") != null ? c.Attribute("PaddingChar").Value.ToLower() : "space",
                            };

            int lineLength = positions.Last().Start + positions.Last().Length;
            Int64 prevPurchaseID = 0;
            Int64 addressType = 0;
            int giftSeq = 0;
            foreach (DataRow row in Table.Rows)
            {


                StringBuilder line = new StringBuilder(lineLength);
                foreach (var p in positions)
                {
                    try
                    {
                        if (fullfillmentCom == "cds")
                        {
                            //Gift Sequence calculation
                            if (p.Name == "GiftSequenceNumber" && Convert.ToInt64(row["PurchaseID"].ToString()) == prevPurchaseID && addressType != Convert.ToInt64(row["AddressTypeID"].ToString()) && Convert.ToInt64(row["AddressTypeID"].ToString()) == 12)
                            {
                                giftSeq = 0;
                                giftSeq = giftSeq + 1;
                                row[p.Name] = giftSeq.ToString();
                            }
                            else if (p.Name == "GiftSequenceNumber" && Convert.ToInt64(row["PurchaseID"].ToString()) == prevPurchaseID && addressType == Convert.ToInt64(row["AddressTypeID"].ToString()) && addressType == 12)
                            {
                                giftSeq = giftSeq + 1;
                                row[p.Name] = giftSeq.ToString();
                            }
                        }
                        String colValue = row[p.Name].ToString().ToUpper().Length > p.Length ? row[p.Name].ToString().ToUpper().Substring(0, p.Length) : row[p.Name].ToString().ToUpper();
                        line.Insert(p.Start,
                            p.Justification == "left" ? colValue.ToString().PadRight(p.Length, (p.PaddingChar != "space") ? p.PaddingChar.ToCharArray()[0] : ' ')
                                                      : colValue.ToString().PadLeft(p.Length, (p.PaddingChar != "space") ? p.PaddingChar.ToCharArray()[0] : ' ')
                            );
                    }
                    catch (Exception ex)
                    {
                    }
                }
                Output.WriteLine(line.ToString());
                if (fullfillmentCom == "cds")
                {
                    prevPurchaseID = Convert.ToInt64(row["PurchaseID"].ToString());
                    addressType = Convert.ToInt64(row["AddressTypeID"].ToString());
                }

            }
            Output.Flush();
            //}
            //catch
            //{
            //}

            Output.Close();
        }
    }
}

using System;
using System.Data;
using System.Web.UI.WebControls;
using System.IO;
using System.Globalization;
using System.Configuration;
using Library.DAL;

/// <summary>
/// Summary description for Util
/// </summary>

namespace Library.Utility
{

    public class Util
    {
        public Util()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static void HideError(Label lbl)
        {
            lbl.Visible = false;
        }

        public static void ShowError(Label lbl, String msg)
        {
            lbl.Text = msg;
            lbl.Visible = true;
        }

        public static Boolean IsValidDataSet(DataSet ds)
        {
            Boolean retVal = false;

            if ((ds != null) && (ds.Tables.Count > 0) && (ds.Tables[0].Rows.Count > 0))
            {
                retVal = true;
            }

            return retVal;
        }

        public static Boolean IsValidDataTable(DataTable dt)
        {
            Boolean retVal = false;

            if ((dt != null) && (dt.Rows.Count > 0))
            {
                retVal = true;
            }

            return retVal;
        } 

        public static DataTable GetDataTable(DataSet ds)
        { 
            return ds.Tables[0];
        }

        public static String GetDataString(DataTable dt, Int32 rowIndex, String columnName)
        {
           return Convert.IsDBNull(dt.Rows[rowIndex][columnName]) ? "" : dt.Rows[rowIndex][columnName].ToString();
        }

        public static Int32 GetDataInt32(DataTable dt, Int32 rowIndex, String columnName)
        {
            return Convert.IsDBNull(dt.Rows[rowIndex][columnName]) ? 0 : Int32.Parse(dt.Rows[rowIndex][columnName].ToString());
        }

        public static Int64 GetDataInt64(DataTable dt, Int32 rowIndex, String columnName)
        {
            return Convert.IsDBNull(dt.Rows[rowIndex][columnName]) ? 0 : Int64.Parse(dt.Rows[rowIndex][columnName].ToString());
        }

        public static Boolean GetDataBool(DataTable dt, Int32 rowIndex, String columnName)
        {
            return Convert.IsDBNull(dt.Rows[rowIndex][columnName]) ? false : Boolean.Parse(dt.Rows[rowIndex][columnName].ToString());
        }

        public static Decimal GetDataDecimal(DataTable dt, Int32 rowIndex, String columnName)
        {
            return Convert.IsDBNull(dt.Rows[rowIndex][columnName]) ? 0 : Convert.ToDecimal(dt.Rows[rowIndex][columnName].ToString());
        }

        public static Double GetDataDouble(DataTable dt, Int32 rowIndex, String columnName)
        {
            return Convert.IsDBNull(dt.Rows[rowIndex][columnName]) ? 0 : Convert.ToDouble(dt.Rows[rowIndex][columnName].ToString());
        }

        public static String GetDataString(String str)
        {
            return Convert.ToString(String.IsNullOrEmpty(str) ? "" : str);
        }

        public static Int32 GetDataInt32(String str)
        {
            return Convert.ToInt32(String.IsNullOrEmpty(str) ? "0" : str);
        }

        public static Decimal GetDataDecimal(String str)
        {
            return Convert.ToDecimal(String.IsNullOrEmpty(str) ? "0" : str);
        }

        public static Int64 GetDataInt64(String str)
        {
            return Convert.ToInt64(String.IsNullOrEmpty(str) ? "0" : str);
        }

        public static Boolean GetDataBool(String str)
        {
            return Convert.ToBoolean(String.IsNullOrEmpty(str) ? "0" : str);
        }

        public static DateTime GetDataDateTime(String str)
        {
            return Convert.ToDateTime(String.IsNullOrEmpty(str) ? "1/1/1900" : str);
        }
 
        /// <summary>
        /// Set text & values in drop down list by using a datasource.
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="dataSource"></param>
        /// <param name="dataTextField"></param>
        /// <param name="dataValueField"></param>
        public static void setValue(DropDownList ddl, Object dataSource, String dataTextField, String dataValueField)
        {
            ddl.DataSource = dataSource;
            ddl.DataTextField = dataTextField;
            ddl.DataValueField = dataValueField;
            ddl.DataBind();
        }

        /// <summary>
        /// Set text & values in check box list by using a datasource.
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="dataSource"></param>
        /// <param name="dataTextField"></param>
        /// <param name="dataValueField"></param>
        public static void setValue(CheckBoxList cbl, Object dataSource, String dataTextField, String dataValueField)
        {
            cbl.DataSource = dataSource;
            cbl.DataTextField = dataTextField;
            cbl.DataValueField = dataValueField;
            cbl.DataBind();
        }

        /// <summary>
        /// Set selected value in drop down list.
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="obj"></param>
        public static void setValue(DropDownList ddl, Object obj)
        {
            ddl.SelectedValue = (obj == null) ? "0" : obj.ToString();  
        }
        
        /// <summary>
        /// Bind data with grid view using data source.
        /// </summary>
        /// <param name="gv"></param>
        /// <param name="dataSource"></param>
        public static void setValue(GridView gv, Object dataSource)
        {
            gv.DataSource = dataSource;
            gv.DataBind();
        }
         
        /// <summary>
        /// Set value in a label server control.
        /// </summary>
        /// <param name="lbl"></param>
        /// <param name="obj"></param>
        public static void setValue(Label lbl, object obj)
        {
            lbl.Text = (obj == null) ? "" : obj.ToString();
        }

        /// <summary>
        /// Set value in text box server control.
        /// </summary>
        /// <param name="txt"></param>
        /// <param name="obj"></param>
        public static void setValue(TextBox txt, object obj)
        {
            txt.Text = (obj == null) ? "" : obj.ToString();
        }

        /// <summary>
        /// Set value in check box server control.
        /// </summary>
        /// <param name="cb"></param>
        /// <param name="isChecked"></param>
        public static void setValue(CheckBox cb, Boolean isChecked)
        {
            cb.Checked = isChecked;
        }

        /// <summary>
        /// Set value in HiddenField server control.
        /// </summary>
        /// <param name="hdf"></param>
        /// <param name="obj"></param>
        public static void setValue(HiddenField hdf, object obj)
        {
            hdf.Value = obj.ToString();
        }

        /// <summary>
        /// Get selected value fron the dropdoenlist server control.
        /// </summary>
        /// <param name="ddl"></param>
        /// <returns></returns>
        public static string getValue(DropDownList ddl)
        {
            return ddl.SelectedValue.Trim().ToLower();
        }

        /// <summary>
        /// Get selected text from the drop down list.
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string getValue(DropDownList ddl, String type)
        {
            return ddl.SelectedItem.Text.Trim();
        }

        /// <summary>
        /// Get value from textbox server control.
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static string getValue(TextBox txt)
        {
            return txt.Text.Replace("___-___-____","").Trim();
        }

        /// <summary>
        /// Get value from HiddenField server control.
        /// </summary>
        /// <param name="hdf"></param>
        /// <returns></returns>
        public static string getValue(HiddenField hdf)
        {
            return hdf.Value.Trim();
        }

        /// <summary>
        /// Get value from Label server control.
        /// </summary>
        /// <param name="lbl"></param>
        /// <returns></returns>
        public static string getValue(Label lbl)
        {
            return lbl.Text.Trim();
        }

        /// <summary>
        /// Get value from CheckBox server control.
        /// </summary>
        /// <param name="cb"></param>
        /// <returns></returns>
        public static bool getValue(CheckBox cb)
        {
            return cb.Checked;
        }

        /// <summary>
        /// Get current date time from the server.
        /// </summary>
        /// <returns></returns>
        public static DateTime GetServerDate()
        {
            String dateTime = string.Empty;
            dateTime = "1/1/1900";
            DataSet dsServerDateTime = SqlPipe.ExecuteDataset(SqlConnect.GetConnectionString(), "usp_GetServerDate", null);
            DataTable dtServerDateTime;
            if (Util.IsValidDataSet(dsServerDateTime))
            {
                dtServerDateTime = Util.GetDataTable(dsServerDateTime);
                dateTime = string.Format(Util.GetDataString(dtServerDateTime, 0, "ServerDateTime"), "YYYY-MM-DD HH24:MI:SS");
            }
            return Convert.ToDateTime(dateTime);
        }

        /// <summary>
        /// Check if the date is Valid or not.
        /// </summary>
        /// <returns></returns>
        public static Boolean IsValidDate(String date)
        {
            Boolean retval = false;
            try
            {
                DateTime dt = DateTime.Parse(date);
                retval = true;
            }
            catch 
            { 
            }
            return retval;
        }
         
        public static byte[] StringToByteArray(string str)
        {
            return Convert.FromBase64String(str);

        }
    }
}

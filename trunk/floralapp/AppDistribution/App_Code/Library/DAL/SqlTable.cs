
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Web.Security;


using Library;

namespace Library.DAL
{
    public enum ConcurrenyType { LastIn = 0, RowVer };

    /// <summary>
    /// Summary description for TableSql.
    /// </summary>
    public class TableSql
    {
        protected int _returnCode = 0;
        protected string _tableName = String.Empty;
        protected int _fieldCount = 0;
        protected int _pkField = 0;
        protected int _rowverField = -1;
        protected string _connectionString = String.Empty;
        protected SqlConnection _connection;
        protected TypeSql[] _fields = null;
        protected SqlParameter[] _parametersSql = null;
        public ConcurrenyType Concurrency = ConcurrenyType.RowVer;

        public TableSql()
        {
            if (_connectionString == String.Empty)
            {
                _connectionString = SqlConnect.GetConnectionString();
            }

            SetFields();
        }

        public TableSql(string ConnectionString)
        {
            _connectionString = ConnectionString;

            SetFields();
        }

        public virtual void SetFields()
        {

        }

        public void Fill(ref DataRow drRow)
        {
            //int x;
            for (int x = 0; x < _fieldCount; x++)
            {
                _fields[x].Fill(ref drRow);
            }
        }

        public void Fill(ref SqlDataReader dr)
        {
            for (int x = 0; x < _fieldCount; x++)
            {
                _fields[x].Fill(ref dr);
            }
        }

        public void Reset()
        {
            for (int x = 0; x < _fieldCount; x++)
            {
                _fields[x].Reset();
            }
        }

        protected void SetParams()
        {
            for (int x = 0; x < _fieldCount; x++)
            {
                _parametersSql[x] = _fields[x].SqlParam();
            }

            _parametersSql[_fieldCount] = new SqlParameter("@ConcurrencyCode", SqlDbType.Int, 4);
            if (_rowverField < 0)
                Concurrency = ConcurrenyType.LastIn;
            _parametersSql[_fieldCount].Value = (int)Enum.Parse(typeof(ConcurrenyType), Concurrency.ToString());
            _parametersSql[_fieldCount + 1] = new SqlParameter("@returnCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, _returnCode);
        }

        public bool Save()
        {
            SetParams(); 

            Boolean isInsert = false;

            switch( _fields[_pkField].dbType.ToString().ToLower())
            {
                case "uniqueidentifier":
                     if (((PKUniqueIdentifierSql)_fields[_pkField]).value == Guid.Empty)
                         isInsert = true;
                     break;

                case "bigint":
                     if (((PKBigIntSql)_fields[_pkField]).value == 0)
                         isInsert = true;
                     break;

                case "int":
                     if (((PKIntSql)_fields[_pkField]).value == 0)
                         isInsert = true;
                     break;
            }

            //Insert
            //if (((_fields[_pkField].dbType == SqlDbType.BigInt) ? ((PKBigIntSql)_fields[_pkField]).value : ((PKIntSql)_fields[_pkField]).value)  == 0)

            if (isInsert) 
            {
                SqlPipe.ExecuteNonQuery(_connectionString, _tableName + "_Insert", _parametersSql);
                _returnCode = (Int32)_parametersSql[_fieldCount + 1].Value;
                if (_returnCode != 0)
                {
                    return false;
                }

                //((PKIntSql)_fields[_pkField]).value  = (Int32) _parametersSql[_pkField].Value;

                if (_fields[_pkField].dbType == SqlDbType.BigInt)
                    ((PKBigIntSql)_fields[_pkField]).value = (Int64)_parametersSql[_pkField].Value;
                else if (_fields[_pkField].dbType == SqlDbType.Int)
                    ((PKIntSql)_fields[_pkField]).value = (Int32)_parametersSql[_pkField].Value;
                else if (_fields[_pkField].dbType == SqlDbType.UniqueIdentifier)
                    ((PKUniqueIdentifierSql)_fields[_pkField]).value = (Guid)_parametersSql[_pkField].Value;
                return true;
            }
            else
            {
                //Update                
                SqlPipe.ExecuteNonQuery(_connectionString, _tableName + "_Update", _parametersSql);

                _returnCode = (Int32)_parametersSql[_fieldCount + 1].Value;
                return (_returnCode == 0);
            }
        }

        public bool LoadID(Guid LoadID)
        {
            Reset();
            if (LoadID == Guid.Empty || LoadID == null)
                return true;

            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = _fields[_pkField].SqlParam();
            parameters[0].Value = LoadID;

            parameters[1] = new SqlParameter("@returnCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, _returnCode);

            SqlDataReader dr;
            dr = SqlPipe.ExecuteReader(_connectionString, _tableName + "_LoadID", parameters);

            while (dr.Read())
            {
                Fill(ref dr);
            }

            dr.Close();

            return true;

        }

        public bool LoadID(Int64 LoadID)
        {
            Reset();
            if (LoadID == 0)
                return true;

            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = _fields[_pkField].SqlParam();
            parameters[0].Value = LoadID;

            parameters[1] = new SqlParameter("@returnCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, _returnCode);

            SqlDataReader dr;
            dr = SqlPipe.ExecuteReader(_connectionString, _tableName + "_LoadID", parameters);

            while (dr.Read())
            {
                Fill(ref dr);
            }

            dr.Close();


            return (((_fields[_pkField].dbType == SqlDbType.BigInt) ? ((PKBigIntSql)_fields[_pkField]).value : ((PKIntSql)_fields[_pkField]).value) != 0);

        }

        public bool LoadID(int LoadID)
        {
            Reset();
            if (LoadID == 0)
                return true;

            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = _fields[_pkField].SqlParam();
            parameters[0].Value = LoadID;

            parameters[1] = new SqlParameter("@returnCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, _returnCode);

            SqlDataReader dr;
            dr = SqlPipe.ExecuteReader(_connectionString, _tableName + "_LoadID", parameters);

            while (dr.Read())
            {
                Fill(ref dr);
            }

            dr.Close();
            return (((PKIntSql)_fields[_pkField]).value != 0);

        }

        public bool LoadID_ParentKey(int LoadID)
        {
            Reset();
            if (LoadID == 0)
                return true;

            SqlParameter[] parameters = new SqlParameter[6];
            parameters[0] = _fields[_pkField].SqlParam();
            parameters[0].Value = LoadID;
            parameters[1] = new SqlParameter("@returnCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, _returnCode);

            SqlDataReader dr;
            dr = SqlPipe.ExecuteReader(_connectionString, _tableName + "_LoadID_ParentKey", parameters);
            while (dr.Read())
            {
                Fill(ref dr);
            }

            dr.Close();
            return (((PKIntSql)_fields[_pkField]).value != 0);
        }

        public bool DeleteX(TypeSql[] fields, int DeleteID)
        {
            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = _fields[_pkField].SqlParam();
            parameters[0].Value = DeleteID;

            _parametersSql[1] = new SqlParameter("@ConcurrencyCode", SqlDbType.Int, 4);
            if (_rowverField < 0)
                Concurrency = ConcurrenyType.LastIn;
            _parametersSql[1].Value = (int)Enum.Parse(typeof(ConcurrenyType), Concurrency.ToString());

            parameters[2] = new SqlParameter("@returnCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, _returnCode);

            SqlPipe.ExecuteNonQuery(_connectionString, _tableName + "_Delete", parameters);

            _returnCode = (Int32)parameters[2].Value;
            return (_returnCode == 0);
        }

        public bool Delete(int DeleteID, byte[] rowver)
        {
            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = _fields[_pkField].SqlParam();
            parameters[0].Value = DeleteID;
            //parameters[1] = _fields[_rowverField].SqlParam();
            //parameters[1].Value  = rowver;	
            parameters[1] = new SqlParameter("@ConcurrencyCode", SqlDbType.Int, 4);
            if (_rowverField < 0)
                Concurrency = ConcurrenyType.LastIn;
            if (rowver == null)
                Concurrency = ConcurrenyType.LastIn;

            parameters[1].Value = (int)Enum.Parse(typeof(ConcurrenyType), Concurrency.ToString());
            parameters[2] = new SqlParameter("@returnCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, _returnCode);

            try
            {
                SqlPipe.ExecuteNonQuery(_connectionString, _tableName + "_Delete", parameters);
                _returnCode = (Int32)parameters[2].Value;
            }
            catch
            {
                return false;
            }
            return (_returnCode == 0);
        }

        public bool Delete(int DeleteID)
        {
            return Delete(DeleteID, null);
        }

        public DataSet LoadList()
        {
            SqlParameter[] parameters = new SqlParameter[1];
            return SqlPipe.ExecuteDataset(_connectionString, _tableName + "_LoadList", parameters);
        }

        public void LoadList(ref DataSet ds)
        {
            SqlParameter[] parameters = new SqlParameter[1];
            ds = SqlPipe.ExecuteDataset(_connectionString, _tableName + "_LoadList", parameters);
        }

        public void LoadList(ref SqlDataReader dr)
        {
            SqlParameter[] parameters = new SqlParameter[1];
            dr = SqlPipe.ExecuteReader(_connectionString, _tableName + "_LoadList", parameters);
        }

        public DataSet List()
        {
            SqlParameter[] parameters = new SqlParameter[1];
            return SqlPipe.ExecuteDataset(_connectionString, _tableName + "_List", parameters);
        }

        public void List(ref DataSet ds)
        {
            ds = List();
        }

        public void List(ref SqlDataReader dr)
        {
            SqlParameter[] parameters = new SqlParameter[1];
            dr = SqlPipe.ExecuteReader(_connectionString, _tableName + "_List", parameters);
        }

        public DataSet List(int ParentKey)
        {
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = new SqlParameter("@ID", SqlDbType.Int, 4);
            parameters[0].Value = ParentKey;
            return SqlPipe.ExecuteDataset(_connectionString, _tableName + "_List_Key", parameters);
        }

        public void List(int ParentKey, ref DataSet ds)
        {
            ds = List(ParentKey);
        }

        public void List(int ParentKey, ref SqlDataReader dr)
        {
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = new SqlParameter("@ID", SqlDbType.Int, 4);
            parameters[0].Value = ParentKey;

            dr = SqlPipe.ExecuteReader(_connectionString, _tableName + "_List_Key", parameters);
        }

        public DataSet FilteredList(Int32 pageIndex, Int32 pageSize, String sortExpression, String filterClause, ref Int32 rowCount, ref Int32 totalRowCount)
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@pageIndex", SqlDbType.Int);
            param[0].Value = pageIndex;
            param[1] = new SqlParameter("@pageSize", SqlDbType.Int);
            param[1].Value = pageSize;
            param[2] = new SqlParameter("@sortExpression", SqlDbType.NVarChar, 100);
            param[2].Value = sortExpression;
            param[3] = new SqlParameter("@filterClause", SqlDbType.NVarChar, 4000);
            param[3].Value = filterClause;
            param[4] = new SqlParameter("@rowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, rowCount);
            param[5] = new SqlParameter("@totalRowCount", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Default, totalRowCount);

            DataSet ds = SqlPipe.ExecuteDataset(_connectionString, _tableName + "_GetList", param);
            rowCount = (Int32)param[4].Value;
            totalRowCount = (Int32)param[5].Value;
            return ds;
        }
         
    }
}

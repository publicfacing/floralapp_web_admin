  
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text; 

namespace Library.DAL
{
	/// <summary>
	/// Summary description for SqlType.
	/// </summary>


	/// <summary>
	/// set an interface class for all to follow   -- tosql, sqlparam and so on
	/// </summary>

	public interface ITypeSql
	{
		SqlParameter SqlParam();
		bool Reset();
	}
     
	public class TypeSql : ITypeSql
	{ 
		protected string _dbName;
		protected int _dbLength;
		protected byte _dbPrecision;
		protected byte _dbScale;
		protected SqlDbType _dbType; 

		public virtual SqlParameter SqlParam() { return new SqlParameter(); }
		public virtual bool Reset() { return true; }
		public virtual void value() { return; }
		public virtual void Fill(ref DataRow rw) { return; }
		public virtual void Fill(ref SqlDataReader rw) { return; }

		public virtual void Bind(ref System.Web.UI.WebControls.TextBox tb) { }
		public virtual void UnBind(System.Web.UI.WebControls.TextBox tb) { }
		public virtual void UnBind(System.Web.UI.WebControls.DropDownList ddl) { }

		public TypeSql()
		{
		}

		public SqlDbType dbType
		{
			get
			{
				return _dbType;
			}
		}

		public string dbName
		{
			get
			{
				return _dbName;
			}
		}

		public int dbLength
		{
			get
			{
				return _dbLength;
			}
		}

		public byte dbPrecision
		{
			get
			{
				return _dbPrecision;
			}
		}

		public byte dbScale
		{
			get
			{
				return _dbScale;
			}
		}
  
		public string dbParamName
		{
			get
			{
				return "@" + _dbName;
			}
		}

	}

    public class UniqueIdentifierSql : TypeSql
    {
        private Guid _value;
        private Guid _valueDefault;           

        public UniqueIdentifierSql(string fieldName, SqlDbType fieldType)
        {
            _dbName = fieldName;
            _dbType = fieldType; 
            _dbLength = 16;
            _dbPrecision = 0;
            _dbScale = 0;
            _valueDefault = Guid.Empty;  
        }  

        public override bool Reset()
        {
            _value = _valueDefault;
            return true;
        }

        public override void Fill(ref DataRow rw)
        {
            try
            {
                _value = new Guid(rw[_dbName].ToString());
            }
            catch
            {
                _value = Guid.Empty;
            }
        }

        public override void Fill(ref SqlDataReader rw)
        {
            try
            {
                _value = new Guid(rw[_dbName].ToString());
            }
            catch
            {
                _value = Guid.Empty;
            }
        }

        public override SqlParameter SqlParam()
        {
            return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.Input, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, _value);
        }

        public Guid valueDefault
        {
            get
            {
                return _valueDefault;
            }
            set
            {
                _valueDefault = value;
            }
        }

        public new Guid value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        } 
    }

	public class StringSql : TypeSql
	{
		private string _value;
		private string _valueDefault; 

		public StringSql(string fieldName, int fieldLength, string startValue, SqlDbType fieldType)
		{
			_dbName = fieldName;
			_dbType = fieldType;
			_dbLength = fieldLength;
			_dbPrecision = 0;
			_dbScale = 0;
			_valueDefault = startValue;
		}
         
		public override bool Reset()
		{
			_value = _valueDefault;
			return true;
		}

		public override void Fill(ref DataRow rw)
		{
			_value = ((string)rw[_dbName]).Trim();
		}

		public override void Fill(ref SqlDataReader rw)
		{
			_value = ((string)rw[_dbName]).Trim();
		}

		public override SqlParameter SqlParam()
		{

			return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.Input, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, _value);
		}

		public string valueDefault
		{
			get
			{
				return _valueDefault;
			}
			set
			{
				_valueDefault = value;
			}
		}

		public new string value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
			}
		}

		public override void Bind(ref System.Web.UI.WebControls.TextBox tb)
		{
			tb.Text = this.value;
			tb.MaxLength = this._dbLength;
			if (tb.Rows <= 1)
			{
				if (this._dbType == SqlDbType.NVarChar)
				{

					if (this._dbLength < 201)
						tb.Width = 6 + ((this._dbLength / 2) * 6) + 6;
					else
						tb.Width = 6 + (100 * 6) + 6;

				}
				else
				{
					if (this._dbLength < 60)
						tb.Width = 6 + (this._dbLength * 6) + 6;
					else
					{
						if (this._dbLength < 150)
							tb.Width = 6 + ((this._dbLength / 2) * 6) + 6;
						else
							tb.Width = 6 + ((60) * 6) + 6;
					}
					//tb.Width = 6+((this._dbLength/2)*6)+6;

				}
			}
			//	tb.Width  = this._dbLength*6 ;
		}

		public override void UnBind(System.Web.UI.WebControls.TextBox tb)
		{
			this.value = tb.Text;
		}
		
        public override void UnBind(System.Web.UI.WebControls.DropDownList ddl)
		{
			this.value = ddl.SelectedItem.Value;
		}
         
	}


	public class TextSql : StringSql
	{
		//		private string _value ;
		//		private string _valueDefault;
          
		public TextSql(string fieldName, string startValue, SqlDbType fieldType)
			: base(fieldName, 0, startValue, fieldType)
		{

		}
         
		public override SqlParameter SqlParam()
		{
			return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.Input, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, value);
		}
	}

	public class BigIntSql : TypeSql
	{
        private long _value;
        private long _valueDefault;
        private long _maxvalue = int.MaxValue;
		private long _minvalue = int.MinValue;

        public BigIntSql(string fieldName, long startValue, SqlDbType fieldType)
		{
			_dbName = fieldName;
			_dbType = fieldType;
			_dbLength = 4;
			_dbPrecision = 0;
			_dbScale = 0;
			_valueDefault = startValue;
		}

        public BigIntSql(string fieldName, long startValue, SqlDbType fieldType, long minValue, long maxValue)
		{
			_dbName = fieldName;
			_dbType = fieldType;
			_dbLength = 4;
			_dbPrecision = 0;
			_dbScale = 0;
			_maxvalue = minValue;
			_minvalue = maxValue;

		}

		public override bool Reset()
		{
			_value = _valueDefault;
			return true;
		}

		public override void Fill(ref DataRow rw)
		{
            try
            {
                _value = long.Parse(rw[_dbName].ToString());
            }
            catch
            {
                _value = 0;
            }
	    }

		public override void Fill(ref SqlDataReader rw)
		{
            try
            {
                _value = long.Parse(rw[_dbName].ToString());
            }
            catch
            {
                _value = 0;
            }
		}
         
		public override SqlParameter SqlParam()
		{
			return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.Input, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, _value);
		}

        public long valueDefault
		{
			get
			{
				return _valueDefault;
			}
			set
			{
				_valueDefault = value;
			}
		}

		public new long value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
			}
		}

		public override void Bind(ref System.Web.UI.WebControls.TextBox tb)
		{
			tb.Text = this.value.ToString();
			tb.MaxLength = 11;
		}

		public override void UnBind(System.Web.UI.WebControls.TextBox tb)
		{
            this.value = long.Parse(tb.Text);
		}

		public override void UnBind(System.Web.UI.WebControls.DropDownList ddl)
		{
            this.value = long.Parse(ddl.SelectedItem.Value);
		}
	}

    public class IntSql : TypeSql
    {
        private int _value;
        private int _valueDefault;
        private int _maxvalue = int.MaxValue;
        private int _minvalue = int.MinValue;

        public IntSql(string fieldName, int startValue, SqlDbType fieldType)
        {
            _dbName = fieldName;
            _dbType = fieldType;
            _dbLength = 4;
            _dbPrecision = 0;
            _dbScale = 0;
            _valueDefault = startValue;
        }

        public IntSql(string fieldName, int startValue, SqlDbType fieldType, int minValue, int maxValue)
        {
            _dbName = fieldName;
            _dbType = fieldType;
            _dbLength = 4;
            _dbPrecision = 0;
            _dbScale = 0;
            _maxvalue = minValue;
            _minvalue = maxValue;

        }

        public override bool Reset()
        {
            _value = _valueDefault;
            return true;
        }

        public override void Fill(ref DataRow rw)
        {
            try
            {
                _value = int.Parse(rw[_dbName].ToString());
            }
            catch
            {
                _value = 0;
            }
        }

        public override void Fill(ref SqlDataReader rw)
        {
            try
            {
                _value = int.Parse(rw[_dbName].ToString());
            }
            catch
            {
                _value = 0;
            }
        }

        public override SqlParameter SqlParam()
        {
            return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.Input, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, _value);
        }

        public int valueDefault
        {
            get
            {
                return _valueDefault;
            }
            set
            {
                _valueDefault = value;
            }
        }

        public new int value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        public override void Bind(ref System.Web.UI.WebControls.TextBox tb)
        {
            tb.Text = this.value.ToString();
            tb.MaxLength = 11;
        }

        public override void UnBind(System.Web.UI.WebControls.TextBox tb)
        {
            this.value = int.Parse(tb.Text);
        }

        public override void UnBind(System.Web.UI.WebControls.DropDownList ddl)
        {
            this.value = int.Parse(ddl.SelectedItem.Value);
        }
    }

	public class RowVersionSql : TypeSql
	{
		private byte[] _value;
		private byte[] _valueDefault;

		public RowVersionSql(string fieldName)
		{
			_dbName = fieldName;
			_valueDefault = null;
			value = _valueDefault;
		}
         
		public override bool Reset()
		{
			return true;
		}

		public override void Fill(ref DataRow rw)
		{
			_value = (byte[])rw[_dbName];
			//_value.
		}

		public override void Fill(ref SqlDataReader rw)
		{
			_value = (byte[])rw[_dbName];
			//_value.
		}
         
		public override SqlParameter SqlParam()
		{
			if (value == null)
				return new SqlParameter(dbParamName, SqlDbType.Timestamp, 8, ParameterDirection.Input, true, _dbPrecision, _dbScale, "", DataRowVersion.Original, System.DBNull.Value);
			else
				return new SqlParameter(dbParamName, SqlDbType.Timestamp, 8, ParameterDirection.Input, true, _dbPrecision, _dbScale, "", DataRowVersion.Original, value);
		}

		public byte[] valueDefault
		{
			get
			{
				return _valueDefault;
			}
			set
			{
				_valueDefault = value;
			}
		}

		public new byte[] value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
			}
		}

		public override void Bind(ref System.Web.UI.WebControls.TextBox tb)
		{
			tb.Text = this.value.ToString();
			tb.MaxLength = this._dbLength;
		}

		public override void UnBind(System.Web.UI.WebControls.TextBox tb)
		{
			//this.value = Encoding.UTF8.GetBytes(tb.Text) ;
		}
		public override void UnBind(System.Web.UI.WebControls.DropDownList ddl)
		{
			//this.value = int.Parse (ddl.SelectedItem.Value);
		}
  
	}

    public class PKUniqueIdentifierSql : UniqueIdentifierSql
    {
        public PKUniqueIdentifierSql(string fieldName,  SqlDbType fieldType)
            : base(fieldName, fieldType)
        {

        }

        public override SqlParameter SqlParam()
        {
            return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.InputOutput, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, value);
        }
    }

    public class PKBigIntSql : BigIntSql
    {
        public PKBigIntSql(string fieldName, long startValue, SqlDbType fieldType)
            : base(fieldName, startValue, fieldType)
        {

        }

        public override SqlParameter SqlParam()
        {
            return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.InputOutput, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, value);
        }
    }

	public class PKIntSql : IntSql
	{ 
		public PKIntSql(string fieldName, int startValue, SqlDbType fieldType)
			: base(fieldName, startValue, fieldType)
		{

		}
         
		public override SqlParameter SqlParam()
		{
			return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.InputOutput, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, value);
		}
	}
     
	public class DateTimeSql : TypeSql
	{
		private DateTime _value;
		private DateTime _valueDefault;
          
		public DateTimeSql(string fieldName, SqlDbType fieldType)
		{
			_dbName = fieldName;
			_dbType = fieldType;
			_dbLength = 100;
			_dbPrecision = 0;
			_dbScale = 0;
			//_valueDefault=DateTime.Now;
			_valueDefault = DateTime.Parse("1/1/1900");
		}
         
		public override bool Reset()
		{
			_value = _valueDefault;
			return true;
		}

		public override void Fill(ref DataRow rw)
		{
			_value = Convert.ToDateTime(rw[_dbName]);
		}

		public override void Fill(ref SqlDataReader rw)
		{
			_value = Convert.ToDateTime(rw[_dbName]);
		}
         
		public override SqlParameter SqlParam()
		{

			return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.Input, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, _value);
		}
         
		public DateTime valueDefault
		{
			get
			{
				return _valueDefault;
			}
			set
			{
				_valueDefault = value;
			}
		}

		public new DateTime value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
			}
		}

		public override void Bind(ref System.Web.UI.WebControls.TextBox tb)
		{
			tb.Text = this.value.ToString();
			tb.MaxLength = 30;
		}
		public override void UnBind(System.Web.UI.WebControls.TextBox tb)
		{
			this.value = DateTime.Parse(tb.Text);
		}
		public override void UnBind(System.Web.UI.WebControls.DropDownList ddl)
		{
			this.value = DateTime.Parse(ddl.SelectedItem.Value);
		}
         
	}



	public class SingleSql : TypeSql
	{
		private Single _value;
		private Single _valueDefault;
         
		public SingleSql(string fieldName, SqlDbType fieldType)
		{
			_dbName = fieldName;
			_dbType = fieldType;
			_dbLength = 10;
			_dbPrecision = 24;
			_dbScale = 0;
			_valueDefault = 0;
		} 
		
        public override bool Reset()
		{
			_value = _valueDefault;
			return true;
		} 
		
        public override void Fill(ref DataRow rw)
		{
			_value = (Single)rw[_dbName];
		}
		
        public override void Fill(ref SqlDataReader rw)
		{
			_value = (Single)rw[_dbName];
		}
         
		public override SqlParameter SqlParam()
		{

			return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.Input, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, _value);
		}
         
		public Single valueDefault
		{
			get
			{
				return _valueDefault;
			}
			set
			{
				_valueDefault = value;
			}
		}

		public new Single value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
			}
		}

		public override void Bind(ref System.Web.UI.WebControls.TextBox tb)
		{
			tb.Text = this.value.ToString();
			tb.MaxLength = 30;
		}

		public override void UnBind(System.Web.UI.WebControls.TextBox tb)
		{
			this.value = Single.Parse(tb.Text);
		}
		
        public override void UnBind(System.Web.UI.WebControls.DropDownList ddl)
		{
			this.value = Single.Parse(ddl.SelectedItem.Value);
		}
         
	}
     
	public class BitSql : TypeSql
	{
		private bool _value;
		private bool _valueDefault;
         

		public BitSql(string fieldName, bool startValue, SqlDbType fieldType)
		{
			_dbName = fieldName;
			_dbType = fieldType;
			_dbLength = 1;
			_dbPrecision = 0;
			_dbScale = 0;
			_valueDefault = startValue;
		}
         
		public override bool Reset()
		{
			_value = _valueDefault;
			return true;
		}

		public override void Fill(ref DataRow rw)
		{
			_value = bool.Parse(rw[_dbName].ToString());
		}

		public override void Fill(ref SqlDataReader rw)
		{
			_value = bool.Parse(rw[_dbName].ToString());
		}
         
		public override SqlParameter SqlParam()
		{
			return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.Input, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, _value);
		}
         
		public bool valueDefault
		{
			get
			{
				return _valueDefault;
			}
			set
			{
				_valueDefault = value;
			}
		}

		public new bool value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
			}
		}

		public override void Bind(ref System.Web.UI.WebControls.TextBox tb)
		{
			tb.Text = this.value.ToString();
			tb.MaxLength = this._dbLength;
		}

		public void Bind(ref System.Web.UI.WebControls.CheckBox cb)
		{
			cb.Checked = this.value;
		}

		public override void UnBind(System.Web.UI.WebControls.TextBox tb)
		{
			//this.value = .Parse( tb.Text );
		}

		public void UnBind(System.Web.UI.WebControls.CheckBox cb)
		{
			this.value = cb.Checked;
			//this.value = .Parse( tb.Text );
		}

		public override void UnBind(System.Web.UI.WebControls.DropDownList ddl)
		{
			//this.value = int.Parse (ddl.SelectedItem.Value);
		}
         
	}
     
	public class MoneySql : DecimalSql
	{

		public MoneySql(string fieldName, decimal startValue, SqlDbType fieldType)
			: base(fieldName, 19, 4, startValue, fieldType)
		{
			_dbLength = 8;
		}

		public MoneySql(string fieldName, decimal startValue, SqlDbType fieldType, decimal minValue, decimal maxValue)
			: base(fieldName, 19, 4, startValue, fieldType, minValue, maxValue)
		{
			_dbLength = 8;
		}
		public override void Bind(ref System.Web.UI.WebControls.TextBox tb)
		{
			tb.Text = this.value.ToString("F2");
			tb.MaxLength = 30;

		}

	}

	public class DecimalSql : TypeSql
	{
		private decimal _value;
		private decimal _valueDefault;
		private decimal _maxvalue = decimal.MaxValue;
		private decimal _minvalue = decimal.MinValue;
         
		public DecimalSql(string fieldName, byte precision, byte scale, decimal startValue, SqlDbType fieldType)
		{
			_dbName = fieldName;
			_dbType = fieldType;
			_dbLength = 4;
			_dbPrecision = precision;
			_dbScale = scale;
			_valueDefault = startValue;
		}

		public DecimalSql(string fieldName, byte precision, byte scale, decimal startValue, SqlDbType fieldType, decimal minValue, decimal maxValue)
		{
			_dbName = fieldName;
			_dbType = fieldType;
			_dbLength = 4;
			_dbPrecision = precision;
			_dbScale = scale;
			_maxvalue = minValue;
			_minvalue = maxValue;

		}
         
		public override bool Reset()
		{
			_value = _valueDefault;
			return true;
		}

		public override void Fill(ref DataRow rw)
		{
			_value = decimal.Parse(rw[_dbName].ToString());
		}

		public override void Fill(ref SqlDataReader rw)
		{
			_value = decimal.Parse(rw[_dbName].ToString());
		}
         
		public override SqlParameter SqlParam()
		{
			return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.Input, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, _value);
		}
         
		public decimal valueDefault
		{
			get
			{
				return _valueDefault;
			}
			set
			{
				_valueDefault = value;
			}
		}

		public new decimal value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
			}
		}

		public override void Bind(ref System.Web.UI.WebControls.TextBox tb)
		{
			tb.Text = this.value.ToString();
			tb.MaxLength = 30;
		}

		public override void UnBind(System.Web.UI.WebControls.TextBox tb)
		{
			this.value = decimal.Parse(tb.Text);
		}
		public override void UnBind(System.Web.UI.WebControls.DropDownList ddl)
		{
			this.value = decimal.Parse(ddl.SelectedItem.Value);
		}
         
	} 

	public class FloatSql : TypeSql
	{
		private double _value;
		private double _valueDefault;
		private double _maxvalue = double.MaxValue;
		private double _minvalue = double.MinValue;
         
		public FloatSql(string fieldName, float startValue, SqlDbType fieldType)		
        {
			_dbName = fieldName;
			_dbType = fieldType;
			_dbLength = 8;
//			_dbPrecision = 38;
            _dbPrecision = 4;
			_dbScale = 0;
			_valueDefault = startValue;
		}

		public FloatSql(string fieldName, float startValue, SqlDbType fieldType, float minValue, float maxValue)
		{
			_dbName = fieldName;
			_dbType = fieldType;
			_dbLength = 4;
			//_dbPrecision = 0;
            _dbPrecision = 4;
			_dbScale = 0;
			_maxvalue = minValue;
			_minvalue = maxValue;

		}

        public override bool Reset()
		{
			_value = _valueDefault;
			return true;
		}

		public override void Fill(ref DataRow rw)
		{
			_value = double.Parse(rw[_dbName].ToString());
		}

		public override void Fill(ref SqlDataReader rw)
		{
			_value = double.Parse(rw[_dbName].ToString());
		}
         
		public override SqlParameter SqlParam()
		{
			//return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.Input, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, _value);
            SqlParameter x = new SqlParameter (dbParamName, dbType );
            x.Value = _value;
            return x ;
		}
         
		public double valueDefault
		{
			get
			{
				return _valueDefault;
			}
			set
			{
				_valueDefault = value;
			}
		}

		public new double value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
			}
		}

		public override void Bind(ref System.Web.UI.WebControls.TextBox tb)
		{
			tb.Text = this.value.ToString();
			tb.MaxLength = 30;
		}

		public override void UnBind(System.Web.UI.WebControls.TextBox tb)
		{
			this.value = float.Parse(tb.Text);
		}
		
        public override void UnBind(System.Web.UI.WebControls.DropDownList ddl)
		{
			this.value = float.Parse(ddl.SelectedItem.Value);
		}
	}

    public class ImageSql : TypeSql
    {
        private byte[] _value;
        private byte[] _valueDefault;

        public ImageSql(string fieldName)
        {
            _dbName = fieldName;
            _valueDefault = null;
            value = _valueDefault;
        }

        public ImageSql(string fieldName, SqlDbType fieldType)
        {
            _dbName = fieldName;
            _dbType = fieldType;
            _valueDefault = null;
            value = _valueDefault;
        }
         
        public override bool Reset()
        {
            return true;
        }

        public override void Fill(ref DataRow rw)
        {
            _value = (byte[])rw[_dbName];
            //_value.
        }

        public override void Fill(ref SqlDataReader rw)
        {
            _value = (byte[])rw[_dbName];
            //_value.
        }
         
        public override SqlParameter SqlParam()
        {
            return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.Input, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, value);
        }

        public byte[] valueDefault
        {
            get
            {
                return _valueDefault;
            }
            set
            {
                _valueDefault = value;
            }
        }

        public new byte[] value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        public override void Bind(ref System.Web.UI.WebControls.TextBox tb)
        {
        }

        public override void UnBind(System.Web.UI.WebControls.TextBox tb)
        {
            //this.value = Encoding.UTF8.GetBytes(tb.Text) ;
        }
        
        public override void UnBind(System.Web.UI.WebControls.DropDownList ddl)
        {
            //this.value = int.Parse (ddl.SelectedItem.Value);
        }
    }

    public class BinarySql : TypeSql
    {
        private byte[] _value;
        private byte[] _valueDefault;

        public BinarySql(string fieldName)
        {
            _dbName = fieldName;
            _valueDefault = null;
            value = _valueDefault;
        }

        public BinarySql(string fieldName, SqlDbType fieldType)
        {
            _dbName = fieldName;
            _dbType = fieldType;
            _valueDefault = null;
            value = _valueDefault;
        }

        public override bool Reset()
        {
            return true;
        }

        public override void Fill(ref DataRow rw)
        {
            _value = (byte[])rw[_dbName];
            //_value.
        }

        public override void Fill(ref SqlDataReader rw)
        {
            _value = (byte[])rw[_dbName];
            //_value.
        }

        public override SqlParameter SqlParam()
        {
            return new SqlParameter(dbParamName, dbType, dbLength, ParameterDirection.Input, false, _dbPrecision, _dbScale, "", DataRowVersion.Default, value);
        }

        public byte[] valueDefault
        {
            get
            {
                return _valueDefault;
            }
            set
            {
                _valueDefault = value;
            }
        }

        public new byte[] value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }                 
    }

}

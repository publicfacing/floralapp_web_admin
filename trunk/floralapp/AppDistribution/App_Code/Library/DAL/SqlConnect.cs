using System;
using System.Data;
using System.Data.SqlClient ;
using System.Data.SqlTypes ;
using System.Collections ;
using System.Configuration;
using System.Web;
using System.Web.Caching ;
using System.Web.Security ;

namespace Library.DAL
{
	/// <summary>
	/// Summary description for SqlConnect.
	/// </summary>
	public abstract class SqlConnect
	{
		public SqlConnect() { }

        public static string GetStorageConnectionString()
        {
            return GetString("AppStorageConnectionString");
        }

		public static string GetConnectionString()
		{
            return GetString("AppConnectionString");
		}

        private static string GetString(string ConnectionStringType)
        {
            string _connectionString = string.Empty;
            if (HttpContext.Current.Cache[ConnectionStringType] != null)
                _connectionString = HttpContext.Current.Cache[ConnectionStringType].ToString();
            else
            {
                if ((HttpContext.Current.Session != null) && (HttpContext.Current.Session[ConnectionStringType] != null))
                    _connectionString = HttpContext.Current.Session[ConnectionStringType].ToString();
                else
                    _connectionString = ConfigurationManager.ConnectionStrings[ConnectionStringType].ConnectionString;
            }
            return _connectionString; 
        }
	}
}

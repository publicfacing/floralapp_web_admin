
/// <summary>
/// Summary description for Enum
/// </summary>
namespace Library.AppSettings
{
    public static class AppEnum
    {
        public enum ApplicationType
        {
            WEBSITE = 1,
            WEBSERVICE = 2, 
            IPHONE = 3,
            ANDROID = 4
        }

        public enum ActiveStatus
        {
            INACTIVE = 0,
            ACTIVE = 1,
            ARCHIVE = 5
        }

        public enum UserRole
        {
            SUPERADMIN = 1,
            RESELLERADMIN = 2,
            CONSUMER = 3
        }

        public enum AddressType
        {
            BILLING = 10,
            SHIPPING = 11 
        }

        public enum DeviceType
        { 
            IPHONE = 1,
            ANDROID = 2
        }

        public enum Gateway
        {
            PAYPAL = 1,
            HOUSEACCOUNT = 2,
            AUTHNET = 3,
            MAS = 4,
            FTD = 5
        }

        public enum CouponUsageType
        {
            OneTime = 1,
            Multiple = 2,
            AppLaunch = 3
        }

        public enum ColorThemeHeightWidth
        {
            Height = 75,
            Width = 30
        }
    } 
}

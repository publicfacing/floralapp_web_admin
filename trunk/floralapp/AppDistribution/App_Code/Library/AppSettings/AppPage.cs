using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Library.Utility;
using Library.AppSettings; 

/// <summary>
/// Summary description for AppPage
/// </summary>
public class AppPage : System.Web.UI.Page
{
    AppVars v = new AppVars();
    protected String _errorMsg = String.Empty; 

	public AppPage()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    
    public virtual void Page_Load(object sender, System.EventArgs args)
    {
        Page.MaintainScrollPositionOnPostBack = true;

    }

    protected override void OnLoad(EventArgs e)
    {
        ExpirePage();

        // Custom logic here ...
        if (v.UserID <= 0)
        {
            Response.Redirect("../Login.aspx");
        }

        // Be sure to call the base class's OnLoad method!
        base.OnLoad(e);
    }

    protected void ExpirePage()
    {
        // Make sure page is not cached
        System.Web.HttpContext.Current.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
        System.Web.HttpContext.Current.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        System.Web.HttpContext.Current.Response.AppendHeader("Pragma", "no-cache");
    }

    public void HideError(Label lbl)
    {  
        lbl.Visible = false;
    }

    public void ShowError(Label lbl, String msg)
    {
        lbl.Text = msg;
        lbl.Visible = true;
    }

    public void HideMsg(Label lbl)
    {
        lbl.Visible = false;
    }

    public void ShowMsg(Label lbl, String msg)
    {
        lbl.Text = msg;
        lbl.Visible = true;
    }

    protected void AddErrorMessage(String errorMessage)
    {
        _errorMsg += (_errorMsg != String.Empty) ? "<br /> " + errorMessage : errorMessage;
    } 
}

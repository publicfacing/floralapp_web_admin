using System;
using System.Web;

/// <summary>
/// Summary description for AppVars
/// </summary>
namespace Library.AppSettings
{
    public class AppVars
    {
        public AppVars()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public bool ValidSessionVar(string VarName)
        {
            try
            {
                return (HttpContext.Current.Session[VarName] != null);
            }
            catch
            {
                return false;
            }
        }

        //public String LoginID
        //{
        //    get
        //    {
        //        if (ValidSessionVar("LoginID"))
        //            return HttpContext.Current.Session["LoginID"].ToString();
        //        else
        //            return String.Empty;
        //    }
        //    set
        //    {
        //        HttpContext.Current.Session["LoginID"] = value;
        //    }
        //}

        public String DisplayName
        {
            get
            {
                if (ValidSessionVar("DisplayName"))
                    return HttpContext.Current.Session["DisplayName"].ToString();
                else
                    return String.Empty;
            }
            set
            {
                HttpContext.Current.Session["DisplayName"] = value;
            }
        }

        public Int64 UserID
        {
            get
            {
                if (ValidSessionVar("UserID"))
                    return Int64.Parse(HttpContext.Current.Session["UserID"].ToString());
                else
                    return 0;
            }
            set
            {
                HttpContext.Current.Session["UserID"] = value;
            }
        }

        public Int32 UserRoleID
        {
            get
            {
                if (ValidSessionVar("UserRoleID"))
                    return Int32.Parse(HttpContext.Current.Session["UserRoleID"].ToString());
                else
                    return 0;
            }
            set
            {
                HttpContext.Current.Session["UserRoleID"] = value;
            }
        }

        public Int32 UserTypeID
        {
            get
            {
                if (ValidSessionVar("UserTypeID"))
                    return Int32.Parse(HttpContext.Current.Session["UserTypeID"].ToString());
                else
                    return 0;
            }
            set
            {
                HttpContext.Current.Session["UserTypeID"] = value;
            }
        }

        public Int32 ResellerID
        {
            get
            {
                if (ValidSessionVar("ResellerID"))
                    return Int32.Parse(HttpContext.Current.Session["ResellerID"].ToString());
                else
                    return 0;
            }
            set
            {
                HttpContext.Current.Session["ResellerID"] = value;
            }
        }

        public String IPAddress
        {
            get
            {
                if (ValidSessionVar("IPAddress"))
                    return HttpContext.Current.Session["IPAddress"].ToString();
                else
                    return String.Empty;
            }
            set
            {
                HttpContext.Current.Session["IPAddress"] = value;
            }
        }

        public String SelectedTab
        {
            get
            {
                if (ValidSessionVar("SelectedTab"))
                    return HttpContext.Current.Session["SelectedTab"].ToString();
                else
                    return String.Empty;
            }
            set
            {
                HttpContext.Current.Session["SelectedTab"] = value;
            }
        }
    }
}

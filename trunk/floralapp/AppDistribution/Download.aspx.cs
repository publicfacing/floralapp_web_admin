﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Specialized;
using Library.Utility;
using System.IO;
using Library.AppSettings;

public partial class AppDistribution_Download : System.Web.UI.Page
{
    private Int32 ResellerID
    {
        get;
        set;
    }

    private Int64 UserID
    {
        get;
        set;
    }

    private String AppType
    {
        get;
        set;
    }

    private String UserEmailID
    {
        get;
        set;
    }

    private String ReferralCode
    {
        get;
        set;
    }

    private String KeyCode
    {
        get;
        set;
    }

    public string mAndroidLinks = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        Util.HideError(lblErrMsg);
        appDownloadDiv.Visible = false;

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["eqs"]))
            {
                NameValueCollection nvc = Crypto.ConvertNVC(Crypto.DecryptString(Request.QueryString["eqs"].ToString()));

                if (!String.IsNullOrEmpty(nvc["ResellerID"]))
                    ResellerID = Convert.ToInt32(nvc["ResellerID"]);

                if (!String.IsNullOrEmpty(nvc["UserID"]))
                    UserID = Convert.ToInt64(nvc["UserID"]);

                if (!String.IsNullOrEmpty(nvc["AppType"]))
                    AppType = nvc["AppType"];

                if (!String.IsNullOrEmpty(nvc["ReferralCode"]))
                    ReferralCode = nvc["ReferralCode"];

                if (!String.IsNullOrEmpty(nvc["UserEmailID"]))
                    UserEmailID = nvc["UserEmailID"];

                if (!String.IsNullOrEmpty(nvc["KeyCode"]))
                    KeyCode = nvc["KeyCode"];
            }
        }
        catch
        {
            Util.ShowError(lblErrMsg, "Not a valid Url.");
            return;
        }


        if (!IsPostBack)
        {
            String isGenerateLinks = ConfigurationManager.AppSettings["IsGenerateLinks"].ToString();

            if (isGenerateLinks == "1")
                GenerateDownloadLinks();

            if (ResellerID > 0)
            {
                #region insert records into the [AppDownload] table.

                Data.ViralMarketingRequest vmr = new Data.ViralMarketingRequest();
                DataSet dsVmr = vmr.List();
                DataRow[] drVmr = dsVmr.Tables[0].Select("ReferralCode='" + ReferralCode + "'");

                if (drVmr.Length > 0)
                {
                    foreach (DataRow drRequest in drVmr)
                    {
                        Data.AppDownloads ad = new Data.AppDownloads();

                        ad.ResellerID.value = ResellerID;
                        ad.Link.value = Util.GetDataString(drRequest["RequestLink"].ToString());
                        ad.ReferralCode.value = Util.GetDataString(drRequest["ReferralCode"].ToString());
                        ad.UserID.value = UserID;
                        ad.UserIP.value = Request.UserHostAddress;
                        ad.UserEmail.value = Util.GetDataString(UserEmailID);
                        //ad.BrowserXML.value = Util.GetDataString(drRequest["Message"].ToString());
                        ad.DownloadDate.value = Util.GetServerDate();
                        ad.IsActive.value = Convert.ToInt32(AppEnum.ActiveStatus.ACTIVE);
                        ad.Save();

                        
                        Data.AppActivation app = new Data.AppActivation();
                        Int32 appActivationID = 0;

                        switch (AppType.ToUpper())
                        {
                            case "ANDROID":
                                appActivationID = app.Activate(Util.GetDataString(drRequest["ReferralCode"].ToString()), "2", 2);
                                break;
                            case "IPHONE":
                                appActivationID = app.Activate(Util.GetDataString(drRequest["ReferralCode"].ToString()), "1", 1);
                                break;
                        }

                    }
                }

                #endregion End of inserting process...

                switch (AppType.ToUpper())
                {
                    case "ANDROID":

                        //Response.Redirect("http://www.android.com/market", true);
                        Response.Redirect("https://market.android.com/details?id=com.floralapp", true);

                        //#region Redirecting to android download...

                        //DownloadAndroidBuild();

                        //#endregion

                        break;
                    case "IPHONE":

                        Response.Redirect("http://itunes.apple.com/us/app/floralapp/id418046070?mt=8&ls=1", true);
                        break;
                }
            }
        }

    }

    private void DownloadAndroidBuild()
    {
        String apkPath = Server.MapPath(@"~/AppDistribution\Android\com.floralapp.apk");
        String buildName = "floralapp_" + KeyCode + "_" + ReferralCode + ".apk";
        Response.BufferOutput = false;
        Response.Clear();
        Response.AddHeader("Content-Length", new FileInfo(apkPath).Length.ToString());
        Response.AddHeader("Content-Disposition", "attachment; filename=" + buildName);
        Response.ContentType = "application/vnd.android.package-archive";
        Response.Flush();
        Response.WriteFile(apkPath);
    }

    private void GenerateDownloadLinks()
    {
        GenerateiPhoneLinks();
        GenerateAndriodLinks();

        appDownloadDiv.Visible = true;
    }

    private void GenerateAndriodLinks()
    {
        String iPhoneDir = Server.MapPath(@"~/AppDistribution\iPhone\");
        String androidDir = Server.MapPath(@"~/AppDistribution\Android\");
        String appUrl = ConfigurationManager.AppSettings["AppUrl"];


        DirectoryInfo di = new DirectoryInfo(androidDir);
        FileInfo[] fi = di.GetFiles("*.*");

        for (int i = 0; i < fi.Length; i++)
        {
            HyperLink hl = new HyperLink();
            hl.ID = "androidFile_" + i;
            hl.Text = fi[i].Name;
            hl.NavigateUrl = appUrl + "AppDistribution/Android/" + fi[i].Name;
            andriodLinks.Controls.Add(hl);

            LiteralControl br = new LiteralControl("<br />");
            andriodLinks.Controls.Add(br);
        }
    }

    private void GenerateiPhoneLinks()
    {
        String iPhoneDir = Server.MapPath(@"~/AppDistribution\iPhone\");
        String appUrl = ConfigurationManager.AppSettings["AppUrl"];

        DirectoryInfo di = new DirectoryInfo(iPhoneDir);
        FileInfo[] fi = di.GetFiles("*.*");

        for (int i = 0; i < fi.Length; i++)
        {
            HyperLink hl = new HyperLink();
            hl.ID = "iPhoneFiles_" + i;
            hl.Text = fi[i].Name;
            hl.NavigateUrl = appUrl + "AppDistribution/iPhone/" + fi[i].Name;
            iPhoneLinks.Controls.Add(hl);

            LiteralControl br = new LiteralControl("<br />");
            iPhoneLinks.Controls.Add(br);
        }
    }
}
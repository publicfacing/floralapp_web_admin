<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="pages_Login"
    ValidateRequest="true" Title="floralapp&reg; | Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>floralapp&reg; : Login</title>
    <meta http-equiv="pragma" content="no-cache" />
    <link rel="shortcut icon" href="../images/MetaOptionIcon163.ico" type="image/x-icon" />
    <meta content="floralapp" />
    <link href="Css/Default.css" rel="stylesheet" type="text/css" />
    <link href="Css/LoginPage.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmLogin" runat="server">
    <center id="lblIsStagingSite" visible="false" runat="server">
        <div style="color: #ff9900; font-size: 24px; font-weight: bold; margin-top: 10px;">
            <i>STAGING SITE</i></div>
    </center>
    <center>
        <div style="color: #ffffff; margin-top: 10px; font-size: 13px; font-weight: bold;
            margin-top: 200px;">
            <div>
                &nbsp;<span id="spnFirefox">We recommend <a href="http://www.mozilla.org/en-US/firefox/new/"
                    target="_blank" style="color: #ffffff; font-weight: bold;">Firefox</a> for best
                    use!</span>
                <br />
                &nbsp; <span id="spnIE" style="display: none">You are not using the last version of
                    Internet Explorer.</span>
                <br />
                &nbsp;
            </div>
        </div>
    </center>
    <div class="loginContainer">
        <div class="titleBar">
            &nbsp;</div>
        <div class="msgDiv">
            <asp:Label ID="lblErrMsg" runat="server" CssClass="errMsg"></asp:Label>
            <asp:ValidationSummary ID="vsLoginPage" runat="server" ShowSummary="true" />
        </div>
        <table cellspacing="0" cellpadding="0" border="0" class="login">
            <tr>
                <td class="label">
                    Login ID:
                </td>
                <td>
                    <asp:TextBox ID="txtLoginID" TabIndex="1" runat="server" MaxLength="50" CssClass="txtbox"
                        Text=""></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rvLoginidValidator" runat="server" Display="None"
                        ErrorMessage="Please enter your Login ID." ControlToValidate="txtLoginID"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="label">
                    Password:
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" TabIndex="2" runat="server" MaxLength="20" CssClass="txtbox"
                        TextMode="Password" Text=""></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rvPasswordValidator" runat="server" Display="None"
                        ErrorMessage="Enter your Password." ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="label">
                    &nbsp;
                </td>
                <td align="left">
                    <%--<asp:ImageButton ID="btnLogin" runat="server" ImageUrl="~/Images/submit.png" CssClass="btn" OnClick="btnLogin_click" />--%>
                    <div style="text-align: left; vertical-align: top; text-decoration: none">
                        <asp:Button ID="btnLogin" TabIndex="3" runat="server" Text="Login" CssClass="btn"
                            OnClick="btnLogin_click"></asp:Button>
                        <asp:LinkButton ID="lnkLostPassword" runat="server" PostBackUrl="~/NS/LostPassword.aspx"
                            Style="text-decoration: none; font-style: italic; color: #ffffff; font-weight: bold;
                            font-family: Verdana; font-size: 9px; margin-left: 20px;" CausesValidation="false">Lost Password?</asp:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <center>
        <div style="height: 200px; width: 400px; margin-top: 200px; background: url(Image/_default_background.png) 140% 50%;
            display: none;">
            <div style="height: 101px; background: url(Image/4_ProfileEdit_BenevaFlowerswhitebox.png) no-repeat 30px 5px;">
                <div style="position: relative; padding: 20px 0px 0px 0px; margin: 0px 50px 0px 150px;
                    text-align: left;">
                    <h2 class="LoginTitle">
                        Login BenevaFlowers</h2>
                </div>
                <div style="margin: 5px 5px 5px 160px; text-align: left;">
                </div>
            </div>
            <div>
                <!-- Content-->
            </div>
        </div>
        <div style="color: #ffffff; margin-top: 10px; font-size: 12px;">
            For technical or customer support please <a href="mailto:support@floralapp.com" style="color: #ffffff;
                font-weight: bold;">click here</a> to contact a floralapp&reg; representative.</div>
    </center>
    </form>
    <script type="text/javascript" language="javascript">
        if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)) { //test for Firefox/x.x or Firefox x.x (ignoring remaining digits);
            document.getElementById("spnFirefox").style.display = "none";
        } else if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) { //test for MSIE x.x;
            var ieversion = new Number(RegExp.$1) // capture x.x portion and store as a number
            if (ieversion < 8)
                document.getElementById("spnIE").style.display = "inline";
        }    
    </script>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="_Error" %>
<%@ Register Src="~/UserControls/TopMenuBar.ascx" TagName="TopMenuBar" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/LoginStatus.ascx" TagName="LoginStatus" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>floralapp&reg; | Error</title>
    <link href="Css/Default.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <center>
        <div class="mainDiv" id="mainDiv">
            <div class="headerDiv">
                <div class="headerLeft">
                    &nbsp;
                </div>
                <div class="headerRight">
                    <uc2:LoginStatus ID="LoginStatus1" runat="server" />
                </div>
            </div>
            <div id="navbar">
                <uc1:TopMenuBar ID="TopMenuBar1" runat="server" />
                <div class="clearleft">
                </div> 
            </div>
            <div class="contentDiv">
                <table border="0" cellpadding="0" style="border-collapse: collapse; margin: 4px;">
                    <tr>  
                        <td valign="top" width="730px" class="rightPanel" align="left">
                            <!-- Right Panel/Content Panel Start -->
                            <div style="margin: 20px;">
                                <h2>
                                    floralapp Error</h2>
                                <p>
                                    Error occurred in application. Please contact your system administrator.</p>
                                <asp:HyperLink ID="lnkHome" runat="server" NavigateUrl="~/Login.aspx">Click here</asp:HyperLink>
                                to go to Login Page. To see the error check the error log.
                                <div style="display: none;">
                                    <br />
                                    <br />
                                    <p>
                                        <b>Error Message:</b></p>
                                    <hr style="border: 1px solid #999999; margin: 10px 0px 10px 0px; padding: 0px;" />
                                    <asp:Label ID="lblError" runat="server" Text="" CssClass="error"></asp:Label>
                                    <hr style="border: 1px solid #999999; margin: 10px 0px 10px 0px; padding: 0px;" />
                                </div>
                            </div>
                            <!-- Right Panel/Content Panel Start -->
                        </td>
                    </tr>
                </table>
            </div>
            <div class="hrDiv">
            </div>
            <div class="footerDiv">
                &copy; 2010 floralapp&reg;. All Rights Reserved.
            </div>
        </div>  
    </center>
    </form>
</body>
</html>

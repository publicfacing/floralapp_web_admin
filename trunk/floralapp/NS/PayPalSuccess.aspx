﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PayPalSuccess.aspx.cs"
    Inherits="PayPalSuccess" %>

<%@ Register Src="../UserControls/LoginStatus.ascx" TagName="LoginStatus" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>floralapp&reg; | PayPal Transaction Success</title>
    <link href="/floralapp/Css/Default.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        h2
        {
            color: #000000;
            font-size: 14px;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <center>
        <div class="mainDiv" id="mainDiv">
            <div class="headerDiv" style="border-bottom: 5px solid #194275;">
                <div class="headerLeft">
                    &nbsp;
                </div>
                <div class="headerRight">
                     &nbsp;
                </div>
            </div>
            <div class="contentDiv">
                <div>
                    <asp:Label ID="lblErrMsg" runat="server" CssClass="errMsg"></asp:Label>
                </div>
                <div style="margin: 10px; width: 60%">
                    
                </div>
            </div>
            <div class="hrDiv">
            </div>
            <div class="footerDiv">
                &copy; 2012 floralapp&reg;. All Rights Reserved. Call us at (800) 560-0501 or email us at support@floralapp.com
            </div>
        </div>
    </center>
    </form>
 
</body>
</html>

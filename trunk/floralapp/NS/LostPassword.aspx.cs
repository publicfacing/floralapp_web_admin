﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Library.Utility;
using System.Drawing;
using Library.AppSettings;
using System.Net.Mail;
using System.IO;

public partial class LostPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Util.HideError(lblErrMsg);
    }

    protected void btnSendRequest_Click(object sender, EventArgs e)
    {
        try
        {
            Data.Users user = new Data.Users();
            String loginID = Util.getValue(txtLoginID);

            if (!String.IsNullOrEmpty(loginID))
            {
                String fromMail = ConfigurationManager.AppSettings["FromEmail"];
                String bccMail = ConfigurationManager.AppSettings["BccEmailIDs"];
                String errorMsg = String.Empty;
                String mailSubject = "floralapp lost password";

                DataSet dsUser = user.getLostPassword(loginID);
                DataTable dtUserPwd = Util.GetDataTable(dsUser);

                Int32 isValidEmailID = Util.GetDataInt32(dtUserPwd, 0, "isvalidid");
                String msg = Util.GetDataString(dtUserPwd, 0, "msg");
                String pwd = Util.GetDataString(dtUserPwd, 0, "pwd");
                Int64 userID = Util.GetDataInt64(dtUserPwd, 0, "userid");
                String resellerEmailID = Util.GetDataString(dtUserPwd, 0, "reselleremail");
                String userName = Util.GetDataString(dtUserPwd, 0, "username");

                lblErrMsg.ForeColor = Color.Red;

                if (isValidEmailID == 1 && ConfigurationManager.AppSettings["SendEmail"].ToString() == "1")
                {
                    String mailBody = "";
                    String toEmailID = "";
                    Int64 addressID = 0;
                    Int32 addressTypeID = Convert.ToInt32(AppEnum.AddressType.BILLING);                   
                    DataTable dtAddress = null;
                    Data.Address address = new Data.Address();

                    if (address.IsExist(userID, addressTypeID, ref dtAddress))
                    {
                        addressID = Util.GetDataInt64(dtAddress, 0, "AddressID");
                        address.LoadID(addressID);
                    }
                    // User Email ID
                    toEmailID = address.Email.value;
                    mailBody = FormatEmailBody("LostPassword.htm", loginID, pwd, userName);

                    if (String.IsNullOrEmpty(toEmailID))
                    {                      
                        toEmailID = resellerEmailID;
                        toEmailID = String.IsNullOrEmpty(toEmailID) ? bccMail : toEmailID;

                        // Lost Password Request by the user. But Email to different User.
                        mailBody = FormatEmailBody("LostPassword_NotRequestedUser.htm", loginID, pwd, userName);
                    }

                    

                    if (Send(fromMail, toEmailID, "", bccMail, mailSubject, mailBody, ref errorMsg))
                        lblErrMsg.ForeColor = Color.Green;

                    if (!String.IsNullOrEmpty(errorMsg))
                    {
                        Util.ShowError(lblErrMsg, errorMsg);
                        return;
                    }
                }

                Util.ShowError(lblErrMsg, msg);
            }
        }
        catch (Exception ex)
        {
            lblErrMsg.Text = ex.Message;
            lblErrMsg.Visible = true;
            return;
        }
    }

    private String FormatEmailBody(String templateName, String loginID, String pwd, String userName)
    {
        String mailBody = String.Empty;

        mailBody = ReadTemplate(templateName);
        mailBody = mailBody.Replace("{loginid}", loginID);
        mailBody = mailBody.Replace("{password}", pwd);
        mailBody = mailBody.Replace("{username}", userName);

        return mailBody;
    }

    private Boolean Send(String strMailFrom, String strMailTo, String strMailCC, String strMailBCC, String strMailSubject, String strMailMessage, ref String message)
    {
        string strMailServer = string.Empty;

        try
        {

            if (ConfigurationManager.AppSettings["MailServer"] != null)
                strMailServer = ConfigurationManager.AppSettings["MailServer"].ToLower();

            if (String.IsNullOrEmpty(strMailFrom))
                strMailFrom = ConfigurationManager.AppSettings["FromEmail"].ToLower();

            if (ConfigurationManager.AppSettings["BccEmailIDs"] != null)
            {
                String bccIDs = ConfigurationManager.AppSettings["BccEmailIDs"].ToLower();
                strMailBCC = (String.IsNullOrEmpty(strMailBCC)) ? bccIDs : bccIDs + "," + strMailBCC;
            }

            System.Net.Mail.MailMessage objMailMsg = new System.Net.Mail.MailMessage();
            System.Net.Mail.SmtpClient SmtpMail = new SmtpClient();
            SmtpMail.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
            //SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;

            MailAddress fromAddress = new MailAddress(strMailFrom);
            if (strMailCC != string.Empty)
                objMailMsg.CC.Add(strMailCC);

            if (strMailBCC != string.Empty)
                objMailMsg.Bcc.Add(strMailBCC);

            objMailMsg.To.Add(strMailTo);

            objMailMsg.From = fromAddress;
            objMailMsg.Subject = strMailSubject;
            objMailMsg.IsBodyHtml = true;
            objMailMsg.Body = strMailMessage;
            SmtpMail.Host = strMailServer;
            SmtpMail.Send(objMailMsg);

            //Save Mail History
            //SaveMailHistory(strMailFrom, strMailTo, strMailCC, strMailBCC, strMailSubject, strMailMessage);
        }
        catch (Exception ex)
        {
            message = ex.Message;

            Data.ErrorLog el = new Data.ErrorLog();
            el.SmallMessage.value = "Email Sending Error";
            el.Message.value = message;
            el.ModuleInfo.value = "floralapp Lost Password : Email.Send()";
            el.ApplicationType.value = 11;
            el.DateAdded.value = Util.GetServerDate();
            el.Save();
            return false;
        }
        return true;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("../Login.aspx", false);
    }

    private String ReadTemplate(String fileName)
    {
        String path = HttpContext.Current.Server.MapPath(@"../EmailTemplate\");

        String cmdAllPath = path + fileName;
        StreamReader sr = new StreamReader(cmdAllPath);
        String emailTemplate = sr.ReadToEnd();
        sr.Close();
        sr.Dispose();

        return emailTemplate;
    }
}

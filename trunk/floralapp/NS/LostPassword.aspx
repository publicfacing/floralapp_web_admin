﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LostPassword.aspx.cs" Inherits="LostPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>floralapp&reg; | Lost Password</title>
    <link href="/floralapp/Css/Default.css" rel="stylesheet" type="text/css" />
    <link href="/floralapp/Css/LoginPage.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .TableBorder
        {
            border: #bbdef1 1px solid;
            padding: 5px;
            font-weight: normal;
            font-size: 12px;
            color: #535152;
            font-family: Arial, Helvetica, sans-serif;
            text-decoration: none;
        }
        .TableHeadingBg
        {
            background: url(Images/tablebg-aqua.jpg) repeat-x;
            height: 28px;
            border: #bbdef1 1px solid;
            font-weight: bold;
            font-size: 12px;
            color: #535152;
            font-family: Arial, Helvetica, sans-serif;
            text-decoration: none;
            text-transform: uppercase;
            padding-left: 5px;
            text-align: left;
        }
        .col
        {
            font-weight: bold;
            width: 130px;
        }
        .val
        {
            font-weight: normal;
        }
        .reqd
        {
            color: Red;
            font-size: 12px;
        }
        .detailTable
        {
            border: 1px solid #E2E2E2;
            padding: 5px;
            width: 100%;
            border-collapse: collapse;
        }
        .error
        {
            color: #ff0000;
            font-weight: bold;
        }
        .btn
        {
            background-color: #0C4F7C;
            border: 0 solid #0C4F7C;
            color: #FFFFFF;
            font-family: Arial;
            font-size: 12px;
            padding: 3px;
        }
        .actionDiv
        {
            text-align: center;
            margin: 25px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="loginContainer">
        <div class="titleBar">
            &nbsp;</div>
        <div class="msgDiv">
            <asp:Label ID="lblErrMsg" runat="server" CssClass="errMsg"></asp:Label>
            <asp:ValidationSummary ID="vsLoginPage" runat="server" ShowSummary="true" />
        </div>
        <table cellspacing="0" cellpadding="0" border="0" class="login">
            <tr>
                <td class="label" style="vertical-align: baseline;">
                    Login ID<span class="reqd">*</span>:
                </td>
                <td>
                    <asp:TextBox ID="txtLoginID" TabIndex="1" runat="server" MaxLength="50" CssClass="txtbox"
                        Text=""></asp:TextBox> 
                    <asp:RequiredFieldValidator ID="reqdtxtLoginID" runat="server" Display="None" ErrorMessage="Enter your Login ID."
                        ControlToValidate="txtLoginID"></asp:RequiredFieldValidator> 
                </td>
            </tr>
            <tr>
                <td class="label">
                    &nbsp;
                </td>
                <td align="left">
                    <%--<asp:ImageButton ID="btnLogin" runat="server" ImageUrl="~/Images/submit.png" CssClass="btn" OnClick="btnLogin_click" />--%>
                    <div style="text-align: left; vertical-align: top; text-decoration: none">
                        <asp:Button ID="btnSendRequest" TabIndex="1" runat="server" Text="Continue" CssClass="btn"
                            OnClick="btnSendRequest_Click"></asp:Button>
                        &nbsp;
                        <asp:Button ID="btnCancel" TabIndex="2" runat="server" Text="Cancel" CssClass="btn"
                            CausesValidation="false" OnClick="btnCancel_Click"></asp:Button>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

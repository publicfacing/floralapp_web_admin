/*
	Funtion getElementsByClassName
	Written by Jonathan Snook, http://www.snook.ca/jonathan
	Add-ons by Robert Nyman, http://www.robertnyman.com
*/

function getElementsByClassName(oElm, strTagName, oClassNames)
{
	var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
	var arrReturnElements = new Array();
	var arrRegExpClassNames = new Array();
	if (typeof oClassNames == "object")
	{
		for (var i=0; i<oClassNames.length; i++)
		{
			arrRegExpClassNames.push(new RegExp("(^|\s)" + oClassNames[i].replace(/-/g, "\-") + "(\s|$)"));
		}
	}
	else
	{
		arrRegExpClassNames.push(new RegExp("(^|\s)" + oClassNames.replace(/-/g, "\-") + "(\s|$)"));
	}
	var oElement;
	var bMatchesAll;
	for (var j=0; j<arrElements.length; j++)
	{
		oElement = arrElements[j];
		bMatchesAll = true;
		for (var k=0; k<arrRegExpClassNames.length; k++)
		{
			if (!arrRegExpClassNames[k].test(oElement.className))
			{
				bMatchesAll = false;
				break;
			}
		}
		if(bMatchesAll)
		{
			arrReturnElements.push(oElement);
		}
	}
	return (arrReturnElements);
}

function startmenu()
{
	//alert("DEBUG: Starting...");
	// Pop-up menu code for IE only.
	if (!document.body.currentStyle) return;
	//var subs = document.getElementsByName("submenu");
	var subs = getElementsByClassName(document, "a", "submenu");
	for (var i=0; i<subs.length; i++)
	{
		var li = subs[i].parentNode;
		if (li && li.lastChild.style)
		{
			li.onmouseover = function()
			{ 
				this.lastChild.style.zIndex= "-1";
				this.lastChild.style.visibility = 'visible';
			}
			li.onmouseout = function()
			{
				this.lastChild.style.visibility = 'hidden';
			}
		}
	}
	//alert("DEBUG: Finished.");
}
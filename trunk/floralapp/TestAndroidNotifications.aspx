﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestAndroidNotifications.aspx.cs" Inherits="Test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
	
    <div>
        The device id must be signed in to the Development shop!<br /><br />
        Device ID:<asp:TextBox ID="tbxDeviceID" runat="server"></asp:TextBox><br />
        <asp:Button ID="Button1" runat="server" Text="Send notification" onclick="Button1_Click" />
        <asp:Label ID="lblResult" runat="server" Text="Label"></asp:Label>
        <br />
        <br />

        <asp:Button runat="server" ID="btnSendiOSPN" onclick="btnSendiOSPN_Click" Text="Send PN to iOS" />
    </div>
    </form>
    
</body>
</html>

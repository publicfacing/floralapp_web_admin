﻿using System;
using System.Web.UI.WebControls;
using Library.AppSettings;

public partial class UserControls_TopMenuBar : System.Web.UI.UserControl
{
    AppVars v = new AppVars();

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (v.LoginUserTypeID == (Int32)AppEnum.LoginUserType.ADMIN)
        //{
        //    lnkCreateAdministrativeUser.Enabled = true;
        //    lnkViewAdministrativeUsers.Enabled = true;
        //    lnkLoginHistory.Enabled = true;
        //}
        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.SUPERADMIN))
        {
            lnkSuperAdmin.Visible = true;
            lnkReqHelpEmail.Visible = true;
            lnkReminders.Visible = true;
        }

        if (v.UserRoleID == Convert.ToInt32(AppEnum.UserRole.RESELLERADMIN))
        {
            Data.AppUseAcceptance aua = new Data.AppUseAcceptance();

            if (!aua.IsAcceptTerms_n_Conditions(v.UserID))
                Response.Redirect("~/Admin/TermsAndConditions.aspx");

            //lnkFAQ.Visible = false;
            Data.ResellerSetting resellerSetting = new Data.ResellerSetting();

            if (!Convert.ToBoolean(resellerSetting.GetSetting(v.ResellerID).Tables[0].Rows[0]["IsViralMarketingOn"]))
                lnkViralMarketing.Visible = false;
        }

        if (!IsPostBack)
        {
            RemoveSelection();

            switch (v.SelectedTab)
            {
                case "lnkHome":
                    SelectTab(lnkHome);
                    break;

                case "lnkViewAllProducts":
                case "lnkUpsellProducts":
                    SelectTab(lnkProducts);
                    break;

                case "lnkViewAllPurchases":
                    SelectTab(lnkPayments_Orders);
                    break;

                case "lnkCategory":
                case "lnkUpsellCategory":
                case "lnkColorTheme":
                case "lnkCoupons":
                case "lnkSpecialOffers":
                case "lnkUpChargeFee":
                case "lnkBlockDates":
                case "lnkReseller":
                    SelectTab(lnkManage);
                    break;

                case "lnkCouponUsage":
                case "lnkViralMarketing":
                    SelectTab(lnkReports);
                    break;

                case "lnkChangePassword":
                case "lnkMoreSettings":
                case "lnkFAQ":
                    SelectTab(lnkSettings);
                    break;

                case "lnkSuperAdmin":
                case "lnkResellerAdmin":
                case "lnkConsumers":
                    SelectTab(lnkUsers);
                    break;
            }
        }
    }

    private void SelectTab(LinkButton lb)
    {
        lb.CssClass = lb.CssClass + " selected";
    }

    private void SelectTab(HyperLink lnk)
    {
        lnk.CssClass = lnk.CssClass + " selected";
    }

    private void RemoveSelection()
    {
        lnkHome.CssClass = lnkHome.CssClass.Replace(" selected", "");
        lnkProducts.CssClass = lnkProducts.CssClass.Replace(" selected", "");
        lnkPayments_Orders.CssClass = lnkPayments_Orders.CssClass.Replace(" selected", "");
        lnkManage.CssClass = lnkManage.CssClass.Replace(" selected", "");
        lnkReports.CssClass = lnkReports.CssClass.Replace(" selected", "");
        lnkSettings.CssClass = lnkSettings.CssClass.Replace(" selected", "");
        lnkUsers.CssClass = lnkUsers.CssClass.Replace(" selected", "");
    }

    protected void lnkHome_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkHome";
        Response.Redirect("~/Admin/Dashboard.aspx");
    }

    protected void lnkViewAllProducts_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkViewAllProducts";
        Response.Redirect("~/Admin/Products.aspx");
    }

    protected void lnkUpsellProducts_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkUpsellProducts";
        Response.Redirect("~/Admin/UpsellProduct.aspx");
    }

    protected void lnkViewAllPurchases_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkViewAllPurchases";
        Response.Redirect("~/Admin/ViewAllPurchases.aspx");
    }

    protected void lnkCategory_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkCategory";
        Response.Redirect("~/Admin/Category.aspx");
    }

    protected void lnkUpsellCategory_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkUpsellCategory";
        Response.Redirect("~/Admin/UpsellCategory.aspx");
    }

    protected void lnkColorTheme_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkColorTheme";
        Response.Redirect("~/Admin/ColorTheme.aspx");
    }

    protected void lnkCoupons_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkCoupons";
        Response.Redirect("~/Admin/Coupons.aspx");
    }

    protected void lnkSpecialOffers_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkSpecialOffers";
        Response.Redirect("~/Admin/SpecialOffers.aspx");
    }

    protected void lnkUpChargeFee_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkUpChargeFee";
        Response.Redirect("~/Admin/UpChargeFee.aspx");
    }

    protected void lnkBlockDates_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkBlockDates";
        Response.Redirect("~/Admin/BlockDates.aspx");
    }

    protected void lnkReseller_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkReseller";
        Response.Redirect("~/Admin/Reseller.aspx");
    }

    protected void lnkCouponUsage_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkCouponUsage";
        Response.Redirect("~/Admin/CouponUsageReport.aspx");
    }

    protected void lnkViralMarketing_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkViralMarketing";
        Response.Redirect("~/Admin/ViralMarketing.aspx");
    }

    protected void lnkMetricsReport_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkMetricsReport";
        Response.Redirect("~/Admin/MetricsReport.aspx");
    }

    protected void lnkChangePassword_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkChangePassword";
        Response.Redirect("~/Admin/ChangePassword.aspx");
    }

    protected void lnkMoreSettings_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkMoreSettings";
        Response.Redirect("~/Admin/MoreSettings.aspx");
    }

    protected void lnkFAQ_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkFAQ";
        Response.Redirect("~/Admin/FAQ.aspx");
    }

    protected void lnkSuperAdmin_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkSuperAdmin";
        Response.Redirect("~/Admin/Users.aspx?RoleID=1");
    }

    protected void lnkResellerAdmin_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkResellerAdmin";
        Response.Redirect("~/Admin/Users.aspx?RoleID=2");
    }

    protected void lnkConsumers_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkConsumers";
        Response.Redirect("~/Admin/Users.aspx?RoleID=3");
    }

    protected void lnkRequestHelp_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkRequestHelp";
        Response.Redirect("~/Admin/RequestHelp.aspx");
    }

    protected void lnkReqHelpEmail_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkReqHelpEmail";
        Response.Redirect("~/Admin/EmailSettings.aspx");
    }

    protected void lnkReminders_Click(object sender, EventArgs e)
    {
        v.SelectedTab = "lnkReminders";
        Response.Redirect("~/Admin/Reminders.aspx");
    }
}

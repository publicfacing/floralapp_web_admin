﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Library.AppSettings;
using Library.Utility;

public partial class UserControls_LoginStatus : System.Web.UI.UserControl
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AppVars v = new AppVars();
            lblCurrentUser.Text = (v.DisplayName != String.Empty) ?   "Welcome "+ v.DisplayName : "Guest";

            Data.LoginHistory lh = new Data.LoginHistory();
            DataSet dsLH =lh.GetLastLogin(v.UserID);

            if (Util.IsValidDataSet(dsLH))
            {
                DataTable dtLH = Util.GetDataTable(dsLH);
                String lastLoginTime = Util.GetDataString(dtLH, 0, "LastLogin");
                if (!String.IsNullOrEmpty(lastLoginTime))
                    lastLoginTime = "Last login was on : " + lastLoginTime;

                Util.setValue(lblLastLoginOn, lastLoginTime); 
            } 
        }
    }

    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        FormsAuthentication.SignOut(); 
        FormsAuthentication.RedirectToLoginPage();  
    }
}

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopMenuBar.ascx.cs" Inherits="UserControls_TopMenuBar" %>
<ul>
    <li>
        <asp:LinkButton ID="lnkHome" runat="server" CssClass="nav-root" ToolTip="Home" OnClick="lnkHome_Click"
            CausesValidation="false"><span class="navlabel">Home</span></asp:LinkButton>
        <%--<asp:HyperLink ID="lnkHome" runat="server" class="nav-root" ToolTip="Home" NavigateUrl="~/Admin/Dashboard.aspx"><span class="navlabel">Home</span></asp:HyperLink>--%>
    </li>
    <li>
        <asp:HyperLink ID="lnkProducts" runat="server" CssClass="nav-parent submenu" ToolTip="Product"><span class="navlabel">Product</span></asp:HyperLink>
        <ul class="navsub-root">
            <li>
                <asp:LinkButton ID="lnkViewAllProducts" runat="server" ToolTip="Products" OnClick="lnkViewAllProducts_Click"
                    CausesValidation="false">View All Products</asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="lnkUpsellProducts" runat="server" ToolTip="Upsell Products" CausesValidation="false"
                    OnClick="lnkUpsellProducts_Click">Upsell Products</asp:LinkButton>
            </li>
        </ul>
    </li>
    <li>
        <asp:HyperLink ID="lnkPayments_Orders" runat="server" CssClass="nav-parent submenu"
            ToolTip="Payments & Orders"><span class="navlabel">Payments & Orders</span></asp:HyperLink>
        <ul class="navsub-root">
            <li>
                <asp:LinkButton ID="lnkViewAllPurchases" runat="server" ToolTip="View All Purchases"
                    OnClick="lnkViewAllPurchases_Click" CausesValidation="false">View All Purchases</asp:LinkButton></li>
            <%--<li >
                <asp:HyperLink ID="lnkViewOpenOrders" runat="server" ToolTip="View Open Orders" NavigateUrl="~/Admin/ViewOrders.aspx">View Pending Orders</asp:HyperLink></li>--%>
        </ul>
    </li>
    <li>
        <asp:HyperLink ID="lnkManage" runat="server" CssClass="nav-parent submenu" ToolTip="Manage"><span class="navlabel">Manage</span></asp:HyperLink>
        <ul class="navsub-root">
            <%--<li>
                <!-- Only for Super Admin-->
                <asp:HyperLink ID="HyperLink1" runat="server" ToolTip="Category"
                    NavigateUrl="~/Admin/AddDefaultCategory.aspx">Add Default Category</asp:HyperLink></li>--%>
            <li>
                <asp:LinkButton ID="lnkCategory" runat="server" ToolTip="Category" OnClick="lnkCategory_Click"
                    CausesValidation="false">Category</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lnkUpsellCategory" runat="server" ToolTip="UpsellCategory" OnClick="lnkUpsellCategory_Click"
                    CausesValidation="false">Upsell Category</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lnkColorTheme" runat="server" ToolTip="Category" OnClick="lnkColorTheme_Click"
                    CausesValidation="false">Color Theme</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lnkCoupons" runat="server" ToolTip="Category" OnClick="lnkCoupons_Click"
                    CausesValidation="false">Coupons</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lnkSpecialOffers" runat="server" ToolTip="Special Offers" OnClick="lnkSpecialOffers_Click"
                    CausesValidation="false">Special Offers</asp:LinkButton></li>
            <li style="display: none;">
                <asp:LinkButton Enabled="false" ID="lnkUpChargeFee" runat="server" ToolTip="UpCharge Fee"
                    OnClick="lnkUpChargeFee_Click" CausesValidation="false">UpCharge Fee</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lnkBlockDates" runat="server" ToolTip="Block Dates" OnClick="lnkBlockDates_Click"
                    CausesValidation="false">Block Dates</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lnkReseller" runat="server" ToolTip="Shop" OnClick="lnkReseller_Click"
                    CausesValidation="false">Shop</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lnkReminders" runat="server" ToolTip="Reminders" OnClick="lnkReminders_Click"
                    CausesValidation="false" Visible="false">Reminders</asp:LinkButton></li>
        </ul>
    </li>
    <li>
        <asp:HyperLink ID="lnkReports" runat="server" CssClass="nav-parent submenu" ToolTip="Reports"><span class="navlabel">Reports</span></asp:HyperLink>
        <ul class="navsub-root">
            <li>
                <asp:LinkButton ID="lnkCouponUsage" runat="server" ToolTip="Payment" OnClick="lnkCouponUsage_Click"
                    CausesValidation="false">Coupon Usage</asp:LinkButton></li>
            <%--<li>
                <asp:HyperLink ID="lnkOrders" runat="server" ToolTip="Orders" NavigateUrl="~/Admin/NeedsWork.aspx">Orders</asp:HyperLink></li>--%>
            <li>
                <asp:LinkButton ID="lnkViralMarketing" runat="server" ToolTip="Viral Marketing" OnClick="lnkViralMarketing_Click"
                    CausesValidation="false">Viral Marketing Report</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lnkMetricsReport" runat="server" ToolTip="Metrics Report" OnClick="lnkMetricsReport_Click"
                    CausesValidation="false">Metrics Report</asp:LinkButton></li>
        </ul>
    </li>
    <li>
        <asp:HyperLink ID="lnkSettings" runat="server" CssClass="nav-parent submenu" ToolTip="Settings"><span class="navlabel">Settings</span></asp:HyperLink>
        <ul class="navsub-root">
            <li>
                <asp:LinkButton ID="lnkChangePassword" runat="server" ToolTip="Change Password" OnClick="lnkChangePassword_Click"
                    CausesValidation="false">Change Password</asp:LinkButton></li>
            <%--  <li>
                <asp:HyperLink ID="lnkTerms_Conditions" runat="server" ToolTip="Terms & Conditions"
                    NavigateUrl="#">Terms & Conditions</asp:HyperLink></li>--%>
            <li>
                <asp:LinkButton ID="lnkMoreSettings" runat="server" ToolTip="More Settings" OnClick="lnkMoreSettings_Click"
                    CausesValidation="false">More Settings</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lnkFAQ" runat="server" ToolTip="FAQ" OnClick="lnkFAQ_Click" CausesValidation="false"
                    Visible="false">FAQ</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lnkReqHelpEmail" runat="server" ToolTip="Email Settings" OnClick="lnkReqHelpEmail_Click"
                    CausesValidation="false" Visible="false">Email Settings</asp:LinkButton></li>
        </ul>
    </li>
    <li>
        <asp:HyperLink ID="lnkUsers" runat="server" CssClass="nav-parent submenu" ToolTip="Users"><span class="navlabel">Users</span></asp:HyperLink>
        <ul class="navsub-root">
            <li>
                <asp:LinkButton ID="lnkSuperAdmin" Visible="false" runat="server" ToolTip="Super Admin"
                    OnClick="lnkSuperAdmin_Click" CausesValidation="false">Super Admin</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lnkResellerAdmin" runat="server" ToolTip="Shop Admin" OnClick="lnkResellerAdmin_Click"
                    CausesValidation="false">Shop Admin</asp:LinkButton></li>
            <li>
                <asp:LinkButton ID="lnkConsumers" runat="server" ToolTip="Consumers" OnClick="lnkConsumers_Click"
                    CausesValidation="false">Consumers</asp:LinkButton></li>
        </ul>
    </li>
    <li>
        <asp:HyperLink ID="lnkResources" runat="server" CssClass="nav-parent submenu" ToolTip="Resources"><span class="navlabel">Resources</span></asp:HyperLink>
        <ul class="navsub-root">
            <li><a href="http://www.floralapp.com/flickr" target="_blank" title="View Our flickr Gallery">
                View Our flickr&reg; Gallery</a></li>
            <li>
                <asp:LinkButton ID="lnkRequestHelp" runat="server" ToolTip="Request Help" OnClick="lnkRequestHelp_Click"
                    CausesValidation="false">Request Help</asp:LinkButton></li>
        </ul>
    </li>
</ul>

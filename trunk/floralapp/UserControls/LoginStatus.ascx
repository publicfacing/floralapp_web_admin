﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LoginStatus.ascx.cs" Inherits="UserControls_LoginStatus" %>

<div class="loginStatus">
    <div>
       <%-- <asp:LinkButton ID="lnkMoreSettings" runat="server">More Settings</asp:LinkButton>
        <span style="font-size:12px;">&nbsp;|&nbsp; </span>--%>
        <asp:Label ID="lblCurrentUser" runat="server" Text=""></asp:Label> <span style="font-size:12px;">&nbsp;|&nbsp; </span>
        <asp:LinkButton ID="lnkLogout" runat="server" class="logout" OnClick="lnkLogout_Click" CausesValidation="false">Logout</asp:LinkButton> 
    </div>
    <div class="lastLogin">
        <asp:Label ID="lblLastLoginOn" runat="server" Text=""></asp:Label>
    </div>  
</div> 